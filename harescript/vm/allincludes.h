// Necessary includes (these aren't #included at all)
#include <string>
#include <memory>
#include <algorithm>
#include <vector>
#include <set>
#include <deque>
#include <queue>
#include <list>
#include <map>
#include <sstream>
#include <stack>
#include <iterator>
#include <signal.h>
#include <blex/blexlib.h>

// Optional includes to speed up compilation (probably not all #included)

#include <blex/datetime.h>
#include <blex/stream.h>
#include <blex/unicode.h>
#include <blex/utils.h>
