# Contributing

Thank you for considering a contribution to the WebHare Platform!
We recommend creating an issue describing the changes you want to make before
starting a (major) change to the WebHare Platform. This will help us to confirm
that we think your proposal is inside the scope of the project and will increase
the chance of your contribution being (quickly) accepted.

Contributions that might be out of scope for the general WebHare Platform may
be offered as separate modules, possibly requiring just a few additional hooks
to the WebHare Platform to allow your module to integrate with it. We are
generally open to adding additional hooks and integration points, especially if
we can build them in a way that makes them generally useful to other modules too.

Contributions should come with automatic tests runnable by GitLab CI whenever
reasonably possible.
