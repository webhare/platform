# Future

- Support for 'selectable'

- Don't drag with right mouse key

- Drag on an edge (but not a corner)
  - left / right - resize horizontally only
  - top / bottom - resize vertically only

- Support for Aspect ratio's
- Support for fixed overlay sizes (disable resize corner/edge resizehandlers)
