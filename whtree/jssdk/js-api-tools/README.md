# WebHare JavaScript API tools

A base package used to implement nicer JavaScript and TypeScript APIs. Eg. this library provides the helper APIs
used by the WebHare JSON/RPC client to build cross-call stack traces.
