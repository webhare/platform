<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::ipc.whlib";

LOADLIB "mod::consilio/lib/catalogs.whlib";
LOADLIB "mod::consilio/lib/pagelists.whlib";
LOADLIB "mod::consilio/lib/screens/catalog.whlib";
LOADLIB "mod::consilio/lib/internal/support.whlib";
LOADLIB "mod::consilio/lib/internal/whfscatalog/whfsobjectsource.whlib";
LOADLIB "mod::consilio/lib/whfscatalog.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/services.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";
LOADLIB "mod::system/lib/internal/whfs/support.whlib";

LOADLIB "mod::tollium/lib/dialogs.whlib";
LOADLIB "mod::tollium/lib/screenbase.whlib";

PUBLIC RECORD ARRAY FUNCTION GetSubFiles(INTEGER objid)
{
  OpenPrimary();

  RECORD objinfo := SELECT id, type, isfolder, modificationdate, parent FROM system.fs_objects WHERE id = objid;
  IF(NOT RecordExists(objinfo))
    RETURN RECORD[];

  RETURN GetSearchSubfiles(objinfo).subfiles;
}

PUBLIC RECORD ARRAY FUNCTION GetPageList(INTEGER objid)
{
  OpenPrimary();

  RETURN SELECT link, modificationdate, title, consiliofields FROM GetConsilioPagesForFile(objid);
}


PUBLIC STATIC OBJECTTYPE ConsilioTab EXTEND TolliumFragmentBase
<
  INTEGER objid;
  OBJECT fsobj;
  RECORD ARRAY subfiles;
  OBJECT subfilespromise;
  OBJECT pagelistpromise;

  PUBLIC MACRO Setup(INTEGER objid)
  {
    this->objid := objid;
  }
  PUBLIC MACRO Refresh(OBJECT fsobj)
  {
    this->fsobj := fsobj;
    this->RefreshSubFiles();
    this->RefreshConsilioPagelist(FALSE);
    this->RefreshContentSources();
  }

  MACRO DoReindex()
  {
    ReindexWHFSChanges([[ id := this->objid
                        , isfolder := this->fsobj->isfolder
                        , isdelete := FALSE
                        ]]);

    this->Refresh(this->fsobj);
  }
  MACRO DoInspectConsilio()
  {
    IF(NOT RunConsilioInspectDialog(this, whconstant_consilio_catalog_whfs, "", ^subfile->selection.groupid))
      this->owner->RunSimpleScreen("error", this->GetTid(".cannotinspectconsilio"));
  }

  ASYNC MACRO RefreshSubFiles()
  {
    IF (ObjectExists(this->subfilespromise))
      RETURN;

    this->subfiles := RECORD[];
    this->subfilespromise := AsyncCallFunctionFromJob(Resolve("#GetSubFiles"), this->objid);
    TRY
    {
      this->subfiles := AWAIT this->subfilespromise;

      IF(Length(this->subfiles) > 0)
      {
        ^subfile->options := SELECT rowkey := #subfiles + 1
                                  , title := RecordExists(subfiles) ? `${filename} (${this->contexts->user->FormatFileSize(Length64(data), 0, FALSE)})`
                                                                    : `#${#subfiles}` //not sure what case triggers this?
                                  , groupid := "fsobj_" || this->objid || (#subfiles > 0 ? "_" || #subfiles: "")
                               FROM this->subfiles;
      }
      ELSE
      {
        ^subfile->options := [[ rowkey := 0, title := this->GetTid(".nosearchinfo"), groupid := "fsobj_" || this->objid ]];
      }
    }
    CATCH(OBJECT e)
    {
      LogHarescriptException(e);
      ^subfile->options := [[ rowkey := 0, title := this->GetTid(".searchinfoerror"), groupid := "fsobj_" || this->objid ]];
      RunExceptionReportDialog(this, e);
    }
    FINALLY
    {
      this->subfilespromise := DEFAULT OBJECT;
    }

    //Only make the control readonly if we have no data: makes it a bit clearer more options would exist if more than one file is present
    ^subfile->readonly := NOT RecordExists(SELECT FROM ^subfile->options WHERE rowkey > 0);
  }

  MACRO OnSubfileChange()
  {
    RECORD matchfile := ^subfile->value > 0 AND ^subfile->value <= Length(this->subfiles) ? this->subfiles[^subfile->value - 1] : DEFAULT RECORD;
    IF(RecordExists(matchfile))
      ^subfilecontent->value := [ htmltext := MakeSlicedBlob(matchfile.data, 0, 50*1024) ]; //showing 50KB is more than enough
    ELSE
      ^subfilecontent->value := DEFAULT RECORD;
  }

  MACRO DoRefreshPageList()
  {
    this->RefreshConsilioPagelist(TRUE);
  }

  ASYNC MACRO RefreshConsilioPagelist(BOOLEAN interactive)
  {
    IF(ObjectExists(this->pagelistpromise)) //already running
      RETURN; //ignore (or cancel running promise?)

    ^pagelist->rows := RECORD[];
    ^pagelist->empty := this->GetTid(".refreshingpagelist");
    this->pagelistpromise := AsyncCallFunctionFromJob(Resolve("#GetPagelist"), this->objid);
    TRY
    {
      RECORD ARRAY pages := AWAIT this->pagelistpromise;
      ^pagelist->empty := "";
      ^pagelist->rows := SELECT rowkey := link, url := link, modificationdate, title FROM pages;
    }
    CATCH(OBJECT e)
    {
      LogHarescriptException(e);
      ^pagelist->empty := this->GetTid(".refreshingpagelisterror");
      IF(interactive)
        RunExceptionReportDialog(this, e);
    }
    FINALLY
    {
      this->pagelistpromise := DEFAULT OBJECT;
    }
  }

  MACRO RefreshContentSources()
  {
    INTEGER ARRAY whfstree := GetWHFSTree(this->objid, 0);
    RECORD ARRAY candidates := GetContentSourceMatches(whfstree);
    RECORD ARRAY sources;
    FOREVERY(RECORD candidate FROM candidates)
    {
      OBJECT contentsource := OpenContentSourceById(candidate.contentsourceid);
      IF(NOT ObjectExists(contentsource))
        CONTINUE;

      INSERT [ catalog := contentsource->catalog->tag
             , contentsource := contentsource->tag ?? "#" || contentsource->id
             , contentsourceid := contentsource->id
             , root := (SELECT AS STRING whfspath FROM system.fs_objects WHERE id = candidate.fsobject)
             ] INTO sources AT END;
    }
    ^contentsources->rows := sources;
  }

  MACRO DoInspectSource()
  {
    RECORD toinspect := ^contentsources->selection;
    OBJECT csource := OpenContentSourceById(toinspect.contentsourceid)->__OpenProvider();
    STRING groupid := ToString(this->objid);
    RECORD ARRAY objs := csource->ListObjects(GetCurrentDatetime(), groupid);
    objs := SELECT *, fetched := csource->FetchObject(GetCurrentDatetime(), groupid, id) FROM objs;
    Reflect(objs);
  }
>;
