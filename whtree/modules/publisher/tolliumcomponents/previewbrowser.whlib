<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::internet/urls.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/services.whlib";
LOADLIB "mod::system/lib/internal/whfs/support.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

LOADLIB "mod::publisher/lib/control.whlib";
LOADLIB "mod::publisher/lib/database.whlib";
LOADLIB "mod::publisher/lib/publisher.whlib";
LOADLIB "mod::publisher/lib/siteprofiles.whlib";
LOADLIB "mod::publisher/lib/internal/support.whlib";
LOADLIB "mod::publisher/lib/internal/tollium-helpers.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/cache.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/structuredef.whlib";

LOADLIB "mod::tollium/lib/componentbase.whlib";
LOADLIB "mod::tollium/lib/gettid.whlib";
LOADLIB "mod::tollium/lib/screenbase.whlib";


BOOLEAN FUNCTION IsPreviewableWidget(INTEGER widgetid)
{
  //Only preview widgets that have a RTDTYPE
  RECORD settings := GetRTDSettingsForFile(widgetid);
  RETURN settings.namespace != "";
}

PUBLIC STATIC OBJECTTYPE PreviewBrowser EXTEND TolliumFragmentBase
< // ---------------------------------------------------------------------------
  //
  // Variables
  //

  STRING lastsethtmlvalue;
  ///'official' target we preview, eg where we take apply rules from (targetobject)
  INTEGER previewfileid;
  ///actual content to preview
  INTEGER contentfileid;
  ///Actual URL we want to show
  STRING currenturl;
  BOOLEAN currentsandbox;
  ///Suppress page rendering?
  BOOLEAN suppressrendering;

  INTEGER devicewidth;
  INTEGER deviceheight;

  BOOLEAN refresh_unpublished;
  RECORD ARRAY previewmessages;

  // ---------------------------------------------------------------------------
  //
  // Public variables and properties
  //

  PUBLIC MACRO PTR onpreviewedobjectchange;
  // ---------------------------------------------------------------------------
  //
  // Init
  //

  UPDATE PUBLIC MACRO StaticInit(RECORD data)
  {
    TolliumFragmentBase::StaticInit(data);
    this->onpreviewedobjectchange := data.onpreviewedobjectchange;
  }

  UPDATE MACRO PostInitComponent()
  {
    TolliumFragmentBase::PostInitComponent();

    RECORD ARRAY devices;
    devices := [ [ title := GetTid("publisher:tolliumcomponents.previewbrowser.devices.phone")
                 // iPhone 4
                 , width := 320, height := 480, pixelratio := 2
                 ]
               , [ title := GetTid("publisher:tolliumcomponents.previewbrowser.devices.phone")
                 // iPhone 6(s)+
                 , width := 414, height := 736, pixelratio := 3
                 ]
               , [ title := GetTid("publisher:tolliumcomponents.previewbrowser.devices.tablet")
                 // iPad landscape
                 , width := 1024, height := 768, pixelratio := 2
                 ]
               , [ title := GetTid("publisher:tolliumcomponents.previewbrowser.devices.tablet")
                 // iPad portrait
                 , width := 768, height := 1024, pixelratio := 2
                 ]
               , [ title := GetTid("publisher:tolliumcomponents.previewbrowser.devices.desktop")
                 // Desktop 4:3
                 , width := 1280, height := 960, pixelratio := 1
                 ]
               , [ title := GetTid("publisher:tolliumcomponents.previewbrowser.devices.desktop")
                 // Desktop HD
                 , width := 1920, height := 1080, pixelratio := 1
                 ]
               ];


    devices := SELECT *
                    , rowkey := "d-" || width || "x" || height
                    , title := title || " (" || width || "x" || height || ")"
                 FROM devices;

    INSERT [ rowkey := "f"
           , title := GetTid("publisher:tolliumcomponents.previewbrowser.devices.fill")
           ] INTO devices AT 0;
    INSERT [ isdivider := TRUE ] INTO devices AT 1;
    INSERT [ rowkey := "c"
           , title := GetTid("publisher:tolliumcomponents.previewbrowser.devices.customsize") || "…"
           ] INTO devices AT END;

    STRING selected := this->tolliumuser->GetRegistryKey("publisher.previewbrowser.device", "f");
    IF (selected LIKE "c-*")
      INSERT [ rowkey := selected
             , title := GetTid("publisher:tolliumcomponents.previewbrowser.devices.customsize") || " (" || Tokenize(selected, "-")[1] || ")"
             ] INTO devices AT END;

    ^deviceselect->options := devices;
    ^deviceselect->value := selected;

    ^previewmessage->linkactions :=
        [ [ url := "preview:showsite"
          , action := ^viewsite
          ]
        , [ url := "preview:showexternallink"
          , action := ^showexternallink
          ]
        ];

  }

  // ---------------------------------------------------------------------------
  //
  // Helper functions
  //

  MACRO CheckHTTP()
  {
    IF (this->owner->tolliumcontroller->secure AND this->currenturl LIKE "http://*")
    {
      INSERT [ level := 0
             , message := GetTid("publisher:tolliumcomponents.previewbrowser.nonsecuresite") || ' <a href="preview:showsite">' || GetTid("publisher:tolliumcomponents.previewbrowser.clickviewsite") || '</a>'
             ] INTO this->previewmessages AT END;
      this->suppressrendering := TRUE; // Cannot show unsecure content within secure WebHare, show default page
    }
  }

  STRING FUNCTION GetPreviewToken(RECORD selectedfile, RECORD contentfile)
  {
    RETURN EncryptForThisServer("publisher:previewbrowser",
                [ source := selectedfile.id
                , when := GetCurrentDatetime()
                , content := contentfile.id
                , lang := GetTidLanguage()
                ]);
  }

  RECORD FUNCTION GetPreviewData(INTEGER fileid, INTEGER usecontentfile)
  {
    /* Determine the viewer to use. In order we'll try:
       - any explicit previewsetting: <preview browserurl=...> - this will handle eg. images
       - widget rendering
       - see if we can expect HTML (the resulting link is a 'needstemplate') - then we'll directly link
       - fall back to the unknown.shtml handler
       */

    RECORD selectedfile := SELECT id, name, link, type, published, filelink, externallink, parentsite, isfolder FROM system.fs_objects WHERE id = VAR fileid;
    IF(NOT RecordExists(selectedfile))
      RETURN DEFAULT RECORD;

    this->currenturl := "";
    BOOLEAN ispreview := fileid != usecontentfile AND usecontentfile != 0; //is this a draft preview request?
    RECORD data := [ fileid := fileid
                   ];

    RECORD contentfile := selectedfile;
    INTEGER targetfiletype := selectedfile.type;

    IF(usecontentfile != 0)
    {
      RECORD target := SELECT id, name, type, published, parentsite, indexurl, fullpath, isfolder FROM system.fs_objects WHERE id = usecontentfile;
      IF(RecordExists(target))
      {
        targetfiletype := target.type;
        contentfile := target;
      }
    }
    IF(selectedfile.type IN [ whconstant_whfstype_internallink, whconstant_whfstype_contentlink ])
    {
      // Find internal or content link target
      RECORD target := SELECT id, name, type, published, parentsite, indexurl, fullpath, isfolder FROM system.fs_objects WHERe id = selectedfile.filelink AND isactive;
      IF(RecordExists(target))
      {
        targetfiletype := target.type;
        IF(selectedfile.type = 20 ) //contentlink
          contentfile := target;

        // Show link target
        RECORD site := SELECT name FROM system.sites WHERE id = target.parentsite;
        STRING message := selectedfile.type = whconstant_whfstype_internallink
                             ? (target.indexurl = "" ? GetTid("publisher:tolliumcomponents.previewbrowser.internallink-unpublished", (RecordExists(site) ? site.name || ":" : "") || target.fullpath)
                                                     : GetTid("publisher:tolliumcomponents.previewbrowser.internallink", (RecordExists(site) ? site.name || ":" : "") || target.fullpath))
                             : GetTid("publisher:tolliumcomponents.previewbrowser.contentlink", (RecordExists(site) ? site.name || ":" : "") || target.fullpath);
        INSERT [ level := 0
               , message := message
               ] INTO this->previewmessages AT END;

        IF (selectedfile.type = whconstant_whfstype_internallink)
        {
          IF (GetErrorFromPublished(target.published) NOT IN [ 0, 112 ] AND NOT IsQueuedForPublication(target.published)) // Ignore 'locked site' errors
          {
            // Publication errors
            INSERT [ level := 2
                   , message := GetTid("publisher:tolliumcomponents.previewbrowser.errors") || ' <a href="preview:showerrors">' || GetTid("publisher:tolliumcomponents.previewbrowser.clickerrors") || '</a>'
                   ] INTO this->previewmessages AT END;

            IF (GetOncePublishedFromPublished(target.published))
            {
              INSERT [ level := 0
                     , message := GetTid("publisher:tolliumcomponents.previewbrowser.internallink-cachedversion")
                     ] INTO this->previewmessages AT END;
            }
            ELSE
            {
              this->suppressrendering := TRUE; // Never published, nothing to show
            }
          }
          ELSE IF(TestFlagFromPublished(target.published, 400000))
          {
            // Publication warnings
            INSERT [ level := 1
                   , message := GetTid("publisher:tolliumcomponents.previewbrowser.warnings") || ' <a href="preview:showwarnings">' || GetTid("publisher:tolliumcomponents.previewbrowser.clickwarnings") || '</a>'
                   ] INTO this->previewmessages AT END;

          }
        }
      }
      ELSE
      {
        // Internal link not found
        INSERT [ level := 1
               , message := GetTid("publisher:tolliumcomponents.previewbrowser.internallink-broken")
               ] INTO this->previewmessages AT END;
      }
    }
    ELSE IF(selectedfile.type = whconstant_whfstype_externallink)
    {
      // Show external link target
      INSERT [ level := 0
             , message := GetTid("publisher:tolliumcomponents.previewbrowser.externallink", EncodeHTML(selectedfile.externallink)) || ' <a href="preview:showexternallink">' || GetTid("publisher:tolliumcomponents.previewbrowser.clickexternallink") || '</a>'
             ] INTO this->previewmessages AT END;
      this->suppressrendering := TRUE; // Don't show the external link within the preview browser
    }

    this->refresh_unpublished := FALSE;

    OBJECT applytester := GetApplyTesterForObject(selectedfile.id);
    RECORD previewinfo := applytester->GetPreviewSettings();


    BOOLEAN generatewidgetpreview;
    IF(previewinfo.browserurl != "")
    {
      //Takes precedence over standard rendering
      IF(previewinfo.browseinsite)
      {
        STRING siteroot := SELECT AS STRING webroot FROM system.sites WHERE id = selectedfile.parentsite;
        IF(siteroot != "")
          this->currenturl := ResolveToAbsoluteURL(siteroot, previewinfo.browserurl);
      }
      ELSE
      {
        this->currenturl := ResolveToAbsoluteURL(this->contexts->controller->baseurl, previewinfo.browserurl);
      }

      IF(this->currenturl != "")
        this->currenturl := UpdateURLVariables(this->currenturl, [ preview := this->GetPreviewToken(selectedfile, contentfile) ]);
    }
    ELSE
    {
      IF(selectedfile.type != 0 AND NOT selectedfile.isfolder)
      {
        RECORD typeinfo := LookupContentTypeById(selectedfile.type); //TODO can't we figure out whether or not this is a widget with a preview from the applytester?
        generatewidgetpreview := RecordExists(typeinfo) AND RecordExists(typeinfo.filetype) AND typeinfo.filetype.generatepreview;
      }
      IF(generatewidgetpreview)
      {
        // The selected file is a widget preview
        INSERT [ level := 0
               , message := GetTid("publisher:tolliumcomponents.previewbrowser.widgetpreview")
               ] INTO this->previewmessages AT END;

        this->currenturl := CreateWidgetPreviewLink(AddTimeToDate(publisher_settings.widget_preview_expiry, GetCurrentDateTime()), "", fileid, fileid);
        this->refresh_unpublished := TRUE;
      }
    }

    IF (NOT generatewidgetpreview AND NOT ispreview AND GetErrorFromPublished(selectedfile.published) = 0 AND NOT GetOncePublishedFromPublished(selectedfile.published))
    {
      RECORD fileinfo := DescribeContentTypeById(selectedfile.type, [ isfolder := FALSE, mockifmissing := TRUE ]);
      INSERT [ level := 0
             , message := fileinfo.ispublishable ? GetTid("publisher:tolliumcomponents.previewbrowser.notpublished") : GetTid("publisher:tolliumcomponents.previewbrowser.hasnopreview")
             ] INTO this->previewmessages AT END;
      this->suppressrendering := TRUE; // Nothing to show
    }

    IF (GetErrorFromPublished(selectedfile.published) NOT IN [ 0, 112 ] AND NOT IsQueuedForPublication(selectedfile.published)) // Ignore 'locked site' errors
    {
      // Publication errors
      INSERT [ level := 2
             , message := GetTid("publisher:tolliumcomponents.previewbrowser.errors") || ' <a href="preview:showerrors">' || GetTid("publisher:tolliumcomponents.previewbrowser.clickerrors") || '</a>'
             ] INTO this->previewmessages AT END;

      IF (GetOncePublishedFromPublished(selectedfile.published))
      {
        INSERT [ level := 0
               , message := GetTid("publisher:tolliumcomponents.previewbrowser.internallink-cachedversion")
               ] INTO this->previewmessages AT END;
      }
      ELSE
      {
        this->suppressrendering := TRUE; // Never published, nothing to show
      }
    }
    ELSE IF(TestFlagFromPublished(selectedfile.published, PublishedFlag_Warning))
    {
      // Publication warnings
      INSERT [ level := 1
             , message := GetTid("publisher:tolliumcomponents.previewbrowser.warnings") || ' <a href="preview:showwarnings">' || GetTid("publisher:tolliumcomponents.previewbrowser.clickwarnings") || '</a>'
             ] INTO this->previewmessages AT END;

    }

    IF(this->currenturl = "" AND NOT this->suppressrendering) //still no preview
    {
      //We need to know if we can expect HTML if we show the file's URL. So we look at the target's type (and for internal & contentlink, we'll look at the final file)
      RECORD fileinfo := DescribeContentTypeById(targetfiletype, [ isfolder := selectedfile.isfolder, mockifmissing := TRUE ]);
      IF(fileinfo.needstemplate OR fileinfo.id = whconstant_whfstype_dynamicfoldercontents) //dynamic folder contents
      {
        IF(usecontentfile = 0) //we can show the live version
          this->currenturl := selectedfile.link;
        ELSE //generate a preview (used by versioning app)
          this->currenturl := CreatePreviewLink(AddDaysToDate(1, GetCurrentDatetime()), "", selectedfile.id, usecontentfile);
      }
      ELSE
      {
        INSERT [ level := 0
              , message := GetTid("publisher:tolliumcomponents.previewbrowser.hasnopreview")
              ] INTO this->previewmessages AT END;
        this->suppressrendering := TRUE; // Nothing to show
      }
    }

    IF (GetScheduledFromPublished(selectedfile.published))
    {
      // There are tasks defined for the selected file, display the first one
      RECORD task := SELECT * FROM publisher.schedule WHERE file = fileid ORDER BY when;
      IF (RecordExists(task))
      {
        SWITCH (task.event)
        {
          CASE 1
          {
            INSERT [ level := 0
                   , message := GetTid("publisher:tolliumcomponents.previewbrowser.scheduledpublish", this->owner->tolliumuser->FormatDateTime(task.when, "minutes", TRUE, TRUE))
                   ] INTO this->previewmessages AT END;
          }
          CASE 2
          {
            INSERT [ level := 0
                   , message := GetTid("publisher:tolliumcomponents.previewbrowser.scheduledunpublish", this->owner->tolliumuser->FormatDateTime(task.when, "minutes", TRUE, TRUE))
                   ] INTO this->previewmessages AT END;
          }
          CASE 3
          {
            INSERT [ level := 0
                   , message := GetTid("publisher:tolliumcomponents.previewbrowser.scheduledmove", this->owner->tolliumuser->FormatDateTime(task.when, "minutes", TRUE, TRUE))
                   ] INTO this->previewmessages AT END;
          }
          CASE 4
          {
            INSERT [ level := 0
                   , message := GetTid("publisher:tolliumcomponents.previewbrowser.scheduleddelete", this->owner->tolliumuser->FormatDateTime(task.when, "minutes", TRUE, TRUE))
                   ] INTO this->previewmessages AT END;
          }
        }
      }
      ELSE
      {
        INSERT [ level := 0
               , message := GetTid("publisher:tolliumcomponents.previewbrowser.scheduled")
               ] INTO this->previewmessages AT END;
      }
    }

    this->CheckHTTP();

    //PDF sandboxing is broken in chrome, https://bugs.chromium.org/p/chromium/issues/detail?id=413851 - so drop sandbox for PDFs
    RETURN CELL[ ...data
               , sandbox := NOT(contentfile.type = 11 AND ToUppercase(contentfile.name) LIKE "*.PDF")
               ];
  }

  MACRO ShowErrors()
  {
    IF(this->previewfileid != 0)
    {
      INTEGER fileid := SELECT AS INTEGER filelink FROM system.fs_objects WHERE id = this->previewfileid AND type = 19;
      IF (fileid = 0)
        fileid := this->previewfileid;
      OBJECT faultinfo := this->owner->LoadScreen("publisher:filemgrdialogs.failedpublish", [ id := fileid ]);
      faultinfo->RunModal();
    }
  }


  // Load browser frame if needed
  MACRO LoadBrowserFrame()
  {
    STRING src;
    IF(this->suppressrendering)
      src := this->contexts->controller->baseurl || ".publisher/backend/preview/nopreview.html";
    ELSE IF(IsAbsoluteURL(this->currenturl))
      src := this->currenturl;

    // (Re)load the browserframe if the src changed
    IF (src != ^browserframe->src)
    {
      ^browserframe->src := src;
      ^browserframe->sandbox := this->currentsandbox ? "allow-forms allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts" : "none";
    }
  }

  // Refresh browser frame contents
  MACRO RefreshBrowserFrame()
  {
    this->LoadBrowserFrame();

    this->lastsethtmlvalue := Detokenize((SELECT AS STRING ARRAY message FROM this->previewmessages), '<br />');
    ^previewmessage->htmlvalue := this->lastsethtmlvalue;
    ^info->visible := Length(this->previewmessages) > 0;

    STRING src := this->suppressrendering ? this->contexts->controller->baseurl || ".publisher/backend/preview/nopreview.html"
                : UpdateURLVariables(this->currenturl,
                      [ "__whpub_clock_" := FormatISO8601DateTime(GetCurrentDateTime(), "", "milliseconds", "", FALSE)
                      ]);

    ^browserframe->src := src;
  }

  MACRO ReloadCurrent(BOOLEAN reloadmessages, BOOLEAN reloadsource)
  {
    IF(this->previewfileid = 0 AND this->currenturl != "" )
    {
      this->RefreshBrowserFrame();
    }
    ELSE
    {
      this->SetFileId(this->previewfileid, reloadmessages, reloadsource, this->contentfileid);
    }
  }

  // Set the currently previewed file
  MACRO SetFileId(INTEGER fileid, BOOLEAN reloadmessages, BOOLEAN reloadsource, INTEGER contentfile)
  {
    BOOLEAN was_iframe_interaction;
    IF (fileid = this->previewfileid AND contentfile = this->contentfileid AND NOT reloadmessages AND NOT reloadsource)
      RETURN;

    this->currenturl := ""; //reset for building new url
    this->suppressrendering := FALSE;
    this->currentsandbox := TRUE;

    RECORD baseinfo;
    IF(fileid>0)
      baseinfo := SELECT id, name, type, parent, indexdoc, isfolder, parentsite, objecturl FROM system.fs_objects WHERE id = fileid AND isactive;

    // Test if site isn't locked (but allow history entries)
    IF (RecordExists(baseinfo))
    {
      RECORD site := SELECT locked FROM system.sites WHERE id = baseinfo.parentsite;
      IF (NOT RecordExists(site) OR site.locked)
        baseinfo := DEFAULT RECORD;
    }

    STRING url := RecordExists(baseinfo) ? baseinfo.objecturl : "";
    ^previewurl->value := url;
    this->ClearForNewURL(url);

    RECORD previewfile; // The file we're actually viewing (e.g. index document of selected folder)
    IF(RecordExists(baseinfo))
      IF(baseinfo.isfolder)
        previewfile := SELECT id, type, parent, COLUMN url FROM system.fs_objects WHERE id = baseinfo.indexdoc AND isactive;
      ELSE
        previewfile := baseinfo;


    IF(RecordExists(previewfile) AND previewfile.id != 0)
    {

      RECORD previewdata := this->GetPreviewData(previewfile.id, contentfile);
      this->previewfileid := previewdata.fileid;
      this->contentfileid := contentfile;
      this->currentsandbox := previewdata.sandbox;

      INTEGER warninglevel := SELECT AS INTEGER MAX(level) FROM this->previewmessages;
      //STRING previewicon := [ "messageboxes/information", "status/warning", "status/serious_error"][warninglevel];
      //this->previewicon->SetImageSrc("tollium:" || previewicon, "");

      ^previewlistener->masks := [ "system:whfs.folder." || previewfile.parent, "publisher:publish.folder." || previewfile.parent ];
    }
    ELSE
    {
      IF(url != "" AND RecordExists(baseinfo))
      {
        INSERT [ level := 0
               , message := GetTid("publisher:tolliumcomponents.previewbrowser.folderwithoutindex", baseinfo.name)
               ] INTO this->previewmessages AT END;
      }
      this->previewfileid := 0;
      this->contentfileid := 0;
      ^previewlistener->masks := DEFAULT STRING ARRAY;
      this->suppressrendering := TRUE;
    }

    this->RefreshBrowserFrame();
  }

  // ---------------------------------------------------------------------------
  //
  // Callbacks
  //

  MACRO OnbrowserMessage(RECORD data, STRING origin)
  {
    data := EnforceStructure([ type := "", location := ""], data);
    IF(LookupPublisherURL(origin).webserver = 0) //not hosted here
    {
      //TODO consider telling the server to block this origin from communicating any further messages? a hostile site could trigger a lot of traffic - but that might be better handled at iframe.whlib level
      Print(`Ignoring postmessage from unknown origin '${origin}'`);
      RETURN; //ignore
    }

    IF(data.type = "webhare-navigation")
    {
      Print(`Got webhare-location with url ${data.location}\n`);
      //^browserframe->src := data.location; //TODO why update location? this would only trigger navigational resets!
      IF(this->onpreviewedobjectchange != DEFAULT MACRO PTR)
      {
        STRING url := Tokenize(Tokenize(data.location, "#")[0], "?")[0];

        //TODO not sure why we need to update currenturl ... why should we worry about that state which we can't 100% guaranteed follow anyway?
        this->currenturl := url;

        RECORD findit := LookupPublisherURL(url);
        this->onpreviewedobjectchange(findit.file ?? findit.folder, url);
      }
    }
  }
  MACRO OnBrowserFrameCallback(RECORD data)
  {
    SWITCH (data.type)
    {
      CASE "navigationprevented"
      {
        IF (data.uri = "")
          RETURN;

        this->owner->RunSimpleScreen("info", GetTid("publisher:tolliumcomponents.previewbrowser.cannotshowinsecurecontent", data.uri));
      }
    }
  }

  MACRO OnDeviceChange()
  {
    STRING selected := ^deviceselect->value;
    IF (selected LIKE "d-*")
    {
      RECORD selection := ^deviceselect->selection;
      DELETE FROM ^deviceselect->options WHERE rowkey LIKE "c-*";

      this->devicewidth := selection.width;
      this->deviceheight := selection.height;
      ^browserframe->SetViewport(this->devicewidth, this->deviceheight);
    }
    ELSE IF (selected LIKE "c*")
    {
      IF (selected LIKE "c-*")
      {
        RECORD selection := ^deviceselect->selection;
        this->devicewidth := selection.width;
        this->deviceheight := selection.height;
      }
      ELSE
      {
        DELETE FROM ^deviceselect->options WHERE rowkey LIKE "c-*";

        OBJECT dlg := this->owner->LoadScreen(Resolve("previewbrowser.xml#previewbrowsercustom"));
        dlg->width->value := this->devicewidth;
        dlg->height->value := this->deviceheight;
        IF (dlg->RunModal() = "ok" AND dlg->width->value > 0 AND dlg->height->value > 0)
        {
          this->devicewidth := dlg->width->value;
          this->deviceheight := dlg->height->value;
          selected := "c-" || this->devicewidth || "x" || this->deviceheight;
          INSERT [ rowkey := selected
                 , title := GetTid("publisher:tolliumcomponents.previewbrowser.devices.customsize") || " (" || this->devicewidth || "x" || this->deviceheight || ")"
                 , width := this->devicewidth
                 , height := this->deviceheight
                 ] INTO ^deviceselect->options AT END;
          ^deviceselect->value := selected;
        }
        ELSE
        {
          ^deviceselect->value := this->tolliumuser->GetRegistryKey("publisher.previewbrowser.device", "f");
          RETURN;
        }
      }

      ^browserframe->SetViewport(this->devicewidth, this->deviceheight);
    }
    ELSE IF (selected = "f")
    {
      DELETE FROM ^deviceselect->options WHERE rowkey LIKE "c-*";
      this->devicewidth := 0;
      this->deviceheight := 0;
      ^browserframe->ResetViewport();
    }
    ELSE
    {
      selected := "";
    }

    IF (selected != "")
    {
      IF (IsDatabaseWritable())
        this->tolliumuser->DelayedSetRegistryKey("publisher.previewbrowser.device", selected);
      this->ReloadCurrent(TRUE, FALSE); //force reresh of the error messages
    }
  }

  MACRO OnPreviewMessageClick(STRING href)
  {
    IF (href LIKE "preview:*")
    {
      href := Substring(href, 8);
      SWITCH (href)
      {
        /*ADDME: To open a new browser window, the windowopenaction must be triggered by the user directly. Maybe we can
                 somehow link the preview message link to the action client-side?
        CASE "showexternallink"
        {
          IF (RecordExists(this->filelist->selection) AND this->filelist->selection.externallink != "")
            this->frame->OpenBrowserWindow(this->filelist->selection.externallink, this->previewbrowsername); //FIXME use windowopenaction
        }
        CASE "viewsite"
        {
        }
        */
        CASE "showwarnings", "showerrors"
        {
          this->ShowErrors();
        }
      }
    }
  }

  MACRO OnPreviewUpdateEvents(RECORD ARRAY events)
  {
    BOOLEAN file_updated;
    FOREVERY (RECORD event FROM events)
      FOREVERY (RECORD file FROM event.data.files)
      {
        IF (file.file = this->previewfileid) //TODO or watch contentfileid ?
        {
          file_updated := file_updated OR "update" IN file.events;
          IF ((this->refresh_unpublished AND "update" IN file.events)
              OR (NOT this->refresh_unpublished AND "pub" IN file.events))
          {
            this->ReloadCurrent(TRUE, TRUE); //force reresh of both frame and the error messages
            RETURN;
          }
        }
      }
  }

  // ---------------------------------------------------------------------------
  //
  // Actions
  //

  MACRO DoShowExternalLink(OBJECT handler)
  {
    RECORD fsobj := SELECT externallink FROM system.fs_objects WHERE id = this->previewfileid;

    IF (RecordExists(fsobj) AND fsobj.externallink != "")
      handler->SendURL(fsobj.externallink);
  }

  MACRO DoViewSite(OBJECT handler)
  {
    IF(this->currenturl != "")
      handler->SendURL(this->currenturl);
  }

  MACRO DoPreviewRefresh()
  {
    this->SetFileId(this->previewfileid, TRUE, TRUE, this->contentfileid); //force reresh of both frame and the error messages
  }

  MACRO ClearForNewURL(STRING url)
  {
    this->previewmessages := DEFAULT RECORD ARRAY;
    this->currenturl := url;
    this->suppressrendering := url = "";
  }


  // ---------------------------------------------------------------------------
  //
  // Public API
  //

  PUBLIC MACRO SetPreview(INTEGER objid, RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    options := ValidateOptions([ contentfile := 0], options);
    Print(`Owner asked previewbrowser to go to #${objid} ${SELECT AS STRING "(" || whfspath || ")" FROM system.fs_objects WHERE id=objid}\n`);
    this->SetFileId(objid, FALSE, FALSE, options.contentfile);
  }

  PUBLIC MACRO SetPreviewURL(STRING newurl)
  {
    this->previewfileid := 0;
    ^previewurl->value := newurl;
    this->ClearForNewURL(newurl);
    this->CheckHTTP();
    this->RefreshBrowserFrame();
  }

  PUBLIC RECORD FUNCTION GetPreviewState() //introspection needed for testing
  {
    RETURN [ displayurl := ^previewurl->value
           , browserurl := ^browserframe->src
           , showingmessages := ^info->visible
           , messages := ^info->visible ? this->lastsethtmlvalue : ""
           , messagecomp := ^previewmessage
           , previewrefresh := ^previewrefresh
           ];
  }
>;
