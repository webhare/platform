What is publisher forms? One or more of:
- A standardised way of defining forms in HTML
- A DOM and class setup that allows you to standardise how your CSS targets the forms
- RPC handling to easily connect forms to the backend

Build beautiful, responsive and GDPR-proof forms.
