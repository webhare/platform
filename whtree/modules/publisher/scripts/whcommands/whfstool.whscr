<?wh
// short: Apply bulk WHFS changes
// syntax: <actions...> <path...>

LOADLIB "wh::os.whlib";

LOADLIB "mod::consilio/lib/whfscatalog.whlib";

LOADLIB "mod::publisher/lib/urlhistory.whlib";
LOADLIB "mod::publisher/lib/internal/dbschema.whlib";

LOADLIB "mod::system/lib/whfs.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

RECORD args := ParseArguments(GetConsoleArguments(), [ [ name := "recursive", type := "switch" ]
                                                     , [ name := "pin", type := "switch" ]
                                                     , [ name := "striprtdextension", type := "switch" ]
                                                     , [ name := "savemeta", type := "switch" ]
                                                     , [ name := "restoremeta", type := "stringopt" ]
                                                     , [ name := "unpin", type := "switch" ]
                                                     , [ name := "reindex", type := "switch" ]
                                                     , [ name := "dryrun", type := "switch" ]
                                                     , [ name := "debug", type := "switch" ]
                                                     , [ name := "paths", type := "paramlist" ]
                                                     ]);
IF (NOT RecordExists(args) OR Length(args.paths) = 0)
{?>Usage: wh publisher:whfstool [--dryrun] [--debug] [--recursive] [--<actions>] <path...>
  --dryrun     Do not commit changes to the database
  --recursive  Apply to subdirectories too
  path         The whfs path(s) to process

  Actions:
  --pin / --unpin      Add/remove pins
  --striprtdextension  Remove the .rtd file extension
  --savemeta           Save url history and creation/update dates to an exportable instance
  --restoremeta=...    Restore specific metadata. Comma separated list of 'urlhistory', 'modificationdate', 'firstpublishdate' or 'contentmodificationdate'
  --reindex            Reindex for Publisher search

<?wh
  SetConsoleExitCode(1);
  RETURN;
}
IF(args.pin AND args.unpin)
{
  Print("Cannot pin & unpin at the same time\n");
  SetConsoleExitCode(1);
  RETURN;
}

OBJECT trans := OpenPrimary();
OBJECT saveurlhistorytype := OpenWHFSType("http://www.webhare.net/xmlns/publisher/savedurlhistory");
BOOLEAN listonly := NOT (args.pin OR args.unpin OR args.savemeta OR args.restoremeta != "" OR args.striprtdextension OR args.reindex);
STRING ARRAY restorefields := Tokenize(args.restoremeta, ",");

IF(args.restoremeta != "" AND args.savemeta)
  TerminateScriptWithError("save and restore cannot be mixed");

INTEGER ARRAY startobjids;
FOREVERY(STRING path FROM args.paths)
{
  INTEGER objid := ToInteger(path, 0);
  IF (objid > 0)
    INSERT objid INTO startobjids AT END;
  ELSE
  {
    OBJECT dest := OpenWHFSObjectByPath(path);
    IF(NOT ObjectExists(dest))
    {
      Print("Invalid path: " || path || "\n");
      SetConsoleExitCode(1);
      RETURN;
    }
    INSERT dest->id INTO startobjids AT END;
  }
}

RECORD ARRAY FUNCTION GetInterestingStuff(INTEGER ARRAY objids, BOOLEAN matchparent)
{
  RETURN SELECT id, name, whfspath, isfolder, ispinned, parent
           FROM system.fs_objects
          WHERE (matchparent ? (parent IN objids AND id NOT IN [whconstant_whfsid_private]) //don't recurse into webhare-private, you need to explicitly request that
                             : (id IN objids))
       ORDER BY ToUppercase(whfspath);
}

RECORD ARRAY FUNCTION ExecuteAction(RECORD ARRAY objects, BOOLEAN recursive)
{
  INTEGER ARRAY updatedids;
  IF(listonly)
  {
    FOREVERY(RECORD path FROM objects)
      Print(path.whfspath||"\n");
  }
  ELSE
  {
    GetPrimary()->BeginWork();

    IF(args.pin OR args.unpin)
    {
      FOREVERY(RECORD topin FROM SELECT id,whfspath FROM objects WHERE ispinned = args.unpin)
      {
        Print( (args.pin ? "Pin: " : "Unpin: ") || topin.whfspath || "\n");
        OpenWHFSObject(topin.id)->UpdateMetadata( [ ispinned := args.pin ]);
        INSERT topin.id INTO updatedids AT END;
      }
    }
    IF(args.striprtdextension)
    {
      FOREVERY(RECORD tofix FROM SELECT * FROM objects WHERE ToUppercase(name) LIKE "*.RTD" AND NOT isfolder)
      {
        STRING newname := TrimWhitespace(Left(tofix.name, Length(tofix.name) - 4));
        IF(RecordExists(SELECT FROM objects WHERE objects.parent = tofix.parent AND ToUppercase(name) = ToUppercase(newname)))
        {
          PrintTo(2, `Cannot rename ${tofix.whfspath} to ${newname} - conflicting item in folder\n`);
        }
        ELSE IF(NOT IsValidWHFSName(newname,FALSE))
        {
          PrintTo(2, `Cannot rename ${tofix.whfspath} to ${newname} - resulting filename is invalid\n`);
        }
        ELSE
        {
          Print(`Renaming ${tofix.whfspath} to ${newname}\n`);
          //UPDATE system.fs_objects SET name := newname WHERE id = tofix.id;
          OpenWHFSObject(tofix.id)->UpdateMetadata([ name := newname ]);
          INSERT tofix.id INTO updatedids AT END;
        }
      }
    }
    IF(args.savemeta)
    {
      INTEGER ARRAY ids := SELECT AS INTEGER ARRAY id FROM objects;
      RECORD ARRAY history := SELECT urlhistory.creationdate
                                   , urlhistory.url
                                   , urlhistory.fsobject
                                FROM publisher.urlhistory
                               WHERE fsobject IN VAR ids;
      RECORD ARRAY objinfo := SELECT originalid := id
                                   , objecturl
                                   , creationdate
                                   , modificationdate
                                   , modifiedby
                                   , path := whfspath
                                   , firstpublishdate
                                   , contentmodificationdate
                                FROM system.fs_objects
                               WHERE id IN VAR ids;

      RECORD ARRAY targeturls := SELECT source.id
                                      , target.whfspath
                                   FROM system.fs_objects AS source
                                      , system.fs_objects AS target
                                  WHERE source.filelink = target.id
                                        AND source.id IN VAR ids;

      FOREVERY(RECORD obj FROM objinfo)
      {
        saveurlhistorytype->SetInstanceData(obj.originalid,
          [ ...obj
          , targetpath := (SELECT AS STRING whfspath FROM targeturls WHERE id = obj.originalid)
          , urlhistory := (SELECT creationdate, url FROM history WHERE fsobject = obj.originalid)
          ], [ isvisibleedit := FALSE ]);
        INSERT obj.originalid INTO updatedids AT END;
      }
    }
    IF(args.restoremeta != "")
    {
      objects := saveurlhistorytype->Enrich(objects, "ID", [ "urlhistory", "modificationdate", "firstpublishdate", "contentmodificationdate" ]);
      FOREVERY(RECORD obj FROM objects)
      {
        RECORD metaupdates;
        FOREVERY(STRING field FROM restorefields)
        {
          SWITCH(field)
          {
            CASE "urlhistory"
            {
              FOREVERY(RECORD row FROM obj.urlhistory)
              {
                AddURLHistoryEntry(obj.id, row.url LIKE "//*" ? "https:" || row.url : row.url);
                INSERT obj.id INTO updatedids AT END;
              }
            }
            CASE "modificationdate"
            {
              metaupdates := CellInsert(metaupdates, field, GetCell(obj, field));
            }
            CASE "firstpublishdate", "contentmodificationdate"
            {
              IF(NOT obj.isfolder)
                metaupdates := CellInsert(metaupdates, field, GetCell(obj, field));
            }
            DEFAULT
            {
              TerminateScriptWithError(`Unknown field to restore '${field}'`);
            }
          }
        }
        IF(RecordExists(metaupdates))
          OpenWHFSObject(obj.id)->UpdateMetadata(metaupdates);
      }
    }

    IF(args.dryrun)
      GetPrimary()->RollbackWork();
    ELSE
      GetPrimary()->CommitWork();
  }

  RECORD ARRAY reindexobjs := SELECT * FROM objects WHERE id NOT IN updatedids;
  IF(NOT recursive)
    RETURN reindexobjs;

  INTEGER ARRAY folderids := SELECT AS INTEGER ARRAY id FROM objects WHERE isfolder;
  IF(Length(folderids)=0)
    RETURN reindexobjs;

  RETURN reindexobjs CONCAT ExecuteAction(GetInterestingStuff(folderids, TRUE), recursive);
}

RECORD ARRAY toprocess := GetInterestingStuff(startobjids, 0 IN startobjids); //recursive if root is one of the targets
RECORD ARRAY to_reindex := ExecuteAction(toprocess, args.recursive);
IF (args.reindex AND NOT args.dryrun)
{
  to_reindex :=
      (SELECT id
            , isfolder
            , isdelete := FALSE
         FROM to_reindex)
      CONCAT
      (SELECT id
            , isfolder := TRUE // If unknown, assume it was a folder
            , isdelete := TRUE
         FROM ToRecordArray(startobjids, "id")
        WHERE id NOT IN (SELECT AS INTEGER ARRAY id FROM toprocess));

  IF (Length(to_reindex) > 0)
    ReindexWHFSChanges(to_reindex, CELL[ __debug := args.debug, __progress := TRUE ]);
}
