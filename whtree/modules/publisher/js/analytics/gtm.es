export * from "./gtm";

import { warnESFile } from "@mod-system/js/internal/es-warning";
warnESFile("@mod-publisher/js/analytics/gtm.es");
