<?wh
/** @topic testframework/publisher */
LOADLIB "wh::internet/urls.whlib";

/** @public
    @short Publisher forms (CI) tester returned by %OpenFormsapiFormTester */
STATIC OBJECTTYPE FormsapiFormTester
<
  STRING baseurl;
  STRING target;
  RECORD formdata;
  OBJECT browser;

  MACRO NEW(OBJECT browser, STRING baseurl, STRING target)
  {
    this->browser := browser;
    this->baseurl := baseurl;
    this->target := target;
  }

  PUBLIC RECORD FUNCTION Invoke(RECORD fields, STRING methodname, VARIANT ARRAY args, RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    options := ValidateOptions([ extrasubmit := DEFAULT RECORD
                               ], options);

    STRING formservice := ResolveToAbsoluteURL(this->baseurl, "/wh_services/publisher/forms");
    RECORD formdata := CELL[ vals := (SELECT name := ToLowercase(name), value FROM UnpackRecord(fields))
                           , this->target
                           , url := UnpackURL(this->baseurl).urlpath
                           , methodname
                           , args
                           , options.extrasubmit
                           ];

    RECORD result := this->browser->CallJSONRPC(formservice,"FormInvoke",formdata);
    RETURN result;
  }

  /** Submit the form
      @param fields Name/value pairs of submitted fields
      @cell(record) options.extrasubmit Optional 'extra data' that would be submitted to the form by JavaScirpt
      @return Submission result*/
  PUBLIC RECORD FUNCTION SubmitForm(RECORD fields, RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    options := ValidateOptions([ extrasubmit := DEFAULT RECORD
                               ], options);

    STRING formservice := ResolveToAbsoluteURL(this->baseurl, "/wh_services/publisher/forms");
    RECORD formdata := CELL[ vals := (SELECT name := ToLowercase(name), value FROM UnpackRecord(fields))
                           , target := this->target
                           , url := UnpackURL(this->baseurl).urlpath
                           ];

    IF(RecordExists(options.extrasubmit))
      INSERT CELL extrasubmit := options.extrasubmit INTO formdata;
    RECORD result := this->browser->CallJSONRPC(formservice,"FormSubmit",formdata);
    RETURN result;
  }
>;

/** @loadlib mod::publisher/lib/testfw/forms.whlib
    @param(object %WebBrowser) Browser to use (eg `testfw->browser`)
    @cell(string) options.selector CSS selector to find the form (defaults to first formsapi form on the page)
    @return(object #FormsapiFormTester) Forms tester. Throws if the form wasn't found
*/
PUBLIC OBJECT FUNCTION OpenFormsapiFormTester(OBJECT browser, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ selector := "form[data-wh-form-target]"
                             , target := ""
                             ], options);

  STRING target := options.target;
  IF(target = "")
  {
    OBJECT ARRAY candidates := browser->document->GetElements(options.selector);
    IF(Length(candidates)=0)
      THROW NEW Exception(`No matches for selector '${options.selector}' on ${browser->href}`);
    IF(Length(candidates)>1)
      THROW NEW Exception(`Multiple matches (${Length(candidates)}) for selector '${options.selector}' on ${browser->href}`);

    target := candidates[0]->GetAttribute("data-wh-form-target");
  }

  RETURN NEW FormsapiFormTester(browser, browser->href, target);
}
