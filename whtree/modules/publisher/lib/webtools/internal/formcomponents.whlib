<?wh

LOADLIB "wh::adhoccache.whlib";
LOADLIB "wh::util/algorithms.whlib";
LOADLIB "wh::xml/dom.whlib";
LOADLIB "wh::xml/validation.whlib";
LOADLIB "wh::xml/xsd.whlib";

LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/internal/modules/defreader.whlib";
LOADLIB "mod::system/lib/internal/resources.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

LOADLIB "mod::tollium/lib/gettid.whlib";

//ADDME: passthrough_formeditorextension_options can be removed if all extendformeditor extendobjects are migrated to paextensions
PUBLIC RECORD passthrough_formeditorextension_options;

PUBLIC CONSTANT STRING xmlns_forms := "http://www.webhare.net/xmlns/publisher/forms";

CONSTANT STRING ARRAY supportedvalues_hstypes :=
    [ "string", "integer", "money", "float", "boolean", "datetime", "record"
    , "stringarray", "integerarray", "moneyarray", "floatarray", "booleanarray", "datetimearray", "recordarray"
    ];

CONSTANT STRING ARRAY supportedvalues_interfaces :=
    [ "email", "emailarray"
    , "options", "file", "rtd", "address", "image"
    ];

PUBLIC RECORD FUNCTION ParseSupportedValues(STRING ARRAY inattr)
{
  RECORD retval := [ supportedvalues := STRING[]
                   , errors := STRING[]
                   ];

  IF("none" IN inattr)
  {
    IF(Length(inattr) > 1)
      INSERT `Supportedvalues 'none' can not be combined with any other value` INTO retval.errors AT END; //we need a GetValueAs for conditions to select the proper value PLUS a practical use case for testing

    retval.supportedvalues := ["none"];
    RETURN retval;
  }

  //Deal with implicit types
  IF("email" IN inattr AND "string" NOT IN inattr)
    INSERT "string" INTO inattr AT END;
  IF("emailarray" IN inattr AND "stringarray" NOT IN inattr)
    INSERT "stringarray" INTO inattr AT END;
  IF("image" IN inattr AND "file" NOT IN inattr)
    INSERT "file" INTO inattr AT END;

  IF("record" NOT IN inattr)
    FOREVERY(STRING recordtype FROM ["rtd","file","address"])
      IF(recordtype IN inattr)
      {
        INSERT "record" INTO inattr AT END;
        BREAK;
      }

  STRING hstype;

  FOREVERY(STRING val FROM inattr)
  {
    IF(val IN supportedvalues_hstypes)
    {
      IF(hstype = "")
      {
        hstype := val;
        INSERT val INTO retval.supportedvalues AT END;
      }
      ELSE
        INSERT `Multiple HareScript types claimed, '${hstype}' and '${val}' - this is not yet supported` INTO retval.errors AT END; //we need a GetValueAs for conditions to select the proper value PLUS a practical use case for testing
    }
    ELSE IF(val IN supportedvalues_interfaces)
      INSERT val INTO retval.supportedvalues AT END;
    ELSE
      INSERT `Unknown supportedvalues value '${val}'` INTO retval.errors AT END;
  }

  RETURN retval;
}

RECORD FUNCTION ParseComponent(RECORD element, OBJECT compinfo, STRING modulename, STRING resourcename, RECORD matchattribute)
{
  RECORD ARRAY errors, warnings;
  RECORD elrec := [ name := element.name
                  , namespace := element.namespaceuri
                  , matchattribute := matchattribute
                  , icon := compinfo->GetAttribute("tolliumicon") ?? compinfo->GetAttribute("icon")
                  , linecomponent := ParseXSBoolean(compinfo->GetAttribute("linecomponent"))
                  , title := ParseXMLTidPtr(resourcename, "", compinfo, "title")
                  , description := ParseXMLTidPtr(resourcename, "" , compinfo, "description")
                  , editextension := ""
                  , editdefaults := DEFAULT STRING ARRAY
                  , supportedvalues := STRING[]
                  , supportedvaluesfunc := ""
                  , hasautocomplete := compinfo->HasAttribute("autocomplete")
                  , autocomplete := ParseXSList(compinfo->GetAttribute("autocomplete"))
                  , parserfunc := ""
                  , fieldobject := ""
                  , ordering := ParseXSInt(compinfo->GetAttribute("ordering"))
                  , parsertype := ""
                  , conditionscope := compinfo->GetAttribute("conditionscope")
                  ];
  //Print(elrec.name||"\n");

  IF (elrec.title != "" AND elrec.title NOT LIKE "*:*")
    elrec.title := modulename || ":" || elrec.title;
  IF(element.namespaceuri = "http://www.webhare.net/xmlns/publisher/forms")
    elrec.parsertype := elrec.name; //legacy parser support

  IF (compinfo->HasAttribute("editextension"))
    elrec.editextension := MakeAbsoluteScreenReference(resourcename, compinfo->GetAttribute("editextension"));
  IF (compinfo->HasAttribute("editdefaults"))
    elrec.editdefaults :=
        SELECT AS STRING ARRAY attr
          FROM ToRecordArray(ParseXSList(compinfo->GetAttribute("editdefaults")), "attr")
         WHERE attr IN VAR whconstant_forms_valideditdefaults;

  IF (compinfo->GetAttribute("parserfunc") LIKE "*#*")
  {
    STRING parserlib := Tokenize(compinfo->GetAttribute("parserfunc"), "#")[0];
    STRING parserfunc := Substring(compinfo->GetAttribute("parserfunc"), Length(parserlib) + 1);
    parserlib := MakeAbsoluteResourcePath(resourcename, parserlib);
    elrec.parserfunc := parserlib || "#" || parserfunc;
  }
  IF (compinfo->HasAttribute("supportedvalues") OR compinfo->HasAttribute("defaultvalue"))
  {
    RECORD parseresult := ParseSupportedValues(ParseXSList(compinfo->GetAttribute("supportedvalues") ?? compinfo->GetAttribute("defaultvalue")));
    errors := errors CONCAT SELECT resourcename := resourcename, line := compinfo->linenum, col := 0, message := err FROM ToRecordArray(parseresult.errors, 'err');
    elrec.supportedvalues := parseresult.supportedvalues;
  }
  ELSE IF (compinfo->GetAttribute("supportedvaluesfunc") LIKE "*#*")
  {
    STRING supportedvalueslib := Tokenize(compinfo->GetAttribute("supportedvaluesfunc"), "#")[0];
    STRING supportedvaluesfunc := Substring(compinfo->GetAttribute("supportedvaluesfunc"), Length(supportedvalueslib) + 1);
    supportedvalueslib := MakeAbsoluteResourcePath(resourcename, supportedvalueslib);
    elrec.supportedvaluesfunc := supportedvalueslib || "#" || supportedvaluesfunc;
    elrec.supportedvalues := DEFAULT STRING ARRAY;
  }

  IF (compinfo->GetAttribute("fieldobject") LIKE "*#*")
  {
    STRING fieldlib := Tokenize(compinfo->GetAttribute("fieldobject"), "#")[0];
    STRING fieldobject := Substring(compinfo->GetAttribute("fieldobject"), Length(fieldlib) + 1);
    fieldlib := MakeAbsoluteResourcePath(resourcename, fieldlib);
    elrec.fieldobject := fieldlib || "#" || fieldobject;
  }
  ELSE IF(element.namespaceuri != "http://www.webhare.net/xmlns/publisher/forms" OR element.name NOT IN ["page","customfield"])
  {
    INSERT CELL[ resourcename
               , line := compinfo->linenum
               , col := 0
               , message := `Invalid fieldobject reference '${compinfo->GetAttribute("fieldobject")}' in ${resourcename}:${compinfo->linenum}`
               ] INTO errors AT END;
  }

  RETURN CELL[ errors, warnings, el := elrec ];
}

RECORD FUNCTION ParseHandler(RECORD element, OBJECT handler, STRING modulename, STRING resourcename)
{
  RECORD ARRAY errors, warnings;
  RECORD elrec := [ name := element.name
                  , namespace := element.namespaceuri
                  , icon := handler->GetAttribute("tolliumicon") ?? handler->GetAttribute("icon")
                  , title := ParseXMLTidPtr(resourcename, "", handler, "title")
                  , description := ParseXMLTidPtr(resourcename, "" , handler, "description")
                  , editextension := ""
                  , editdefaults := STRING[]
                  , parserfunc := ""
                  , handlerobject := ""
                  , handlertask := handler->GetAttribute("handlertask")
                  , handlertaskfor := ParseXSList(handler->HasAttribute("handlertaskfor") ? handler->GetAttribute("handlertaskfor") : "submit")
                  , ordering := ParseXSInt(handler->GetAttribute("ordering"))
                  ];
  //Print(elrec.name||"\n");

  IF (elrec.handlertask != "" AND elrec.handlertask NOT LIKE "*:*")
    elrec.handlertask := modulename || ":" || elrec.handlertask;

  IF (handler->HasAttribute("editextension"))
    elrec.editextension := MakeAbsoluteScreenReference(resourcename, handler->GetAttribute("editextension"));
  IF (handler->HasAttribute("editdefaults"))
    elrec.editdefaults :=
        SELECT AS STRING ARRAY attr
          FROM ToRecordArray(ParseXSList(handler->GetAttribute("editdefaults")), "attr")
         WHERE attr IN VAR whconstant_forms_validhandlereditdefaults;

  IF (handler->GetAttribute("parserfunc") LIKE "*#*")
  {
    STRING parserlib := Tokenize(handler->GetAttribute("parserfunc"), "#")[0];
    STRING parserfunc := Substring(handler->GetAttribute("parserfunc"), Length(parserlib) + 1);
    parserlib := MakeAbsoluteResourcePath(resourcename, parserlib);
    elrec.parserfunc := parserlib || "#" || parserfunc;
  }
  IF (handler->GetAttribute("handlerobject") LIKE "*#*")
  {
    STRING handlerlib := Tokenize(handler->GetAttribute("handlerobject"), "#")[0];
    STRING handlerobject := Substring(handler->GetAttribute("handlerobject"), Length(handlerlib) + 1);
    handlerlib := MakeAbsoluteResourcePath(resourcename, handlerlib);
    elrec.handlerobject := handlerlib || "#" || handlerobject;
  }

  RETURN CELL[ errors, warnings, el := elrec ];
}

RECORD FUNCTION GetAnnotiationUserDocumentation(OBJECT node)
{
  RECORD comment := [ line := node->source_xml_node->linenum, col := 0, text := "" ];

  FOREVERY (OBJECT annotation FROM node->annotations)
    FOREVERY (OBJECT userinfo FROM annotation->user_information)
      IF (TrimWhitespace(userinfo->textcontent) != "")
        comment := [ line := userinfo->linenum, col := 0, text := TrimWhitespace(userinfo->textcontent) ];

  RETURN comment;
}

PUBLIC RECORD FUNCTION ParseFormDefComponents(STRING resourcename, OBJECT doc, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(
        [ domimpl :=      DEFAULT OBJECT
        , withcomments := FALSE
        ], options);

  STRING modulename := GetModuleNameFromResourcePath(resourcename);
  STRING targetns := doc->documentelement->GetAttribute("targetNamespace");

  OBJECT domimpl := options.domimpl ?? GetWebhareXMLDOMImplementation();
  OBJECT parser := CreateXSD2001Parser(domimpl);
  ParseXSDSchema(parser, doc->documentelement, resourcename);

  RECORD commentdata;
  IF (options.withcomments)
  {
    commentdata :=
        [ comment :=  GetAnnotiationUserDocumentation(parser->schema_information)
        ];
  }

  RECORD ARRAY components, handlers, errors, warnings;

  FOREVERY (RECORD element FROM parser->elements)
  {
    IF (element.namespaceuri != targetns)
      CONTINUE; //don't parse imported elements, as that's not what we're being asked to do right now

    OBJECT ARRAY node_priority_list := [ OBJECT(element.node) ];

    //Process typedef and any extensions
    OBJECT walkext := element.node->type_definition;
    WHILE (ObjectExists(walkext))
    {
      INSERT walkext INTO node_priority_list AT END;
      IF (walkext->type="SIMPLETYPE" OR walkext->derivation_method!='extension')
        BREAK;

      walkext := walkext->base_type_definition;
    }

    RECORD ARRAY nodecomponents;
    FOREVERY (OBJECT node FROM node_priority_list)
    {
      FOREVERY (OBJECT annotation FROM node->annotations)
        FOREVERY (OBJECT appinfo FROM annotation->application_information)
        {
          OBJECT ARRAY compinfos := appinfo->ListElements("*", "formcomponent");
          OBJECT ARRAY handlerinfos := appinfo->ListElements("*", "formhandler");

          IF(Length(compinfos) + Length(handlerinfos) = 0)
            CONTINUE;
          IF(Length(handlerinfos) > 1 OR (Length(handlerinfos) = 1 AND Length(compinfos) >= 1))
          {
            INSERT CELL [ resourcename
                        , line := handlerinfos[END-1]->linenum
                        , col := 0
                        , message := `Multiple formcomponent/formhandler definitions in ${resourcename}:${appinfo->linenum}`
                        ] INTO errors AT END;
          }

          IF(Length(handlerinfos)=1)
          {
            RECORD handlernode := ParseHandler(element, handlerinfos[0], modulename, resourcename);
            errors := errors CONCAT handlernode.errors;
            warnings := warnings CONCAT handlernode.warnings;
            IF(Length(handlernode.errors) = 0)
            {
              IF (options.withcomments)
                INSERT CELL comment := GetAnnotiationUserDocumentation(node), componenttype := "formhandler", componentxsdnode := element.node INTO handlernode.el;

              INSERT handlernode.el INTO handlers AT END;
            }
          }

          FOREVERY(OBJECT compinfo FROM compinfos)
          {
            STRING matchattributestr := compinfo->GetAttribute("matchattribute");
            RECORD matchattribute := matchattributestr != "" ? DecodeHSON(matchattributestr) : DEFAULT RECORD;

            IF (RecordExists(SELECT FROM nodecomponents WHERE COLUMN matchattributestr = VAR matchattributestr))
            {
              INSERT CELL [ resourcename
                          , line := compinfo->linenum
                          , col := 0
                          , message := `Duplicate {${xmlns_forms}}formcomponent${ RecordExists(matchattribute) ? Substring(EncodeHSON(matchattribute), 5) : ""} in element`
                          ] INTO errors AT END;
            }

            RECORD componentnode := ParseComponent(element, compinfo, modulename, resourcename, matchattribute);
            errors := errors CONCAT componentnode.errors;
            warnings := warnings CONCAT componentnode.warnings;
            IF(Length(componentnode.errors) = 0)
            {
              IF (options.withcomments)
                INSERT CELL comment := GetAnnotiationUserDocumentation(node), componenttype := "formcomponent", componentxsdnode := element.node INTO componentnode.el;

              INSERT componentnode.el INTO components AT END;
              INSERT CELL[ ...componentnode.el, matchattributestr ] INTO nodecomponents AT END;
            }
          }
        }
    }
  }

  STRING ARRAY invalidation_masks := GetResourceEventMasks([resourcename]);
  RETURN CELL[ invalidation_masks := invalidation_masks
             , components
             , handlers
             , errors
             , warnings
             ,...commentdata
             ];
}

RECORD FUNCTION GetCacheableFormComponentsForFormDef(STRING resourcename)
{
  BLOB xsdfile := GetWebHareResource(resourcename);
  OBJECT doc := MakeXMLDocument(xsdfile);
  RECORD parseres := ParseFormDefComponents(resourcename, doc);

  RETURN [ value := CELL[ parseres.invalidation_masks
                        , parseres.components
                        , parseres.handlers
                        ]
         , eventmasks := parseres.invalidation_masks
         ];
}

RECORD FUNCTION GetFormComponentsForFormDef(STRING namespace)
{
  RETURN GetAdhocCached([ xsd := namespace ], PTR GetCacheableFormComponentsForFormDef(namespace));
}

RECORD FUNCTION GetCacheableNamespaceMap()
{
  STRING ARRAY invalidation_masks := [ "system:modulesupdate" ];

  RECORD ARRAY map;
  FOREVERY (RECORD mod FROM GetWebHareModules())
    FOREVERY (RECORD xsdrec FROM mod.formcomponents)
      INSERT CELL[ ns := xsdrec.uri, xsd := xsdrec.xsd ] INTO map AT END;

  map := SELECT * FROM map ORDER BY ns;
  RETURN [ value := map
         , eventmasks := invalidation_masks
         ];
}

RECORD ARRAY FUNCTION GetFormNamespaceResourceMapping()
{
  RETURN GetAdhocCached([ type := "mapping" ], PTR GetCacheableNamespaceMap);
}

RECORD FUNCTION GetCacheableFormComponents()
{
  // Invalidate cache upon soft reset and module update, event masks for the individual form XSD files are added as well
  STRING ARRAY invalidation_masks := [ "system:modulesupdate" ];

  RECORD ARRAY components, handlers;
  FOREVERY (RECORD xsdrec FROM GetFormNamespaceResourceMapping())
  {
    RECORD result := GetFormComponentsForFormDef(xsdrec.xsd);
    invalidation_masks := invalidation_masks CONCAT result.invalidation_masks;
    components := components CONCAT result.components;
    handlers := handlers CONCAT result.handlers;
  }

  RETURN
      [ value := CELL[ components, handlers ]
      , eventmasks := invalidation_masks
      ];
}

PUBLIC RECORD ARRAY FUNCTION GetAllFormComponents()
{
  RETURN GetAdhocCached([ getall := TRUE ], PTR GetCacheableFormComponents).components;
}
PUBLIC RECORD ARRAY FUNCTION GetAllFormHandlers()
{
  RETURN GetAdhocCached([ getall := TRUE ], PTR GetCacheableFormComponents).handlers;
}

PUBLIC RECORD ARRAY FUNCTION GetFormComp(BOOLEAN ishandler, STRING namespace, STRING type, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ nofail := FALSE ], options);

  // Infer namespace from type if not explicity specified
  IF(namespace = "")
  {
    IF(type LIKE "*#*")
    {
      namespace := Left(type, SearchSubstring(type,'#'));
      type := Substring(type, Length(namespace) + 1);
    }
    ELSE
    {
      namespace := "http://www.webhare.net/xmlns/publisher/forms";
    }
  }

  // Remove matchattribute specifier
  type := Tokenize(type, "?")[0];

  RECORD ARRAY mapping := GetFormNamespaceResourceMapping();
  INTEGER pos := RecordBinaryFind(mapping, [ ns := namespace ], ["NS"]);
  IF(pos >= 0)
  {
    RECORD defs := GetFormComponentsForFormDef(mapping[pos].xsd);
    RECORD ARRAY source := ishandler ? defs.handlers : defs.components;
    RECORD ARRAY results :=  SELECT * FROM source WHERE source.namespace = VAR namespace AND source.name = VAR type;

    IF(NOT RecordExists(results) AND options.nofail)
      THROW NEW Exception(`Unknown ${ishandler ? "handler" : "component"} '${namespace}#${type}'`);

    RETURN results;
  }

  IF(options.nofail)
    THROW NEW Exception(`No formdefinition is claiming the namespace for ${ishandler ? "handler" : "component"} '${ishandler ? "handler" : "component"} '${namespace}#${type}'`);

  RETURN RECORD[];
}

PUBLIC RECORD ARRAY FUNCTION GetFormComponentDef(STRING namespace, STRING type, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  RETURN GetFormComp(FALSE, namespace, type, options);
}

PUBLIC RECORD ARRAY FUNCTION GetFormHandlerDef(STRING namespace, STRING type, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  RETURN GetFormComp(TRUE, namespace, type, options);
}

PUBLIC RECORD FUNCTION GetFormComponentDefByNode(OBJECT compnode, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ nofail := FALSE ], options);

  RECORD ARRAY matches := GetFormComp(FALSE, compnode->namespaceuri, compnode->localname, options);
  FOREVERY(RECORD match FROM matches)
    IF (ComponentMatches(compnode, match))
      RETURN match;

  IF(options.nofail)
    THROW NEW Exception(`Unable to match specific version of '${compnode->namespaceuri}#${compnode->localname}'`);

  RETURN DEFAULT RECORD;
}

PUBLIC BOOLEAN FUNCTION ComponentMatches(OBJECT compnode, RECORD compdef)
{
  FOREVERY (RECORD attr FROM UnpackRecord(compdef.matchattribute))
  {
    IF (IsTypeIDArray(TypeID(attr.value)))
    {
      IF (compnode->GetAttribute(ToLowercase(attr.name)) NOT IN attr.value)
        RETURN FALSE;
    }
    ELSE
    {
      IF (compnode->GetAttribute(ToLowercase(attr.name)) != attr.value)
        RETURN FALSE;
    }
  }
  RETURN TRUE;
}

PUBLIC BOOLEAN FUNCTION AttributesMatch(RECORD attributes, RECORD compdef)
{
  FOREVERY (RECORD attr FROM UnpackRecord(compdef.matchattribute))
  {
    IF (IsTypeIDArray(TypeID(attr.value)))
    {
      IF (NOT CellExists(attributes, attr.name) OR GetCell(attributes, attr.name) NOT IN attr.value)
        RETURN FALSE;
    }
    ELSE
    {
      IF (NOT CellExists(attributes, attr.name) OR GetCell(attributes, attr.name) != attr.value)
        RETURN FALSE;
    }
  }
  RETURN TRUE;
}
