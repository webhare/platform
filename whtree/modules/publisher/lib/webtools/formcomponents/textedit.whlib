<?wh
/** @short Standard form components
    @topic forms/standardcomponents
*/

LOADLIB "wh::internet/mime.whlib";
LOADLIB "wh::xml/xsd.whlib";

LOADLIB "mod::publisher/lib/forms/components.whlib";
LOADLIB "mod::publisher/lib/forms/editor.whlib";
LOADLIB "mod::publisher/lib/internal/forms/support.whlib";
LOADLIB "mod::publisher/lib/internal/forms/rpc.whlib";

LOADLIB "mod::tollium/lib/gettid.whlib";
LOADLIB "mod::tollium/lib/internal/inputvalidation.whlib";

PUBLIC STATIC OBJECTTYPE TextEditFieldBase EXTEND FormFieldBase
< // ---------------------------------------------------------------------------
  //
  // Variables
  //

  /// Trim whitespace on incoming data?
  BOOLEAN trimwhitespace;

  STRING ARRAY acceptableemailmasks;

  STRING unacceptableemailerror;

  // ---------------------------------------------------------------------------
  //
  // Public properties
  //

  PUBLIC PROPERTY value(GetValue, SetValue);

  /// The literal value we're exchanging with the <input> field
  PUBLIC STRING textvalue;

  /// The value that is converted to an empty textedit
  PUBLIC STRING emptyvalue;

  /// Validation checks
  PUBLIC STRING ARRAY validationchecks;

  /// Placeholder (shown if no text entered)
  PUBLIC STRING placeholder;

  /// Type of the value. One of "boolean", "integer", "money", "float", "string"
  PUBLIC STRING valuetype;

  /// Min and/or max (for integer types)
  PUBLIC RECORD range;

  //TODO: JavaScript uses UTF16 pairs as counter? should actually use that by default or allow users to specify the length measure

  /// Minimum length in unicode codepoints
  PUBLIC INTEGER minlength;

  /// Maximum length in unicode codepoints, ignored if <= 0
  PUBLIC INTEGER maxlength;

  // ---------------------------------------------------------------------------
  //
  // Constructor
  //

  MACRO NEW(OBJECT form, OBJECT parent, RECORD field)
  : FormFieldBase(form, parent, field)
  {
    this->trimwhitespace := CellExists(field,'trimwhitespace') ? field.trimwhitespace : TRUE;
    this->valuetype := "string";
    this->validationchecks := CellExists(field,'validationchecks') ? field.validationchecks : STRING[];

    IF(CellExists(field,'placeholder')) //dynamic fields don't supply this attribute
      this->placeholder := GetTid(field.placeholder);
    IF(CellExists(field,'range'))
      this->range := field.range;
    IF(CellExists(field,'minlength'))
      this->minlength := field.minlength;
    IF(CellExists(field,'maxlength'))
      this->maxlength := field.maxlength;
    IF(CellExists(field,'acceptableemailmasks'))
      this->acceptableemailmasks := field.acceptableemailmasks;
    IF(CellExists(field,'unacceptableemailerror'))
      this->unacceptableemailerror := GetTid(field.unacceptableemailerror);
  }

  // ---------------------------------------------------------------------------
  //
  // Getter & setters
  //

  VARIANT FUNCTION GetValue()
  {
    RETURN MapValueType(this->valuetype, this->textvalue, this->emptyvalue);
  }

  MACRO SetValue(VARIANT newval)
  {
    this->textvalue := UnmapValueType(this->valuetype, newval, this->emptyvalue, TRUE);
    this->form->__SendFormMessage(this->field.name, 'value', this->textvalue);
  }

  UPDATE PUBLIC MACRO UpdateFromJS(VARIANT jsdata)
  {
    IF(this->trimwhitespace)
      jsdata := TrimWhitespace(jsdata);

    this->textvalue := jsdata;
    this->form->__SendFormMessage(this->field.name, 'value', this->textvalue);
  }

  PUBLIC RECORD FUNCTION GetEmailRestrictions()
  {
    RETURN CELL[ acceptableemailmasks := this->acceptableemailmasks
               , unacceptableemailerror := this->unacceptableemailerror
               ];
  }

  // ---------------------------------------------------------------------------
  //
  // Public API
  //

  UPDATE PUBLIC RECORD FUNCTION __GetRenderData()
  {
    RECORD base := CELL[ ...this->GetBaseRenderData()
                       , type := "textedit"
                       , value := this->textvalue
                       , validationchecks := this->validationchecks
                       , placeholder := this->placeholder
                       , range := this->range
                       , minlength := this->minlength
                       , maxlength := this->maxlength
                       ];

    RETURN base;
  }

  UPDATE PUBLIC RECORD FUNCTION GetRenderTree()
  {
    RECORD renderinfo := CELL[ ...FormFieldBase::GetRenderTree()
                             , placeholder := this->placeholder
                             , type := "text"
                             , minlength := CellExists(this->field, 'minlength') ? this->field.minlength : 0
                             , maxlength := CellExists(this->field, 'maxlength') ? this->field.maxlength : 0
                             , this->value
                             , this->textvalue
                             ];

    IF(CellExists(this->field,'password') AND this->field.password)
    {
      renderinfo.type := 'password';
    }
    ELSE IF(CellExists(this->field,'valuetype') AND this->field.valuetype = "integer")
    {
      renderinfo.type := 'number';

      IF(RecordExists(this->range) AND CellExists(this->range, 'min'))
        INSERT CELL min := this->range.min INTO renderinfo;
      ELSE
        INSERT CELL min := -2147483648 INTO renderinfo;

      IF(RecordExists(this->range) AND CellExists(this->range, 'max'))
        INSERT CELL max := this->range.max INTO renderinfo;
      ELSE
        INSERT CELL max := 2147483647 INTO renderinfo;

      INSERT CELL step := 1 INTO renderinfo;
    }
    ELSE IF(CellExists(this->field,'valuetype') AND this->field.valuetype = "money")
    {
      renderinfo.type := 'number';
    }
    ELSE IF(CellExists(this->field, 'validationchecks') AND  'email' IN this->field.validationchecks)
    {
      renderinfo.type := 'email';
    }
    ELSE IF(CellExists(this->field, 'validationchecks') AND 'url' IN this->field.validationchecks)
    {
      renderinfo.type := 'url';
    }

    RETURN renderinfo;
  }

  UPDATE PUBLIC BOOLEAN FUNCTION IsSet()
  {
    RETURN this->textvalue != "";
  }

  UPDATE PUBLIC MACRO ValidateValue(OBJECT work)
  {
    FormFieldBase::ValidateValue(work);

    IF(this->IsSet())
    {
      STRING err := ValidateInput(this->textvalue, this->title, this->validationchecks);
      IF(err != "")
      {
        work->AddErrorFor(this, err);
        RETURN;
      }

      IF(this->maxlength > 0 AND UCLength(this->textvalue) > this->maxlength)
        work->AddErrorFor(this, GetTid("publisher:site.forms.commonerrors.maxlength", ToString(this->maxlength)));
      IF(this->minlength > 0 AND UCLength(this->textvalue) < this->minlength)
        work->AddErrorFor(this, GetTid("publisher:site.forms.commonerrors.minlength", ToString(this->minlength)));

      IF(this->IsNowSettable())
      {
        IF(this->valuetype = "integer")
        {
          INTEGER64 val := ToInteger64(this->textvalue,-1);
          IF(val = -1 AND ToInteger(this->textvalue,0) = 0) //not a valid integer
          {
            //FIXME no way to trigger this.. as valuetype causes early filtering
            work->AddErrorFor(this, GetTid("publisher:site.forms.commonerrors.integer"));
          }
          ELSE
          {
            RECORD range  := CELL[ min := -2147483648, max := 2147483647, ...this->range];
            IF(val < range.min OR val > range.max)
              work->AddErrorFor(this, GetTid("publisher:site.forms.commonerrors.range", ToString(range.min), ToString(range.max)));
          }
        }

        RECORD ARRAY emailstocheck := this->GetMailsToCheck(this->textvalue);
        IF(Length(emailstocheck) > 0)
        {
          FOREVERY(RECORD tocheck FROM emailstocheck)
          {
            RECORD checkresult := RunEmailChecks(GetTidLanguage(), tocheck.email, this->GetEmailRestrictions());
            IF(RecordExists(checkresult) AND CellExists(checkresult,'blocked'))
            {
              work->AddErrorFor(this, checkresult.blocked);
              RETURN;
            }

            IF(RecordExists(checkresult) AND CellExists(checkresult,'force'))
              emailstocheck[#tocheck].email := checkresult.force;
          }
          this->textvalue := Detokenize((SELECT AS STRING ARRAY email FROM emailstocheck),", ");
        }
      }
    }
  }

  // ---------------------------------------------------------------------------
  //
  // Helper functions
  //

  RECORD ARRAY FUNCTION GetMailsToCheck(STRING sourcevalue)
  {
    RECORD ARRAY emailstocheck;
    IF("email" IN this->validationchecks OR "email-maysendfrom" IN this->validationchecks OR "email-maysendto" IN this->validationchecks)
      INSERT SplitEmailName(sourcevalue) INTO emailstocheck AT END;
    ELSE IF("emails" IN this->validationchecks OR "emails-maysendto" IN this->validationchecks)
      emailstocheck := TokenizeEmailAddressList(sourcevalue, FALSE);

    RETURN emailstocheck;
  }

  VARIANT FUNCTION GetRangeProperty(STRING name, VARIANT defaultvalue)
  {
    IF (CellExists(this->range, name))
      RETURN GetCell(this->range, name);
    RETURN defaultvalue;
  }

  MACRO SetRangeProperty(STRING name, VARIANT value)
  {
    IF (IsDefaultValue(value))
      this->range := CellDelete(this->range, name);
    ELSE IF (CellExists(this->range, name))
      this->range := CellUpdate(this->range, name, value);
    ELSE
      this->range := CellInsert(this->range, name, value);
  }
>;

PUBLIC OBJECTTYPE FormEmail EXTEND FormComponentExtensionBase
<
  MACRO OnChangeEmails()
  {
    ^unacceptableemailerror->placeholder := GetTidForLanguage(this->contexts->formcomponentapi->languagecode, "publisher:site.forms.commonerrors.unacceptableemail");
  }

  UPDATE PUBLIC MACRO PostInitExtension()
  {
    ^acceptableemailmasks->value := ToRecordArray(ParseXSList(this->node->GetAttribute("acceptableemailmasks")), "emailmask");
    ^limitacceptableemails->value := Length(^acceptableemailmasks->value) > 0;
    this->ReadTidAttribute(^unacceptableemailerror);
    this->onchangeemails();
  }

  UPDATE PUBLIC MACRO SubmitExtension(OBJECT work)
  {
    IF (NOT work->HasFailed())
    {
      IF(this->^limitacceptableemails->value)
        this->node->SetAttribute("acceptableemailmasks", Detokenize((SELECT AS STRING ARRAY emailmask FROM ^acceptableemailmasks->value ORDER BY emailmask), " "));
      ELSE
        this->node->RemoveAttribute("acceptableemailmasks");

      this->WriteTidAttribute(^unacceptableemailerror);
    }
  }
>;

PUBLIC OBJECTTYPE FormNumericTextEdit EXTEND FormComponentExtensionBase
<
  STRING valuetype;
  RECORD ARRAY emptyattributemap;

  UPDATE PUBLIC MACRO PostInitExtension()
  {
    this->valuetype := this->node->GetAttribute("valuetype");
    this->emptyattributemap := [[ name := "min", comp := ^minvalue ]
                               ,[ name := "max", comp := ^maxvalue ]
                               ,[ name := "emptyvalue", comp := ^emptyvalue ]
                               ];

    //"Older" numeric textedits (pre 4.35.2) will have their emptyvalue set to 0 but there's really no use in that. Let's just not set it
    FOREVERY(RECORD attr FROM this->emptyattributemap)
      IF (this->node->HasAttribute(attr.name))
        attr.comp->value := ParseXSInt(this->node->GetAttribute(attr.name));

    this->emptyvalue->visible := this->contexts->formcomponentapi->showadvanced;
  }

  UPDATE PUBLIC MACRO SubmitExtension(OBJECT work)
  {
    IF (NOT work->HasFailed())
    {
      IF (this->valuetype IN [ "integer" ])
        this->node->SetAttribute("valuetype", this->valuetype);
      ELSE
        this->node->RemoveAttribute("valuetype");

      FOREVERY(RECORD attr FROM this->emptyattributemap)
        IF(attr.comp->value != attr.comp->emptyvalue)
          this->node->SetAttribute(attr.name, ToString(attr.comp->value));
        ELSE
          this->node->RemoveAttribute(attr.name);
    }
  }
>;

PUBLIC OBJECTTYPE FormTextEdit EXTEND FormComponentExtensionBase
<
  UPDATE PUBLIC MACRO PostInitExtension()
  {
    this->maxlength->value := ToInteger(this->node->GetAttribute("maxlength"), 0);
    IF (this->maxlength->value < 0)
      this->maxlength->value := 0;
    this->usemaxlength->value := this->maxlength->value > 0;

    this->validationchecks->value := this->node->GetAttribute("validationchecks");
  }

  UPDATE PUBLIC MACRO SubmitExtension(OBJECT work)
  {
    IF (NOT work->HasFailed())
    {
      this->node->RemoveAttribute("valuetype");

      IF (this->usemaxlength->value AND this->maxlength->value > 0)
        this->node->SetAttribute("maxlength", ToString(this->maxlength->value));
      ELSE
        this->node->RemoveAttribute("maxlength");
      IF (this->validationchecks->value != "")
        this->node->SetAttribute("validationchecks", this->validationchecks->value);
      ELSE
        this->node->RemoveAttribute("validationchecks");
    }
  }
>;

PUBLIC RECORD FUNCTION ParseFormEmail(RECORD fielddef, OBJECT node, RECORD parsecontext)
{
  fielddef := CELL[ ...ParseFormTextedit(fielddef, node, parsecontext)
                  , acceptableemailmasks := ParseXSList(node->GetAttribute("acceptableemailmasks"))
                  , unacceptableemailerror := parsecontext.parsexmltidptr(node, "unacceptableemailerror")
                  ];
  RETURN fielddef;
}

PUBLIC RECORD FUNCTION ParseFormTextedit(RECORD fielddef, OBJECT node, RECORD parsecontext)
{
  RECORD range;
  INSERT CELL password := FALSE
            , valuetype := node->GetAttribute("valuetype") ?? "string"
            , minlength := ToInteger(node->GetAttribute("minlength"),0)
            , maxlength := ToInteger(node->GetAttribute("maxlength"),0)
         INTO fielddef;

  INSERT CELL value := parsecontext.parsexmltidptr(node, "value") INTO fielddef;
  INSERT CELL emptyvalue := parsecontext.parsexmltidptr(node, "emptyvalue") INTO fielddef;
  INSERT CELL trimwhitespace := node->HasAttribute("trimwhitespace") ? ParseXSBoolean(node->GetAttribute("trimwhitespace")) : TRUE INTO fielddef;

  IF(node->HasAttribute("validationchecks"))
  {
    INSERT CELL validationchecks := ParseXSList(node->GetAttribute("validationchecks")) INTO fielddef;
    INSERT CELL checks := fielddef.validationchecks INTO fielddef;
  }
  ELSE
  {
    INSERT CELL validationchecks := DEFAULT STRING ARRAY INTO fielddef;
  }

  IF(node->HasAttribute("min"))
  {
    IF(fielddef.valuetype != "integer")
      THROW NEW Exception("min= only supported for textedits of valuetype integer");
    INSERT CELL min := ToInteger(node->GetAttribute("min"),0) INTO range;
  }
  IF(node->HasAttribute("max"))
  {
    IF(fielddef.valuetype != "integer")
      THROW NEW Exception("max= only supported for textedits of valuetype integer");
    INSERT CELL max := ToInteger(node->GetAttribute("max"),0) INTO range;
  }

  IF(node->HasAttribute("decimals"))
    INSERT CELL decimals := ParseXSInt(node->GetAttribute("decimals")) INTO fielddef;

  INSERT CELL placeholder := parsecontext.parsexmltidptr(node, "placeholder")
            , prefix := parsecontext.parsexmltidptr(node, "prefix")
            , suffix := parsecontext.parsexmltidptr(node, "suffix")
            , range := range
            INTO fielddef;
  RETURN fielddef;
}

PUBLIC STRING ARRAY FUNCTION GetFormTextEditSupportedValues(RECORD field)
{
  IF(CellExists(field,'validationchecks')) //These may not have been set if we came through AppendFormField and no match option was set
  {
    IF ("email" IN field.validationchecks)
      RETURN [ "email" ];
    IF ("emails" IN field.validationchecks)
      RETURN [ "emailarray" ];
  }

  IF(CellExists(field,'valuetype')) //These may not have been set if we came through AppendFormField and no match option was set
    RETURN STRING[ field.valuetype ];

  RETURN ["string"];
}

PUBLIC STATIC OBJECTTYPE TextEditField EXTEND TextEditFieldBase
<
  // ---------------------------------------------------------------------------
  //
  // Constructor
  //

  MACRO NEW(OBJECT form, OBJECT parent, RECORD field)
  : TextEditFieldBase(form, parent, field)
  {
    IF(field.isstatic)
    {
      this->valuetype := field.valuetype;
      this->emptyvalue := GetTid(field.emptyvalue);
      this->UpdateFromJS(GetTid(field.value));
    }
  }
>;

PUBLIC STATIC OBJECTTYPE NumericTextEditField EXTEND TextEditFieldBase
< // ---------------------------------------------------------------------------
  //
  // Public properties
  //

  PUBLIC PROPERTY min(GetMin, SetMin);
  PUBLIC PROPERTY max(GetMax, SetMax);


  // ---------------------------------------------------------------------------
  //
  // Constructor
  //

  MACRO NEW(OBJECT form, OBJECT parent, RECORD field)
  : TextEditFieldBase(form, parent, field)
  {
    IF(field.isstatic)
    {
      this->valuetype := field.valuetype;
      this->emptyvalue := GetTid(field.emptyvalue);
      this->UpdateFromJS(GetTid(field.value));
    }
    ELSE IF(CellExists(field,'valuetype')) //dynamic creation
    {
      this->valuetype := field.valuetype;
    }
  }


  // ---------------------------------------------------------------------------
  //
  // Public API
  //

  UPDATE PUBLIC RECORD FUNCTION __GetRenderData()
  {
    RECORD base := CELL[ ...TextEditFieldBase::__GetRenderData()
                       , min := this->min
                       , max := this->max
                       ];
    RETURN base;
  }


  // ---------------------------------------------------------------------------
  //
  // Property getters/setters
  //

  INTEGER FUNCTION GetMin()
  {
    RETURN this->GetRangeProperty("min", 0);
  }

  MACRO SetMin(INTEGER min)
  {
    this->SetRangeProperty("min", min);
  }

  INTEGER FUNCTION GetMax()
  {
    RETURN this->GetRangeProperty("max", 0);
  }

  MACRO SetMax(INTEGER max)
  {
    this->SetRangeProperty("max", max);
  }
>;
