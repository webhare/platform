<?wh

LOADLIB "wh::files.whlib";
LOADLIB "wh::filetypes/markdown.whlib";

LOADLIB "mod::publisher/lib/siteprofiles.whlib";
LOADLIB "mod::publisher/lib/search/richdocuments.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/whfs.whlib";


OBJECT richdocumentfile_type;
OBJECT markdownfile_type;

/** @short An object that provides search information for publisher search
*/
PUBLIC STATIC OBJECTTYPE SearchProviderBase
<
  /** @short Get the file contents of an object as indexable content
      @long If the filetype contains an array index field, this function can return a wrapped blob record for each of the
          array elements.
      @param objid The id of the object
      @return The contents of the file as a list of wrapped blobs
  */
  UPDATE PUBLIC RECORD ARRAY FUNCTION GetSearchContents(INTEGER objid)
  {
    RECORD obj := SELECT name, data FROM system.fs_objects WHERE id = objid;
    IF(RecordExists(obj))
      RETURN [ WrapBlob(obj.data, obj.name) ];
    ELSE
      RETURN DEFAULT RECORD ARRAY;
  }
>;

PUBLIC STATIC OBJECTTYPE RichDocumentSearchProvider EXTEND SearchProviderBase
<
  UPDATE PUBLIC RECORD ARRAY FUNCTION GetSearchContents(INTEGER objid)
  {
    IF (NOT ObjectExists(richdocumentfile_type))
      richdocumentfile_type := OpenWHFSType("http://www.webhare.net/xmlns/publisher/richdocumentfile");

    RECORD data := richdocumentfile_type->GetInstanceData(objid);
    IF (NOT RecordExists(data) OR NOT RecordExists(data.data))
      RETURN DEFAULT RECORD ARRAY;

    OBJECT applytester := GetApplyTesterForObject(objid);
    RECORD content := GetRichDocumentIndexableContent(data.data, CELL[applytester]);
    IF(NOT RecordExists(content))
      RETURN DEFAULT RECORD ARRAY;

    RETURN [ WrapBlob(content.htmltext, "richdocumentfile.html") ];
  }
>;

PUBLIC STATIC OBJECTTYPE MarkdownSearchProvider EXTEND SearchProviderBase
<
  UPDATE PUBLIC RECORD ARRAY FUNCTION GetSearchContents(INTEGER objid)
  {
    IF (NOT ObjectExists(markdownfile_type))
      markdownfile_type := OpenWHFSType("http://www.webhare.net/xmlns/publisher/markdownfile");

    RECORD data := markdownfile_type->GetInstanceData(objid);
    IF (NOT RecordExists(data) OR NOT RecordExists(data.data))
      RETURN DEFAULT RECORD ARRAY;

    STRING text := RenderMarkdownToHTML(ParseMarkDown(BlobToString(data.data.text)));
    RETURN [ WrapBlob(StringToBlob(text), "markdown.html") ];
  }
>;
