<?wh

LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

/** looks up a second level named node by qualified name */
PUBLIC OBJECT FUNCTION LookupModuleDefNamedNode(STRING topnodename, STRING subnodename, STRING qualifiedname, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(CELL[ title := `<${subnodename}>`
                                 , nameattribute := "name"
                                 ], options);

  STRING ARRAY toks := Tokenize(qualifiedname, ':');
  IF(Length(toks) != 2)
    THROW NEW Exception(`Invalid ${options.title} name '${qualifiedname}'`);

  OBJECT moduledef := GetModuleDefinitionXML(toks[0]);
  OBJECT topnode := PickFirst(moduledef->documentelement->ListChildren(whconstant_xmlns_moduledef, topnodename));
  IF(ObjectExists(topnode))
    FOREVERY(OBJECT subnode FROM topnode->ListChildren(whconstant_xmlns_moduledef, subnodename))
      IF(subnode->GetAttribute(options.nameattribute) = toks[1])
        RETURN subnode;

  THROW NEW Exception(`No such ${options.title} '${toks[1]}' in module '${toks[0]}'`);
}


PUBLIC RECORD FUNCTION GetSearchContentProvider(STRING name)
{
  OBJECT node := LookupModuleDefNamedNode("publisher", "searchcontentprovider", name, [ title := "searchcontentprovider"]);
  RETURN [ objectname := MakeAbsoluteResourcePath(`mod::${Tokenize(name,':')[0]}/`, node->GetAttribute("objectname"))
         , version := node->GetAttribute("version") ?? "unspecified"
         ];
}

