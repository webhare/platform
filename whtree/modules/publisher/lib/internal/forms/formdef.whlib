<?wh

LOADLIB "wh::crypto.whlib";
LOADLIB "wh::xml/dom.whlib";
LOADLIB "wh::xml/xsd.whlib";

LOADLIB "mod::system/lib/whfs.whlib";

LOADLIB "mod::publisher/lib/internal/forms/conditions.whlib";
LOADLIB "mod::publisher/lib/webtools/internal/formcomponents.whlib";
LOADLIB "mod::publisher/lib/webtools/internal/formhandlers.whlib";
LOADLIB "mod::publisher/lib/internal/forms/opener.whlib";
LOADLIB "mod::publisher/lib/internal/forms/parser.whlib";
LOADLIB "mod::publisher/lib/internal/rtd/support.whlib";

LOADLIB "mod::tollium/lib/gettid.whlib";


STRING ARRAY pageorder := [ "", "overview", "thankyou" ];

INTEGER FUNCTION GetPageNumber(OBJECT page)
{
  INTEGER pagenum := 1;
  FOR(page := page->previouselementsibling; ObjectExists(page); page := page->previouselementsibling)
    IF(page->localname = "page")
      pagenum := pagenum + 1;
  RETURN pagenum;
}

MACRO UpdateAttribute(OBJECT child, RECORD attr, STRING newvalue)
{
  IF (attr.localname = "")
   child->SetAttribute(attr.nodename, newvalue);
  ELSE
   child->SetAttributeNS(attr.namespaceuri, attr.nodename, newvalue);
}

STATIC OBJECTTYPE Duplicator
<
  STRING type;
  OBJECT ARRAY comps;
  OBJECT ARRAY duplicates;
  OBJECT formdefinition;
  STRING ARRAY currentinstanceids;
  RECORD ARRAY guidmapping;

  MACRO NEW(OBJECT formdefinition, STRING type, OBJECT ARRAY comps)
  {
    this->formdefinition := formdefinition;
    this->type := type;
    this->comps := comps;

    this->currentinstanceids := this->formdefinition->formdefinitions->__GetInstanceids();
  }


  /** Duplicate the specified component */
  OBJECT FUNCTION DuplicateComponent(OBJECT source)
  {
    //verify it's actually a component TODO: allow components to specify after-clone callbacks?
    //RECORD comptype := GetFormComponentDef(source->namespaceuri, source->element, [ nofail := TRUE ]);

    OBJECT newquestion := source->CloneNode(TRUE); //deep copy
    source->parentnode->InsertBefore(newquestion, source->nextelementsibling); //insert behind source

    FOREVERY(RECORD comp FROM GatherFormComponentsFromComponent(newquestion, [ getallchildren := TRUE ]))
    {
      IF(comp.type="component")
      {
        STRING oldguid := comp.node->GetAttribute("guid");
        IF(oldguid = "" OR oldguid NOT LIKE "?*:?*")
          THROW NEW Exception("Component unexpectedly without guid");

        STRING newguid := this->formdefinition->formdefinitions->__CreateGUID(Tokenize(oldguid,':')[0]);
        comp.node->SetAttribute("guid", newguid);
        INSERT CELL[ oldguid, newguid ] INTO this->guidmapping AT END;
        comp.node->RemoveAttribute("name");
        comp.node->RemoveAttribute("pins");
      }

      this->duplicates := OBJECT[...this->duplicates, comp.node, ...comp.allchildren];
    }

    RETURN newquestion;
  }

  MACRO DuplicateInstances(OBJECT child)
  {
    FOREVERY(RECORD attr FROM child->ListAttributes())
    {
      IF(attr.nodevalue IN this->currentinstanceids)
      {
        RECORD instance := this->formdefinition->formdefinitions->GetInstance(attr.nodevalue);
        IF(attr.nodename IN ["visibleconditionid", "enabledconditionid", "requiredconditionid"])
          instance.data := this->FixReferences(instance.data);

        STRING newinstanceid := this->formdefinition->formdefinitions->CreateInstance(instance);
        UpdateAttribute(child, attr, newinstanceid);
      }
    }
  }

  RECORD FUNCTION FixReferences(RECORD condition)
  {
    IF(CellExists(condition, "condition")) //not
      condition.condition:= this->FixReferences(condition.condition);

    IF(CellExists(condition, "conditions")) //and & or
      FOREVERY(RECORD sub FROM condition.conditions)
        condition.conditions[#sub] := this->FixReferences(sub);

    IF(CellExists(condition, "field"))
    {
      STRING remapped := SELECT AS STRING newguid FROM this->guidmapping WHERE oldguid = condition.field;
      IF(remapped != "")
        condition.field := remapped;
    }

    RETURN condition;
  }

  /** Duplicate the specified components (or pages/groups) */
  PUBLIC OBJECT ARRAY FUNCTION Run()
  {
    OBJECT ARRAY newitems;
    OBJECT insertbefore := this->comps[END-1]->nextelementsibling; //it's okay if this is a DEFAULT OBJECT
    OBJECT insertparent := this->comps[END-1]->parentnode;

    FOREVERY(OBJECT todupe FROM this->comps)
    {
      OBJECT dupe := this->DuplicateComponent(todupe);
      INSERT dupe INTO newitems AT END;
      insertparent->insertbefore(dupe, insertbefore);
    }

    FOREVERY(OBJECT duped FROM this->duplicates)
      this->DuplicateInstances(duped);

    RETURN newitems;
  }
>;

STATIC OBJECTTYPE FormPage
<
  OBJECT __formdefinition;
  OBJECT pvt_node;

  PUBLIC PROPERTY node(this->pvt_node, -);
  PUBLIC PROPERTY guid(GetGUID, -);
  PUBLIC PROPERTY role(GetRole, -);
  PUBLIC PROPERTY redirect(GetRedirect, -);
  PUBLIC PROPERTY redirectdelay(GetRedirectDelay, -);
  PUBLIC PROPERTY visiblecondition(GetVisibleCondition, SetVisibleCondition);
  PUBLIC PROPERTY formdefinition(__formdefinition, -);

  MACRO NEW(OBJECT formdefinition, OBJECT page)
  {
    this->__formdefinition := formdefinition;
    this->pvt_node := page;
  }

  PUBLIC OBJECT FUNCTION AppendComponent(STRING element)
  {
    RECORD comptype := GetFormComponentDef("", element, [ nofail := TRUE ]);
    OBJECT newnode := this->node->ownerdocument->CreateElementNS(comptype.namespace, comptype.name);
    newnode->SetAttribute("guid", this->formdefinition->formdefinitions->__CreateGUID("formcomp") );
    this->node->AppendChild(newnode);
    RETURN newnode;
  }

  PUBLIC OBJECT FUNCTION LookupComponent(STRING name)
  {
    OBJECT match := this->node->GetEvaluatedElement(DEFAULT OBJECT, `//*[@name="${EncodeJava(name)}"]|//*[@guid="${EncodeJava(name)}"]`);
    //TODO properly verify we've hit a component
    RETURN match;
  }

  /** Duplicate the specified component */
  PUBLIC OBJECT FUNCTION DuplicateComponent(OBJECT source)
  {
    RETURN PickFirst(this->formdefinition->PrepareDuplicate([source]).duplicator->Run());
  }

  PUBLIC OBJECT ARRAY FUNCTION GetComponents(OBJECT questiongroup DEFAULTSTO DEFAULT OBJECT)
  {
    OBJECT node := this->node;
    IF (ObjectExists(questiongroup))
    {
      IF (NOT questiongroup->parentnode->IsSameNode(this->node))
        THROW NEW Exception("Question group not on this page");
      node := questiongroup;
    }
    RETURN
        SELECT AS OBJECT ARRAY nodes.node
          FROM ToRecordArray(node->childnodes->GetCurrentElements(), "node") AS nodes
         WHERE (nodes.node->namespaceuri = xmlns_forms AND nodes.node->localname = "group")
               OR RecordExists(GetFormComponentDef(nodes.node->namespaceuri, nodes.node->localname));
  }

  PUBLIC MACRO DeleteComponent(OBJECT component)
  {
    IF (NOT component->parentnode->IsSameNode(this->node))
      THROW NEW Exception("Component not on this page");
    this->formdefinition->DeleteComponent(component);
  }

  STRING FUNCTION GetGUID()
  {
    RETURN this->node->GetAttribute("guid");
  }

  STRING FUNCTION GetRole()
  {
    RETURN this->node->GetAttribute("role");
  }

  STRING FUNCTION GetRedirect()
  {
    RETURN this->formdefinition->formdefinitions->ResolveLinkRefToUrl(this->node->GetAttribute("redirect"));
  }

  INTEGER FUNCTION GetRedirectDelay()
  {
    RETURN ParseXSInt(this->node->GetAttribute("delay"));
  }

  RECORD FUNCTION GetVisibleCondition()
  {
    RETURN this->formdefinition->formdefinitions->GetConditionAttribute(this->node, "visibleconditionid");
  }
  MACRO SetVisibleCondition(RECORD newcondition)
  {
    this->formdefinition->formdefinitions->SetConditionAttribute(this->node, "visibleconditionid", newcondition);
  }
>;

STATIC OBJECTTYPE FormDefinition
<
  OBJECT pvt_formdefinitions;
  OBJECT pvt_node;
  OBJECT pvt_results;

  PUBLIC PROPERTY node(pvt_node, -);
  PUBLIC PROPERTY name(GetName, -);
  PUBLIC PROPERTY formdefinitions(pvt_formdefinitions, -);
//  PUBLIC PROPERTY formresults(GetFormResults, -);
  PUBLIC PROPERTY __resourcename(GetResourceName, -);

  MACRO NEW(OBJECT formdefinitions, OBJECT node)
  {
    this->pvt_formdefinitions := formdefinitions;
    this->pvt_node := node;
  }

  OBJECT ARRAY FUNCTION GetPageNodes()
  {
    OBJECT ARRAY pagenodes := this->node->GetChildElementsByTagNameNS(xmlns_forms, "page")->GetCurrentElements();
    RETURN Length(pagenodes) > 0 ? pagenodes : OBJECT[this->node];
  }

  PUBLIC RECORD ARRAY FUNCTION ListPages()
  {
    RETURN SELECT pagenum := #nodes
                , role := node->GetAttribute("role")
                , guid := node->GetAttribute("guid")
             FROM ToRecordArray(this->GetPageNodes(), "node") AS nodes;
  }

  PUBLIC INTEGER FUNCTION GetNumPages()
  {
    RETURN Length(this->GetPageNodes());
  }

  PUBLIC OBJECT FUNCTION GetPage(INTEGER pagenum)
  {
    RETURN NEW FormPage(this, this->GetPageNodes()[pagenum]);
  }
  PUBLIC OBJECT FUNCTION AddPage(STRING role)
  {
    IF(role NOT IN pageorder)
      THROW NEW Exception(`Invalid role '${role}'`);

    OBJECT newnode := this->node->ownerdocument->CreateElementNS(xmlns_forms, "page");
    newnode->SetAttribute("guid", this->formdefinitions->__CreateGUID("formpage"));
    IF(role!="")
      newnode->SetAttribute("role", role);

    OBJECT refnode;
    FOREVERY (RECORD page FROM this->ListPages())
    {
      IF (role != "" AND page.role = role)
        THROW NEW Exception(`A page with role '${role}' already exists`); //we don't know how to handle that yet

      IF (SearchElement(pageorder, role) < SearchElement(pageorder, page.role))
      {
        refnode := this->GetPage(page.pagenum)->node;
        BREAK;
      }
    }
    this->node->InsertBefore(newnode, refnode);

    RETURN NEW FormPage(this, newnode);
  }

  PUBLIC MACRO DeletePage(OBJECT pagenode)
  {
    IF (NOT ObjectExists(pagenode) OR pagenode->IsSameNode(this->node) OR pagenode->localname != "page" OR pagenode->GetAttribute("role") = "thankyou")
      THROW NEW Exception("Cannot delete this page");

    OBJECT page :=NEW FormPage(this, pagenode);

    FOREVERY (OBJECT comp FROM page->GetComponents())
    {
      IF (comp->namespaceuri = xmlns_forms AND comp->localname = "group")
        this->DeleteGroup(comp);
      ELSE
        this->DeleteComponent(comp);
    }
    pagenode->Dispose();
  }


  PUBLIC MACRO DeleteGroup(OBJECT groupnode)
  {
    // Move all group questions to the trash, delete the group node itself
    FOREVERY (OBJECT comp FROM groupnode->childnodes->GetCurrentElements())
      this->DeleteComponent(comp);
    groupnode->Dispose();
  }

  PUBLIC MACRO DeleteComponent(OBJECT component)
  {
    component->Dispose();
    this->GetTrashNode()->AppendChild(component);
  }

  STRING FUNCTION GetOutlineLevel(OBJECT node, INTEGER indent)
  {
    STRING guid := node->GetAttribute("guid");
    STRING name := node->GetAttribute("name");
    STRING title := node->GetAttribute("title");
    STRING output := RepeatText(" ", indent) || `- ${node->localname}${title != "" ? ` ${title}` : ""}${guid != "" ? ` (guid=${guid})` : ""}${name != "" ? ` (name=${name})` : ""}\n`;
    FOREVERY(OBJECT child FROM node->ListChildren("*","*"))
      output := output || this->GetOutlineLevel(child, indent + 2);

    RETURN output;
  }

  PUBLIC STRING FUNCTION GetOutline()
  {
    RETURN this->GetOutlineLevel(this->node, 0);
  }

  /** Prepare duplication */
  PUBLIC RECORD FUNCTION PrepareDuplicate(OBJECT ARRAY sources)
  {
    STRING ARRAY todupeguids;
    //Gather guids to dupe. ALl form editor items should have a guid
    FOREVERY(OBJECT source FROM sources)
    {
      STRING guid := source->GetAttribute("guid");
      IF(guid != "")
        INSERT guid INTO todupeguids AT END;
    }

    //Gather the components to duplicate in form order. Group by type
    OBJECT ARRAY dupepages, dupegroups, dupecomps;
    FOREVERY(OBJECT page FROM this->GetPageNodes())
    {
      IF(page->GetAttribute("guid") IN todupeguids)
        INSERT page INTO dupepages AT END;

      FOREVERY(OBJECT subitem FROM page->ListChildren("*","*"))
      {
        IF(subitem->namespaceuri = xmlns_forms AND subitem->localname = "group")
        {
          IF(subitem->GetAttribute("guid") IN todupeguids)
            INSERT subitem INTO dupegroups AT END;

          FOREVERY(OBJECT groupeditem FROM subitem->ListChildren("*","*"))
            IF(groupeditem->GetAttribute("guid") IN todupeguids)
              INSERT groupeditem INTO dupecomps AT END;
        }
        ELSE IF(subitem->GetAttribute("guid") IN todupeguids)
          INSERT subitem INTO dupecomps AT END;
      }
    }

    //How many types to dupe
    INTEGER dupetypes;
    IF(Length(dupepages) > 0)
      dupetypes := dupetypes + 1;
    IF(Length(dupegroups) > 0)
      dupetypes := dupetypes + 1;
    IF(Length(dupecomps) > 0)
      dupetypes := dupetypes + 1;

    IF(dupetypes != 1) //mixed types
      RETURN CELL [ error := "mixedtypes" ];

    IF(Length(dupepages) > 0)
      RETURN CELL [ error := "", duplicator := NEW Duplicator(this, "pages", dupepages) ];
    IF(Length(dupegroups) > 0)
      RETURN CELL [ error := "", duplicator := NEW Duplicator(this, "groups", dupegroups) ];

    RETURN CELL [ error := "", duplicator := NEW Duplicator(this, "componentss", dupecomps) ];
  }

  STRING FUNCTION GetName()
  {
    RETURN this->node->GetAttribute("name");
  }

  OBJECT FUNCTION GetTrashNode()
  {
    OBJECT trashnode := this->node->lastchild;
    IF (NOT ObjectExists(trashnode)
        OR NOT (trashnode->namespaceuri = xmlns_forms
            AND trashnode->localname = "trash"))
    {
      trashnode := this->node->ownerdocument->CreateElementNS(xmlns_forms, "trash");
      this->node->AppendChild(trashnode);
    }
    RETURN trashnode;
  }

  PUBLIC OBJECT ARRAY FUNCTION GetTrashedComponents()
  {
    OBJECT trash := this->GetTrashNode();
    IF(NOT ObjectExists(trash))
      RETURN OBJECT[];

    RETURN
        SELECT AS OBJECT ARRAY node
          FROM ToRecordArray(trash->childnodes->GetCurrentElements(), "node") AS nodes
         WHERE RecordExists(GetFormComponentDef(nodes.node->namespaceuri, nodes.node->localname));
  }

  OBJECT FUNCTION GetHandlersNode(BOOLEAN create)
  {
    OBJECT handlersnode := this->node->firstchild;
    IF (NOT ObjectExists(handlersnode)
        OR NOT (handlersnode->namespaceuri = xmlns_forms
            AND handlersnode->localname = "handlers"))
    {
      IF (NOT create)
        RETURN DEFAULT OBJECT;
      handlersnode := this->node->ownerdocument->CreateElementNS(xmlns_forms, "handlers");
      this->node->InsertBefore(handlersnode, this->node->firstchild);
    }

    RETURN handlersnode;
  }

  PUBLIC OBJECT ARRAY FUNCTION GetHandlers()
  {
    OBJECT handlersnode := this->GetHandlersNode(FALSE);
    IF (NOT ObjectExists(handlersnode))
      RETURN DEFAULT OBJECT ARRAY;

    OBJECT ARRAY handlers := SELECT AS OBJECT ARRAY nodes.node
          FROM ToRecordArray(handlersnode->childnodes->GetCurrentElements(), "node") AS nodes
         WHERE RecordExists(GetFormHandlerDef(nodes.node->namespaceuri, nodes.node->localname));

    RETURN handlers;
  }

  PUBLIC RECORD ARRAY FUNCTION GetParsedHandlers()
  {
    RETURN ParseFormHandlers(this->node, "", "", this->formdefinitions, FALSE); //ADDME missing parameters... but who needed 'module' anyway?
  }

  PUBLIC OBJECT FUNCTION AddHandler(STRING handler)
  {
    RECORD handlertype := GetFormHandlerDef("", handler, [ nofail := TRUE ]);
    OBJECT handlersnode := this->GetHandlersNode(TRUE);

    //ADDME: Add prefix if ns != xmlns_forms
    OBJECT newnode := this->node->ownerdocument->CreateElementNS(handlertype.namespace, handlertype.name);
    newnode->SetAttribute("guid", this->formdefinitions->__CreateGUID("formhandler"));
    handlersnode->AppendChild(newnode);
    RETURN newnode;
  }

  PUBLIC MACRO DeleteHandler(OBJECT component)
  {
    IF (NOT component->parentnode->IsSameNode(this->GetHandlersNode(FALSE)))
      THROW NEW Exception("Component not on this page");
    component->Dispose();
  }

  STRING FUNCTION GetResourceName()
  {
    RETURN this->formdefinitions->__resourcename ?? ObjectExists(this->formdefinitions->whfsobject) ? this->formdefinitions->whfsobject->GetResourceName() : "";
  }

  PUBLIC OBJECT FUNCTION __OpenAsLiveForm(STRING languagecode, OBJECT applytester)
  {
    RECORD formdef := ParseFormDef(this->node, this->name, this->__resourcename, "", this->formdefinitions,
                                   [ webtoolformid := ObjectExists(this->formdefinitions->whfsobject) ? this->formdefinitions->whfsobject->id : 0 ]);
    OBJECT form := OpenFormByDef(formdef, CELL[ nocustomformbase := TRUE, languagecode, applytester, prepareforfrontend := FALSE ]);
    RETURN form;
  }

  PUBLIC RECORD ARRAY FUNCTION ListFields(RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    options := ValidateOptions([ languagecode := ""
                               , applytester := DEFAULT OBJECT
                               ], options);
    OBJECT actualform := this->__OpenAsLiveForm(options.languagecode, options.applytester);
    RETURN actualform->ListFields();
  }

  PUBLIC RECORD FUNCTION DescribeField(STRING guid, RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    CONSTANT STRING pathseparator := " \u2013 "; // EN DASH ( – )

    options := ValidateOptions(
        [ field_fallback_title := TRUE // Fall back to field tag (name) or guid if title is empty
        , languagecode := "en"
        , __listfields := RECORD[] /* quick hack to pass the known ListFields result.
                                      we probably need better thought through datastorage if we're starting to build workarounds like this...
                                      but this hack alone brought a form with 4 fields from 11sec to render the 'edit handler' dialog to <2...
                                      */
        ], options);

    OBJECT field := this->node->GetElement(`*[guid="${guid}"]`);
    IF(NOT ObjectExists(field))
      RETURN DEFAULT RECORD;

    //TODO DescribeField is repeatedly invoked by GetConditionSources (for every field) so now we're triggering a form reparse for every field.. O^2!
    RECORD ARRAY listedfields := options.__listfields ?? this->ListFields();
    STRING fieldtitle := DecodeHTML(SELECT AS STRING GetHTMLTidForLanguage(options.languagecode, title) FROM listedfields WHERE COLUMN guid = VAR guid);
    IF(fieldtitle != "" AND field->parentnode->localname = "option") //handle <select> <option> <textedit> when deriving from a title. our title is generally not unique enough (eg ", Other: ")
    {
      OBJECT selectfield := field->parentnode->parentnode;
      IF(selectfield->HasAttribute("guid"))
      {
        //recursive invocation to describe the parent, but make sure it won't reselect ListFields
        RECORD descr := this->DescribeField(selectfield->GetAttribute("guid"), CELL[ ...options, __listfields := listedfields ]);
        descr.fieldtitle := descr.fieldtitle || pathseparator || fieldtitle;
        descr.pathtitle := descr.pathtitle || pathseparator || fieldtitle;
        RETURN descr;
      }
    }
    IF (options.field_fallback_title AND fieldtitle = "")
      fieldtitle := field->GetAttribute("name") ?? field->GetAttribute("guid");

    STRING grouptitle;
    OBJECT page := field->parentnode;
    IF (page->nodename = "group")
    {
      grouptitle := page->GetAttribute("title") ?? GetTidForLanguage(options.languagecode, page->GetAttribute("tid"));
      page := page->parentnode;
    }

    //ADDME: Use page component definition instead of hard-coding tid's
    STRING pagerole := page->GetAttribute("role");
    STRING pagetitle := pagerole != ""
        ? GetTid("publisher:formcomponents.page.name-" || pagerole)
        : GetTid("publisher:formcomponents.page.name", ToString(GetPageNumber(page)));
    STRING giventitle := page->GetAttribute("pagetitle") ?? GetTidForLanguage(options.languagecode, page->GetAttribute("pagetid"));
    IF (giventitle != "")
      pagetitle := pagetitle || ": " || giventitle;

    STRING ARRAY titles;
    IF (pagetitle != "")
      INSERT pagetitle INTO titles AT END;
    IF (grouptitle != "")
      INSERT grouptitle INTO titles AT END;
    IF (fieldtitle != "")
      INSERT fieldtitle INTO titles AT END;

    RETURN
        CELL[ pagetitle
            , grouptitle
            , fieldtitle
            , pathtitle := Detokenize(titles, pathseparator)
            ];
  }
>;

PUBLIC STATIC OBJECTTYPE FormDefinitionsFile
<
  OBJECT formdoc;
  OBJECT pvt_whfsobject;
  STRING pvt_resourcename;

  RECORD ARRAY instances;
  RECORD ARRAY links;

  PUBLIC PROPERTY whfsobject(pvt_whfsobject, -);
  PUBLIC PROPERTY __resourcename(pvt_resourcename, -); // formdefinition file for non-webtool forms

  MACRO NEW()
  {
  }

  PUBLIC PROPERTY documentelement(GetDocumentElement, -);

  OBJECT FUNCTION GetFormDoc()
  {
    IF(NOT ObjectExists(this->formdoc))
      this->formdoc := NEW XMLDomImplementation->CreateDocument(xmlns_forms, "formdefinitions", DEFAULT OBJECT);
    this->RemoveEmptyTextNodes(this->formdoc->documentelement);
    RETURN this->formdoc;
  }

  MACRO RemoveEmptyTextNodes(OBJECT node)
  {
    FOREVERY (OBJECT subnode FROM node->childnodes->GetCurrentNodes())
    {
      IF (subnode->nodetype = subnode->TEXT_NODE AND TrimWhitespace(subnode->nodevalue) = "")
        subnode->Dispose();
      ELSE IF (subnode->nodetype = subnode->ELEMENT_NODE)
        this->RemoveEmptyTextNodes(subnode);
    }
  }

  OBJECT FUNCTION GetDocumentElement()
  {
    RETURN this->GetFormDoc()->documentelement;
  }

  PUBLIC STRING ARRAY FUNCTION GetFormDefinitions()
  {
    STRING ARRAY names;
    FOREVERY (OBJECT formnode FROM this->GetFormDoc()->documentelement->GetChildElementsByTagNameNS(xmlns_forms,"form")->GetCurrentElements())
      INSERT formnode->GetAttribute("name") INTO names AT END;
    RETURN names;
  }

  PUBLIC OBJECT FUNCTION OpenFormDefinition(STRING name)
  {
    FOREVERY (OBJECT formnode FROM this->GetFormDoc()->documentelement->GetChildElementsByTagNameNS(xmlns_forms,"form")->GetCurrentElements())
      IF(formnode->GetAttribute("name") = name)
        RETURN NEW FormDefinition(this, formnode);

    RETURN DEFAULT OBJECT;
  }

  PUBLIC OBJECT FUNCTION CreateFormDefinition(STRING name)
  {
    IF(ObjectExists(this->OpenFormDefinition(name)))
      THROW NEW Exception(`A form named '${name}' already exists'`);

    OBJECT formnode := this->GetFormDoc()->CreateElementNS(xmlns_forms, "form");
    formnode->SetAttribute("name",name);
    this->GetFormDoc()->documentelement->AppendChild(formnode);

    OBJECT formdef := NEW FormDefinition(this, formnode);
    formdef->AddPage("");

    OBJECT thankyou := formdef->AddPage("thankyou");
    thankyou->node->SetAttribute("pins","existence");

    RETURN formdef;
  }

  PUBLIC STRING FUNCTION ResolveLinkRefToUrl(STRING link)
  {
    IF (link LIKE "x-wh-internallink:*")
    {
      // Internal link
      STRING ARRAY parts := Tokenize(link, "|");
      INTEGER fileid := this->GetLinkRef(parts[0]);
      OBJECT fileobj := OpenWHFSObject(fileid);
      IF (ObjectExists(fileobj) AND fileobj->link != "")
      {
        link := fileobj->link;
        IF (Length(parts) > 1)
          link := link || parts[1];
      }
      ELSE
        link := "";
    }
    RETURN link;
  }

  PUBLIC INTEGER FUNCTION GetLinkRef(STRING tag)
  {
    IF(tag NOT LIKE "x-wh-internallink:*")
      RETURN 0;

    RETURN SELECT AS INTEGER linkref FROM this->links WHERE links.tag = Substring(VAR tag, 18);
  }

  PUBLIC STRING FUNCTION CreateLinkRef(INTEGER newlinkref)
  {
    IF(newlinkref=0)
      RETURN "";

    STRING linkid := SELECT AS STRING tag FROM this->links WHERE linkref = VAR newlinkref;
    IF(linkid="")
    {
      linkid := "RL-" || GenerateUFS128BitId();
      INSERT [ tag := linkid, linkref := newlinkref ] INTO this->links AT END;
    }
    RETURN "x-wh-internallink:" || linkid;
  }

  PUBLIC MACRO SetLinkRef(STRING tag, INTEGER newlinkref)
  {
    IF(tag NOT LIKE "x-wh-internallink:*")
      THROW NEW Exception("Unrecognized link " || tag);

    INTEGER existinglinkpos := SELECT AS INTEGER #links + 1 FROM this->links WHERE links.tag = Substring(VAR tag, 18);
    IF(existinglinkpos > 0)
    {
      this->links[existinglinkpos-1].linkref := newlinkref;
    }
    ELSE
    {
      INSERT [ tag := Substring(tag,14), linkref := newlinkref ] INTO this->links AT END;
    }
  }

  PUBLIC RECORD FUNCTION GetInstance(STRING getid)
  {
    RETURN SELECT AS RECORD data FROM this->instances WHERE instanceid = getid;
  }

  PUBLIC STRING FUNCTION CreateInstance(RECORD data)
  {
    OBJECT type := OpenWHFSType(data.whfstype);
    IF(NOT ObjectExists(type))
      THROW NEW Exception(`No such contenttype '${data.whfstype}'`);

    STRING newid := GenerateUFS128BitId();
    WHILE(RecordExists(SELECT FROM this->instances WHERE instanceid=newid))
      newid := GenerateUFS128BitId();

    INSERT [ instanceid := newid, data := data ] INTO this->instances AT END;
    RETURN newid;
  }

  PUBLIC MACRO SetInstance(STRING updateid, RECORD data)
  {
    INTEGER updatepos := SELECT AS INTEGER #instances+1 FROM this->instances WHERE instanceid = updateid;
    IF(updatepos=0)
      THROW NEW Exception(`No such instance '${updateid}'`);

    IF(RecordExists(data))
    {
      IF(this->instances[updatepos-1].data.whfstype != data.whfstype)
        THROW NEW Exception(`Cannot update instance with different type (want '${this->instances[updatepos-1].data.whfstype}', got '${data.whfstype}')`);

      this->instances[updatepos-1].data := MakeReplacedRecord(this->instances[updatepos-1].data, data);
    }
    ELSE
    {
      DELETE FROM this->instances AT updatepos-1;
    }
  }

  PUBLIC RECORD FUNCTION GetConditionAttribute(OBJECT node, STRING attributename)
  {
    STRING currentinstance := node->GetAttribute(attributename);
    RECORD res;
    IF(currentinstance != "")
      res := this->GetInstance(currentinstance);
    RETURN RecordExists(res) ? res.data : DEFAULT RECORD;
  }

  PUBLIC MACRO SetConditionAttribute(OBJECT node, STRING attributename, RECORD cond)
  {
    STRING currentinstance := node->GetAttribute(attributename);
    IF(NOT RecordExists(cond)) //resetting
    {
      IF(currentinstance != "")
        DELETE FROM this->instances WHERE instanceid = currentinstance;
      node->RemoveAttribute(attributename);
      RETURN;
    }

    RECORD toset := [ whfstype := "http://www.webhare.net/xmlns/publisher/formcondition"
                    , data := ValidateFormCondition(cond)
                    ];

    IF(currentinstance = "")
      node->SetAttribute(attributename, this->CreateInstance(toset));
    ELSE
      this->SetInstance(currentinstance, toset);
  }

  PUBLIC RECORD FUNCTION ExportAsRecord()
  {
    IF(NOT ObjectExists(this->formdoc))
      RETURN DEFAULT RECORD;

    this->formdoc->NormalizeDocument();
    RETURN [ text := this->formdoc->GetDocumentBlob(TRUE)
           , instances := this->instances
           , links := (SELECT tag, linkref FROM this->links ORDER BY tag)
           , type := "publisher:formdefinition"
           ];
  }

  PUBLIC MACRO ImportFromRecord(RECORD data, RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    options := ValidateOptions([ resourcename := "" ], options);
    IF (options.resourcename != "")
      this->pvt_resourcename := options.resourcename;

    this->formdoc := DEFAULT OBJECT;
    data := __ValidateRichDocumentLike(data, FALSE, FALSE);

    IF(RecordExists(data) AND Length(data.text) > 0)
      this->formdoc := MakeXMLDocument(data.text);
    IF(NOT ObjectExists(this->formdoc))
      this->formdoc := NEW XMLDomImplementation->CreateDocument(xmlns_forms, "formdefinitions", DEFAULT OBJECT);

    IF(CellExists(data,"instances"))
    {
      //ensure even manual imports have whfsfileid and whfssettingid set for every instance
      this->instances := SELECT AS RECORD ARRAY CELL[ instanceid, data := [ whfssettingid := 0i64, whfsfileid := 0, ...instances.data] ] FROM data.instances;
    }
    IF(CellExists(data,"links"))
    {
      this->links := SELECT tag
                          , linkref
                       FROM data.links;
    }
  }
  PUBLIC MACRO LoadFromWHFS(OBJECT whfsobject)
  {
    this->ImportFromRecord(whfsobject->GetInstanceData("http://www.webhare.net/xmlns/publisher/formwebtool").data);
    this->pvt_whfsobject := whfsobject;
  }
  PUBLIC MACRO SaveToWHFS(OBJECT whfsobject)
  {
    whfsobject->SetInstanceData("http://www.webhare.net/xmlns/publisher/formwebtool", [ data := this->ExportAsRecord() ]);
    whfsobject->UpdateMetadata(DEFAULT RECORD); //ensure modtime update
    this->pvt_whfsobject := whfsobject;
  }

  PUBLIC STRING ARRAY FUNCTION __GetInstanceids()
  {
    RETURN SELECT AS STRING ARRAY instanceid FROM this->instances;
  }
  PUBLIC STRING FUNCTION __CreateGUID(STRING prefix)
  {
    RETURN prefix || ":" || GenerateUFS128BitId();
  }

  PUBLIC RECORD FUNCTION LookupComponentDef(OBJECT question)
  {
    RETURN GetFormComponentDef(question->namespaceuri, question->localname);
  }
>;

/** @short Creates a new form definition object
    @return(object #FormDefinitionsFile) Form definition object
    @topic forms/api
    @public
    @loadlib mod::publisher/lib/forms/api.whlib
*/
PUBLIC OBJECT FUNCTION CreateNewFormdefinitionsFile()
{
  RETURN NEW FormDefinitionsFile;
}

/** Open a form definition file from WHFS
    @param whfsobject WHFS object of a form
    @return(object #FormDefinitionsFile) Form definition object
    @topic forms/api
    @public
    @loadlib mod::publisher/lib/forms/api.whlib
*/
PUBLIC OBJECT FUNCTION OpenWHFSFormDefinitionsFile(OBJECT whfsobject)
{
  OBJECT def := NEW FormDefinitionsFile;
  def->LoadFromWHFS(whfsobject);
  RETURN def;
}
