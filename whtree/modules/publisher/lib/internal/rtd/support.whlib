<?wh

LOADLIB "wh::xml/xsd.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

PUBLIC BOOLEAN FUNCTION IsEmbeddedObject(OBJECT node)
{
  IF(ToUppercase(node->nodename) NOT IN ["DIV","SPAN"])
    RETURN FALSE;
  STRING ARRAY classlist := ParseXSList(node->GetAttribute("class"));
  RETURN "wh-rtd-embeddedobject" IN classlist OR "-wh-rtd-embeddedobject" IN classlist;

}

PUBLIC STRING ARRAY FUNCTION FilterAllowTags(STRING ARRAY baseset, STRING ARRAY allowtags)
{
  STRING ARRAY keep;
  FOREVERY(STRING tag FROM allowtags)
    IF(ToUppercase(tag) IN baseset)
      INSERT ToUppercase(tag) INTO keep AT END;
  RETURN keep;
}

PUBLIC MACRO TrimRTDWhitespace(OBJECT body)
{
  WHILE(IsRTDEmptyOrWhitespace(body->firstchild))
    body->RemoveChild(body->firstchild);
  WHILE(IsRTDEmptyOrWhitespace(body->lastchild))
    body->RemoveChild(body->lastchild);
}

PUBLIC BOOLEAN FUNCTION IsRTDEmptyOrWhitespace(OBJECT node)
{
  IF(NOT ObjectExists(node))
    RETURN FALSE;
  IF(node->nodetype=3)
    RETURN TrimWhitespace(Substitute(node->nodevalue, '\u00A0', ' '))=""; //already text
  IF(node->nodetype!=1)
    RETURN FALSE;
  IF(ToUppercase(node->nodename) = "BR")
    RETURN TRUE;
  IF(ToUppercase(node->nodename) IN ["IMG","IFRAME"] OR IsEmbeddedObject(node))
    RETURN FALSE; //NEVER whitespace

  FOR(node := node->firstchild; ObjectExists(node); node := node->nextsibling)
  {
    IF(NOT IsRTDEmptyOrWhitespace(node))
      RETURN FALSE;
  }
  RETURN TRUE;
}


PUBLIC RECORD FUNCTION SplitIntLink(STRING intlink)
{
  INTEGER splitpos := Length(intlink);
  FOREVERY(STRING app FROM whconstant_internallink_startcharacters)
  {
    INTEGER pos := SearchSubstring(intlink, app);
    IF(pos >= 0 AND pos < splitpos)
     splitpos := pos;
  }

  RETURN [ append := Substring(intlink, splitpos)
         , link := Left(intlink, splitpos)
         ];
}

PUBLIC OBJECT FUNCTION CreateInstanceBlockNode(OBJECT doc, STRING instanceid)
{
  IF(NOT Objectexists(doc))
    THROW NEW Exception("Document is empty");

  OBJECT myblock := doc->CreateElement("div");
  myblock->SetAttribute("class", "wh-rtd-embeddedobject");
  myblock->SetAttribute("data-instanceid", instanceid);
  RETURN myblock;
}

/** Context for widgeteditors opened by <rtdtype> */
PUBLIC OBJECTTYPE RichDocumentContext
<
  PUBLIC RECORD __rtdtypeinfo; //used to implement <richdocument> rtdtype and 'inherit'. might change or go away

  ///<rtdtype> of current <richdocument>
  PUBLIC PROPERTY rtdtype(this->__rtdtypeinfo.namespace, -);
>;

PUBLIC RECORD FUNCTION __ValidateRichDocumentLike(RECORD inrichdoc, BOOLEAN isrtd, BOOLEAN iswhfsrefmapped)
{
  IF(NOT RecordExists(inrichdoc))
    RETURN DEFAULT RECORD;

  RECORD baserec := [ embedded := DEFAULT RECORD ARRAY
                    , links := DEFAULT RECORD ARRAY
                    , instances := DEFAULT RECORD ARRAY
                    ];
  IF(isrtd)
    baserec := CELL[...baserec, htmltext := DEFAULT BLOB];
  ELSE
  {
    IF(CellExists(inrichdoc, 'formtext') AND NOT CellExists(inrichdoc, 'text')) //This must be a pre-5.2 WebHare talking to us. We'll convert it for now. Once no more pre-5.2s should exist we can drop the conversion
      inrichdoc := CELL[ ...inrichdoc, text := inrichdoc.formtext, type := "publisher:formdefinition", DELETE formtext ];
    IF(CellExists(inrichdoc, 'formtext'))
      THROW NEW Exception(`Received a 'formtext' but it should now be just 'text' with a type of 'publisher:formdefinition'`);
    IF(NOT CellExists(inrichdoc, 'type'))
      THROW NEW Exception(`Composed documents require a 'type'`);
    ELSE IF(inrichdoc.type NOT IN ["publisher:formdefinition", "publisher:markdown"]) //we might allow more/any types in the future, but to catch errors we'll limit for now.
      THROW NEW Exception(`Unrecognized type '${inrichdoc.type}'`);
    baserec := CELL[...baserec, text := DEFAULT BLOB, type := ""];
  }

  inrichdoc := ValidateOptions(baserec, inrichdoc);

  //using *, cell to validate that these cells exists
  IF (iswhfsrefmapped)
  {
    inrichdoc.embedded := SELECT *
                               , contentid
                               , data
                               , source_fsobject := CellExists(embedded,'source_fsobject') ? embedded.source_fsobject : ""
                            FROM inrichdoc.embedded;
  }
  ELSE
  {
    inrichdoc.embedded := SELECT *
                               , contentid
                               , data
                               , source_fsobject := CellExists(embedded,'source_fsobject') ? embedded.source_fsobject : 0
                            FROM inrichdoc.embedded;
  }

  inrichdoc.instances := SELECT AS RECORD ARRAY ValidateOptions([ data := DEFAULT RECORD
                                                                , instanceid := ""
                                                                ], instances, [ required := ["data", "instanceid"]
                                                                              , title := `instances[${#instances}]`
                                                                              ])
                           FROM inrichdoc.instances;

  //check for whfstype and add any missing whfssettingid/whfsfileid, widget users expect these
  UPDATE inrichdoc.instances SET data := ValidateOptions([ whfstype := ""
                                                         , whfssettingid := 0i64
                                                         , whfsfileid := 0
                                                         ], data, [ required := [ "whfstype" ]
                                                                  , title := `instances[${#instances}].data`
                                                                  , passthrough := TRUE
                                                                  ]);

  IF(RecordExists(SELECT FROM inrichdoc.embedded GROUP BY contentid HAVING Count(*)>1))
    THROW NEW Exception("Embedded data contains duplicate content ids");
  IF (RecordExists(SELECT FROM inrichdoc.instances GROUP BY instances.instanceid HAVING Count(*)>1))
    THROW NEW Exception("Embedded instances contain duplicate instance ids");

  RETURN inrichdoc;
}
