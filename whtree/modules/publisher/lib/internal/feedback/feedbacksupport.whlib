<?wh

LOADLIB "wh::witty.whlib";
LOADLIB "wh::internet/jwt.whlib";

LOADLIB "mod::tollium/lib/gettid.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/internal/cluster/secrets.whlib";
LOADLIB "mod::system/lib/services.whlib";
LOADLIB "mod::system/lib/userrights.whlib";


PUBLIC CONSTANT STRING tollium_feedback_scope := "tollium:webharebackend";

PUBLIC OBJECT FUNCTION GetScreenshotWitty()
{
  RETURN LoadWittyLibrary(Resolve("screenshot.witty"), "HTML-NI");
}

PUBLIC STRING FUNCTION GetFeedbackScreenshotURL(INTEGER issueid)
{
  RETURN GetPrimaryWebHareInterfaceURL() || ".wh/common/feedback/screenshot.shtml?ss=" || EncryptForThisServer("tollium:screenshot",CELL[issueid]);
}

PUBLIC RECORD ARRAY FUNCTION GetFeedbackExtraFields(RECORD extradata)
{
  RECORD ARRAY fields;

  // The 'extrafields' cell contains field definitions to show extra data
  IF (NOT (CellExists(extradata, "extrafields") AND TypeID(extradata.extrafields) = TypeID(RECORD ARRAY)))
    RETURN fields;

  FOREVERY (RECORD extra FROM extradata.extrafields)
  {
    extra := EnforceStructure(
        [ field := ""
        , type := ""
        , title := ""
        , tid := ""
        , optiongid := ""
        ], extra);

    IF (NOT CellExists(extradata, extra.field))
      CONTINUE;

    STRING type, value;
    SWITCH (extra.type)
    {
      CASE "string"
      {
        type := "text";
        value := GetCell(extradata, extra.field);
      }
      CASE "integer"
      {
        type := "text";
        value := ToString(GetCell(extradata, extra.field));
      }
      CASE "tid"
      {
        type := "text";
        STRING tid := GetCell(extradata, extra.field);

        // Try to resolve the tid if this is not an absolute tid reference
        IF (tid NOT LIKE "*:*")
        {
          IF (extra.optiongid != "")
            tid := extra.optiongid || "." || tid;
          ELSE
            tid := extra.tid || "-" || tid;
        }
        IF (tid LIKE "*:*")
          value := GetTid(tid);
        ELSE
          value := tid;
      }
      DEFAULT
      {
        CONTINUE; //not understood..
      }
    }

    INSERT CELL[ name := extra.field
               , title := extra.tid = "" ? extra.title : GetTid(extra.tid)
               , type
               , value
               ] INTO fields AT END;
  }
  RETURN fields;
}

STRING FUNCTION GetFeedbackSecret(STRING kid_unused)
{
  // The secret key should be at least 256 bits, which the cookiesecret already is
  RETURN GetClusterKeyBase("cookiesecret") || tollium_feedback_scope;
}

// @cell(string) userdata.name The user's full name
// @cell(string) userdata.email The user's email address
// @return A JSON Web Token
PUBLIC STRING FUNCTION GetFeedbackWebToken(OBJECT tolliumuser)
{
  RECORD userdata := tolliumuser->GetUserDataForLogging();
  IF(userdata.wrd_guid = "")
    RETURN ""; //anonymous user or override token

  STRING wrdschema := tolliumuser->wrdschema->tag;

  // Convert GetUserDataForLogging data to JWT claims (see https://www.iana.org/assignments/jwt/jwt.xhtml)
  userdata :=
      [ iap := userdata.when
      , sub := userdata.wrd_guid
      , name := userdata.realname
      , email := userdata.emailaddress
      , preferred_username := userdata.login
      ];
  IF(wrdschema != GetPrimaryWebhareAuthPlugin()->wrdschema->tag)
    INSERT CELL wrdschema := tolliumuser->wrdschema->tag INTO userdata;

  OBJECT token := NEW JSONWebToken("HS256", GetfeedbackSecret(""));
  token->payload := userdata;
  RETURN token->GetAuthorizationValue();
}

// @param token A JSON Web Token
// @cell(string) return.name The user's full name
// @cell(string) return.email The user's email address
PUBLIC RECORD FUNCTION VerifyFeedbackWebToken(STRING token)
{
  IF (token != "")
  {
    TRY
    {
      RECORD userdata := EnforceStructure(
          [ iap := DEFAULT DATETIME
          , sub := ""
          , name := ""
          , email := ""
          , preferred_username := ""
          , wrdschema := ""
          ], VerifyJSONWebToken(token, [ alg := [ "HS256" ], secret_callback := PTR GetFeedbackSecret ])->payload);
      // Convert JWT claims to GetUserDataForLogging data (see https://www.iana.org/assignments/jwt/jwt.xhtml)
      RETURN
          [ when := userdata.iap
          , wrd_guid := userdata.sub
          , realname := userdata.name
          , emailaddress := userdata.email
          , login := userdata.preferred_username
          , wrdschema := userdata.wrdschema
          ];
    }
    CATCH (OBJECT e)
      LogHareScriptException(e);
  }
  RETURN DEFAULT RECORD;
}

PUBLIC MACRO UpdateFeedbackItem(OBJECT wrdschema, INTEGER feedbackid, RECORD newentry, RECORD webhareuser)
{
  newentry := ValidateOptions(CELL[ role := 0
                                  , remarks := ""
                                  ], newentry, [ optional := [ "role" ], title := "newentry" ]);

  RECORD logentry := [ wrd_leftentity := feedbackid
                     , webhareuser := webhareuser
                     , text := newentry.remarks
                     ];
  RECORD itemupdates;

  IF(CellExists(newentry,'role') AND newentry.role != wrdschema->^feedback->GetEntityField(feedbackid, "role"))
  {
    INSERT CELL newrole := newentry.role INTO logentry;
    INSERT CELL role := newentry.role INTO itemupdates;
  }

  IF(newentry.remarks = "" AND NOT RecordExists(itemupdates))
    RETURN;

  wrdschema->^log->CreateEntity(logentry);
  IF(RecordExists(itemupdates))
    wrdschema->^feedback->UpdateEntity(feedbackid, itemupdates);
}
