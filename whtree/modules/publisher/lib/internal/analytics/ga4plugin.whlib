<?wh

LOADLIB "wh::xml/xsd.whlib";

LOADLIB "mod::publisher/lib/webdesign.whlib";

PUBLIC STATIC OBJECTTYPE GoogleAnalytics4Plugin EXTEND WebDesignPluginBase
<
  RECORD ARRAY datalayerpushes;
  OBJECT webdesign;
  STRING __account;
  STRING __integration;
  BOOLEAN __anonymizeip;

  PUBLIC PROPERTY account(__account, SetAccount);
  PUBLIC PROPERTY integration(__integration, SetIntegration);
  PUBLIC PROPERTY anonymizeip(__anonymizeip, SetAnonymizeIP);

  UPDATE PUBLIC RECORD FUNCTION ParseConfigurationNode(OBJECT siteprofile, OBJECT node)
  {
    RETURN [ account := node->GetAttribute("account")
           , integration := node->GetAttribute("integration") ?? "onload"
           , anonymizeip := node->HasAttribute("anonymizeip") ? ParseXSBoolean(node->GetAttribute("anonymizeip")) : TRUE
           ];
  }

  MACRO ValidateCode(STRING code)
  {
    IF(code != "" AND code NOT LIKE "G-???????*")
    THROW NEW Exception(`Account '${code}' is not a valid Google Analytics 4 account`);
  }

  UPDATE PUBLIC MACRO ConfigurePlugin(OBJECT webdesign, RECORD config)
  {
    this->webdesign := webdesign;

    this->__account := config.account;
    this->__integration := config.integration;
    this->__anonymizeip := config.anonymizeip;
  }

  //UPDATE PUBLIC MACRO DatalayerPush(RECORD instruction)
  //{
  //  INSERT instruction INTO this->datalayerpushes AT END;
  //}

  //MACRO PrintDatalayerPushes()
  //{
  //  IF(Length(this->datalayerpushes) = 0)
  //    RETURN;

  //  STRING pushes := EncodeJSON(this->datalayerpushes);

  //  IF(this->__integration = "script" AND this->__launch="pagerender")
  //  {
  //    pushes := Substring(pushes, 1, Length(pushes)-2); //Remove the [ and ] wrapping the array
  //    Print(`<script>(window.dataLayer||[]).push(${pushes})</script>`);
  //  }
  //  ELSE //yep, we can legally do this: https://html.spec.whatwg.org/multipage/custom-elements.html#custom-elements
  //  {
  //    Print(`<wh-socialite-gtm push="${EncodeValue(pushes)}"></wh-socialite-gtm>`);
  //  }
  //}

  MACRO SetupPluginConfig()
  {
    RECORD config;
    IF(this->webdesign->ispreviewpage)
      RETURN;

    IF(this->__integration != "inpage" AND this->__account != "")
    {
      config := [ a := this->__account ];
      IF(this->__integration = "manual")
        INSERT CELL m := 1 INTO config;
      IF(NOT this->__anonymizeip)
        INSERT CELL ip := 1 INTO config;
    }
    this->webdesign->SetJSPluginConfig("ga4", config);
  }
  MACRO SetAccount(STRING newaccount)
  {
    IF(this->webdesign->startedrendering)
      THROW NEW Exception("The GA4 account cannot be modified when the page has started rendering - update should have been done in the prepare phase");

    this->__account := newaccount;
    this->SetupPluginConfig();
  }
  MACRO SetIntegration(STRING newintegration)
  {
    IF(this->webdesign->startedrendering)
      THROW NEW Exception("The GA4 integration cannot be modified when the page has started rendering - update should have been done in the prepare phase");

    this->__integration := newintegration;
    this->SetupPluginConfig();
  }
  MACRO SetAnonymizeIP(BOOLEAN newanoymizeip)
  {
    IF(this->webdesign->startedrendering)
      THROW NEW Exception("The GA4 integration cannot be modified when the page has started rendering - update should have been done in the prepare phase");

    this->__anonymizeip := newanoymizeip;
    this->SetupPluginConfig();
  }

  UPDATE PUBLIC MACRO PrepareForRendering(OBJECT webdesign)
  {
    this->SetupPluginConfig();

    /* https://support.google.com/tagmanager/answer/6103696?hl=en
       - the script part as high as possible in the head, the noscript as high as possible in the body
       */
    webdesign->InsertWithCallback(PTR this->DoTagManager_Head(webdesign), "dependencies-top");
    //TODO ?webdesign->InsertWithCallback(PTR this->PrintDatalayerPushes, "body-bottom");
  }

  MACRO DoTagManager_Head(OBJECT webdesign)
  {
    IF(this->__account = "" OR this->__integration != "inpage" OR webdesign->ispreviewpage)
      RETURN;

    RECORD settings := [ anonymize_ip := this->__anonymizeip ];
    BOOLEAN compact := webdesign->wittyencoding LIKE "*-NI";
    //Both tags want the ID... my guess the first one is just for measure noscript hits
    Print(`${compact?"":"\n"}<script async src="https://www.googletagmanager.com/gtag/js?id=${EncodeURL(this->__account)}"></script>`
          || `${compact?"":"\n"}<script>window.dataLayer=window.dataLayer||[];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', '${EncodeJava(this->__account)}', ${EncodeJSON(settings)});</script>${compact?"":"\n"}`);
  }
  //TODO hook 'anl' ?
  //UPDATE PUBLIC RECORD ARRAY FUNCTION HookGetDebugSettings()
  //{
  //  RETURN [[ grouptitle := "Commonly used", tag := "sne", title := "Don't embed external scripts like GTM" ]];
  //}
>;
