<?wh

LOADLIB "wh::internet/urls.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/webserver.whlib";

LOADLIB "mod::tollium/lib/testframework.whlib";

LOADLIB "mod::wrd/lib/auth.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";


OBJECT plugin;
OBJECT trans := OpenPrimary();

IF(UnpackURL(GetRequestURL()).urlpath LIKE "wrd/authtest/*")
{
  OBJECT myschema;
  myschema := OpenTestsuiteWRDSchema();
  IF(NOT ObjectExists(myschema))
    THROW NEW Exception("No test schema");

  BOOLEAN hasdomaintype := ObjectExists(myschema->GetType("WRD_AUTHDOMAIN"));
  IF(hasdomaintype)
    plugin := GetWRDAuthPlugin(OpenTestsuiteSite()->webroot || "portal1/");
  ELSE //domainless test
    plugin := GetWRDAuthPlugin(OpenTestsuiteSite()->webroot || "portal1-domainless/");
}
ELSE
{
  plugin := GetWRDAuthPlugin(UnpackURL(GetRequestURL()).origin || "/" || GetWebVariable("forurl"));
}

RECORD FUNCTION GetStatus()
{
  BOOLEAN isloggedin := plugin->IsLoggedIn();
  IF(isloggedin != plugin->IsLoggedIn())
    THROW NEW Exception("IsLoggedIn is inconsistent!"); //This was triggered by deleting a user when a sessioncookie still points to him, caused by this->sessiondata not being cleared

  RETURN [ isloggedin := isloggedin
         , loginname := (plugin->GetLoggedinEntityFields(["wrd_contact_email"]) ?? [wrd_contact_email:=""]).wrd_contact_email
         , entityid := plugin->GetLoggedinEntity()
         , expires := plugin->sessionexpires
         ];
}

RECORD FUNCTION ProcessReq()
{
  STRING browsertriplet := GetWebVariable("browsertriplet");
  UpdateAuditContext(CELL[ browsertriplet ]);

  RECORD loginopts := CELL[ persistent := GetWebVariable("persistent") = "1"
                          ];

  SWITCH(GetWebVariable("action"))
  {
    CASE "login"
    {
      loginopts := CELL[ ...loginopts
                       , skippasswordcheck := GetWebVariable("skippasswordcheck") = "1"
                       ];

      RECORD res := plugin->Login(GetWebVariable("username"), GetWebVariable("pwd"), loginopts);
      RETURN CELL[ ...res
                 , plugin_entityid := plugin->entityid
                 ];
    }
    CASE "loginid"
    {
      RECORD res := plugin->LoginById(ToInteger(GetWebVariable("userid"), 0), GetWebVariable("impersonation") = "1", loginopts);
      RETURN CELL[ ...res
                 , plugin_entityid := plugin->entityid
                 ];
    }
    CASE "logout-checkstatus"
    {
      RECORD oldstatus := GetStatus();
      plugin->__GetWRDAuth()->Logout();

      RECORD res := CELL[...GetStatus(), oldstatus];
      RETURN res;
    }
    CASE "requirelogin"
    {
      OBJECT user := GetTestsuiteWRDauthPlugin()->RequireExternalLoggedinUser();
      RETURN [ login := user->login ];
    }
    CASE "checkstatus"
    {
      RECORD res := GetStatus();
      RETURN res;
    }
    CASE "checkstatus-work"
    {
      trans->BeginWork();
      RECORD res := GetStatus();
      trans->RollbackWork();
      RETURN res;
    }
    CASE "loginsecondfactor"
    {
      RECORD res := plugin->LoginSecondFactor(
          GetWebVariable("firstfactorproof"),
          GetWebVariable("type"),
          DecodeJSON(GetWebVariable("data")));
      RETURN res;
    }

  }
  THROW NEW Exception("Unrecognized function '" || GetwebVariable('action') || "'");
}

RECORD res := ProcessReq();
AddHTTPHeader("Content-Type","application/x-hson",FALSE);
Print(EncodeHSON(res));
