<?wh
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::tollium/lib/testframework.whlib";

MACRO TestPasswordSyntax()
{
  OBJECT screen := GetTestController()->LoadScreen(`inline::
<screens xmlns="http://www.webhare.net/xmlns/tollium/screens">
  <screen name="test" implementation="none">
    <body>
      <passwordfield name="passwordfield" />
    </body>
  </screen>
</screens>#test`);

  TestEq("", screen->passwordfield->value);
}

OBJECT ASYNC FUNCTION TestPasswordField()
{
  OBJECT browser := GetTestController()->LoadScreen("webhare_testsuite:tests/anycomponent.anycomponent",
                                                   [ component := "passwordfield"
                                                   , settings := [ required := true
                                                                 , entercurrentpassword := false
                                                                 ]
                                                   ]);

  AWAIT ExpectScreenChange(+1, PTR browser->RunModal());

  TestEq("", topscreen->comp->value);

  topscreen->comp->validationchecks := "hibp minlength:8 uppercase:3 lowercase:3 digits:3 symbols:3";
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit"));

  TT("newpassword")->value := "aaaaaaaaaaa";
  TT("confirmpassword")->value := "aaaaaaaaaaa";
  AWAIT ExpectAndAnswerMessageBox("ok", PTR TTClick(":Ok"),
      [ messagemask :="*doesn't have*properties*not*database of compromised passwords*3 uppercase*3 digits*3 symbols.*" ]);

  AWAIT ExpectScreenChange(-1, PTR TTEscape());

  // remove HIBP, so future leaks of the passwords we're testing won't let the tests fail
  topscreen->comp->validationchecks := "minlength:8 uppercase:3 lowercase:3 digits:3 symbols:3";
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit"));

  TT("newpassword")->value := "abAB12.,";
  TT("confirmpassword")->value := "abAB12.,";
  AWAIT ExpectAndAnswerMessageBox("ok", PTR TTClick(":Ok"),
      [ messagemask :="*doesn't have*properties*3 lowercase*3 uppercase*3 digits*3 symbols.*" ]);

  TT("newpassword")->value := "abcABC123.,/";
  TT("confirmpassword")->value := "abcABC123.,/";

  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));

  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteCancel());

  RETURN TRUE;
}

RunTestframework([ PTR TestPasswordSyntax
                 , PTR TestPasswordField
                 ]);
