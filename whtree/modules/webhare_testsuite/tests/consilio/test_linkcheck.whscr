<?wh

LOADLIB "wh::crypto.whlib";
LOADLIB "wh::internet/urls.whlib";

LOADLIB "mod::consilio/lib/internal/linkchecker/linkchecking.whlib";
LOADLIB "mod::consilio/lib/internal/http_api.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/testframework.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";


// Regression test: GetHTTPUrl not returning ERRORCODE's on server error (#1252)
MACRO TestServerStatus()
{
  //Verify our 'mocks' work, needed for further linkcheck tests
  STRING uniquelinksuffix := GenerateUFS128BitId(); //make sure the linkchecker has never seen us before
  RECORD result := CheckLink(`http://linkchecker.test/expect-404/${uniquelinksuffix}`);
  TestEq(404, result.code);
  result := CheckLink(`https://linkchecker.test/expect-403/${uniquelinksuffix}`);
  TestEq(403, result.code);

  result := GetHTTPUrl(TRUE/*get*/, "http://nonexisting.nodomain");
  TestEq("RESOLVE", result.errorcode);
  TestEq(-16, result.code);

  result := GetHTTPUrl(TRUE/*get*/, `http://localhost:${GetWebHareConfiguration().baseport - 1}/`);
  TestEq("CONNECT", result.errorcode);
  TestEq(-8, result.code); // connection refused

  OBJECT testfolder := GetTestsuiteTempFolder();
  result := GetHTTPUrl(TRUE/*get*/, testfolder->objecturl);
  TestEq("", result.errorcode);
  TestEq(404, result.code); // no content yet in site

  // This page sleeps for 2 seconds:
  result := GetHTTPUrl(TRUE/*get*/, ResolveToAbsoluteURL(GetPrimaryWebhareInterfaceURL(),"/.webhare_testsuite/tests/consilio/timeout.shtml"), [ timeout := 1000 ]);
  TestEq("CONNECT", result.errorcode); // Kinda expecting it to be "TIMEOUT", but apparently the WebBrowser object doesn't return a separate error code for socket timeouts
  TestEq(0, result.code);

  //Create a file that breaks on HEAD but works with get. Some remote servers do that!
  result := CheckLink(ResolveToAbsoluteURL(GetPrimaryWebhareInterfaceURL(),"/.webhare_testsuite/tests/consilio/head-broken.shtml?head=500&get=200"));
  TestEq("", result.errorcode);
  TestEq(200, result.code);

  //But we shouldn't look past eg. a 404
  result := CheckLink(ResolveToAbsoluteURL(GetPrimaryWebhareInterfaceURL(),"/.webhare_testsuite/tests/consilio/head-broken.shtml?head=404&get=200"));
  TestEq("", result.errorcode);
  TestEq(404, result.code);

  // robots.txt is always denylisted
  result := CheckLink(`http://example.com/robots.txt`);
  TestEQ(14, result.code);
}

RunTestFramework( [ PTR TestServerStatus
                  ]);
