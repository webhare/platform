<?wh
LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/testframework.whlib";

LOADLIB "mod::consilio/lib/api.whlib";
LOADLIB "mod::consilio/lib/internal/dbschema.whlib";
LOADLIB "mod::consilio/lib/internal/updateindices.whlib";
LOADLIB "mod::consilio/lib/internal/opensearch.whlib";

LOADLIB "mod::tollium/lib/testframework.whlib";
LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";


PUBLIC MACRO TestModernSiteProfileIndices()
{
  testfw->BeginWork();
  DELETE FROM consilio.catalogs WHERE name LIKE "webhare_testsuite:testsitecatalog";
  testfw->CommitWork();

  FixConsilioIndices([ catalogmask := "webhare_testsuite:testsitecatalog" ]);

  OBJECT maincatalog := OpenConsilioCatalog("webhare_testsuite:testsitecatalog");
  Testeq(TRUE, ObjectExists(maincatalog));
  Testeq(TRUE, ObjectExists(maincatalog->OpenContentSource("webhare_testsuite:dbindex")));
  TestThrowsLike('*cannot update*fieldgroups*', PTR maincatalog->UpdateCatalog([ fieldgroups := [ "webhare_testsuite:nosuchfieldyet", ...maincatalog->fieldgroups ]]));

  RECORD ARRAY testsitecatalog_mapping := maincatalog->GetExpectedMapping();
  TestEqMembers([[ name := "myfield1", type := "text" ]], (SELECT * FROM testsitecatalog_mapping WHERE name = "myfield1"),"*");
  TestEqMembers([[ name := "groupid", type := "keyword" ]], (SELECT * FROM testsitecatalog_mapping WHERE name = "groupid"),"*");
  TestEqMembers([[ name := "mapping_integer", type := "integer" ]], (SELECT * FROM testsitecatalog_mapping WHERE name = "mapping_integer"),"*");
  TestEqMembers([[ name := "description", type := "text", suggested := TRUE, settings := [ index_options := "offsets"] ]], (SELECT * FROM testsitecatalog_mapping WHERE name = "description"),"*");
  TestEq(["body", "description", "keywords", "title"], GetSortedSet(maincatalog->__GetSuggestedFields())); //TODO If __GetSuggestedFields goes, consider checking testsitecatalog_mapping

  RECORD testindex := maincatalog->ListAttachedIndices()[0];
  RECORD ARRAY diff := VerifyOpensearchMapping(testindex.indexmanager, testindex.indexname, "", maincatalog->GetExpectedMapping());
  TestEq(RECORD[], diff);

  //having indexnames makes log analysis easier
  Print(maincatalog->GetStorageInfo() || "\n");

  RECORD ARRAY contentsources := maincatalog->ListContentSources();
  contentsources := SELECT *, whfspath := (SELECT AS STRING whfspath FROM system.fs_objects WHERE fs_objects.id = contentsources.fsobject) FROM contentsources WHERE NOT isorphan;
  TestEq([ '/webhare-tests/webhare_testsuite.testsite/portal1/' //portal1 is added through ongetsources=
         , '/webhare-tests/webhare_testsuite.testsite/TestPages/'
         , '/webhare-tests/webhare_testsuite.testsite/webtools/'
         ], SELECT AS STRING ARRAY whfspath FROM contentsources WHERE whfspath != "" ORDER BY ToUppercase(whfspath));
  TestEq([ 'webhare_testsuite:dbindex'
         , 'webhare_testsuite:loremindex'
         ], SELECT AS STRING ARRAY tag FROM contentsources WHERE tag != "" ORDER BY tag);

  maincatalog->ReindexCatalog();
  FOREVERY(RECORD csource FROM contentsources)
    maincatalog->OpenContentSourceById(csource.id)->WaitForIndexingDone();

  RECORD result := RunConsilioSearch("webhare_testsuite:testsitecatalog", CQParseUserQuery("simpletest"));
  TestEq(TRUE, Length(result.results) > 0);
  TestEq("webhare_testsuite.testsite", result.results[0].title);

  result := RunConsilioSearch("webhare_testsuite:testsitecatalog", CQParseUserQuery("Pellentesque"));
  TestEq(TRUE, Length(result.results) > 0);

  result := RunConsilioSearch("webhare_testsuite:testsitecatalog", CQParseUserQuery("Welcome"));
  TestEq(RECORD[], result.results);
}

ASYNC MACRO TestCatalogApp()
{
  testfw->BeginWork();
  OBJECT toindexfolder1 := GetTestsuiteTempFolder()->CreateFolder([name := "index-me-1"]);
  OBJECT toindexfolder2 := GetTestsuiteTempFolder()->CreateFolder([name := "index-me-2"]);
  testfw->CommitWork();

  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("consilio"));
  TestEq(1, Length(SELECT * FROM TT("catalogs")->rows WHERE tag = "webhare_testsuite:testsitecatalog"));
  TT("catalogs")->selection := SELECT * FROM TT("catalogs")->rows WHERE tag = "webhare_testsuite:testsitecatalog";

  AWAIT ExpectScreenChange(+1,  PTR TTClick("addcontentsource"));
  TestEq("WebHare website", TT("contentsourcetype")->value);
  TT("folder")->value := toindexfolder1->id;
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  TestEqMembers([[ tag := "/webhare-tests/webhare_testsuite.testsite/tmp/index-me-1/" ]], TT("catalogs")->selection, "*");

  AWAIT ExpectScreenChange(+1,  PTR TTClick("edititem"));
  TestEq(toindexfolder1->id, TT("folder")->value);
  TT("folder")->value := toindexfolder2->id;
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  TestEqMembers([[ tag := "/webhare-tests/webhare_testsuite.testsite/tmp/index-me-2/" ]], TT("catalogs")->selection, "*");

  //TODO check if the content source is indeed restarting, cleaning out old stuff and indexing new!

  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

RunTestFramework( [ PTR TestModernSiteProfileIndices
                  , PTR TestCatalogApp
                  ], [ testusers := [[ login := "sysop", grantrights := ["system:sysop"] ]
                                    ]
                     ]);
