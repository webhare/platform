﻿<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::devsupport.whlib";
LOADLIB "wh::float.whlib";

LOADLIB "mod::consilio/lib/api.whlib";
LOADLIB "mod::consilio/lib/internal/opensearch.whlib";
LOADLIB "mod::consilio/lib/internal/queuemgmt.whlib";

LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/testframework.whlib";

LOADLIB "mod::webhare_testsuite/lib/database.whlib";


OBJECT contentsource;

RECORD ARRAY FUNCTION SearchAll(STRING searchquery, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(
      [ extrafields :=    DEFAULT RECORD
      , storequery :=     FALSE
      ], options);

  RECORD query := CQMatch("_contentsource", "=", contentsource->id);
  IF (searchquery != "")
    query := CQAnd([ query, CQParseUserQuery(searchquery) ]);

  RECORD res := RunConsilioSearch("consilio:testfw_customcontent", query,
      [ first := 0
      , count := 1000
      , mapping :=
          [ groupid :=        ""
          , objectid :=       ""
          , ...options.extrafields
          ]
      , validatemapping := FALSE
      , save_searches := options.storequery
      ]);
  RETURN
      SELECT groupid
           , objectid
           , ...MakeReplacedRecord(options.extrafields, results)
        FROM res.results
    ORDER BY groupid, objectid;
}

// Wait for command completion, then require that index & db are fully synced
MACRO TestCompleteSyncAfterCommand()
{
  contentsource->WaitForIndexingDone();
  TestEq((SELECT groupid, objectid FROM webhare_testsuite.consilio_index ORDER BY groupid, objectid), SearchAll(""));
}

MACRO TestCustomContent()
{
  TestEq(0, __GetNumSubJobs());

  testfw->BeginWork();
  OBJECT catalog := CreateConsilioCatalog("consilio:testfw_customcontent", [ fieldgroups := [ "webhare_testsuite:pagelistfields" ]
                                                                           , priority := 9
                                                                           ]);
  TestEq(0, __GetNumSubJobs());

  RECORD res;

  // Prefix for object/group ids, just to stress the itf
  STRING idprefix := "\n\r\t()[]{}^$+-%!\"";//" \n\r\t()[]{}^$+-%!\"";


  // Clear the current contents
  DELETE FROM webhare_testsuite.consilio_index;

  TestEq(RECORD[], SELECT * FROM catalog->GetExpectedMapping() WHERE name = "mapping_datetime");

  contentsource := catalog->AddCustomContentSource("webhare_testsuite:beta_customsource", "mod::webhare_testsuite/lib/consilio/dbindex.whlib#DBContent",
      [ title :=            "BETA_CUSTOMSOURCE"
      ]);
  TestEq(0, __GetNumSubJobs());

  TestEqMembers(RECORD[[ name := "mapping_datetime", type := "datetime" ]],
                (SELECT * FROM catalog->GetExpectedMapping() WHERE name = "mapping_datetime"), '*');
  TestEqMembers(RECORD[[ name := "_suggested", type := "completion" ]],
                (SELECT * FROM catalog->GetExpectedMapping() WHERE name = "_suggested"), '*', "verify _suggested is in the catalog as expected (though its a low level thingy?)");

  TestThrowsLike(`*contentsource with tag '${"webhare_testsuite:beta_customsource"}' already exists*`, PTR catalog->AddCustomContentSource("webhare_testsuite:beta_customsource", "mod::webhare_testsuite/lib/consilio/dbindex.whlib#DBContent",
      [ title :=            "BETA_CUSTOMSOURCE"
      ]));

  testfw->CommitWork();
  Print(catalog->GetStorageInfo() || "\n");

  TestEq(0, __GetNumSubJobs());
  TestEq(RECORD[], SELECT * FROM contentsource->ListGroups() ORDER BY groupid);

  // Clear the index
  catalog->ReindexCatalog();
  contentsource->WaitForIndexingDone();

  TestEQ(0, LENGTH(SearchAll("")));

  testfw->BeginWork();
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, indexdate, adate, text)
      VALUES(idprefix||"a", idprefix||"a1", MakeDate(1970, 1, 10), MakeDate(2010, 1, 1), "a a1");
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, indexdate, adate, text)
      VALUES(idprefix||"a", idprefix||"a2", MakeDate(1970, 1, 10), MakeDate(2011, 1, 1), "a a2");
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, indexdate, text)
      VALUES(idprefix||"b", idprefix||"b1", MakeDate(1970, 1, 10), "b b1");
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, indexdate, text)
      VALUES(idprefix||"c", idprefix||"c1", MakeDate(1970, 1, 10), "c c1");
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, indexdate, text)
      VALUES(idprefix||"c", idprefix||"c2", MakeDate(1970, 1, 10), "c c2");
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, indexdate, text)
      VALUES(idprefix||"c", idprefix||"c3", MakeDate(1970, 1, 10), "c c3");
  testfw->CommitWork();


  TestEq([[ groupid := '\n\r\t()[]{}^$+-%!\"a' ]
         ,[ groupid := '\n\r\t()[]{}^$+-%!\"b' ]
         ,[ groupid := '\n\r\t()[]{}^$+-%!\"c' ]
         ], SELECT * FROM contentsource->ListGroups() ORDER BY groupid);

  TestEq(0, __GetNumSubJobs());

  // Rebuild the index
  catalog->ReindexCatalog();

  DATETIME start := GetCurrentDatetime();

  TestCompleteSyncAfterCommand();

  // Test logged user queries
  RECORD ARRAY items := ReadJSONLogLines("consilio:queries", start);
  TestEq(0, Length(SELECT FROM items WHERE catalogid = catalog->id));
  start := GetCurrentDatetime();
  RECORD ARRAY results := SearchAll("a", [ storequery := TRUE ]);
  TestEQ((SELECT groupid, objectid FROM webhare_testsuite.consilio_index WHERE groupid = idprefix||"a" ORDER BY groupid, objectid), results);
  items := ReadJSONLogLines("consilio:queries", start);

  RECORD ARRAY queries := SELECT * FROM items WHERE catalogid = catalog->id;
  TestEq(1, Length(queries));
  TestEq([ "a" ], ExtractUserQueries(queries[0].query));
  TestEq(2, queries[0].result.results);

  // Test content source id in custom content source object
  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("indexed_by", "=", contentsource->id), [ first := 0, count := 1000 ]);
  TestEQ(6, res.totalcount);

  // Test date range search
  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("date_adate", ">=", MakeDate(2010,1,1)), [ first := 0, count := 1000 ]);
  TestEQ(2, res.totalcount);
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a1"));
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a2"));

  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("date_adate", ">", MakeDate(2010,1,1)), [ first := 0, count := 1000 ]);
  TestEQ(1, res.totalcount);
  TestEq(FALSE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a1"));
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a2"));

  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("date_adate", "<=", MakeDate(2010,1,1)), [ first := 0, count := 1000 ]);
  TestEQ(1, res.totalcount);
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a1"));
  TestEq(FALSE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a2"));


  // Test result fields
  // By default all known fields are returned
  // Also, if summary_length > 0 (which it is by default), a "summary" is returned
  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("date_adate", "<=", MakeDate(2010,1,1)), [ first := 0, count := 1000 ]);
  TestEq(
      [ groupid := idprefix||"a"
      , objectid := idprefix||"a1"
      , objecturl := "obj://" || idprefix||"a" || "/" || idprefix||"a1" //
      , date_adate := MakeDate(2010, 1, 1)
      , summary := "a a1"
      , indexed_by := ToString(contentsource->id)
      , datafrom := "fetch"
      , mapping_string := ""
      , mapping_boolean := FALSE
      , mapping_integer := 0
      , mapping_integer64 := 0i64
      , mapping_money := 0m
      , mapping_float := 0f
      , mapping_datetime := DEFAULT DATETIME
      , mapping_string_array := STRING[]
      , mapping_boolean_array := BOOLEAN[]
      , mapping_integer_array := INTEGER[]
      , mapping_integer64_array := INTEGER64[]
      , mapping_money_array := MONEY[]
      , mapping_float_array := FLOAT[]
      , mapping_datetime_array := DATETIME[]
      , fieldgroup_integer := 0
      , fieldgroup_integer64 := 0i64
      , fieldgroup_float := 0f
      , fieldgroup_boolean := FALSE
      , fieldgroup_datetime := DEFAULT DATETIME
      , myfield1 := ""
      , _suggested := [ input := ["a","a1"] ]
      ], res.results[0]);

  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("date_adate", "<=", MakeDate(2010,1,1)),
      [ first := 0
      , count := 1000
      , summaryfield := "" // Don't return "summary"
      ]);
  TestEq(
      [ groupid := idprefix||"a"
      , objectid := idprefix||"a1"
      , objecturl := "obj://" || idprefix||"a" || "/" || idprefix||"a1" //
      , date_adate := MakeDate(2010, 1, 1)
      , indexed_by := ToString(contentsource->id)
      , datafrom := "fetch"
      , mapping_string := ""
      , mapping_boolean := FALSE
      , mapping_integer := 0
      , mapping_integer64 := 0i64
      , mapping_money := 0m
      , mapping_float := 0f
      , mapping_datetime := DEFAULT DATETIME
      , mapping_string_array := STRING[]
      , mapping_boolean_array := BOOLEAN[]
      , mapping_integer_array := INTEGER[]
      , mapping_integer64_array := INTEGER64[]
      , mapping_money_array := MONEY[]
      , mapping_float_array := FLOAT[]
      , mapping_datetime_array := DATETIME[]
      , fieldgroup_integer := 0
      , fieldgroup_integer64 := 0i64
      , fieldgroup_float := 0f
      , fieldgroup_boolean := FALSE
      , fieldgroup_datetime := DEFAULT DATETIME
      , myfield1 := ""
      , _suggested := [ input := ["a","a1"] ]
      ], res.results[0]);

  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("date_adate", "<=", MakeDate(2010,1,1)),
      [ first := 0
      , count := 1000
      , mapping := CELL[] // By default "groupid" and "objectid" are returned (using empty record, because default record returns all fields)
      , summaryfield := "" // Don't return "summary"
      ]);
  TestEq(
      [ groupid := idprefix||"a"
      , objectid := idprefix||"a1"
      ], res.results[0]);

  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("date_adate", "<=", MakeDate(2010,1,1)),
      [ first := 0
      , count := 1000
      , mapping := CELL[] // By default "groupid" and "objectid" are returned
      , scorefield := "score" // Add the result score in the "score" field
      , summaryfield := "" // Don't return "summary"
      ]);
  TestEq(
      [ groupid := idprefix||"a"
      , objectid := idprefix||"a1"
      ], CELL[ ...res.results[0], DELETE score ]);
  TestEq(TRUE, res.results[0].score > 0);

  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("date_adate", "<=", MakeDate(2010,1,1)),
      [ first := 0
      , count := 1000
      , mapping := [ date_adate := DEFAULT DATETIME ] // Return custom field "date_adate"
      , summaryfield := "" // Don't return "summary"
      ]);
  TestEq(
      [ groupid := idprefix||"a"
      , objectid := idprefix||"a1"
      , date_adate := MakeDate(2010, 1, 1)
      ], res.results[0]);

  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("date_adate", "<=", MakeDate(2010,1,1)),
      [ first := 0
      , count := 1000
      , mapping := [ nonexisting := "doesn't exist" ] // Undefined field is filled with default value in Consilio
      , summaryfield := "" // Don't return "summary"
      ]);
  TestEq(
      [ groupid := idprefix||"a"
      , objectid := idprefix||"a1"
      , nonexisting := "doesn't exist"
      ], res.results[0]);

  //Find nothing
  res := RunConsilioSearch("consilio:testfw_customcontent", DEFAULT RECORD);
  TestEq(0,Length(res.results));

  res := RunConsilioSearch("consilio:testfw_customcontent", CQAll());
  TestEq(6,Length(res.results));

  res := RunConsilioSearch("consilio:testfw_customcontent", CQParseUserQuery(""));
  TestEq(0,Length(res.results));

  // Rebuild testing update object, delete object, delete group, keep group, add group/object
  testfw->BeginWork();
  UPDATE webhare_testsuite.consilio_index
     SET indexdate := MakeDate(1970, 1, 11)
   WHERE objectid = idprefix||"a1";
  DELETE FROM webhare_testsuite.consilio_index WHERE objectid = idprefix||"a2";
  DELETE FROM webhare_testsuite.consilio_index WHERE groupid = idprefix||"b";
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, indexdate)
      VALUES(idprefix||"d", idprefix||"d1", MakeDate(1970, 1, 10));
  testfw->CommitWork();

  catalog->ReindexCatalog();

  TestCompleteSyncAfterCommand();

  // check a single group
  testfw->BeginWork();
  UPDATE webhare_testsuite.consilio_index
     SET indexdate := MakeDate(1970, 1, 10)
   WHERE objectid = idprefix||"a1";
  testfw->CommitWork();

  contentsource->ReindexGroup(idprefix||"a", [ rebuild := TRUE ]);

  TestCompleteSyncAfterCommand();

  // check a single object
  testfw->BeginWork();
  UPDATE webhare_testsuite.consilio_index
     SET indexdate := MakeDate(1970, 1, 11)
   WHERE objectid = idprefix||"a1";
  testfw->CommitWork();

  contentsource->ReindexObject(idprefix||"a", idprefix||"a1");

  TestCompleteSyncAfterCommand();

  // delete a single group & object, check new object & group
  testfw->BeginWork();
  DELETE FROM webhare_testsuite.consilio_index
   WHERE objectid = idprefix||"a1";
  DELETE FROM webhare_testsuite.consilio_index
   WHERE objectid = idprefix||"d1";
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, indexdate)
      VALUES(idprefix||"e", idprefix||"e1", MakeDate(1970, 1, 10));
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, indexdate)
      VALUES(idprefix||"f", idprefix||"f1", MakeDate(1970, 1, 10));
  testfw->CommitWork();

  contentsource->UpdateMultiple([[ action := "delete", groupid := idprefix||"a" ]
                                ,[ action := "delete", objectid := idprefix||"d1" ]
                                ,[ action := "reindex", groupid := idprefix||"e" ]
                                ,[ action := "reindex", groupid := idprefix||"f", objectid :=  idprefix||"f1" ]
                                ]);
  TestCompleteSyncAfterCommand();

  // Updates
  testfw->BeginWork();
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, indexdate)
      VALUES(idprefix||"a", idprefix||"a1", MakeDate(1970, 1, 10));
  UPDATE webhare_testsuite.consilio_index
     SET indexdate :=                   MakeDate(1970, 1, 11)
       , objectrequiredindexdate :=     MakeDate(1970, 1, 10)
       , grouprequiredindexdate :=      MakeDate(1970, 1, 12)
   WHERE objectid = idprefix||"c1";
  UPDATE webhare_testsuite.consilio_index
     SET indexdate :=                   MakeDate(1970, 1, 11)
       , objectrequiredindexdate :=     MakeDate(1970, 1, 12)
       , grouprequiredindexdate :=      MakeDate(1970, 1, 10)
   WHERE objectid = idprefix||"c2";
  UPDATE webhare_testsuite.consilio_index
     SET indexdate :=                   MakeDate(1970, 1, 11)
       , objectrequiredindexdate :=     MakeDate(1970, 1, 12)
       , grouprequiredindexdate :=      MakeDate(1970, 1, 12)
   WHERE objectid = idprefix||"c3";
  DELETE FROM webhare_testsuite.consilio_index
   WHERE objectid = idprefix||"e1";

  testfw->CommitWork();

  catalog->ReindexCatalog();
  contentsource->WaitForIndexingDone();

  TestEq(   //This test was sensitive to the difference between update and rebuild
      [ [ groupid := idprefix||"a",   objectid := idprefix||"a1" ]
      , [ groupid := idprefix||"c",   objectid := idprefix||"c1" ]
      , [ groupid := idprefix||"c",   objectid := idprefix||"c2" ]
      , [ groupid := idprefix||"c",   objectid := idprefix||"c3" ]
      , [ groupid := idprefix||"f",   objectid := idprefix||"f1" ]
      ], SearchAll(""));

  // Prepare for data in list tests
  testfw->BeginWork();
  DELETE FROM webhare_testsuite.consilio_index;

  {
    INTEGER b1_id := MakeAutonumber(webhare_testsuite.consilio_index, "id");
    FOREVERY (RECORD rec FROM
        [ [ groupid := idprefix||"a",   objectid := idprefix||"a1_withdata" ]
        , [ groupid := idprefix||"b",   objectid := idprefix||"b1_withdata", id := b1_id ]
        , [ groupid := idprefix||"c",   objectid := idprefix||"c1" ]
        , [ groupid := idprefix||"d",   objectid := idprefix||"d1_withdata_fromlist" ]
        ])
    {
      INSERT rec INTO webhare_testsuite.consilio_index;
    }
  }
  testfw->CommitWork();

  catalog->ReindexCatalog();
  contentsource->WaitForIndexingDone();

  TestEQ(
      [ [ groupid := idprefix||"a",   objectid := idprefix||"a1_withdata",          datafrom := "list" ]
      , [ groupid := idprefix||"b",   objectid := idprefix||"b1_withdata",          datafrom := "list" ]
      , [ groupid := idprefix||"c",   objectid := idprefix||"c1",                   datafrom := "fetch" ]
      , [ groupid := idprefix||"d",   objectid := idprefix||"d1_withdata_fromlist", datafrom := "list" ]
      ], SearchAll("", [ extrafields := [ datafrom := "" ] ]));

  contentsource->ReindexObject(idprefix||"a", idprefix||"a1_withdata");
  contentsource->ReindexObject(idprefix||"d", idprefix||"d1_withdata_fromlist");
  contentsource->WaitForIndexingDone();

  TestEQ(
      [ [ groupid := idprefix||"a",   objectid := idprefix||"a1_withdata",          datafrom := "fetch" ]
      , [ groupid := idprefix||"b",   objectid := idprefix||"b1_withdata",          datafrom := "list" ]
      , [ groupid := idprefix||"c",   objectid := idprefix||"c1",                   datafrom := "fetch" ]
      , [ groupid := idprefix||"d",   objectid := idprefix||"d1_withdata_fromlist", datafrom := "fetchfromlist-list" ]
      ], SearchAll("", [ extrafields := [ datafrom := "" ] ]));

  // Clear for error tests
  testfw->BeginWork();
  DELETE FROM webhare_testsuite.consilio_index;
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, indexdate, text)
      VALUES(idprefix||"b", idprefix||"b1", MakeDate(1970, 1, 10), "");
  testfw->CommitWork();

  catalog->ReindexCatalog();
  TestCompleteSyncAfterCommand();

  // Throw in fetchobject; old content must remain & error must be reported
  DATETIME starttest := GetCurrentDatetime();

  testfw->BeginWork();
  UPDATE webhare_testsuite.consilio_index
     SET indexdate := MakeDate(1970, 1, 11)
       , text:=       "THROW";
  testfw->CommitWork();

  __ConsilioClearErrors(contentsource->id);
  catalog->ReindexCatalog();
  contentsource->WaitForIndexingDone();

  RECORD ARRAY relevanterrors  := ReadJSONLogLines("system:notice", starttest);
  relevanterrors := SELECT * FROM relevanterrors WHERE type='script-error' AND CellExists(relevanterrors,'info') AND info.context='consilio:fetcher';

  TestEq(1,Length(relevanterrors), "Cannot find my error - may also imply a backwards-incompatible notice.log error format change!");
  TestEq("Exception: THROWN", relevanterrors[0].errors[0].message);
  TestEQ(
      [ [ groupid := idprefix||"b",   objectid := idprefix||"b1" ]
      ], SearchAll(""));

  // Abort in fetchobject; old content must remain & error must be reported
  starttest := GetCurrentDatetime();
  testfw->BeginWork();
  UPDATE webhare_testsuite.consilio_index
     SET indexdate := MakeDate(1970, 1, 11)
       , text:=       "ABORT";
  testfw->CommitWork();

  __ConsilioClearErrors(contentsource->id);
  catalog->ReindexCatalog();
  contentsource->WaitForIndexingDone();

  relevanterrors := ReadJSONLogLines("system:notice", starttest);
  relevanterrors := SELECT * FROM relevanterrors WHERE type='script-error' AND CellExists(relevanterrors,'info') AND info.context='consilio:fetcher';
  TestEq(1,Length(relevanterrors));
  TestEq("Custom error message: 'ABORT'.", relevanterrors[0].errors[0].message);
  TestEQ(
      [ [ groupid := idprefix||"b",   objectid := idprefix||"b1" ]
      ], SearchAll(""));

  // Also test the foreground version
  TestThrowsLike("*ABORT*", PTR contentsource->ReindexGroup(idprefix||"b", [ foreground := TRUE ]));

  TestEQ(
      [ [ groupid := idprefix||"b",   objectid := idprefix||"b1" ]
      ], SearchAll(""));


  // Prepare for field existence query tests
  testfw->BeginWork();
  DELETE FROM webhare_testsuite.consilio_index;
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, indexdate, adate)
      VALUES(idprefix||"a", idprefix||"a1", MakeDate(1970, 1, 10), MakeDate(2010, 1, 1));
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, indexdate, adate, text)
      VALUES(idprefix||"b", idprefix||"b1", MakeDate(1970, 1, 10), MakeDate(2010, 2, 2), "b b1");
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, indexdate, text)
      VALUES(idprefix||"c", idprefix||"c1", MakeDate(1970, 1, 10), "c c1");
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, indexdate)
      VALUES(idprefix||"d", idprefix||"d1", MakeDate(1970, 1, 10));
  testfw->CommitWork();

  catalog->ReindexCatalog();
  contentsource->WaitForIndexingDone();

  TestEQ(
      [ [ groupid := idprefix||"a", objectid := idprefix||"a1", date_adate := MakeDate(2010, 1, 1) ]
      , [ groupid := idprefix||"b", objectid := idprefix||"b1", date_adate := MakeDate(2010, 2, 2) ]
      , [ groupid := idprefix||"c", objectid := idprefix||"c1", date_adate := DEFAULT DATETIME ]
      , [ groupid := idprefix||"d", objectid := idprefix||"d1", date_adate := DEFAULT DATETIME ]
      ], SearchAll("", [ extrafields := [ date_adate := DEFAULT DATETIME ] ]));

  // Test exists queries
  res := RunConsilioSearch("consilio:testfw_customcontent", CQHas("date_adate"), [ first := 0, count := 1000 ]);
  TestEQ(2, res.totalcount);
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a1"));
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"b1"));

  res := RunConsilioSearch("consilio:testfw_customcontent", CQHas("body"), [ first := 0, count := 1000 ]);
  TestEQ(2, res.totalcount);
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"b1"));
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"c1"));

  res := RunConsilioSearch("consilio:testfw_customcontent", CQOr([ CQHas("body"), CQHas("date_adate") ]), [ first := 0, count := 1000 ]);
  TestEQ(3, res.totalcount);
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a1"));
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"b1"));
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"c1"));

  res := RunConsilioSearch("consilio:testfw_customcontent", CQAnd([ CQHas("body"), CQHas("date_adate") ]), [ first := 0, count := 1000 ]);
  TestEQ(1, res.totalcount);
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"b1"));

  res := RunConsilioSearch("consilio:testfw_customcontent", CQNot(CQHas("body")), [ first := 0, count := 1000 ]);
  TestEQ(2, res.totalcount);
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a1"));
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"d1"));

  res := RunConsilioSearch("consilio:testfw_customcontent", CQAnd([ CQMatch("_contentsource", "=", contentsource->id), CQHas("date_adate") ]));
  TestEQ(2, res.totalcount);
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a1"));
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"b1"));

  // Prepare for non-ASCII query tests
  testfw->BeginWork();
  DELETE FROM webhare_testsuite.consilio_index;
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, indexdate, adate, text)
      VALUES(idprefix||"a", idprefix||"a1", MakeDate(1970, 1, 10), MakeDate(2010, 1, 1), "å1 🍻");
  testfw->CommitWork();

  catalog->ReindexCatalog();
  contentsource->WaitForIndexingDone();

  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("body", "CONTAINS", "a1"), [ first := 0, count := 1000 ]);
  TestEQ(1, res.totalcount);
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a1"));

  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("body", "CONTAINS", "å1"), [ first := 0, count := 1000 ]);
  TestEQ(1, res.totalcount);
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a1"));

  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("body", "=", "a1"), [ first := 0, count := 1000 ]);
  TestEQ(1, res.totalcount);
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a1"));

  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("body", "=", "å1"), [ first := 0, count := 1000 ]);
  TestEQ(1, res.totalcount);
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a1"));

  // OpenSearch searches for the term 'å1"\r\n€', which isn't indexed
  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("body", "=", "å1\"\r\n€"), [ first := 0, count := 1000 ]);
  TestEq(0, res.totalcount);

  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("body", "=", "🍻"), [ first := 0, count := 1000 ]);
  TestEq(1, res.totalcount);
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a1"));

  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("body", "CONTAINS", "🍻"), [ first := 0, count := 1000 ]);
  TestEq(1, res.totalcount);
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a1"));

  res := RunConsilioSearch("consilio:testfw_customcontent", CQParseUserQuery("🍻"), [ first := 0, count := 1000 ]);
  TestEq(1, res.totalcount);
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = idprefix||"a1"));

  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("body", "=", "🍺"), [ first := 0, count := 1000 ]);
  TestEq(0, res.totalcount);

  // OpenSearch has a limit on the length of a field that is used for summary generating/search result highlighting. This
  // limit is 1000000 characters by default. The text "JUMBOTEXT" creates a long (>1000000 bytes) body, which should not
  // result in an error "The length of [body] field of [0] doc of [c_169796706807a7a5e599837bf630d4c1] index has exceeded
  // [1000000] - maximum allowed to be analyzed for highlighting". The 'index_options := "offsets"' setting of text fields
  // should prevent this error and return the indexed document.
  testfw->BeginWork();
  DELETE FROM webhare_testsuite.consilio_index;
  INSERT INTO webhare_testsuite.consilio_index(groupid, objectid, text)
       VALUES(idprefix||"a", idprefix||"a1", "JUMBOTEXT");
  testfw->CommitWork();

  catalog->ReindexCatalog();
  contentsource->WaitForIndexingDone();

  res := RunConsilioSearch("consilio:testfw_customcontent", CQMatch("body", "=", "aap"), [ first := 0, count := 1000 ]);
  TestEq(1, res.totalcount);

  testfw->BeginWork();
  DELETE FROM webhare_testsuite.consilio_index;
  testfw->CommitWork();

  catalog->ReindexCatalog();
  contentsource->WaitForIndexingDone();
}

MACRO TestTypes()
{
  OBJECT catalog := OpenConsilioCatalog("consilio:testfw_customcontent");
  OBJECT maincatalog := OpenConsilioCatalog("webhare_testsuite:testsitecatalog");
  OBJECT maincontentsource := maincatalog->OpenContentSource("webhare_testsuite:dbindex");

  //having indexnames makes log analysis easier
  Print(maincatalog->GetStorageInfo() || ", " || catalog->GetStorageInfo() || "\n");

  // Prefix for object/group ids, just to stress the itf
  STRING idprefix := "\n\r\t()[]{}^$+-%!\"";//" \n\r\t()[]{}^$+-%!\"";

  testfw->BeginWork();

  // Clear the current contents
  DELETE FROM webhare_testsuite.consilio_index;

  // Add some test data
  DATETIME now := GetCurrentDateTime();
  INSERT
      [ groupid := idprefix || "a"
      , objectid := idprefix || "a1"
      , extradata := EncodeHSON(
          [ mapping_string := "lorem ipsum dolor sit amet"
          , mapping_integer := 2147483647
          , mapping_integer64 := 9223372036854775807i64//TODO: Any number outside the 32-bit INTEGER range is decoded as a FLOAT
          , mapping_money := 90071992547.40991m // A scaled float is returned as a double with 53-bit precision (and decoded as a FLOAT anyway)
          , mapping_float := 9223372036854775808
          , mapping_boolean := TRUE
          , mapping_datetime := MakeDateTime(2000, 1, 1, 12, 34, 56, 789)
          , mapping_string_array := [ "lorem ipsum", "dolor sit amet" ]
          , mapping_integer_array := INTEGER[ 0, 1, 1, 2, 3, 5, 8 ]
          , mapping_integer64_array := INTEGER64[ 9876543210i64, 7521 ]
          , mapping_money_array := [ 3.14, 5, 90071992547.40990m, 45035996273.70496m ]
          , mapping_float_array := [ pi, 5 ]
          , mapping_boolean_array := [ FALSE, TRUE, FALSE ]
          , mapping_datetime_array := [ MakeDateTime(2000, 1, 1, 12, 34, 56, 789), DEFAULT DATETIME, now ]
          , fieldgroup_integer := 1234
          , fieldgroup_integer64 := 2147483648i64
          , fieldgroup_float := 2.20371f
          , fieldgroup_boolean := TRUE
          , fieldgroup_datetime := MakeDate(2038, 1, 19)
          ])
      ] INTO webhare_testsuite.consilio_index;

  testfw->CommitWork();

  catalog->ReindexCatalog();
  maincatalog->ReindexCatalog();
  contentsource->WaitForIndexingDone();
  maincontentsource->WaitForIndexingDone();

  RECORD res := RunConsilioSearch(catalog->tag, CQMatch("objectid", "=", idprefix || "a1"));
  TestEq(
      [ groupid := idprefix || "a"
      , objectid := idprefix || "a1"
      , objecturl := "obj://" || idprefix || "a" || "/" || idprefix || "a1" //
      , date_adate := DEFAULT DATETIME
      , summary := ""
      , indexed_by := ToString(contentsource->id)
      , datafrom := "fetch"
      , mapping_string := "lorem ipsum dolor sit amet"
      , mapping_integer := 2147483647
      , mapping_integer64 := 9223372036854775807i64
      , mapping_money := 90071992547.40991m
      , mapping_float := 9223372036854775808
      , mapping_boolean := TRUE
      , mapping_datetime := MakeDateTime(2000, 1, 1, 12, 34, 56, 789)
      , mapping_string_array := [ "lorem ipsum", "dolor sit amet" ]
      , mapping_boolean_array := [ FALSE, TRUE, FALSE ]
      , mapping_integer_array := INTEGER[ 0, 1, 1, 2, 3, 5, 8 ]
      , mapping_integer64_array := INTEGER64[ 9876543210i64, 7521 ]
      , mapping_money_array := MONEY[ 3.14, 5m, 90071992547.40990m, 45035996273.70496m ]
      , mapping_float_array := FLOAT[ pi, 5f ]
      , mapping_datetime_array := [ MakeDateTime(2000, 1, 1, 12, 34, 56, 789), DEFAULT DATETIME, now ]
      , fieldgroup_integer := 1234
      , fieldgroup_integer64 := 2147483648i64
      , fieldgroup_float := 2.20371f
      , fieldgroup_boolean := TRUE
      , fieldgroup_datetime := MakeDate(2038, 1, 19)
      , myfield1 := ""
      , _suggested := DEFAULT RECORD
      ], res.results[0]);

  res := RunConsilioSearch(catalog->tag, CQMatch("mapping_string", "=", "lorem ipsum"));
  TestEq(0, res.totalcount);
  res := RunConsilioSearch(catalog->tag, CQMatch("mapping_string", "=", "lorem ipsum dolor sit amet"));
  TestEq(1, res.totalcount);
  res := RunConsilioSearch(catalog->tag, CQMatch("mapping_string_array", "=", "lorem ipsum"));
  TestEq(1, res.totalcount);
  res := RunConsilioSearch(catalog->tag, CQMatch("mapping_string_array", "=", "lorem ipsum dolor sit amet"));
  TestEq(0, res.totalcount);
  res := RunConsilioSearch(catalog->tag, CQMatch("mapping_integer64", "=", 9223372036854775807i64));
  TestEq(1, res.totalcount);
  res := RunConsilioSearch(catalog->tag, CQMatch("mapping_integer64_array", "CONTAINS", 7521));
  TestEq(1, res.totalcount);
  res := RunConsilioSearch(catalog->tag, CQMatch("mapping_integer64", "<=", 9223372036854775807i64));
  TestEq(1, res.totalcount);
  res := RunConsilioSearch(catalog->tag, CQMatch("mapping_integer64", ">=", 9223372036854775807i64));
  TestEq(1, res.totalcount);
  res := RunConsilioSearch(catalog->tag, CQMatch("mapping_integer64", "<", 9223372036854775807i64));
  TestEq(0, res.totalcount);
  res := RunConsilioSearch(catalog->tag, CQMatch("mapping_integer64", ">", 9223372036854775806i64));
  TestEq(1, res.totalcount);
  res := RunConsilioSearch(catalog->tag, CQMatch("mapping_money", "=", 90071992547.40991m));
  TestEq(1, res.totalcount);
  res := RunConsilioSearch(catalog->tag, CQMatch("mapping_money_array", "CONTAINS", 3.14m));
  TestEq(1, res.totalcount);
  res := RunConsilioSearch(catalog->tag, CQMatch("mapping_money", "<=", 90071992547.40991m));
  TestEq(1, res.totalcount);
  res := RunConsilioSearch(catalog->tag, CQMatch("mapping_money", ">=", 90071992547.40991m));
  TestEq(1, res.totalcount);
  res := RunConsilioSearch(catalog->tag, CQMatch("mapping_money", "<", 90071992547.40991m));
  TestEq(0, res.totalcount);
  res := RunConsilioSearch(catalog->tag, CQMatch("mapping_money", ">", 90071992547.40990m));
  TestEq(1, res.totalcount);

  res := RunConsilioSearch(maincatalog->tag, CQMatch("objectid", "=", idprefix || "a1"));
  TestEq(
      [ groupid := idprefix || "a"
      , objectid := idprefix || "a1"
      , objecturl := "obj://" || idprefix || "a" || "/" || idprefix || "a1" //
      , date_adate := DEFAULT DATETIME
      , summary := ""
      , indexed_by := ToString(maincontentsource->id)
      , datafrom := "fetch"
      , mapping_string := "lorem ipsum dolor sit amet"
      , mapping_integer := 2147483647
      , mapping_integer64 := 9223372036854775807i64
      , mapping_money := 90071992547.40991m
      , mapping_float := 9223372036854775808
      , mapping_boolean := TRUE
      , mapping_datetime := MakeDateTime(2000, 1, 1, 12, 34, 56, 789)
      , mapping_string_array := [ "lorem ipsum", "dolor sit amet" ]
      , mapping_boolean_array := [ FALSE, TRUE, FALSE ]
      , mapping_integer_array := INTEGER[ 0, 1, 1, 2, 3, 5, 8 ]
      , mapping_integer64_array := INTEGER64[ 9876543210i64, 7521 ]
      , mapping_money_array := MONEY[ 3.14, 5m, 90071992547.40990m, 45035996273.70496m ]
      , mapping_float_array := FLOAT[ pi, 5f ]
      , mapping_datetime_array := [ MakeDateTime(2000, 1, 1, 12, 34, 56, 789), DEFAULT DATETIME, now ]
      , fieldgroup_integer := 1234
      , fieldgroup_integer64 := 2147483648i64
      , fieldgroup_float := 2.20371f
      , fieldgroup_boolean := TRUE
      , fieldgroup_datetime := MakeDate(2038, 1, 19)
      , myfield1 := ""

      , modificationdate := DEFAULT DATETIME
      , date_test := DEFAULT DATETIME
      , description := ""
      , filetype := ""
      , keywords := ""
      , size := 0
      , title := ""
      , url := ""
      , validuser := ""
      , whfspath := ""
      , _suggested := DEFAULT RECORD
      ], res.results[0]);
}

RunTestframework(
    [ PTR TestCustomContent
    , PTR TestTypes
    ],
    [ testusers :=
        [ [ login := "sysop", grantrights := [ "system:sysop" ] ]
        ]
    ]);
