<?wh

LOADLIB "wh::files.whlib";

LOADLIB "mod::system/lib/testframework.whlib";

LOADLIB "mod::consilio/lib/parsers/parser_html.whlib";


MACRO TestParserHtml()
{
  // Regression: Consilio returned the text of all 'title' texts as title instead of only html>head>title
  RECORD parsed := ParseHTMLPage(StringToBlob(`<!doctype html>
    <html>
      <head>
        <title>test</title>
      </head>
      <body>
        <svg>
          <title>nope</title>
        </svg>
      </body>
    </html>`),TRUE);
  TestEq("test", parsed.title);

  // Treat <li> as a block element
  parsed := ParseHTMLPage(StringToBlob(`<!doctype html>
    <html>
      <body>
        <ul>
          <li>aap</li><li>noot</li><li>mies</li>
        </ul>
      </body>
    </html>`),TRUE);
  TestEq("aap noot mies", parsed.text);

  // consilio fields (legacy and new)
  parsed := ParseHTMLPage(StringToBlob(`<!doctype html>
    <html>
      <head>
        <meta name="consilio-firstfield" content="first" />
        <meta name="consilio-firstfield" content="keepme" />
        <meta name="consilio-pagekeywords" content="overwriteme" />
      </head>
      <body>
        <ul>
          <li>aap</li><li>noot</li><li>mies</li>
        </ul>
        <script type="application/x-hson" id="wh-consiliofields">hson:{"pagekeywords":sa["by-name testfile6.rtd","<\\/script>","by-id 60039"]}</script>
      </body>
    </html>`),TRUE);
  TestEq("aap noot mies", parsed.text);
  TestEq([[ name := "firstfield", value := "keepme" ]
         ,[ name := "pagekeywords", value := ["by-name testfile6.rtd","</script>","by-id 60039"]]
         ], parsed.extrafields);

  // Regression: Consilio returned the text of all 'title' texts as title instead of only html>head>title
  parsed := ParseHTMLPage(StringToBlob(`<!doctype html>
    <html>
      <head>
        <title>test\uFFFDtitle</title>
      </head>
      <body>
        <p>
          test\uFFFDbody
        </p>
      </body>
    </html>`),TRUE);
  TestEq("test title", parsed.title);
  TestEq("test body", parsed.text);
}

RunTestframework([ PTR TestParserHtml
                 ]
                 );
