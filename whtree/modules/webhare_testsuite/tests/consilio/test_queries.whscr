<?wh

LOADLIB "wh::devsupport.whlib";

LOADLIB "mod::consilio/lib/api.whlib";

LOADLIB "mod::system/lib/testframework.whlib";

LOADLIB "mod::webhare_testsuite/lib/database.whlib";


STRING ARRAY FUNCTION ExecTestQuery(RECORD inquery)
{
  RECORD searchres := RunConsilioSearch("consilio:testfw_queries", inquery, [ debug := TRUE ]);
  RETURN SELECT AS STRING ARRAY groupid FROM searchres.results ORDER BY groupid;
}

RECORD FUNCTION ExecTestAggregation(RECORD inquery, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  BOOLEAN multipleresults := inquery.aggregation._type IN [ "terms", "top_hits" ];
  RECORD searchres := RunConsilioSearch("consilio:testfw_queries", inquery, CELL[ summaryfield := "", debug := TRUE, ...options ]);
  RECORD result;
  IF (CellExists(options, "aggregation_mapping"))
  {
    result := CELL
        [ searchres.totalcount
        , results := (SELECT AS STRING ARRAY groupid FROM searchres.results ORDER BY groupid)
        ];
    IF (multipleresults)
      INSERT CELL aggregation := searchres.aggregation INTO result;
    ELSE
      INSERT CELL aggregation := searchres.aggregation[0] INTO result;
  }
  ELSE
  {
    result := CELL
        [ searchres.totalcount
        ];
    IF (multipleresults)
      INSERT CELL aggregation := searchres.results INTO result;
    ELSE
      INSERT CELL aggregation := searchres.results[0] INTO result;
  }
  RETURN result;
}

MACRO PrepareCustomCatalog()
{
  TestEq(0, __GetNumSubJobs());

  testfw->BeginWork();
  OBJECT catalog := CreateConsilioCatalog("consilio:testfw_queries", [ priority := 9, fieldgroups := [ "webhare_testsuite:nested_records" ] ]);
  Print(catalog->GetStorageInfo() || "\n");
  testfw->CommitWork();

  //TODO can we remove this intermediate work? it'll probably require the fetcher to not start indexing objects until the index attachments are complete
  testfw->BeginWork();
  DELETE FROM webhare_testsuite.consilio_index;
  OBJECT contentsource := catalog->AddCustomContentSource("webhare_testsuite:beta_customsource", Resolve("data/querycontent.whlib#QueryContent"),
      [ title :=            "BETA_CUSTOMSOURCE"
      ]);

  RECORD ARRAY mapping := catalog->GetExpectedMapping();
  // dumpvalue(mapping,'tree');

  TestEqMembers([[ name := "textfield", type := "text" ]], (SELECT * FROM mapping WHERE name = "textfield"),"*");
  TestEqMembers([[ name := "keywordfield", type := "keyword" ]], (SELECT * FROM mapping WHERE name = "keywordfield"),"*");
  TestEqMembers([[ name := "myrec", type := "record"
                 , properties := [[ name := "reckeyword", type := "keyword", defaultvalue := STRING[] ]]
                ]], (SELECT * FROM mapping WHERE name = "myrec"),"*");
  testfw->CommitWork();

  TestEq([[ groupid := "QC1" ], [ groupid := "QC2" ], [ groupid := "QC3" ]],
         SELECT * FROM contentsource->ListGroups() ORDER BY groupid);
  contentsource->WaitForIndexingDone();
}

MACRO RunTheMatchQueries()
{
  TestThrowsLike("No such*nosuchfield*", PTR RunConsilioSearch("consilio:testfw_queries", CQMatch("nosuchfield", "=", "test2")));

  //CQMatch is for keyword fields (inequality comparisions make sense here), CQContains will be for text fields
  TestEq(["QC1"], ExecTestQuery(CQMatch("groupid", "=", "QC1")));
  TestEq(["QC2","QC2"], ExecTestQuery(CQMatch("groupid", "=", "QC2")));
  TestEq(["QC2","QC2","QC3"], ExecTestQuery(CQMatch("groupid", "IN", ["QC3","QC2"])));

  //Keywords like equality operators
  TestEq(STRING[], ExecTestQuery(CQMatch("shortkeyword", "=", "zero")));
  TestEq(["QC1"], ExecTestQuery(CQMatch("shortkeyword", "=", "eins")));

  TestEq(["QC1"], ExecTestQuery(CQMatch("keywordfield", "=", "keyword number one")));
  //opensearch supports treating space-separated as opaque in keyword fields
  TestEq(STRING[], ExecTestQuery(CQMatch("keywordfield", "=", "Keyword number one")), "keywordfields are case sensitive");
  TestEq(STRING[], ExecTestQuery(CQMatch("keywordfield", "=", "keyword")), "Must match exactly");

  //Inequality
  TestEq(["QC1","QC2","QC2"], ExecTestQuery(CQMatch("shortkeyword", ">=", "eins")));
  TestEq(["QC2","QC2"], ExecTestQuery(CQMatch("shortkeyword", ">", "eins")));
  TestEq(["QC1","QC3"], ExecTestQuery(CQMatch("shortkeyword", "<", "zwei")));
  TestEq(["QC1","QC2","QC3"], ExecTestQuery(CQMatch("shortkeyword", "<=", "zwei")));

  //opensearch supports treating space-separated as opaque in keyword fields
  TestEq(["QC1","QC2","QC2","QC3"], ExecTestQuery(CQMatch("keywordfield", ">=", "keyword number one")));
  TestEq(["QC2","QC2","QC3"], ExecTestQuery(CQMatch("keywordfield", ">", "keyword number one")));
  TestEq(["QC1","QC3"], ExecTestQuery(CQMatch("keywordfield", "<", "keyword number two")));
  TestEq(["QC1","QC2","QC3"], ExecTestQuery(CQMatch("keywordfield", "<=", "keyword number two")));

  TestEqMembers([ results := [[ groupid := "QC1"
                              , myrec := [ reckeyword := ["rk1","rk2" ] ]
                             ]]
                ], RunConsilioSearch("consilio:testfw_queries", CQMatch("groupid", "=", "QC1")), "*");

  // Regression: defaultfields without boost
  TestEq(["QC1"], ExecTestQuery(CQParseUserQuery("one", [ defaultfields := [ [ field := "textfield" ] ] ])));
}

MACRO RunTheAggregationQueries()
{
  // Terms aggregation
  TestEq([ totalcount := 4, aggregation :=
             [ [ groupid := "QC1" ]
             , [ groupid := "QC2" ]
             , [ groupid := "QC3" ]
             ]
         ],
         ExecTestAggregation(CQAggregate("groupid", "terms"),
         [ mapping := [ groupid := "" ] ]));
  TestEq([ totalcount := 4, aggregation :=
             [ [ groupid := "QC1", numdocs := 1 ]
             , [ groupid := "QC2", numdocs := 2 ]
             , [ groupid := "QC3", numdocs := 1 ]
             ]
         ],
         ExecTestAggregation(CQAggregate("groupid", "terms", [ countfield := "numdocs" ]),
         [ mapping := [ groupid := "", numdocs := 0 ] ]));
  TestEq([ totalcount := 3, aggregation :=
             [ [ groupid := "QC1", numdocs := 1 ]
             , [ groupid := "QC2", numdocs := 2 ]
             ]
         ],
         ExecTestAggregation(CQAggregate("groupid", "terms", [ countfield := "numdocs", query := CQMatch("shortkeyword", ">=", "eins") ]),
         [ mapping := [ groupid := "", numdocs := 0 ] ]));
  TestEq([ totalcount := 3, aggregation :=
             [ [ groupid := "QC1", numdocs := 1 ]
             , [ groupid := "QC2", numdocs := 1 ]
             , [ groupid := "QC3", numdocs := 1 ]
             ]
         ],
         ExecTestAggregation(CQAggregate("groupid", "terms", [ countfield := "numdocs", query := CQMatch("shortkeyword", "<", "zweite") ]),
         [ mapping := [ groupid := "", numdocs := 0 ] ]));
  TestEq([ totalcount := 4, aggregation :=
             [ [ groupid := "QC3" ]
             , [ groupid := "QC2" ]
             , [ groupid := "QC1" ]
             ]
         ],
         ExecTestAggregation(CQAggregate("groupid", "terms", [ orderdesc := TRUE ]),
         [ mapping := [ groupid := "" ] ]));
  TestEq([ totalcount := 4, aggregation :=
             [ [ groupid := "QC1" ]
             ]
         ],
         ExecTestAggregation(CQAggregate("groupid", "terms", [ size := 1 ]),
         [ mapping := [ groupid := "" ] ]));
  TestEq([ totalcount := 4, aggregation :=
             [ [ groupid := "QC3" ]
             ]
         ],
         ExecTestAggregation(CQAggregate("groupid", "terms", [ orderdesc := TRUE, size := 1 ]),
         [ mapping := [ groupid := "" ] ]));
  // Terms aggregation with terms subaggregation
  TestEq([ totalcount := 4, aggregation :=
              [ [ groupid := "QC1", shortkeywords := [ [ shortkeywords := "eins", numdocs := 1 ] ] ]
              , [ groupid := "QC2", shortkeywords := [ [ shortkeywords := "zwei", numdocs := 1 ], [ shortkeywords := "zweite", numdocs := 1 ] ] ]
              , [ groupid := "QC3", shortkeywords := [ [ shortkeywords := "drei", numdocs := 1 ] ] ]
              ]
          ],
          ExecTestAggregation(CQAggregate("groupid", "terms",
              [ aggregations :=
                  [ CQAggregate("shortkeyword", "terms", [ name := "shortkeywords", countfield := "numdocs" ])
                  ]
              ]),
          [ mapping := [ groupid := "", shortkeywords := [ [ shortkeywords := "", numdocs := 0 ] ] ] ]));

  // Count aggregation (should just be the totalcount)
  TestThrowsLike("Name option is required for aggregation type 'count'", PTR CQAggregate("groupid", "count"));
  TestEq([ totalcount := 4, aggregation := [ count := 4 ] ],
         ExecTestAggregation(CQAggregate("groupid", "count", [ name := "count" ]),
         [ mapping := [ count := 0 ] ]));
  TestEq([ totalcount := 3, aggregation := [ count := 3 ] ],
         ExecTestAggregation(CQAggregate("groupid", "count", [ name := "count", query := CQMatch("shortkeyword", ">=", "eins") ]),
         [ mapping := [ count := 0 ] ]));
  TestEq([ totalcount := 2, aggregation := [ count := 2 ] ],
         ExecTestAggregation(CQAggregate("groupid", "count", [ name := "count", query := CQMatch("shortkeyword", ">", "eins") ]),
         [ mapping := [ count := 0 ] ]));

  // Min and max values
  TestEq([ totalcount := 4, aggregation := [ price := 10m ] ],
         ExecTestAggregation(CQAggregate("price", "min"),
         [ mapping := [ price := 0m ] ]));
  TestEq([ totalcount := 4, aggregation := [ price := 30m ] ],
         ExecTestAggregation(CQAggregate("price", "max"),
         [ mapping := [ price := 0m ] ]));
  // Add a query
  TestEq([ totalcount := 2, aggregation := [ price := 20m ] ],
         ExecTestAggregation(CQAggregate("price", "min", [ query := CQMatch("shortkeyword", ">", "eins") ]),
         [ mapping := [ price := 0m ] ]));
  TestEq([ totalcount := 3, aggregation := [ price := 20.5m ] ],
         ExecTestAggregation(CQAggregate("price", "max", [ query := CQMatch("shortkeyword", ">=", "eins") ]),
         [ mapping := [ price := 0m ] ]));

  // Cardinality: number of unique values
  TestEq([ totalcount := 4, aggregation := [ groupid := 3 ] ],
         ExecTestAggregation(CQAggregate("groupid", "cardinality"),
         [ mapping := [ groupid := 0 ] ]));
  TestEq([ totalcount := 4, aggregation := [ shortkeyword := 4 ] ],
         ExecTestAggregation(CQAggregate("shortkeyword", "cardinality"),
         [ mapping := [ shortkeyword := 0 ] ]));
  // Rename the result field
  TestEq([ totalcount := 4, aggregation := [ groupid := 4 ] ],
         ExecTestAggregation(CQAggregate("shortkeyword", "cardinality", [ name := "groupid" ]),
         [ mapping := [ groupid := 0 ] ]));
  // Add a query
  TestEq([ totalcount := 3, aggregation := [ groupid := 2 ] ],
         ExecTestAggregation(CQAggregate("groupid", "cardinality", [ query := CQMatch("shortkeyword", ">=", "eins") ]),
         [ mapping := [ groupid := 0 ] ]));
  TestEq([ totalcount := 3, aggregation := [ groupid := 3 ] ],
         ExecTestAggregation(CQAggregate("groupid", "cardinality", [ query := CQMatch("shortkeyword", "<=", "zwei") ]),
         [ mapping := [ groupid := 0 ] ]));
  // Separate aggregation and search results
  TestEq([ totalcount := 3, results := ["QC1","QC2","QC2"], aggregation := [ groupid := 2 ] ],
         ExecTestAggregation(CQAggregate("groupid", "cardinality", [ query := CQMatch("shortkeyword", ">=", "eins") ]),
         [ aggregation_mapping := [ groupid := 0 ] ]));
  TestEq([ totalcount := 3, results := ["QC1","QC2","QC3"], aggregation := [ groupid := 3 ] ],
         ExecTestAggregation(CQAggregate("groupid", "cardinality", [ query := CQMatch("shortkeyword", "<=", "zwei") ]),
         [ aggregation_mapping := [ groupid := 0 ] ]));
}

MACRO RunTheNestedQueries()
{
  // Searching for documents with a specific first and last name within the same record field doesn't work, as the first and
  // last names aren't related. We don't want searching for first name 'charles' and last name 'smithers' to return the
  // document, which has separate record [ firstname := "charles montgomery", lastname := "burns" ] and [ firstname := "waylon",
  // lastname := "smithers" ]
  RECORD searchres := RunConsilioSearch("consilio:testfw_queries", CQAnd(
      [ CQMatch("record_field.firstname", "=", "charles")
      , CQMatch("record_field.lastname", "=", "smithers")
      ]), [ mapping := [ record_field := [ [ firstname := "", lastname := "" ] ] ], debug := TRUE ]);
  TestEq(1, searchres.totalcount);
  TestEqMembers(
      [ record_field :=
          [ [ firstname := "charles montgomery", lastname := "burns" ]
          , [ firstname := "waylon", lastname := "smithers" ]
          ]
      ], searchres.results[0], "record_field");

  // We need a nested query on a nested field to query related first and last names
  searchres := RunConsilioSearch("consilio:testfw_queries", CQNested("nested_field", CQAnd(
      [ CQMatch("nested_field.firstname", "=", "charles")
      , CQMatch("nested_field.lastname", "=", "smithers")
      ])), [ mapping := [ nested_field := [ [ firstname := "", lastname := "" ] ] ], debug := TRUE ]);
  TestEq(0, searchres.totalcount);

  // Test if we can find names that are related, by default only the matching burns subrecord is returned
  searchres := RunConsilioSearch("consilio:testfw_queries", CQNested("nested_field", CQAnd(
      [ CQMatch("nested_field.firstname", "=", "charles")
      , CQMatch("nested_field.lastname", "=", "burns")
      ])), [ mapping := [ nested_field := [ [ firstname := "", lastname := "" ] ] ], debug := TRUE ]);
  TestEq(1, searchres.totalcount);
  TestEqMembers(
      [ nested_field :=
          [ [ firstname := "charles montgomery", lastname := "burns" ]
          ]
      ], searchres.results[0], "nested_field");

  // Test if we can find names that are related, return all subrecords
  searchres := RunConsilioSearch("consilio:testfw_queries", CQNested("nested_field", CQAnd(
      [ CQMatch("nested_field.firstname", "=", "charles")
      , CQMatch("nested_field.lastname", "=", "burns")
      ]), [ returnnonmatching := TRUE ]), [ mapping := [ nested_field := [ [ firstname := "", lastname := "" ] ] ], debug := TRUE ]);
  TestEq(1, searchres.totalcount);
  TestEqMembers(
      [ nested_field :=
          [ [ firstname := "charles montgomery", lastname := "burns" ]
          , [ firstname := "waylon", lastname := "smithers" ]
          ]
      ], searchres.results[0], "nested_field");

  // A more advanced test case:
  // Find publications with non-intro documents with a title containing the word 'lorem' or having pages containing the word
  // 'lorem', return the first two results with the total number and 5 best matching documents, with for each document the
  // total number and 3 best matching pages with an excerpt. The 'lorem' query is highlighted in the document title and the
  // most relevant fragment of the page content in the 'content' field.
  searchres := RunConsilioSearch("consilio:testfw_queries",
      CQNested("documents",
               CQAnd([ CQOr([ CQNested("documents.pages",
                                       CQParseUserQuery("lorem", [ defaultfields := [ [ field := "documents.pages.content" ] ] ]),
                                       [ count := 3
                                       , scorefield := "pagescore"
                                       , totalcountfield := "numpages"
                                       , summaryfield := "summary"
                                       ])
                            , CQParseUserQuery("lorem", [ defaultfields := [ [ field := "documents.title" ] ] ])
                            ])
                     , CQNot(CQMatch("documents.type", "=", "intro"))
                     ]),
               [ count := 5
               , scorefield := "docscore"
               , totalcountfield := "numdocs"
               ]),
      [ count := 2
      , mapping :=
          [ documents :=
              [ [ title := ""
                , pages :=
                    [ [ page := 0
                      ]
                    ]
                ]
              ]
          ]
      , debug := TRUE
      , scorefield := "pubscore"
      , highlightfields := [ "documents.title", "documents.pages.content" ]
      , highlightparts := [ "documents.pages.content" ]
      , summaryfield := ""
      , summary_length := 20
      ]);

  // Of the 3 matching publications, only the first 2 are returned
  TestEq(3, searchres.totalcount);
  TestEq(2, Length(searchres.results));

  // The first returned publication
  RECORD publication := searchres.results[0];
  TestEq("QC2", publication.groupid);
  TestEq("O2", publication.objectid);
  // Of the 9 matching documents, only the first 5 are returned
  TestEq(9, publication.numdocs);
  TestEq(5, Length(publication.documents));
  RECORD document := publication.documents[0];
  TestEq(TRUE, document.docscore > 0);
  TestEq('Second &#60;document&#62;, second object &#38; ninth <span class="consilio--highlight">lorem</span> subdocument', document.title); // Contains lorems in the document title, should be scored highest, 'lorem' should be highlighted
  TestEq(0, document.numpages); // Document has no matching pages
  TestEq(0, Length(document.pages));
  document := publication.documents[1];
  TestEqLike("Second &#60;document&#62;, second object &#38; seventh subdocument", document.title); // Contains 3 lorems, should be scored second highest
  TestEq(1, document.numpages);
  TestEq(TRUE, document.pages[0].pagescore > 0);
  TestEq('<span class="consilio--highlight">Lorem</span> morbi a tortor', document.pages[0].summary);

  // The second returned publication
  publication := searchres.results[1];
  TestEq("QC2", publication.groupid);
  TestEq("O1", publication.objectid);
  TestEq(1, publication.numdocs);
  document := publication.documents[0];
  TestEq(TRUE, document.docscore > 0);
  // Of the 9 matching pages within this document, only the first 3 are returnd
  TestEq(9, document.numpages);
  TestEq(3, Length(document.pages));
  TestEq(TRUE, document.pages[0].pagescore > 0);
  TestEq(7, document.pages[0].page); // Contains 3 lorems, should be scored highest
  TestEq('magna, <span class="consilio--highlight">lorem</span> sed <span class="consilio--highlight">lorem</span>', document.pages[0].summary); // the last two lorems are highlighted, as they are closest

  // The same test case, but with the pages within documents sorted by page num instead of relevance
  searchres := RunConsilioSearch("consilio:testfw_queries",
      CQNested("documents",
               CQAnd([ CQOr([ CQNested("documents.pages",
                                       CQParseUserQuery("lorem", [ defaultfields := [ [ field := "documents.pages.content" ] ] ]),
                                       [ count := 3
                                       , scorefield := "pagescore"
                                       , totalcountfield := "numpages"
                                       , summaryfield := "summary"
                                       , orderby := "page"
                                       ])
                            , CQParseUserQuery("lorem", [ defaultfields := [ [ field := "documents.title" ] ] ])
                            ])
                     , CQNot(CQMatch("documents.type", "=", "intro"))
                     ]),
               [ count := 5
               , scorefield := "docscore"
               , totalcountfield := "numdocs"
               ]),
      [ count := 2
      , mapping :=
          [ documents :=
              [ [ title := ""
                , pages :=
                    [ [ page := 0
                      ]
                    ]
                ]
              ]
          ]
      , debug := TRUE
      , scorefield := "pubscore"
      , highlightfields := [ "documents.title", "documents.pages.content" ]
      , highlightparts := [ "documents.pages.content" ]
      , summaryfield := ""
      , summary_length := 20
      ]);

  // The second returned publication
  publication := searchres.results[1];
  TestEq("QC2", publication.groupid);
  TestEq("O1", publication.objectid);
  TestEq(1, publication.numdocs);
  document := publication.documents[0];
  TestEq(TRUE, document.docscore > 0);
  // Of the 9 matching pages within this document, only the first 3 are returnd
  TestEq(9, document.numpages);
  TestEq(3, Length(document.pages));
  TestEq(0f, document.pages[0].pagescore); // Sorted nested results aren't scored
  TestEq(1, document.pages[0].page); // The first 3 pages are returned in order
  TestEq(2, document.pages[1].page); // The first 3 pages are returned in order
  TestEq(3, document.pages[2].page); // The first 3 pages are returned in order
  TestEq('<span class=\"consilio--highlight\">Lorem<\/span> ipsum dolor sit', document.pages[0].summary);

  // Because the document.pages.content fields are added to each level within the object structure (they're added to their
  // parent documents and to the publication root), we can find matches on each level and we should be able to find multiple
  // words within a document or publication, even if they don't appear on the same page or even within the same document.
  // Searching for two words 'fringilla dapibus' should return all three publications: the first publication (QC1/O0) has
  // both words one the same page of a document, the second publication (QC2/O1) has them on separate pages within the same
  // document and the third pubication (QC2/O2) has them in separate documents
  searchres := RunConsilioSearch("consilio:testfw_queries",
      CQOr(
          [ CQNested("documents",
                     CQOr([
                       CQNested("documents.pages",
                                CQParseUserQuery("fringilla dapibus", [ defaultfields := [ [ field := "documents.pages.content" ] ] ]),
                                [ count := 3
                                , scorefield := "pagescore"
                                , totalcountfield := "numpages"
                                , summaryfield := "summary"
                                ]),
                       CQParseUserQuery("fringilla dapibus", [ defaultfields := [ [ field := "documents.pages.content" ] ] ])
                     ]),
                     [ count := 5
                     , scorefield := "docscore"
                     , totalcountfield := "numdocs"
                     ])
          , CQParseUserQuery("fringilla dapibus", [ defaultfields := [ [ field := "documents.pages.content" ] ] ])
          ]),
      [ count := 10
      , mapping :=
          [ documents :=
              [ [ title := ""
                , pages :=
                    [ [ page := 0
                      ]
                    ]
                ]
              ]
          ]
      , debug := TRUE
      , scorefield := "pubscore"
      , highlightfields := [ "documents.title", "documents.pages.content" ]
      , highlightparts := [ "documents.pages.content" ]
      , summaryfield := ""
      , summary_length := 20
      ]);

  TestEq(3, searchres.totalcount);
  TestEq(3, Length(searchres.results));

  // The first returned publication has one matching page
  publication := searchres.results[0];
  TestEq("QC1", publication.groupid);
  TestEq("O0", publication.objectid);
  // There is 1 matching document
  TestEq(1, publication.numdocs);
  TestEq(1, Length(publication.documents));
  document := publication.documents[0];
  TestEq(TRUE, document.docscore > 0);
  TestEq('Content document', document.title); // Contains lorems in the document title, should be scored highest, 'lorem' should be highlighted
  TestEq(1, document.numpages);
  TestEq(TRUE, document.pages[0].pagescore > 0);
  TestEq('Proin <span class=\"consilio--highlight\">fringilla<\/span> ipsum', document.pages[0].summary);

  // The second returned publication has one matching documents, no pages returned
  publication := searchres.results[1];
  TestEq("QC2", publication.groupid);
  TestEq("O1", publication.objectid);
  // There is 1 matching document
  TestEq(1, publication.numdocs);
  TestEq(1, Length(publication.documents));
  document := publication.documents[0];
  TestEq(TRUE, document.docscore > 0);
  TestEq('Second document, first object', document.title); // Contains lorems in the document title, should be scored highest, 'lorem' should be highlighted
  TestEq(0, document.numpages);

  // The third returned publication has no matching documents
  publication := searchres.results[2];
  TestEq("QC2", publication.groupid);
  TestEq("O2", publication.objectid);
  // There are no matching documents
  TestEq(0, publication.numdocs);
  TestEq(0, Length(publication.documents));
}

MACRO RunExplainQuery()
{
  // Just testing if the '_explanation' is only present if the 'explain' option is set
  RECORD searchres := RunConsilioSearch("consilio:testfw_queries", CQMatch("groupid", "=", "QC1"));
  TestEq(FALSE, CellExists(searchres.results[0], "_explanation"));
  searchres := RunConsilioSearch("consilio:testfw_queries", CQMatch("groupid", "=", "QC1"), [ explain := TRUE ]);
  TestEq(TRUE, CellExists(searchres.results[0], "_explanation"));
  TestEq(TRUE, RecordExists(searchres.results[0]._explanation));
}

MACRO RunTotalTrackTest()
{
  // By default, up to 10,000 results are counted exactly
  RECORD searchres := RunConsilioSearch("consilio:testfw_queries", CQAll(), [ count := 0 ]);
  TestEq(4, searchres.totalcount);
  TestEq(TRUE, searchres.totalexact);

  // If we track up to 3 results, we no longer receive the exact count
  searchres := RunConsilioSearch("consilio:testfw_queries", CQAll(), [ count := 0, totaltotrack := 3 ]);
  TestEq(3, searchres.totalcount);
  TestEq(FALSE, searchres.totalexact);

  // If we track up to 0 results, we no longer receive any count
  searchres := RunConsilioSearch("consilio:testfw_queries", CQAll(), [ count := 0, totaltotrack := 0 ]);
  TestEq(0, searchres.totalcount);
  TestEq(FALSE, searchres.totalexact);

  // Track all results
  searchres := RunConsilioSearch("consilio:testfw_queries", CQAll(), [ count := 0, totaltotrack := -1 ]);
  TestEq(4, searchres.totalcount);
  TestEq(TRUE, searchres.totalexact);
}

MACRO RunQueryBoostTest()
{
  // Search for a term
  RECORD searchres := RunConsilioSearch("consilio:testfw_queries", CQMatch("groupid", "=", "QC2"), [ mapping := CELL[ groupid := "" ], scorefield := "score" ]);
  TestEq(2, Length(searchres.results));
  FLOAT score := searchres.results[0].score;
  // The two scores should be the same
  TestEq(score, searchres.results[1].score);
  // Boost the term query by a factor 2, the scores should double as well
  searchres := RunConsilioSearch("consilio:testfw_queries", CQMatch("groupid", "=", "QC2", [ boost := 2 ]), [ mapping := CELL[ groupid := "" ], scorefield := "score" ]);
  TestEq(score * 2, searchres.results[0].score);
  // Boost the term query by a factor .5, the scores should be halved
  searchres := RunConsilioSearch("consilio:testfw_queries", CQMatch("groupid", "=", "QC2", [ boost := .5 ]), [ mapping := CELL[ groupid := "" ], scorefield := "score" ]);
  TestEq(score / 2, searchres.results[0].score);

  searchres := RunConsilioSearch("consilio:testfw_queries", CQAll(), [ mapping := CELL[ groupid := "" ], scorefield := "score" ]);
  TestEq(4, Length(searchres.results));
  // For an 'all' query, the score of each result is 1
  TestEq(1f, searchres.results[0].score);
  TestEq(1f, searchres.results[3].score);
  // Boost the query by a factor 3.14, so the scores will be 3.14 as well
  searchres := RunConsilioSearch("consilio:testfw_queries", CQAll([ boost := 3.14 ]), [ mapping := CELL[ groupid := "" ], scorefield := "score" ]);
  TestEq(3.14f, searchres.results[0].score);
  TestEq(3.14f, searchres.results[3].score);
}

MACRO RunBooleanQueryRewrite()
{
  // Regression: a AND (b OR (NOT c)) would be 'optimized' into a AND (NOT c) AND (b)
  TestEq(
      [ _type := "boolean"
      , subqueries :=
          [ [ query := [ _type := "literal", field := "a", term := "a", boost := 1f ]
            , required := TRUE
            , prohibited := FALSE
            ]
          , [ query :=
                [ _type := "boolean"
                , subqueries :=
                    [ [ query := [ _type := "literal", field := "b", term := "b", boost := 1f ]
                      , required := FALSE
                      , prohibited := FALSE
                      ]
                    , [ query :=
                          [ _type := "boolean"
                          , subqueries :=
                              [ [ query := [ _type := "literal", field := "c", term := "c", boost := 1f ]
                                , required := FALSE
                                , prohibited := TRUE
                                ]
                              ]
                          , boost := 1f
                          ]
                      , required := FALSE
                      , prohibited := FALSE
                      ]
                    ]
                , boost := 1f
                ]
            , required := TRUE
            , prohibited := FALSE
            ]
          ]
      , boost := 1f
      ], CQAnd(
           [ CQMatch("a", "=", "a")
           , CQOr(
               [ CQMatch("b", "=", "b")
               , CQNot(CQMatch("c", "=", "c"))
               ])
           ]));
}

RunTestframework(
    [ PTR PrepareCustomCatalog
    , PTR RunTheMatchQueries
    , PTR RunTheAggregationQueries
    , PTR RunTheNestedQueries
    , PTR RunExplainQuery
    , PTR RunTotalTrackTest
    , PTR RunQueryBoostTest
    , PTR RunBooleanQueryRewrite
    ]);
