﻿<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::witty.whlib";
LOADLIB "wh::xml/dom.whlib";
LOADLIB "wh::xml/internal/xpath.whlib";

LOADLIB "mod::consilio/lib/reports.whlib";

LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/validation.whlib";

LOADLIB "mod::wrd/lib/api.whlib";
LOADLIB "mod::wrd/lib/imexport.whlib";
LOADLIB "mod::wrd/lib/database.whlib";
LOADLIB "mod::wrd/lib/internal/cache.whlib";

LOADLIB "mod::webhare_testsuite/lib/internal/wrdtesthelpers.whlib";


//Save types and attributes
RECORD FUNCTION ExtractSchemaMetadata(OBJECT wrdschema)
{
  RECORD retval := [ types := DEFAULT RECORD ARRAY ];

  retval.types := SELECT * FROM wrdschema->ListTypes() ORDER BY ToUppercase(tag);
  retval.types := SELECT *, linkfrom := linkfrom != 0 ? wrdschema->GetTypeById(linkfrom)->tag : "" FROM retval.types;
  retval.types := SELECT *, linkto := linkto != 0 ? wrdschema->GetTypeById(linkto)->tag : "" FROM retval.types;
  retval.types := SELECT *, parenttype := parenttype != 0 ? wrdschema->GetTypeById(parenttype)->tag : "" FROM retval.types;

  FOREVERY(RECORD type FROM retval.types)
  {
    //ADDME get arrays attrs too
    RECORD ARRAY attrs := SELECT * FROM wrdschema->GetType(type.tag)->ListAttributes(0) ORDER BY ToUppercase(tag);

    attrs := SELECT *, domain := domain != 0 ? wrdschema->GetTypeById(domain)->tag : "" FROM attrs;
    attrs := SELECT *, DELETE attributeid, DELETE id FROM attrs;

    DELETE CELL id FROM type;
    INSERT CELL attrs := attrs INTO type;

    retval.types[#type]:=type;
  }
  RETURN retval;
}

MACRO Setup()
{
  testfw->BeginWork();

  OBJECT wrdschema :=  testfw->GetWRDSchema();
  OBJECT persontype := wrdschema->GetType("WRD_PERSON");
  OBJECT orgtype := wrdschema->GetType("WRD_ORGANIZATION");

  TestEqMembers([ revision := 0, result := [ postupdaterevision := -1, postentitiesrevision := -1, person_has_add_datetime := FALSE, has_settings_entity := FALSE ]], wrdschema->GetMigrationStatus("testfw:update_post"), "*");
  TestEqMembers([ revision := 0, result := [ postupdaterevision := -1, postentitiesrevision := -1, person_has_add_datetime := FALSE, has_settings_entity := TRUE ]], wrdschema->GetMigrationStatus("testfw:update_postentities"), "*");

  //Create a unique/required email field
  persontype->UpdateAttribute("WRD_CONTACT_EMAIL", [ isunique := TRUE, isrequired := TRUE ]);
  persontype->CreateAttribute("C1", "FREE");

  //Add a domain field..
  OBJECT domtype1 := wrdschema->CreateDomain("DOMAINTYPE1");
  domtype1->CreateEntity([ wrd_tag := "ONE", wrd_title := "First value"]);
  domtype1->CreateEntity([ wrd_tag := "TWO", wrd_title := "Second value"]);


  persontype->CreateAttribute ("DOMAIN1", "DOMAIN", [ domaintag := "DOMAINTYPE1" ]);

  //add a link and a clasisfication type
  OBJECT classitype := wrdschema->CreateType("CLASSIFICATION", [ title := "Classificätion", description := "description", linkfrom := persontype->id]);
  OBJECT linktype := wrdschema->CreateType("LINK", [ linkfrom := persontype->id, linkto := orgtype->id]);

  //create the wrd_relation basetype
  OBJECT reltype := wrdschema->^wrd_relation;
  //create a field in the wrd_relationtype. we want to be sure it doesn't appear in subtypes. Oh, and needed an excuse for a MONEY type
  reltype->CreateAttribute("KARMA","MONEY");

  TestEq(FALSE, reltype->GetAttribute("KARMA").isinherited);
  TestEq(TRUE, persontype->GetAttribute("KARMA").isinherited);

  persontype->CreateAttribute("FREE1", "FREE", [ title := "Free 1" ]);
  RECORD free1 := persontype->GetAttribute("FREE1");
  TestEq(FALSE, free1.checklinks);
  persontype->UpdateAttribute("FREE1", [ checklinks := TRUE ]);
  free1 := persontype->GetAttribute("FREE1");
  TestEq(TRUE, free1.checklinks);

  persontype->CreateAttribute("RICHDOC1", "RICHDOCUMENT", [title:="Rich document 1" ]);
  RECORD richdoc1 := persontype->GetAttribute("RICHDOC1");
  TestEq(TRUE, richdoc1.checklinks);

  //Check that links are checked in both attributes
  OBJECT person := persontype->CreateEntity(
      [ free1 := "http://example.org"
      , richdoc1 := [htmltext := StringToBlob(`<p><a href="http://example.com">example</a></p>`)]
      , wrd_contact_email := "linktest@beta.webhare.net"
      ]);
  testfw->CommitWork();

  RECORD ARRAY checkedlinks := GetWRDLinkCheckReport([ entity := person->id ]);
  TestEq("http://example.org", SELECT AS STRING url FROM checkedlinks WHERE referrer = "FREE1");
  TestEq("http://example.com", SELECT AS STRING url FROM checkedlinks WHERE referrer = "RICHDOC1");

  testfw->BeginWork();
  persontype->UpdateAttribute("RICHDOC1", [checklinks := FALSE]);
  richdoc1 := persontype->GetAttribute("RICHDOC1");
  TestEq(FALSE, richdoc1.checklinks);
  testfw->CommitWork();

  //Check that links from richdoc1 are removed
  checkedlinks := GetWRDLinkCheckReport([ entity := person->id ]);
  TestEq("http://example.org", SELECT AS STRING url FROM checkedlinks WHERE referrer = "FREE1");
  TestEq("", SELECT AS STRING url FROM checkedlinks WHERE referrer = "RICHDOC1");

  //Regression: Update the person, should not have added a link for richdoc1
  testfw->BeginWork();
  persontype->UpdateEntity(person->id,
      [ free1 := "http://example.net"
      , richdoc1 := [htmltext := StringToBlob(`<p><a href="http://example.nl">example</a></p>`)]
      ]);
  testfw->CommitWork();
  checkedlinks := GetWRDLinkCheckReport([ entity := person->id ]);
  TestEq("http://example.net", SELECT AS STRING url FROM checkedlinks WHERE referrer = "FREE1");
  TestEq("", SELECT AS STRING url FROM checkedlinks WHERE referrer = "RICHDOC1");

  testfw->BeginWork();
  TestThrowsLike("*llegal key*", PTR wrdschema->GetSchemaSetting("myteststr"));
  TestThrowsLike("*llegal key*", PTR wrdschema->SetSchemaSetting("myteststr", "My test string!"));
  TestThrowsLike("*llegal key*", PTR wrdschema->GetSchemaSetting("wrd:mytest str"));
  TestThrowsLike("*llegal key*", PTR wrdschema->SetSchemaSetting("wrd:mytest str", "My test string!"));
  //Make sure we can always represent the keys as a tree if we ever want to
  TestThrowsLike("*llegal key*", PTR wrdschema->SetSchemaSetting("wrd:mytest.", "My test string!"));
  TestThrowsLike("*llegal key*", PTR wrdschema->SetSchemaSetting("wrd:.mytest", "My test string!"));
  TestThrowsLike("*llegal key*", PTR wrdschema->SetSchemaSetting("wrd:my..test", "My test string!"));
  TestThrowsLike("*llegal key*", PTR wrdschema->SetSchemaSetting("wrd:sub:test", "My test string!"));
  TestThrowsLike("*llegal key*", PTR wrdschema->SetSchemaSetting("w.rd:sub", "My test string!"));

  wrdschema->SetSchemaSetting("wrd:myteststr", "My test string!");
  TestEq("My test string!", wrdschema->GetSchemaSetting("wrd:MyTestStr"));
  wrdschema->RemoveSchemaSetting("wrd:MYTESTSTR");
  TestThrowsLike('Cannot find*no fallback*',PTR wrdschema->GetSchemaSetting("wrd:MyTestStr"));
  TestEq(42, wrdschema->GetSchemaSetting("wrd:MyTestStr", [fallback  := 42]));

  wrdschema->SetSchemaSetting("wrd:setting1", "My test string!");
  wrdschema->SetSchemaSetting("wrd:setting2", 1234567890);
  wrdschema->SetSchemaSetting("wrd:setting3", TRUE);
  wrdschema->SetSchemaSetting("wrd:setting4", StringToBlob("My test blob!"));

/* FIXME implement custom parent/child suppport
   //createa a custom relation type
  OBJECT customrelation := testfw->GetWRDSchema()->CreateRelationType("Custom relation base", "", "CUSTOMRELATION");
  OBJECT customrelationchild := testfw->GetWRDSchema()->CreateRelationTypeWithParentType("Custom relation child", "", "CUSTOMRELATIONCHILD", customrelation->id);
*/
  testfw->CommitWork();
}

MACRO ExportXMLMetadata()
{
  testfw->BeginWork();

  //Export current structure
  OBJECT wrdschema := testfw->GetWRDSchema();
  BLOB structuredoc := CreateWRDSchemaDefinitionFile(wrdschema);

//  sendblobto(0,structuredoc);

  //Verify some basics
  OBJECT structurexml := MakeXMLDocument(structuredoc);
  OBJECT myresolver := NEW IndependentXPathNSResolver();
  myresolver->AddNS("s", "http://www.webhare.net/xmlns/wrd/schemadefinition");

  //There should only be one KARMA Field, under WRD_RELATION
  TestEq(1, Length(structurexml->GetEvaluatedElements(myresolver, '//s:money[@tag="KARMA"]')));
  TestEq(1, Length(structurexml->GetEvaluatedElements(myresolver, '//s:object[@tag="WRD_RELATION"]//s:money[@tag="KARMA"]')));

  //Save types and attributes
  RECORD saveschema := ExtractSchemaMetadata(wrdschema);

  //Destroy the schema
  wrdschema->DeleteSelf();
  testfw->CommitWork();

  //Try to recreate it from the metadata
  testfw->BeginWork();
  wrdschema := CreateWRDSchema(wrdschema->tag);
  TestEq(RECORD[], ValidateSingleFile("mod::webhare_testsuite/dummy.wrdschema.xml", [ overridedata := structurexml->GetDocumentBlob(FALSE) ]).errors);

  wrdschema->ApplySchemaDefinition([ __schemadefinition := structuredoc, fromexternal := TRUE ]);
  testfw->CommitWork();

  TestEq(TRUE, RecordExists(wrdschema->^wrd_relation->GetAttribute("KARMA")));
  TestEq(TRUE, RecordExists(wrdschema->GetType("WRD_PERSON")->GetAttribute("KARMA")));
  TestEq(TRUE, wrdschema->GetType("WRD_PERSON")->GetAttribute("TEST_FREE_NOCOPY").isunsafetocopy);
  TestEq(TRUE, RecordExists(wrdschema->GetType("WRD_ORGANIZATION")->GetAttribute("KARMA")));

  TestEq("My test string!", wrdschema->GetSchemaSetting("wrd:setting1"));
  TestEq(1234567890, wrdschema->GetSchemaSetting("wrd:setting2"));
  TestEq(TRUE, wrdschema->GetSchemaSetting("wrd:setting3"));
  TestEq(StringToBlob("My test blob!"), wrdschema->GetSchemaSetting("wrd:setting4"));

  //The schemas should match
  RECORD restoredschema := ExtractSchemaMetadata(wrdschema);
  TestEq(saveschema, restoredschema);

  //Another export should be (pretty much?) the same
  BLOB structuredoc2 := CreateWRDSchemaDefinitionFile(wrdschema);
  IF(BlobToString(structuredoc,-1) != BlobToString(structuredoc2,-1))
  {
    SendBLobTo(0,structuredoc);
    SendBLobTo(0,structuredoc2);
    TestEq(structuredoc, structuredoc2);
  }

  // Force reload
  testfw->GetWRDSchema(TRUE);
}

STRING domwitty := `<?xml version="1.0" encoding="UTF-8"?>
<schemadefinition xmlns="http://www.webhare.net/xmlns/wrd/schemadefinition">
  <domain tag="DOMAIN" title="Domain">
    <attributes>
      <free tag="TITLE2" title="Titel2">
        <documentation>dox should be ignored</documentation>
      </free>
      <record tag="MYRECORD" />
    </attributes>
    <values matchattribute="WRD_TAG" overwriteattributes="WRD_TITLE">
      [forevery vals][embed value][/forevery]
    </values>
  </domain>
</schemadefinition>

[component value]
  <value>
    <field tag="WRD_TAG">[tag]</field>
    <field tag="WRD_TITLE">[t1]</field>
    <field tag="TITLE2">[t2]</field>
    [if subvalues]
      <subvalues>
        [forevery subvalues]
          [embed value]
        [/forevery]
      </subvalues>
    [/if]
  </value>
[/component]
`;

MACRO ApplyDomMetadata(RECORD ARRAY vals)
{
  OBJECT witty := NEW WittyTemplate("XML");
  witty->LoadCodeDirect(domwitty);
  BLOB data := witty->RunToBlob([ vals := vals ]);

  OBJECT wrdschema := testfw->GetWRDSchema();
  wrdschema->ApplySchemaDefinition([ __schemadefinition := data ]);
  TestEq("RECORD", wrdschema->^domain->GetAttribute("MYRECORD").attributetypename);
}

MACRO VerifyWRDSettingsRecovery()
{
  OBJECT wrdschema := testfw->GetWRDSchema();
  INTEGER settings := wrdschema->^wrd_settings->Search("WRD_TAG", "WRD_SETTINGS");
  TestAssert(settings != 0);

  testfw->BeginWork();
  UPDATE wrd.entities SET limitdate := GetCurrentDatetime() WHERE id = settings; //lowlevel change as the API won't allow this
  TestEq(0, wrdschema->^wrd_settings->Search("WRD_TAG", "WRD_SETTINGS"));
  wrdschema->ApplySchemaDefinition();
  testfw->CommitWork();

  TestEq(settings, wrdschema->^wrd_settings->Search("WRD_TAG", "WRD_SETTINGS"));
}

STRING attrwitty :=
    '<?xml version="1.0" encoding="UTF-8"?>\n' ||
    '    <schemadefinition xmlns="http://www.webhare.net/xmlns/wrd/schemadefinition">\n' ||
    '  <domain tag="DOMAIN" title="Domain">\n' ||
    '    <attributes>\n' ||
    '      [if type]<[type] tag="TESTATTR" title="Test attribute"/>[/if]\n' ||
    '    </attributes>\n' ||
    '  </domain>\n' ||
    '</schemadefinition>\n';

MACRO ApplyTestAttrMetadata(OBJECT wrdschema, STRING type)
{
  OBJECT witty := NEW WittyTemplate("XML");
  witty->LoadCodeDirect(attrwitty);
  BLOB data := witty->RunToBlob([ type := type ]);
  wrdschema->ApplySchemaDefinition([ __schemadefinition := data ]);
}

RECORD ARRAY FUNCTION GetDomVals()
{
  RECORD ARRAY vals := testfw->GetWRDSchema()->GetType("DOMAIN")->RunQuery(
      [ outputcolumns := [ "WRD_ID", "WRD_LEFTENTITY", "WRD_TAG", "WRD_TITLE", "TITLE2" ] ]);

  RETURN ConstructDomValuesRecursive(vals, 0);
}

RECORD ARRAY FUNCTION ConstructDomValuesRecursive(RECORD ARRAY vals, INTEGER parent)
{
  RETURN
      SELECT tag :=         wrd_tag
           , t1 :=          wrd_title
           , t2 :=          title2
           , subvalues :=   ConstructDomValuesRecursive(VAR vals, wrd_id)
        FROM vals
       WHERE wrd_leftentity = parent
    ORDER BY wrd_tag;
}


MACRO DomainValuesSync()
{
  testfw->BeginWork();

  //Export current structure
  OBJECT wrdschema := testfw->GetWRDSchema();

  // Throw on duplicate tag
  TestThrowsLike("*Multiple*'1'*", PTR ApplyDomMetadata(
      [ [ tag := "1", t1 := "1a", t2 := "1b", subvalues := DEFAULT RECORD ARRAY ]
      , [ tag := "1", t1 := "1a", t2 := "1b", subvalues := DEFAULT RECORD ARRAY ]
      ]));

  // Throw on duplicate tag (recursive)
  TestThrowsLike("*Multiple*'1'*", PTR ApplyDomMetadata(
      [ [ tag := "1", t1 := "1a", t2 := "1b", subvalues :=
            [ [ tag := "1", t1 := "1a", t2 := "1b", subvalues := DEFAULT RECORD ARRAY ]
            ]
        ]
      ]));

  // Write new value
  ApplyDomMetadata(
      [ [ tag := "1", t1 := "1a", t2 := "1b", subvalues := DEFAULT RECORD ARRAY ]
      ]);
  TestEQ([ [ tag := "1", t1 := "1a", t2 := "1b", subvalues := DEFAULT RECORD ARRAY ]
         ], GetDomVals());

  // Only overwriting WRD_TITLE
  ApplyDomMetadata(
      [ [ tag := "1", t1 := "1ax", t2 := "1bx", subvalues := DEFAULT RECORD ARRAY ]
      ]);
  TestEQ([ [ tag := "1", t1 := "1ax", t2 := "1b", subvalues := DEFAULT RECORD ARRAY ]
         ], GetDomVals());

  // Subvalue
  ApplyDomMetadata(
      [ [ tag := "1", t1 := "1a", t2 := "1b", subvalues :=
              [ [ tag := "1_1", t1 := "1_1a", t2 := "1_1b", subvalues := DEFAULT RECORD ARRAY ] ]
        ]
      ]);
  TestEQ(
      [ [ tag := "1", t1 := "1a", t2 := "1b", subvalues :=
              [ [ tag := "1_1", t1 := "1_1a", t2 := "1_1b", subvalues := DEFAULT RECORD ARRAY ] ]
        ]
      ], GetDomVals());

  // Move subvalue to newly created other value
  ApplyDomMetadata(
      [ [ tag := "1", t1 := "1a", t2 := "1b", subvalues := DEFAULT RECORD ARRAY ]
      , [ tag := "2", t1 := "2a", t2 := "2b", subvalues :=
              [ [ tag := "1_1", t1 := "1_1a", t2 := "1_1b", subvalues := DEFAULT RECORD ARRAY ] ]
        ]
      ]);
  TestEQ(
      [ [ tag := "1", t1 := "1a", t2 := "1b", subvalues := DEFAULT RECORD ARRAY ]
      , [ tag := "2", t1 := "2a", t2 := "2b", subvalues :=
              [ [ tag := "1_1", t1 := "1_1a", t2 := "1_1b", subvalues := DEFAULT RECORD ARRAY ] ]
        ]
      ], GetDomVals());

  // Move subvalue to existing other value, overwriting attrs
  ApplyDomMetadata(
      [ [ tag := "1", t1 := "1a", t2 := "1b", subvalues :=
              [ [ tag := "1_1", t1 := "1_1ac", t2 := "1_1b", subvalues := DEFAULT RECORD ARRAY ] ]
        ]
      , [ tag := "2", t1 := "2a", t2 := "2b", subvalues := DEFAULT RECORD ARRAY ]
      ]);
  TestEQ(
      [ [ tag := "1", t1 := "1a", t2 := "1b", subvalues :=
              [ [ tag := "1_1", t1 := "1_1ac", t2 := "1_1b", subvalues := DEFAULT RECORD ARRAY ] ]
        ]
      , [ tag := "2", t1 := "2a", t2 := "2b", subvalues := DEFAULT RECORD ARRAY ]
      ], GetDomVals());

  // Create value and subvalue at the same time
  ApplyDomMetadata(
      [ [ tag := "1", t1 := "1a", t2 := "1b", subvalues :=
              [ [ tag := "1_1", t1 := "1_1ac", t2 := "1_1b", subvalues := DEFAULT RECORD ARRAY ] ]
        ]
      , [ tag := "2", t1 := "2a", t2 := "2b", subvalues := DEFAULT RECORD ARRAY ]
      , [ tag := "3", t1 := "3a", t2 := "3b", subvalues :=
              [ [ tag := "3_1", t1 := "3_1ac", t2 := "3_1b", subvalues := DEFAULT RECORD ARRAY ] ]
        ]
      ]);
  TestEQ(
      [ [ tag := "1", t1 := "1a", t2 := "1b", subvalues :=
              [ [ tag := "1_1", t1 := "1_1ac", t2 := "1_1b", subvalues := DEFAULT RECORD ARRAY ] ]
        ]
      , [ tag := "2", t1 := "2a", t2 := "2b", subvalues := DEFAULT RECORD ARRAY ]
      , [ tag := "3", t1 := "3a", t2 := "3b", subvalues :=
              [ [ tag := "3_1", t1 := "3_1ac", t2 := "3_1b", subvalues := DEFAULT RECORD ARRAY ] ]
        ]
      ], GetDomVals());

  testfw->CommitWork();
}

MACRO TestCreate()
{
  testfw->BeginWork();

  STRING savetag := testfw->GetWRDSchema()->tag;
  testfw->GetWRDSchema()->DeleteSelf();

  OBJECT wrdschema := CreateWRDSchema(savetag);
  TestEq(FALSE, RecordExists(wrdschema->^wrd_person->GetAttribute("WHUSER_PASSWORD")));
  wrdschema->DeleteSelf();

  wrdschema := CreateWRDSchema(savetag, [initialize := TRUE]);
  TestEq(TRUE, RecordExists(wrdschema->^wrd_person->GetAttribute("WHUSER_PASSWORD")));
  TestEq("WebHare testsuite test schema", wrdschema->title);

  testfw->RollbackWork();

  // Force reload
  testfw->GetWRDSchema(TRUE);
}

STRING schema_with_ordering_issues := `<?xml version="1.0" encoding="UTF-8"?>
<schemadefinition xmlns="http://www.webhare.net/xmlns/wrd/schemadefinition">

  <domain tag="TARGET$INTEREST" title="Doelgroep interesse">
    <values matchattribute="WRD_TAG">
      <value>
        <field tag="wrd_tag">$BACHELOR</field>
        <field tag="wrd_title">Bachelor</field>
      </value>
    </values>
  </domain>

  <domain tag="PROGRAMME$TYPE" title="Programmetype">
    <attributes>
      <domain tag="TARGET$INTEREST" title="Doelgroep interesse" domain="TARGET$INTEREST" />
    </attributes>
    <values matchattribute="WRD_TAG">
      <value>
        <field tag="WRD_TAG">HODEX$BACHELOR</field>
        <field tag="TARGET$INTEREST">$BACHELOR</field>
      </value>
    </values>
  </domain>
</schemadefinition>
`;

MACRO TestCreateWithMetadata()
{
  testfw->BeginWork();

  STRING schematag := testfw->GetWRDSchema()->tag || '.temp';
  TestEQ(RECORD[], ValidateSingleFile("mod::webhare_testsuite/dummy.wrdschema.xml", [ overridedata := StringToBlob(schema_with_ordering_issues) ]).errors);
  //test applying simple metadata with ordering issues
  OBJECT wrdschema := CreateWRDSchema(schematag, [ initialize := TRUE,  schemaresource := `inline::${schema_with_ordering_issues}` ]);
  RECORD ARRAY types := wrdschema->^programme$type->RunQuery([outputcolumns:=["TARGET$INTEREST"]]);
  TestEq(1, Length(types));
  TestEq(wrdschema->^Programme$Type->GetDomVal("TARGET$INTEREST", "$BACHELOR"), types[0].target$interest);

  testfw->RollbackWork();
}

MACRO TestMetaTypeChange()
{
  STRING v1schema := `
<schemadefinition xmlns="http://www.webhare.net/xmlns/wrd/schemadefinition">
  <attachment tag="WEBSHOP_REVIEW" linkfrom="WRD_PERSON">
    <attributes>
    </attributes>
  </attachment>
</schemadefinition>`;

  testfw->BeginWork();

  testfw->GetWRDSchema()->ApplySchemaDefinition([ __schemadefinition := StringToBlob(v1schema) ]);
  TestEq(TRUE, ObjectExists(testfw->GetWRDSchema()->GetType("WEBSHOP_REVIEW")));
  TestEQ(TRUE, testfw->GetWRDSchema()->^webshop_review->isattachment);

  STRING v2schema := `
<schemadefinition xmlns="http://www.webhare.net/xmlns/wrd/schemadefinition">
  <object tag="WEBSHOP_REVIEW">
    <attributes>
    </attributes>
  </object>
</schemadefinition>`;

  testfw->GetWRDSchema()->ApplySchemaDefinition([ __schemadefinition := StringToBlob(v2schema) ]);
  TestEq(TRUE, ObjectExists(testfw->GetWRDSchema()->GetType("WEBSHOP_REVIEW")));
  // FIXME - TestEQ(TRUE, testfw->GetWRDSchema()->^webshop_review->isobject); - requires the other WRD fixes
  TestEQ(TRUE, OpenwRDSchemaByID(testfw->GetWRDSchema()->id)->^webshop_review->isobject);

  testfw->RollbackWork();
}


MACRO TestAsyncMetaTypeChange()
{
  OBJECT persontype := testfw->GetWRDSchema()->^wrd_person;;
  TestEq(FALSE, RecordExists(persontype->GetAttribute("ADDED_ATTRIBUTE")));

  CallFunctionFromJob(Resolve("mod::webhare_testsuite/lib/internal/wrdtesthelpers.whlib#ApplyDirectSchemaDefUpdate"), testfw->GetWRDSchema()->tag,
    `<schemadefinition xmlns="http://www.webhare.net/xmlns/wrd/schemadefinition">
  <object tag="WRD_PERSON">
    <attributes>
      <free tag="ADDED_ATTRIBUTE" />
    </attributes>
  </object>
</schemadefinition>`);
  TestEq(TRUE, RecordExists(persontype->GetAttribute("ADDED_ATTRIBUTE")));

  CallFunctionFromJob(Resolve("mod::webhare_testsuite/lib/internal/wrdtesthelpers.whlib#ApplyDirectSchemaDefUpdate"), testfw->GetWRDSchema()->tag,
    `<schemadefinition xmlns="http://www.webhare.net/xmlns/wrd/schemadefinition">
  <object tag="WRD_PERSON">
    <attributes>
      <obsolete tag="ADDED_ATTRIBUTE" />
    </attributes>
  </object>
</schemadefinition>`);
  TestEq(FALSE, RecordExists(persontype->GetAttribute("ADDED_ATTRIBUTE")));

  //test regression - accessing a wrdtype after deletion shouldn't be a harescript crash. I guess an exception we'll have to be able to deal with
  testfw->BeginWork();
  OBJECT temptype := testfw->GetWRDSchema()->CreateType("TEMPTYPE");
  temptype->CreateAttribute("FREE1", "FREE", [ title := "Free 1" ]);
  TestEq(RECORD[], temptype->RunQuery([ outputcolumns := CELL[ "WRD_ID", "FREE1" ]]));
  temptype->DeleteSelf();
  testfw->CommitWork();

  TestThrowsLike("Type*deleted*", PTR temptype->RunQuery([ outputcolumns := CELL[ "WRD_ID", "FREE1" ]]));
}


MACRO TestRename()
{
  STRING savetag := testfw->GetWRDSchema()->tag;

  TestEQ(FALSE, IsWRDMetadataCacheDisabled(), "Verify the caches haven't been shut down already");

  STRING newtag := savetag || ".rename";
  TestEQ(DEFAULT OBJECT, OpenWRDSchema(newtag));

  testfw->BeginWork();
  testfw->GetWRDSchema()->UpdateMetadata([ name := newtag ]);
  testfw->CommitWork();

  OBJECT wrdschema := OpenWRDSchema(newtag);
  TestEQ(TRUE, ObjectExists(wrdschema));

  // rename back
  testfw->BeginWork();
  testfw->GetWRDSchema()->UpdateMetadata([ name := savetag ]);
  testfw->CommitWork();
}

MACRO TestAttributePromotion()
{
  testfw->BeginWork();

  //Export current structure
  OBJECT wrdschema := testfw->GetWRDSchema();

  RECORD ARRAY tests :=
    [ [ org := "free", to := "url", allowed := TRUE, orgvalue := "http://example.com" ]
    , [ org := "free", to := "url", allowed := FALSE, orgvalue := "not-a-url" ]
    , [ org := "url", to := "free", allowed := TRUE, orgvalue := "http://example.com" ]
    , [ org := "free", to := "telephone", allowed := TRUE, orgvalue := "+31612345678" ]
    , [ org := "telephone", to := "free", allowed := TRUE, orgvalue := "+31612345678" ]
    ];

  ApplyTestAttrMetadata(wrdschema, "");

  OBJECT domaintype := wrdschema->GetType("DOMAIN");
  OBJECT entity := domaintype->CreateEntity(DEFAULT RECORD);

  FOREVERY (RECORD rec FROM tests)
  {
    // Create original attr
    ApplyTestAttrMetadata(wrdschema, rec.org);

    // Make sure it contains stuff
    entity->UpdateEntity([ testattr := rec.orgvalue ]);

    IF(rec.allowed)
      ApplyTestAttrMetadata(wrdschema, rec.to);
    ELSE
      TestThrowsLike("*not all settings*", PTR ApplyTestAttrMetadata(wrdschema, rec.to));

    domaintype := wrdschema->GetType("DOMAIN");
    domaintype->DeleteAttribute("TESTATTR");
  }

  testfw->CommitWork();
}

MACRO TestParent()
{
  STRING schemawithparent := `<?xml version="1.0" encoding="UTF-8"?>
<schemadefinition xmlns="http://www.webhare.net/xmlns/wrd/schemadefinition">

  <object tag="WEBSHOP_PRODUCTMIXIN" title="Product mixin">
    <attributes>
      <free tag="WRD_TITLE" title="Title"/>
    </attributes>
  </object>

  <object tag="WEBSHOP_PRODUCT" title="Webshop product" parent="WEBSHOP_PRODUCTMIXIN">
    <attributes>
    </attributes>
  </object>
</schemadefinition>`;

  testfw->BeginWork();

  OBJECT wrdschema := testfw->GetWRDSchema();
  wrdschema->ApplySchemaDefinition([ __schemadefinition := StringToBlob(schemawithparent) ]);

  TestEq(wrdschema->^webshop_productmixin->id, wrdschema->^webshop_product->parent);

  testfw->RollbackWork();
}

MACRO TestDomainTypeToParent()
{
  STRING schemawithparent := `<?xml version="1.0" encoding="UTF-8"?>
<schemadefinition xmlns="http://www.webhare.net/xmlns/wrd/schemadefinition">

  <object tag="WEBSHOP_PRODUCTMIXIN" title="Product mixin">
    <attributes>
      <free tag="WRD_TITLE" title="Title"/>
    </attributes>
  </object>

  <object tag="WEBSHOP_PRODUCT" title="Webshop product" parent="WEBSHOP_PRODUCTMIXIN">
    <attributes>
    </attributes>
  </object>

  <object tag="WEBSHOP_REF">
    <attributes>
      <domain tag="REFERENCE" domain="WEBSHOP_PRODUCT" />
    </attributes>
  </object>
</schemadefinition>`;

  testfw->BeginWork();

  OBJECT wrdschema := CreateWRDSchema(testfw->GetWRDSchema()->tag || '.temp'); //note: we won't be commiting it
  wrdschema->ApplySchemaDefinition([ __schemadefinition := StringToBlob(schemawithparent) ]);

  TestEq(wrdschema->^webshop_product->id, wrdschema->^webshop_ref->GetAttribute("REFERENCE").domain);

  // Make sure the attribute is used so updating it will fail
  INTEGER product := wrdschema->^webshop_product->CreateEntity([ wrd_title := "product" ])->id;
  wrdschema->^webshop_ref->CreateEntity([ reference := product ]);

  schemawithparent := Substitute(schemawithparent, `domain="WEBSHOP_PRODUCT"`, `domain="WEBSHOP_PRODUCTMIXIN"`);
  wrdschema->ApplySchemaDefinition([ __schemadefinition := StringToBlob(schemawithparent) ]);


  TestEq(wrdschema->^webshop_productmixin->id, wrdschema->^webshop_ref->GetAttribute("REFERENCE").domain);

  testfw->RollbackWork();
}

MACRO TestAccountSettings()
{
  //apply account settings to a schema which never had it
  STRING schemawithaccounttype := `
<schemadefinition xmlns="http://www.webhare.net/xmlns/wrd/schemadefinition"
                  accounttype="wrd_person"
                  accountemailfield="WRD_CONTACT_EMAIL"
                  accountloginfield="LOGIN"
                  accountpasswordfield="PASSWORD">
  <object tag="WRD_PERSON">
    <attributes>
      <free tag="LOGIN" />
      <email tag="WRD_CONTACT_EMAIL" />
      <password tag="PASSWORD" />
    </attributes>
  </object>
</schemadefinition>`;

  STRING schemawithauthsettingspassword := `
<schemadefinition xmlns="http://www.webhare.net/xmlns/wrd/schemadefinition"
                  accounttype="wrd_person"
                  accountemailfield="WRD_CONTACT_EMAIL"
                  accountloginfield="LOGIN"
                  accountpasswordfield="AUTHSETTINGS">
  <object tag="WRD_PERSON">
    <attributes>
      <free tag="LOGIN" />
      <email tag="WRD_CONTACT_EMAIL" />
      <authenticationsettings tag="AUTHSETTINGS" />
    </attributes>
  </object>
</schemadefinition>`;

  testfw->BeginWork();

  OBJECT wrdschema := CreateWRDSchema(testfw->GetWRDSchema()->tag || '.temp'); //note: we won't be commiting it

  wrdschema->ApplySchemaDefinition([ __schemadefinition := StringToBlob(schemawithaccounttype) ]);
  TestEq(wrdschema->^wrd_person, wrdschema->accounttype);
  TestEQ("LOGIN", wrdschema->accountlogintag);
  TestEQ("WRD_CONTACT_EMAIL", wrdschema->accountemailtag);
  TestEQ("PASSWORD", wrdschema->accountpasswordtag);
  TestEQ(FALSE, wrdschema->accountpasswordisauthsettings);

  wrdschema->ApplySchemaDefinition([ __schemadefinition := StringToBlob(schemawithauthsettingspassword) ]);
  TestEq(wrdschema->^wrd_person, wrdschema->accounttype);
  TestEQ("AUTHSETTINGS", wrdschema->accountpasswordtag);
  TestEQ(TRUE, wrdschema->accountpasswordisauthsettings);

  testfw->RollbackWork();
}

MACRO TestMerge()
{
  STRING mergeschema := `
<schemadefinition xmlns="http://www.webhare.net/xmlns/wrd/schemadefinition">

  <!-- dupe imports should not be an issue -->
  <import definitionfile="mod::wrd/data/wrdschemas/authschema.xml" /> <!-- direct -->
  <import definitionfile="mod::system/data/wrdschemas/usermgmt.wrdschema.xml" /> <!-- indirectly includes authschema through new path -->
</schemadefinition>`;

  testfw->BeginWork();

  OBJECT wrdschema := CreateWRDSchema(testfw->GetWRDSchema()->tag || '.temp'); //note: we won't be commiting it
  wrdschema->ApplySchemaDefinition([ __schemadefinition := StringToBlob(mergeschema) ]);
  TestEq(TRUE, ObjectExists(wrdschema->GetType("WRD_AUTHDOMAIN")));

  testfw->RollbackWork();
}

MACRO TestConversion()
{
  testfw->BeginWork();

  OBJECT testperson := testfw->GetWRDSchema()->^wrd_person->CreateEntity(
    [ test_datetime := MakeDatetime(2017,5,4,3,2,1)
    , wrd_contact_email := "testperson@beta.webhare.net"
    ]);

  OBJECT testperson2 := testfw->GetWRDSchema()->^wrd_person->CreateEntity(
    [ test_datetime := MAX_DATETIME
    , wrd_contact_email := "testperson2@beta.webhare.net"
    ]);

  DATETIME now := GetCurrentDatetime();

  TestEq("DATETIME", testfw->GetWRDSchema()->^wrd_person->GetAttribute("TEST_DATETIME").attributetypename);
  TestEq(MakeDatetime(2017,5,4,3,2,1), testperson->GetField("TEST_DATETIME"));

  testfw->GetWRDSchema()->ApplySchemaDefinition([ schemaresource := Resolve("data/testconversion-1.wrdschema.xml") ]);

  RECORD status := testfw->GetWRDSchema()->GetMigrationStatus("testfw:update1");
  TestEq(TRUE, RecordExists(status));
  TestEq(TRUE, status.finished > testfw->starttime);
  TestEq(FALSE, status.result.person_has_add_datetime);

  TestEq(FALSE, RecordExists(testfw->GetWRDSchema()->GetMigrationStatus("testfw:update2")));

  TestEqMembers([ revision := 1, result := [ postupdaterevision := 0, postentitiesrevision := 0, person_has_add_datetime := TRUE ]], testfw->GetWRDSchema()->GetMigrationStatus("testfw:update_post"), "*");
  TestEqMembers([ revision := 0, result := [ postupdaterevision := -1, postentitiesrevision := -1 ]], testfw->GetWRDSchema()->GetMigrationStatus("testfw:update_postentities"), "*", "postupdaterevision should still be -1 as the script hasn't rerun");

  TestEq("DATE", testfw->GetWRDSchema()->^wrd_person->GetAttribute("TEST_DATETIME").attributetypename);

  OBJECT testperson3 := testfw->GetWRDSchema()->^wrd_person->CreateEntity(
    [ test_datetime := MAX_DATETIME
    , wrd_contact_email := "testperson3@beta.webhare.net"
    ]);

  OBJECT testperson4 := testfw->GetWRDSchema()->^wrd_person->CreateEntity(
    [ test_datetime := MakeDateFromParts(0x7FFFFFFF,0) // GetRoundedDatetime(MAX_DATETIME, 86400 * 1000) ... except that GetRoundedDatetime doesn't actually round a MAX_DATETIME
    , wrd_contact_email := "testperson4@beta.webhare.net"
    ]);

  TestEq(MakeDatetime(2017,5,4,0,0,0), testperson->GetField("TEST_DATETIME")); //excess data is now hidden
  TestEq(MAX_DATETIME, testperson2->GetField("TEST_DATETIME"));
  TestEq(MAX_DATETIME, testperson3->GetField("TEST_DATETIME"));
  TestEq(MAX_DATETIME, testperson4->GetField("TEST_DATETIME"));

  OBJECT testperson5 := testfw->GetWRDSchema()->^wrd_person->CreateEntity(
    [ test_datetime := MAX_DATETIME
    , wrd_contact_email := "testperson5@beta.webhare.net"
    ]);

  OBJECT testperson6 := testfw->GetWRDSchema()->^wrd_person->CreateEntity(
    [ test_datetime := MakeDateFromParts(0x7FFFFFFF,0) // GetRoundedDatetime(MAX_DATETIME, 86400 * 1000) ... except that GetRoundedDatetime doesn't actually round a MAX_DATETIME
    , wrd_contact_email := "testperson6@beta.webhare.net"
    ]);

  OBJECT testperson7 := testfw->GetWRDSchema()->^wrd_person->CreateEntity(
    [ test_datetime := MakeDatetime(2017,5,4,3,2,1)
    , wrd_contact_email := "testperson7@beta.webhare.net"
    ]);

  TestEq(MAX_DATETIME, testperson5->GetField("TEST_DATETIME"));
  TestEq(MAX_DATETIME, testperson6->GetField("TEST_DATETIME"));
  TestEq(MakeDatetime(2017,5,4,0,0,0), testperson7->GetField("TEST_DATETIME"));

  testfw->GetWRDSchema()->^wrd_person->UpdateAttribute("TEST_DATETIME", [ attributetypename := "DATETIME" ]);
  TestEq(MakeDatetime(2017,5,4,3,2,1), testperson->GetField("TEST_DATETIME")); //but it did not actually change existing data
  TestEq(MAX_DATETIME, testperson2->GetField("TEST_DATETIME"));
  TestEq(MAX_DATETIME, testperson3->GetField("TEST_DATETIME"));
  TestEq(MakeDateFromParts(0x7FFFFFFF,0), testperson4->GetField("TEST_DATETIME"));
  TestEq(MAX_DATETIME, testperson5->GetField("TEST_DATETIME"));
  TestEq(MakeDateFromParts(0x7FFFFFFF, 0), testperson6->GetField("TEST_DATETIME"));
  TestEq(MakeDatetime(2017,5,4,3,2,1), testperson7->GetField("TEST_DATETIME")); //we kept the original data. we might change that at some point..

  testfw->GetWRDSchema()->ApplySchemaDefinition([ schemaresource := Resolve("data/testconversion-1.wrdschema.xml") ]);
  TestEqMembers([ revision := 1, result := [ postupdaterevision := 0, postentitiesrevision := 0 ]], testfw->GetWRDSchema()->GetMigrationStatus("testfw:update_post"), "*");

  testfw->GetWRDSchema()->ApplySchemaDefinition([ schemaresource := Resolve("data/testconversion-2.wrdschema.xml"), skipmigrations := TRUE ]);
  TestEq(FALSE, RecordExists(testfw->GetWRDSchema()->GetMigrationStatus("testfw:update3")));

  testfw->GetWRDSchema()->ApplySchemaDefinition([ schemaresource := Resolve("data/testconversion-2.wrdschema.xml"), fromexternal := TRUE ]);
  TestEq(FALSE, RecordExists(testfw->GetWRDSchema()->GetMigrationStatus("testfw:update3")));

  testfw->GetWRDSchema()->ApplySchemaDefinition([ schemaresource := Resolve("data/testconversion-2.wrdschema.xml")]);
  TestEqMembers([ revision := 2, result := [ postupdaterevision := 1, postentitiesrevision := 0 ]], testfw->GetWRDSchema()->GetMigrationStatus("testfw:update_post"), "*");

  testfw->GetWRDSchema()->ApplySchemaDefinition([ schemaresource := Resolve("data/testconversion-2.wrdschema.xml")]);
  TestEqMembers([ revision := 2, result := [ postupdaterevision := 1 ]], testfw->GetWRDSchema()->GetMigrationStatus("testfw:update_post"), "*", "this shouldn't change anything");

  RECORD status2 := testfw->GetWRDSchema()->GetMigrationStatus("testfw:update1");
  TestEq(status.finished, status2.finished); //shouldn't have run again
  RECORD status3 := testfw->GetWRDSchema()->GetMigrationStatus("testfw:update3");
  TestEq(TRUE, RecordExists(status3), "Migration update3 should have run!");
  TestEq(TRUE, status3.finished > testfw->starttime);
  TestEq(TRUE, status3.result.person_has_add_datetime);

  testfw->GetWRDSchema()->ApplySchemaDefinition([ schemaresource := Resolve("data/testconversion-1.wrdschema.xml")]);
  TestEqMembers([ revision := 2, result := [ postupdaterevision := 1 ]], testfw->GetWRDSchema()->GetMigrationStatus("testfw:update_post"), "*", "rerunning testconversion-1 should not retrigger update_post");

  testfw->GetWRDSchema()->ApplySchemaDefinition([ schemaresource := Resolve("data/testconversion-3.wrdschema.xml")]);
  TestEqMembers([ revision := 3, result := [ postupdaterevision := 2 ]], testfw->GetWRDSchema()->GetMigrationStatus("testfw:update_post"), "*");

  testfw->RollbackWork();
}


RunTestFramework([ PTR CreateWRDTestSchema
                 , PTR Setup
                 , PTR DomainValuesSync
                 , PTR TestCreate
                 , PTR TestCreateWithMetadata
                 , PTR TestMetaTypeChange
                 , PTR TestAsyncMetaTypeChange
                 , PTR TestRename
                 , PTR TestAttributePromotion
                 , PTR TestDomainTypeToParent
                 , PTR TestParent
                 , PTR TestAccountSettings
                 , PTR TestMerge
                 , PTR TestConversion
                 , PTR ExportXMLMetadata
                 , PTR VerifyWRDSettingsRecovery
                 ], [ wrdauth := FALSE
                    , schemaresource := Resolve("data/testconversion-0.wrdschema.xml")
                    ]);
