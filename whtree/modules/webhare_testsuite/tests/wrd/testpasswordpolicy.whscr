<?wh

LOADLIB "wh::crypto.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::os.whlib";

LOADLIB "mod::system/lib/testframework.whlib";

LOADLIB "mod::wrd/lib/auth/passwordpolicy.whlib";
LOADLIB "mod::wrd/lib/internal/auth/support.whlib";


MACRO TestPasswordBreachCount()
{
  TestEQ(TRUE, GetPasswordBreachCount("secret") > 0);
  TestEQ(FALSE, GetPasswordBreachCount(GenerateUFS128BitId()) > 0);
}

MACRO TestParseDuration()
{
  RECORD def :=
      [ years :=        0
      , months :=       0
      , days :=         0
      , weeks :=        0
      , hours :=        0
      , minutes :=      0
      , seconds :=      0
      , milliseconds := 0
      ];

  TestEQ(CELL[ ...def, years := 1 ], ParseDuration("P1Y"));
  TestEQ(CELL[ ...def, months := 1 ], ParseDuration("P1M"));
  TestEQ(CELL[ ...def, days := 1 ], ParseDuration("P1D"));
  TestEQ(CELL[ ...def, weeks := 1 ], ParseDuration("P1W"));
  TestEQ(CELL[ ...def, hours := 1 ], ParseDuration("PT1H"));
  TestEQ(CELL[ ...def, minutes := 1 ], ParseDuration("PT1M"));
  TestEQ(CELL[ ...def, seconds := 1 ], ParseDuration("PT1S"));
  TestEQ(CELL[ ...def, seconds := 1, milliseconds := 200 ], ParseDuration("PT1.2S"));
  TestEQ(CELL[ ...def, seconds := 1, milliseconds := 1 ], ParseDuration("PT1.0012S"));

  TestThrowsLike("Illegal duration specification*\"y\"*", PTR ParseDuration("P1y"));
  TestThrowsLike("Illegal duration specification*\"1D\"*", PTR ParseDuration("P1W1D"));
  TestThrowsLike("Illegal duration specification*\"S\"*", PTR ParseDuration("P1S"));
  TestThrowsLike("Illegal duration specification*", PTR ParseDuration("P-1S"));
  TestThrowsLike("Illegal duration specification*\"\"", PTR ParseDuration("PT"));
}

MACRO TestCheckParser()
{
  TestEQ(
    [ [ check :=      "hibp",       value := 0,   duration :=   "" ]
    , [ check :=      "minlength",  value := 1,   duration :=   "" ]
    , [ check :=      "lowercase",  value := 2,   duration :=   "" ]
    , [ check :=      "uppercase",  value := 3,   duration :=   "" ]
    , [ check :=      "digits",     value := 4,   duration :=   "" ]
    , [ check :=      "symbols",    value := 5,   duration :=   "" ]
    , [ check :=      "maxage",     value := 0,   duration :=   "PT01H" ]
    , [ check :=      "noreuse",    value := 0,   duration :=   "PT02H" ]
    ], ParsePasswordChecks("lowercase:2 noreuse:PT02H digits:4 uppercase:3 minlength:1 symbols:5 hibp maxage:PT01H"));

  TestEQ(
    [ [ check :=      "minlength",  value := 1,   duration :=   "" ]
    ], ParsePasswordChecks("invalid  minlength:1 maxage:15 invalid "));
  TestThrowsLike("*syntax*", PTR ParsePasswordChecks("invalid", [ strict := TRUE ]));
}

MACRO TestGetPasswordMinValueFrom()
{
  DATETIME now := MakeDateFromText("2024-02-28T09:54:56.120Z");
  DATETIME test := GetPasswordMinValidFrom("P1Y2M3DT1H2M3.004S", CELL[ now ]);
  TestEQ(MakeDateFromText("2022-12-25T08:52:53.116Z"), test);
}

MACRO TestDescribePasswordChecks()
{
  SetAnsiCmdMode("enabled");

  STRING res := DescribePasswordChecks("lowercase:2 noreuse:P02D digits:4 uppercase:3 minlength:1 symbols:5 hibp maxage:P01D");
  TestEQLike(
`The new password*
- *not*database*of*compromised*passwords*
- *1*characters*or*longer*
- *2*lowercase*
- *3*uppercase*
- *4*digits*
- *5*symbols*
- *changed*every*1*day*
- *not*reused*2*days*`, DescribePasswordChecks("lowercase:2 noreuse:P02D digits:4 uppercase:3 minlength:1 symbols:5 hibp maxage:P01D"));
}

MACRO TestCheckPassword()
{
  TestEQ("", CheckPassword("", "").message);

  TestEQ("", CheckPassword("lowercase:1 uppercase:2 digits:3 symbols:4 minlength:10", "aBC456#()@").message);
  TestEQLike("*10*characters*1*lowercase*3*digits*", CheckPassword("lowercase:1 uppercase:2 digits:3 symbols:4 minlength:10", "BC46#()@").message);
  TestEQ([ "minlength", "lowercase", "digits" ], CheckPassword("lowercase:1 uppercase:2 digits:3 symbols:4 minlength:10", "BC46#()@").failedchecks);

  // test reuse
  TestEQLike("", CheckPassword("noreuse:P2D", "secret",
      [ authenticationsettings :=
          [ version :=    1
          , passwords :=  [ [ validfrom :=      DEFAULT DATETIME
                            , passwordhash :=   testfw->hashedpasswords.secret
                            ]
                          , [ validfrom :=      AddTimeTodate(-5000, AddDaysToDate(-2, GetCurrentDateTime()))
                            , passwordhash :=   "*"
                            ]
                          ]
          ]
      ]).message);

  TestEQLike("*not*reused*2*days*", CheckPassword("noreuse:P2D", "secret",
      [ authenticationsettings :=
          [ version :=    1
          , passwords :=  [ [ validfrom :=      DEFAULT DATETIME
                            , passwordhash :=   testfw->hashedpasswords.secret
                            ]
                          , [ validfrom :=      AddTimeTodate(+5000, AddDaysToDate(-2, GetCurrentDateTime()))
                            , passwordhash :=   "*"
                            ]
                          ]
          ]
      ]).message);
}


MACRO TestCheckAuthenticationSettings()
{
  TestEQLike("", CheckAuthenticationSettings("maxage:P2D",
      [ version :=    1
      , passwords :=  [ [ validfrom :=      AddTimeTodate(+5000, AddDaysToDate(-2, GetCurrentDateTime()))
                        , passwordhash :=   testfw->hashedpasswords.secret
                        ]
                      ]
      ]).message);

  TestEQLike("*changed*every*2*days*", CheckAuthenticationSettings("maxage:P2D",
      [ version :=    1
      , passwords :=  [ [ validfrom :=      AddTimeTodate(-5000, AddDaysToDate(-2, GetCurrentDateTime()))
                        , passwordhash :=   testfw->hashedpasswords.secret
                        ]
                      ]
      ]).message);
}

RunTestFramework([ PTR TestPasswordBreachCount
                 , PTR TestParseDuration
                 , PTR TestCheckParser
                 , PTR TestGetPasswordMinValueFrom
                 , PTR TestDescribePasswordChecks
                 , PTR TestCheckPassword
                 , PTR TestCheckAuthenticationSettings
                 ]);
