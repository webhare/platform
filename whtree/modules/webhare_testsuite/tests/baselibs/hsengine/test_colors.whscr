<?wh
LOADLIB "wh::internal/hsselftests.whlib";
LOADLIB "wh::graphics/core.whlib";

STRING FUNCTION FormatColor(INTEGER color)
{
  RETURN "0x" || Right("0000000" || ToUppercase(ToString(color, 16)), 8);
}

MACRO CssColorTest()
{
  OpenTest("TestColors: CssColorTest");

  // First, test the color formatting function :-)
  TestEq( "0x00000000", FormatColor(0x00000000));
  TestEq( "0x00000001", FormatColor(0x00000001));
  TestEq( "0x10000000", FormatColor(0x10000000));
  TestEq( "0x12345678", FormatColor(0x12345678));
  TestEq( "0xFEDCBA09", FormatColor(0xFEDCBA09));

  // Test 'transparent'
  TestEq( "0x00000000", FormatColor(GfxCreateColorFromCss("transparent")));
  TestEq( "0x00000000", FormatColor(GfxCreateColorFromCss("  TRANSPARENT    ")));
  TestEq( "0x00000000", FormatColor(GfxCreateColorFromCss("\r\nTransparent\r\n")));

  // Test named colors
  TestEq( "0xFF000000", FormatColor(GfxCreateColorFromCss("Black")));
  TestEq("0xFFC0C0C0", FormatColor(GfxCreateColorFromCss("silver")));
  TestEq("0xFF808080", FormatColor(GfxCreateColorFromCss("GRAY")));
  TestEq("0xFFFFFFFF", FormatColor(GfxCreateColorFromCss("White")));
  TestEq("0xFF800000", FormatColor(GfxCreateColorFromCss("maroon")));
  TestEq("0xFFFF0000", FormatColor(GfxCreateColorFromCss("RED")));
  TestEq("0xFF800080", FormatColor(GfxCreateColorFromCss("Purple")));
  TestEq("0xFFFF00FF", FormatColor(GfxCreateColorFromCss("fuchsia")));
  TestEq("0xFF008000", FormatColor(GfxCreateColorFromCss("GREEN")));
  TestEq("0xFF00FF00", FormatColor(GfxCreateColorFromCss("Lime")));
  TestEq("0xFF808000", FormatColor(GfxCreateColorFromCss("olive")));
  TestEq("0xFFFFFF00", FormatColor(GfxCreateColorFromCss("YELLOW")));
  TestEq("0xFF000080", FormatColor(GfxCreateColorFromCss("Navy")));
  TestEq("0xFF0000FF", FormatColor(GfxCreateColorFromCss("blue")));
  TestEq("0xFF008080", FormatColor(GfxCreateColorFromCss("TEAL")));
  TestEq("0xFF00FFFF", FormatColor(GfxCreateColorFromCss("Aqua")));

  // Test hex values
  TestEq("0xFF000000", FormatColor(GfxCreateColorFromCss("#000")));
  TestEq("0xFF000000", FormatColor(GfxCreateColorFromCss("#000000")));
  TestEq("0xFFFFFFFF", FormatColor(GfxCreateColorFromCss("#FFF")));
  TestEq("0xFFFFFFFF", FormatColor(GfxCreateColorFromCss("#ffffff")));
  TestEq("0xFF112233", FormatColor(GfxCreateColorFromCss("#123")));
  TestEq("0xFFABCDEF", FormatColor(GfxCreateColorFromCss("#abcdef")));

  // Test RGB
  TestEq("0xFF000000", FormatColor(GfxCreateColorFromCss("rgb(0, 0, 0)")));
  TestEq("0xFF000000", FormatColor(GfxCreateColorFromCss("rgb(0,0,0)")));
  TestEq("0xFFFF8000", FormatColor(GfxCreateColorFromCss("rgb(255, 128, 0)")));
  TestEq("0xFF0080FF", FormatColor(GfxCreateColorFromCss("rgb(0,128,255)")));
  TestEq("0xFFFF8000", FormatColor(GfxCreateColorFromCss("rgb(256, 128, -1)")));
  TestEq("0xFF0080FF", FormatColor(GfxCreateColorFromCss("rgb(-1234,128,5678)")));

  // Test RGBA
  TestEq("0x00FF8000", FormatColor(GfxCreateColorFromCss("rgba(255, 128, 0, 0)")));
  TestEq("0xFF0080FF", FormatColor(GfxCreateColorFromCss("rgba(0, 128, 255, 1)")));
  TestEq("0x40FFFFFF", FormatColor(GfxCreateColorFromCss("rgba(255, 255, 255, .25)")));
  TestEq("0x80FFFFFF", FormatColor(GfxCreateColorFromCss("rgba(255, 255, 255, 0.5)")));
  TestEq("0xFFFFFFFF", FormatColor(GfxCreateColorFromCss("rgba(255, 255, 255, 2)")));
  TestEq("0x00FFFFFF", FormatColor(GfxCreateColorFromCss("rgba(255, 255, 255, -1)")));
  TestEq("0x12345678", FormatColor(GfxCreateColorFromCss("rgba(52, 86, 120, .07)")));

  // Test HSL
  TestEq("0xFFFFFFFF", FormatColor(GfxCreateColorFromCss("hsl(0, 0%, 100%)")));
  TestEq("0xFFFFFFFF", FormatColor(GfxCreateColorFromCss("hsl(78, 0%, 100%)")));
  TestEq("0xFFFFFFFF", FormatColor(GfxCreateColorFromCss("hsl(321, 0%, 100%)")));
  TestEq("0xFF808080", FormatColor(GfxCreateColorFromCss("hsl(0, 0%, 50%)")));
  TestEq("0xFF000000", FormatColor(GfxCreateColorFromCss("hsl(0, 0%, 0%)")));
  TestEq("0xFFFF0000", FormatColor(GfxCreateColorFromCss("hsl(0, 100%, 50%)")));
  TestEq("0xFF00FF00", FormatColor(GfxCreateColorFromCss("hsl(120, 100%, 50%)")));
  TestEq("0xFF0000FF", FormatColor(GfxCreateColorFromCss("hsl(240, 100%, 50%)")));
  TestEq("0xFFBF40BF", FormatColor(GfxCreateColorFromCss("hsl(300, 50%, 50%)")));
  TestEq("0xFF8080FF", FormatColor(GfxCreateColorFromCss("hsl(240, 100%, 75%)")));
  TestEq("0xFF352697", FormatColor(GfxCreateColorFromCss("hsl(248, 60%, 37%)")));
  TestEq("0xFF362698", FormatColor(GfxCreateColorFromCss("hsl(248.3, 60.1%, 37.3%)")));
  TestEq("0xFF966313", FormatColor(GfxCreateColorFromCss("hsl(37, 78%, 33%)")));
  TestEq("0xFF4E6553", FormatColor(GfxCreateColorFromCss("hsl(135, 13%, 35%)")));
  TestEq("0xFFED7651", FormatColor(GfxCreateColorFromCss("hsl(14.3, 81.7%, 62.4%)")));
  TestEq("0xFFB430E5", FormatColor(GfxCreateColorFromCss("hsl(-76.3, 77.5%, 54.2%)")));

  // Test unpacking colors
  TestEq(255, GfxUnpackColor(GfxCreateColorFromCss("#ffffff")).r);
  TestEq(255, GfxUnpackColor(GfxCreateColorFromCss("#ffffff")).g);
  TestEq(255, GfxUnpackColor(GfxCreateColorFromCss("#ffffff")).b);
  TestEq(255, GfxUnpackColor(GfxCreateColorFromCss("#ffffff")).a);

  TestEq(0, GfxUnpackColor(GfxCreateColorFromCss("#000")).r);
  TestEq(0, GfxUnpackColor(GfxCreateColorFromCss("#000")).g);
  TestEq(0, GfxUnpackColor(GfxCreateColorFromCss("#000")).b);
  TestEq(255, GfxUnpackColor(GfxCreateColorFromCss("#000")).a);

  TestEq(52, GfxUnpackColor(GfxCreateColorFromCss("rgba(52, 86, 120, .07)")).r); // 0x12345678
  TestEq(86, GfxUnpackColor(GfxCreateColorFromCss("rgba(52, 86, 120, .07)")).g);
  TestEq(120, GfxUnpackColor(GfxCreateColorFromCss("rgba(52, 86, 120, .07)")).b);
  TestEq(18, GfxUnpackColor(GfxCreateColorFromCss("rgba(52, 86, 120, .07)")).a);

  CloseTest("TestColors: CssColorTest");
}

CssColorTest();
