<?wh
/// @short Internet functions test

LOADLIB "wh::internal/hsselftests.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::os.whlib";
LOADLIB "wh::datetime.whlib";

RECORD ARRAY errors;
RECORD result;

MACRO WaitForMultipleTest()
{
  OpenTest("TestOS: WaitForMultiple");

  DATETIME before_wait := GetCurrentDateTime();
  INTEGER waiter;

//  Print("Current time: " || FormatDateTime("%d-%m-%Y %H:%M:%S.%Q", before_wait) || "\n");

  waiter := WaitForMultiple(DEFAULT INTEGER ARRAY, DEFAULT INTEGER ARRAY, 1000);
  DATETIME after_wait := GetCurrentDateTime();
  DATETIME expect_min := AddTimeToDate(750, before_wait); // windows measures in 10ms quantums

//  Print("After time: " || FormatDateTime("%d-%m-%Y %H:%M:%S.%Q", after_wait) || "\n");

  TestEq(-1, waiter);
  TestEq(true, after_wait > expect_min);

  waiter := WaitForMultiple(DEFAULT INTEGER ARRAY, DEFAULT INTEGER ARRAY, 0);
  TestEq(-1, waiter);

  CloseTest("TestOS: WaitForMultiple");
}

MACRO ProcessTest()
{
  STRING whroot := GetEnvironmentVariable("WEBHARE_DIR");
  IF(whroot = "")
    THROW NEW Exception(`Tests require WEBHARE_DIR to be set (wh runtest would...)`);

  STRING executable := MergePath(whroot, "bin/wh");
  STRING ARRAY extraparams := ["run"];

  OBJECT process := CreateProcess(executable, extraparams CONCAT [ "mod::webhare_testsuite/tests/baselibs/hsengine/testdata_os_a.whscr", "0", "1", "2" ], TRUE, TRUE, TRUE, FALSE);
  process->Start();
  WaitUntil([ read := process->output_handle ], AddTimeToDate(200, GetCurrentDateTime()));
  TestEq("Args:0,1,2\n", ReadLineFrom(process->output_handle, 4096, FALSE));
  PrintTo(process->input_handle, "Test\n");
  WaitUntil([ read := process->output_handle ], AddTimeToDate(200, GetCurrentDateTime()));
  TestEq("Reflect:Test\n", ReadLineFrom(process->output_handle, 4096, FALSE));
  PrintTo(process->input_handle, "Error\n");
  STRING line;
  WHILE (TRUE)
  {
    WaitUntil([ read := process->errors_handle ], AddTimeToDate(200, GetCurrentDateTime()));
    line := ReadLineFrom(process->errors_handle, 4096, FALSE);
    IF (line NOT LIKE "[*") // Debug output
      BREAK;
  }
  TestEq("ReflectError:Error\n", line);
  process->SendInterrupt();
  WaitUntil([ read := process->output_handle ], AddTimeToDate(200, GetCurrentDateTime()));
  TestEq("Interrupt\n", ReadLineFrom(process->output_handle, 4096, FALSE));
  process->Terminate();
  process->Close();
}

WaitForMultipleTest();
ProcessTest();
