export function helloLength(node: Element): number {
  return (node.textContent || "").length;
}
