<?wh
LOADLIB "wh::files.whlib";
LOADLIB "wh::ooxml/spreadsheet.whlib";

LOADLIB "mod::publisher/lib/control.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/whfs.whlib";

LOADLIB "mod::tollium/lib/testframework.whlib";

LOADLIB "mod::consilio/lib/pagelists.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";


MACRO PrepIt()
{
  testfw->BeginWork();
  testfw->GetUserObject("marge")->UpdateGrant("grant", "system:fs_fullaccess",  OpenTestsuiteSite()->rootobject->id, testfw->GetUserObject("marge"), [ allowselfassignment := TRUE ]);
  testfw->GetUserObject("lisa")->UpdateGrant("grant", "system:fs_fullaccess",  OpenTestsuiteSite()->rootobject->id, testfw->GetUserObject("lisa"), [ allowselfassignment := TRUE ]);
  testfw->CommitWork();
}

ASYNC MACRO TestSeoSettings()
{
  testfw->SetTestUser("lisa");

  testfw->BeginWork();

  OBJECT root := OpenTestsuiteSite()->rootobject;
  OBJECT rtdtype := OpenWHFSType("http://www.webhare.net/xmlns/publisher/richdocumentfile");

  OBJECT seotestroot := GetTestsuiteTempFolder()->CreateFolder([name := "seotab"]);
  OBJECT seotestfile := seotestroot->CreateFile([name := "seotestfile", type := rtdtype->id, publish := TRUE ]);
  OBJECT seotitlefile := seotestroot->CreateFile([name := "seotitlefile", type := rtdtype->id, publish := TRUE, title := "navtitle seotitlefile" ]);
  OBJECT seoheadertitlefolder := seotestroot->CreateFolder([name := "seotitle-headertitle-folder" ]);
  OBJECT protectedseotestfile := seotestroot->CreateFile([name := "seotestfile-protected-seotab", type := rtdtype->id, publish := TRUE ]);

  OBJECT noindexfolder := seotestroot->CreateFolder([name := "noindex"]);
  noindexfolder->SetInstanceData("http://www.webhare.net/xmlns/publisher/seosettings", [noindex := TRUE]);
  OBJECT noindexsubfolder := noindexfolder->CreateFolder([name := "noindexsub"]);
  OBJECT noindexsubfile := noindexsubfolder->CreateFile([name := "noindexsubfile", type := rtdtype->id, publish := TRUE ]);
  OBJECT noindeximage := noindexsubfolder->CreateFile([name:="img.jpg", data := GetHarescriptResource("mod::system/web/tests/snowbeagle.jpg"), publish := TRUE ]);

  //we were ignoring settings on index documents as they weren't in the path used for breadcrumbs and we were sharing that. fixing that
  OBJECT noindexsubfolderindex := noindexsubfolder->CreateFile([name := "index", type := rtdtype->id, publish := TRUE ], [ setindex := TRUE ]);
  noindexsubfolderindex->SetInstanceData("http://www.webhare.net/xmlns/publisher/seosettings", [ noarchive := TRUE]);

  OBJECT nofollowfolder := seotestroot->CreateFolder([name := "nofollow"]);
  nofollowfolder->SetInstanceData("http://www.webhare.net/xmlns/publisher/seosettings", [nofollow := TRUE]);
  OBJECT nofollowsubfolder := nofollowfolder->CreateFolder([name := "nofollowsub"]);
  OBJECT nofollowsubfile := nofollowsubfolder->CreateFile([name := "nofollowsubfile", type := rtdtype->id, publish := TRUE ]);
  nofollowsubfile->SetInstanceData("http://www.webhare.net/xmlns/publisher/seosettings", [ canonical := noindexsubfile->id ]);

  OBJECT forced_noindex_file := seotestroot->CreateFile([name := "robots-noindex", type := rtdtype->id, publish := TRUE ]);
  OBJECT forced_nofollow_file := seotestroot->CreateFile([name := "robots-nofollow", type := rtdtype->id, publish := TRUE ]);
  OBJECT forced_noarchive_file := seotestroot->CreateFile([name := "robots-noarchive", type := rtdtype->id, publish := TRUE ]);
  OBJECT forced_noindex_nofollow_noarchive_file := seotestroot->CreateFile([name := "robots-noindex-nofollow-noarchive", type := rtdtype->id, publish := TRUE ]);
  OBJECT forced_noindex_nofollow_yesfollow_file := seotestroot->CreateFile([name := "robots-noindex-nofollow-yesfollow", type := rtdtype->id, publish := TRUE ]);

  testfw->CommitWork();

  RECORD ARRAY links := GenerateSitemapLinks(seotestroot->id);
  TestEq(TRUE, RecordExists(SELECT FROM links WHERE link = nofollowsubfile->link));
  TestEq(FALSE, RecordExists(SELECT FROM links WHERE link = noindexsubfile->link));

  TestEq(RECORD[], GenerateSitemapLinks(noindexfolder->id), "Folder has noindex so this list should be empty");
  TestEq(RECORD[], GenerateSitemapLinks(noindexsubfolder->id), "Parent has noindex so this list should be empty");

  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:app", [ calltype := "direct", params := STRING[ noindeximage->whfspath ] ]));
  AWAIT ExpectScreenChange(+1, PTR TTClick("props"));

  TestEq(FALSE, ObjectExists(TT(":SEO", [ allowmissing := TRUE ])), "The SEO tab should NOT appear for objects that aren't templated");
  TestEq(FALSE, TT("seotitle", [ findinvisible := TRUE ])->visible);

  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  AWAIT ExpectScreenChange(-1, PTR TTEscape); //exit publisher

  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:app", [ calltype := "direct", params := STRING[ seotitlefile->whfspath ] ]));
  AWAIT ExpectScreenChange(+1, PTR TTClick("props"));

  TestEq(TRUE, TT("seotitle", [ findinvisible := TRUE ])->visible);
  TT("seotitle")->value := "The Seo Title";

  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  AWAIT ExpectScreenChange(-1, PTR TTEscape); //exit publisher

  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:app", [ calltype := "direct", params := STRING[ seoheadertitlefolder->whfspath ] ]));
  AWAIT ExpectScreenChange(+1, PTR TTClick("props"));

  TestEq(FALSE, TT("seotitle", [ findinvisible := TRUE ])->visible);

  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  AWAIT ExpectScreenChange(-1, PTR TTEscape); //exit publisher

  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:app", [ calltype := "direct", params := STRING[ protectedseotestfile->whfspath ] ]));
  AWAIT ExpectScreenChange(+1, PTR TTClick("props"));

  TestEq(FALSE, ObjectExists(TT(":SEO", [ allowmissing := TRUE ])), "The SEO tab should be invisible to Lisa");

  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  AWAIT ExpectScreenChange(-1, PTR TTEscape); //exit publisher

  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:app", [ calltype := "direct", params := STRING[ noindexfolder->whfspath ] ]));
  AWAIT ExpectScreenChange(+1, PTR TTClick("props"));

  TestEq(TRUE, ObjectExists(TT(":SEO", [ allowmissing := TRUE ])), "The SEO tab should appear!");
  TestEq("SEO", TT("maintabs")->pages[1]->title);

  TestEq(TRUE, TT("noindex")->value);
  TestEq(TRUE, TT("noindex")->enabled);
  TestEq(FALSE, TT("nofollow")->value);
  TestEq(TRUE, TT("nofollow")->enabled);
  TestEq(FALSE, TT("canonical", [ findinvisible := TRUE ])->visible,"Should not be visible on a folder");

  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  AWAIT ExpectScreenChange(-1, PTR TTEscape); //exit publisher

  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:app", [ calltype := "direct", params := STRING[ noindexsubfolder->whfspath ] ]));
  AWAIT ExpectScreenChange(+1, PTR TTClick("props"));

  TestEq(TRUE, ObjectExists(TT("noindex")));
  TestEq(TRUE, TT("noindex")->value);
  TestEq(FALSE, TT("noindex")->enabled);
  TestEq(FALSE, TT("nofollow")->value);
  TestEq(TRUE, TT("nofollow")->enabled);

  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  AWAIT ExpectScreenChange(-1, PTR TTEscape); //exit publisher

  //Should not have modified the noindex setting
  TestEq(FALSE, noindexsubfolder->GetInstanceData("http://www.webhare.net/xmlns/publisher/seosettings").noindex);

  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:app", [ calltype := "direct", params := STRING[ nofollowsubfolder->whfspath ] ]));
  AWAIT ExpectScreenChange(+1, PTR TTClick("props"));

  TestEq(TRUE, ObjectExists(TT("noindex")));
  TestEq(FALSE, TT("noindex")->value);
  TestEq(TRUE, TT("noindex")->enabled);
  TestEq(TRUE, TT("nofollow")->value);
  TestEq(FALSE, TT("nofollow")->enabled);

  TestEq(FALSE, TT("customrobots", [ findinvisible := TRUE ])->visible);

  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  AWAIT ExpectScreenChange(-1, PTR TTEscape); //exit publisher

  ///////////////////////////////////
  //
  // As Marge, we can see the protected seo tab Lisa couldn't see
  //
  testfw->SetTestUser("marge");
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:app", [ calltype := "direct", params := STRING[ protectedseotestfile->whfspath ] ]));
  AWAIT ExpectScreenChange(+1, PTR TTClick("props"));

  TestEq(TRUE, ObjectExists(TT(":SEO", [ allowmissing := TRUE ])), "The SEO tab should be visible to Marge");

  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  AWAIT ExpectScreenChange(-1, PTR TTEscape); //exit publisher

  ///////////////////////////////////
  //
  // Open nofollow/nofollowsub as sysop, set a customrobots value
  //
  testfw->SetTestUser("sysop");

  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:app", [ calltype := "direct", params := STRING[ nofollowsubfolder->whfspath ] ]));
  AWAIT ExpectScreenChange(+1, PTR TTClick("props"));


  TestEq(TRUE, TT("customrobots")->visible);
  TT("customrobots")->value := "ZwoBot";

  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  AWAIT ExpectScreenChange(-1, PTR TTEscape); //exit publisher

  ///////////////////////////////////
  //
  // Open nofollow/nofollowsub/nofollowsubfile as sysop, check the customrobots value is there as a placeceholder
  //
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:app", [ calltype := "direct", params := STRING[ nofollowsubfile->whfspath ] ]));
  AWAIT ExpectScreenChange(+1, PTR TTClick("props"));

  TestEq(TRUE, TT("customrobots")->visible);
  TestEq(TRUE, TT("customrobots")->enabled);
  TestEq("ZwoBot", TT("customrobots")->placeholder);
  TestEq(TRUE, TT("canonical")->visible, "Canonical should be visible on files");

  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  AWAIT ExpectScreenChange(-1, PTR TTEscape); //exit publisher

  ///////////////////////////////////
  //
  // Open robots-noindex-nofollow-yesfollow as sysop, check that noindex is locked and nofollow isn't
  //
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:app", [ calltype := "direct", params := STRING[ forced_noindex_nofollow_yesfollow_file->whfspath ] ]));
  AWAIT ExpectScreenChange(+1, PTR TTClick("props"));

  TestEq(TRUE, TT("noindex")->value);
  TestEq(FALSE, TT("noindex")->enabled);
  TestEq(FALSE, TT("nofollow")->value);
  TestEq(TRUE, TT("nofollow")->enabled);

  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  // Create a SEO report
  TT("foldertree")->selection := SELECT * FROM TT("foldertree")->rows WHERE rowkey = seotestroot->id;
  AWAIT ExpectScreenChange(+1, PTR TTClick("exportseoreport"));
  RECORD file := (AWAIT ExpectSentWebFile(PTR TTClick(":Download"))).file;
  RECORD ARRAY exprows := SELECT * FROM GetOOXMLSpreadsheetRows(file.data);
  dumpvalue(exprows,'boxed');

  TestEq(seotestroot->objecturl, exprows[0].url, "First link should be the export root");
  Testeq("", (SELECT AS STRING link FROM system.fs_objects WHERE id = seotestroot->id), "Check that we reported objecturl even though seotestroot still has no 'link'");
  TestEq("Is set", SELECT AS STRING nofollow FROM exprows WHERE exprows."site path" = "/tmp/seotab/nofollow/");
  TestEq("Is set from /tmp/seotab/nofollow/", SELECT AS STRING nofollow FROM exprows WHERE exprows."site path" = "/tmp/seotab/nofollow/nofollowsub/");
  TestEq("Is set from /tmp/seotab/nofollow/", SELECT AS STRING nofollow FROM exprows WHERE exprows."site path" = "/tmp/seotab/nofollow/nofollowsub/nofollowsubfile");
  TestEqStructure([[ seotitle := "The Seo Title", navigationtitle := "navtitle seotitlefile" ]]
                  , SELECT * FROM exprows WHERE exprows."site path" = "/tmp/seotab/nofollow/nofollowsub/seotitlefile"
                  , "*");
  TestEqStructure([[ seotitle := "", navigationtitle := "navtitle headertitlefile" ]]
                  , SELECT * FROM exprows WHERE exprows."site path" = "/tmp/seotab/nofollow/nofollowsub/headertitlefile"
                  , "*");
  TestEq("> ", SELECT AS STRING "> " || exprows."canonical url" FROM exprows WHERE exprows."site path" = "/tmp/seotab/noindex/noindexsub/noindexsubfile");
  TEstEq("> " || noindexsubfile->link, SELECT AS STRING "> " || exprows."canonical url" FROM exprows WHERE exprows."site path" = "/tmp/seotab/nofollow/nofollowsub/nofollowsubfile");

  //TestEqMembers([ maincategory := "TEST: another of my categories", sub1 := "TEST: A Subcategory" ], testrows[2],"*");
  AWAIT ExpectScreenChange(-1, DEFAULT MACRO PTR); //download self-closes

  AWAIT ExpectScreenChange(-1, PTR TTEscape); //exit publisher


  ///////////////////////////////////
  //
  // Check the output of all these files
  //
  OBJECT metarobots, linkcanonical;
  WaitForPublishCompletion(root->id);

  TestEq(TRUE, testfw->browser->GotoWebPage(noindexsubfile->link));
  metarobots := testfw->browser->document->QuerySelector("meta[name='robots']");
  TestEq("noindex,follow", metarobots->GetAttribute("content"));
  linkcanonical := testfw->browser->document->QuerySelector("link[rel='canonical']");
  TestEq(noindexsubfile->link, linkcanonical->GetAttribute("href"), "Static files consider themselves their own canonical by default");

  TestEq(TRUE, testfw->browser->GotoWebPage(noindexsubfolderindex->link));
  metarobots := testfw->browser->document->QuerySelector("meta[name='robots']");
  TestEq("noindex,noarchive,follow", metarobots->GetAttribute("content"));

  TestEq(TRUE, testfw->browser->GotoWebPage(nofollowsubfile->link));
  metarobots := testfw->browser->document->QuerySelector("meta[name='robots']");
  TestEq("nofollow,ZwoBot", metarobots->GetAttribute("content"));
  linkcanonical := testfw->browser->document->QuerySelector("link[rel='canonical']");
  TestEq(noindexsubfile->link, linkcanonical->GetAttribute("href"));

  TestEq(TRUE, testfw->browser->GotoWebPage(forced_noindex_file->link));
  metarobots := testfw->browser->document->QuerySelector("meta[name='robots']");
  TestEq("noindex,follow", metarobots->GetAttribute("content"));

  TestEq(TRUE, testfw->browser->GotoWebPage(forced_nofollow_file->link));
  metarobots := testfw->browser->document->QuerySelector("meta[name='robots']");
  TestEq("nofollow", metarobots->GetAttribute("content"));

  TestEq(TRUE, testfw->browser->GotoWebPage(forced_noarchive_file->link));
  metarobots := testfw->browser->document->QuerySelector("meta[name='robots']");
  TestEq("noarchive", metarobots->GetAttribute("content"));

  TestEq(TRUE, testfw->browser->GotoWebPage(forced_noindex_nofollow_noarchive_file->link));
  metarobots := testfw->browser->document->QuerySelector("meta[name='robots']");
  TestEq("noindex,nofollow,noarchive", metarobots->GetAttribute("content"));

  TestEq(TRUE, testfw->browser->GotoWebPage(forced_noindex_nofollow_yesfollow_file->link));
  metarobots := testfw->browser->document->QuerySelector("meta[name='robots']");
  TestEq("noindex,follow", metarobots->GetAttribute("content"));

  TestEq(TRUE, testfw->browser->GotoWebPage(seotitlefile->link));
  metarobots := testfw->browser->document->QuerySelector("meta[name='robots']");
  TestEq("The Seo Title", testfw->browser->document->QuerySelector("title")->textcontent);
}

RunTestframework([ PTR PrepIt
                 , PTR TestSeoSettings
                 ], [ testusers := [[ login := "sysop", grantrights := ["system:sysop"] ]
                                   ,[ login := "lisa" ]
                                   ,[ login := "marge", grantrights := ["system:supervisor"] ]
                                   ]
                    ]);
