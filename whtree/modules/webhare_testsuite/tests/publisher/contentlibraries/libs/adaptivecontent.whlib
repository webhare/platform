<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";

LOADLIB "mod::publisher/lib/contentlibraries.whlib";
LOADLIB "mod::publisher/lib/internal/contentlibraries/publishing.whlib";

LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/whfs.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";

PUBLIC RECORD ARRAY FUNCTION ListSlotsNeedingRepublish()
{
  RETURN FilterSlotsNeedingRepublish(ListDatabaseSlots());
}

PUBLIC RECORD FUNCTION SetupAdaptiveContentTest(RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(CELL[], options);

  testfw->BeginWork();
  OBJECT slotsfolder := GetTestsuiteTempFolder()->EnsureFolder([name := "slots", typens := "http://www.webhare.net/xmlns/publisher/contentlibraries/slots" ]);
  OBJECT beaconsfolder := GetTestsuiteTempFolder()->EnsureFolder([name := "beacons", typens := "http://www.webhare.net/xmlns/publisher/contentlibraries/beacons" ]);

  OBJECT beaconstore := OpenAdaptiveContentBeaconStore(beaconsfolder->id);
  INTEGER beacon1id := beaconstore->StoreBeacon(0, [ title := "Is Employee" ]);
  RECORD beacon1 := beaconstore->GetBeacon(beacon1id);

  INTEGER beacon2id := beaconstore->StoreBeacon(0, [ title := "Content Widget Shown" ]);
  RECORD beacon2 := beaconstore->GetBeacon(beacon2id);

  INTEGER beacon3id := beaconstore->StoreBeacon(0, [ title := "Form Thank You Page" ]);
  RECORD beacon3 := beaconstore->GetBeacon(beacon3id);

  OBJECT slotstore := OpenAdaptiveContentStore(slotsfolder->id);
  OBJECT headerslot := slotstore->CreateSlot(
      [ typens := "http://www.webhare.net/xmlns/webhare_testsuite/headerslot"
      , title := "Headerslot"
      ]);

  TestEq(TRUE, RecordExists(SELECT FROM ListSlotsNeedingRepublish() WHERE id = headerslot->id));
  RepublishSlots(ListSlotsNeedingRepublish());
  TestEq(FALSE, RecordExists(SELECT FROM ListSlotsNeedingRepublish() WHERE id = headerslot->id));

  OBJECT headerwidget := headerslot->CreateWidget(
      [ typens := "http://www.webhare.net/xmlns/webhare_testsuite/headeroption1"
      , condition := // Show during January 2000
          [ _type := "and"
          , conditions :=
              [ [ _type := "visitafterdate", date := MakeDate(2000, 1, 1) ]
              , [ _type := "visitbeforedate", date := MakeDate(2000, 2, 1) ]
              ]
          ]
      ]);
  headerwidget->widgetfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/headeroption1", [ text := "Happy New Millennium!"]);

  OBJECT slot1 := slotstore->CreateSlot(
      [ typens := "http://www.webhare.net/xmlns/webhare_testsuite/contentslot"
      , title := "A SLOT"
      ]);

  OBJECT widget1a := slot1->CreateWidget(
      [ typens := "http://www.webhare.net/xmlns/webhare_testsuite/contentoption1"
      , title := "Widget 1A"
      , condition := [ _type := "beacon", beacon := beacon1.name, maxdays := 1 ]
      ]);

  TestEq(1, widget1a->widgetfile->ordering);
  widget1a->widgetfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/contentoption1",
      [ text := [ htmltext := StringToBlob(`<html><body><p class="normal">Widget 1.A</p></body></html>`) ] ]);

  TestEq(TRUE, RecordExists(SELECT FROM ListSlotsNeedingRepublish() WHERE id = slot1->id));
  RepublishSlots(ListSlotsNeedingRepublish());
  TestEq(FALSE, RecordExists(SELECT FROM ListSlotsNeedingRepublish() WHERE id = slot1->id));

  OBJECT widget1b := slot1->CreateWidget(
      [ typens := "http://www.webhare.net/xmlns/webhare_testsuite/contentoption1"
      , title := "Widget 1B"
      , condition := [ _type := "beacon", beacon := beacon1.name ]
      ]);
  TestEq(2, widget1b->widgetfile->ordering);
  widget1b->widgetfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/contentoption1",
      [ text :=
        [ htmltext := StringToBlob(`<html><body><p class="normal">Widget 1.B</p><div class="wh-rtd-embeddedobject" data-instanceid="beacon" /></body></html>`)
        , instances :=
          [ [ instanceid := "beacon"
            , data :=
              [ whfstype := "http://www.webhare.net/xmlns/publisher/widgets/triggerbeacon"
              , beacon := beacon2id
              ]
            ]
          ]
        ]
      ]);

  OBJECT widget1c := slot1->CreateWidget(
      [ typens := "http://www.webhare.net/xmlns/webhare_testsuite/contentoption1"
      , title := "Widget 1C"
      ]);
  TestEq(3, widget1c->widgetfile->ordering);
  widget1c->widgetfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/contentoption1",
      [ text := [ htmltext := StringToBlob(`<html><body><p class="normal">Widget 1.C</p></body></html>`) ] ]);

  OBJECT slot2 := slotstore->CreateSlot(
      [ typens := "http://www.webhare.net/xmlns/webhare_testsuite/contentslot"
      , title := "a slot"
      ]);

  TestEq("a-slot", slot1->name);
  TestEq("a-slot-2", slot2->name);

  OBJECT widget2a := slot2->CreateWidget(
      [ type := OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/contentoption1")->id
      , condition := [ _type := "returningvisitor" ]
      , title := "Widget 2A"
      ]);
  widget2a->widgetfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/contentoption1",
      [ text := [ htmltext := StringToBlob(`<html><body><p class="normal">Widget 2.A</p></body></html>`) ] ]);

  OBJECT widget2b := slot2->CreateWidget(
      [ typens := "http://www.webhare.net/xmlns/webhare_testsuite/contentoption2"
      , condition := [ _type := "newvisitor" ]
      , title := "Widget 2B"
      ]);
  widget2b->widgetfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/contentoption2", [ text := "Widget 2.B"]);

  OBJECT widget2c := slot2->CreateWidget(
      [ typens := "http://www.webhare.net/xmlns/webhare_testsuite/contentoption2"
      , title := "Widget 2C"
      ]);
  widget2c->widgetfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/contentoption2", [ text := "Widget 2.C"]);

  OBJECT richdoctype := OpenWHFSType("http://www.webhare.net/xmlns/publisher/richdocumentfile");
  OBJECT beacondoc := GetTestsuiteTempFolder()->CreateFile(
      [ name := "beacondoc"
      , type := richdoctype->id
      , publish := TRUE
    ]);
  richdoctype->SetInstanceData(beacondoc->id,
      [ data :=
        [ htmltext := StringToBlob(`<html><body><div class="wh-rtd-embeddedobject" data-instanceid="beacon" /></body></html>`)
        , instances :=
          [ [ instanceid := "beacon"
            , data :=
              [ whfstype := "http://www.webhare.net/xmlns/publisher/widgets/triggerbeacon"
              , beacon := beacon1id
              ]
            ]
          ]
        ]
      ]);

  // Create a form that has a richtext on the first page and the thank you page containing a trigger beacon widget, which
  // should only trigger after the relevant page is being shown
  OBJECT beaconformtype := OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/beaconformpage");
  OBJECT beaconform := GetTestsuiteTempFolder()->CreateFile(
      [ name := "beaconform"
      , type := beaconformtype->id
      , publish := TRUE
    ]);
  beaconformtype->SetInstanceData(beaconform->id,
      [ first :=
        [ htmltext := StringToBlob(`<html><body><p class="normal">This is the first page</p><div class="wh-rtd-embeddedobject" data-instanceid="beacon" /></body></html>`)
        , instances :=
          [ [ instanceid := "beacon"
            , data :=
              [ whfstype := "http://www.webhare.net/xmlns/publisher/widgets/triggerbeacon"
              , beacon := beacon1id
              ]
            ]
          ]
        ]
      , thankyou :=
        [ htmltext := StringToBlob(`<html><body><p class="normal">This is the thank you page</p><div class="wh-rtd-embeddedobject" data-instanceid="beacon" /></body></html>`)
        , instances :=
          [ [ instanceid := "beacon"
            , data :=
              [ whfstype := "http://www.webhare.net/xmlns/publisher/widgets/triggerbeacon"
              , beacon := beacon3id
              ]
            ]
          ]
        ]
      ]);

  testfw->CommitWork();

  //ensure everything is on disk
  WHILE(Length(ListSlotsNeedingRepublish()) > 0)
    Sleep(100);

  RETURN CELL[ headerslot
             , slot1
             , slot2
             , beaconstore
             , slotstore
             , beacondoc
             , beaconform
             ];
}

PUBLIC RECORD FUNCTION Testinvoke_SetupDCTest(RECORD options DEFAULTSTO DEFAULT RECORD)
{
  RuntestFramework(MACRO PTR[]);
  RECORD prepare := SetupAdaptiveContentTest(options);
  RETURN CELL[ url := OpenTestsuiteSite()->OpenbyPath("actests/index")->link
             , beacondoc := prepare.beacondoc->link
             , beaconform := prepare.beaconform->link
             ];
}

