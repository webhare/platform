<?wh

/* wh runtest publisher.forms.testformfile-custom
   ?app=publisher(/WebHare%20testsuite%20site/webtools/customform)
*/

LOADLIB "wh::files.whlib";

LOADLIB "mod::tollium/lib/testframework.whlib";

LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";

LOADLIB "mod::webhare_testsuite/lib/internal/rtetesthelpers.whlib";


PUBLIC ASYNC MACRO TestBuildCustomForm()
{
  testfw->BeginWork();

  OBJECT webtoolsfolder := OpenTestsuiteSite()->OpenByPath("webtools");
  OBJECT customformfile := webtoolsfolder->OpenByName("customform2");
  IF(ObjectExists(customformfile))
    customformfile->RecycleSelf();

  testfw->GetUserObject("sysop")->UpdateGrant("grant", "system:fs_fullaccess", webtoolsfolder->id, testfw->GetUserObject("lisa"));
  testfw->GetUserObject("sysop")->UpdateGrant("grant", "system:fs_browse", webtoolsfolder->id, testfw->GetUserObject("bart"));
  testfw->CommitWork();

  //Build a form using the UI
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:app", [ calltype := "direct", params := STRING[ webtoolsfolder->whfspath ], target := DEFAULT RECORD ]));
  AWAIT ExpectScreenChange(+1, PTR TTClick("newfile"));
  TT("types")->selection := SELECT * FROM TT("types")->rows WHERE title = "WebHare testsuite customform2 type";
  TestEq(TRUE, RecordExists(TT("types")->selection));
  AWAIT ExpectScreenChange(0, PTR topscreen->TolliumExecuteSubmit());
  TT("name")->value := "customform2";
  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteSubmit());
  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteCancel()); //closes publisher

  //Open form editor
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:edit", [ calltype := "direct", params := STRING[ webtoolsfolder->whfspath || "customform2" ], target := DEFAULT RECORD ]));
  customformfile := webtoolsfolder->OpenByName("customform2");

  TestEq(FALSE, TTClick("save", [ allowfailure := TRUE ]), "Doc should not be dirty yet");

  AWAIT ExpectScreenChange(+1, PTR TTClick(":Form settings"));
  TT(":Some data")->value := "Here, have some data!";
  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteSubmit);

  //there's no "try" click so we'll just check the raw frame flag
  TestEq(TRUE, topscreen->frame->flags.isdirty, "Doc should now be dirty");
  TestEq("", customformfile->GetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/customform2").somedata, "somedata should NOT be commited yet");

  AWAIT ExpectAndAnswerMessageBox("YES", PTR TTClick("Publish"), [ awaitcall := TRUE ]); //confirm save and publish

  TestEq(FALSE, TTClick("save", [ allowfailure := TRUE ]), "Doc should again be undirty");

  testfw->WaitForPublishCompletion(webtoolsfolder->id);
  TestEq("Here, have some data!", customformfile->GetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/customform2").somedata);

  //update the custom form setting and save a draft
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Form settings"));
  TT(":Some data")->value := "Here, have some other data!";
  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteSubmit);
  TTClick("save");

  //preview the draft, which fails if the value of the custom form setting is not "Here, have some other data!"
  RECORD openres := ExecuteWindowOpenAction(TT("previewdraft"));
  IF(testfw->debug)
    Print("Preview autosave: " || openres.url || "\n");
  TestEq(TRUE, testfw->browser->GotoWebPage(openres.url));

  //Add a text field - test filing title= based on the RTD concent
  TT("doceditor->questions")->selection := RECORD[TT("doceditor->questions")->rows[0]]; //select first page
  AWAIT ExpectScreenChange(+1, PTR TTClick("{item}:Text"));
  TT("tabs")->ListExtensions()[0].extension->^text->value := GetTestRTD();
  TestEq(FALSE, TTIsVisible("autocomplete"), "Autosuggest should not appear here");
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  TestEqLike("This docs opens with*",TT("doceditor->questions")->selection.title);

  //save a draft version and check if the custom settings isn't reverted to the last published value
  TTClick("save");
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Form settings"));
  TestEq("Here, have some other data!", TT(":Some data")->value);
  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteCancel);

  //Make sure it persists during edits
  AWAIT ExpectScreenChange(+1, PTR TTClick("{item}:Edit"));
  RECORD value := TT("tabs")->ListExtensions()[0].extension->^text->value;
  TestEqLike("*This docs opens with*", BlobToString(value.htmltext));
  TestEq(FALSE, TTIsVisible("autocomplete"), "Autosuggest should still not appear here");
  value.htmltext := StringToBlob(`<html><body><p class="normal">Just another doc</p></body></html>`);
  TT("tabs")->ListExtensions()[0].extension->^text->value := value;
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  TestEq("Just another doc",TT("doceditor->questions")->selection.title);

  AWAIT ExpectScreenChange(+1, PTR TTClick("{item}:Edit"));
  value := TT("tabs")->ListExtensions()[0].extension->^text->value;
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  TestEq("Just another doc",TT("doceditor->questions")->selection.title);

  //Add a text field
  TT("doceditor->questions")->selection := RECORD[TT("doceditor->questions")->rows[0]]; //select first page
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Question", [menuitem := TRUE]));
  TT("builtincomponents")->selection := RECORD(SELECT * FROM TT("builtincomponents")->rows WHERE name = "Short text field");
  AWAIT ExpectScreenChange(0, PTR TTClick(":OK"));
  TestEq(TRUE, TTIsVisible("autocomplete"), "Autosuggest should still not appear here");
  TestEq("m/s-nummer", SELECT AS STRING title FROM TT("autocomplete")->options WHERE rowkey = "username");
  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  //Add a field
  TT("doceditor->questions")->selection := RECORD[TT("doceditor->questions")->rows[0]]; //select first page
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Question", [menuitem := TRUE]));
  TT("builtincomponents")->selection := RECORD(SELECT * FROM TT("builtincomponents")->rows WHERE name = "Email");
  AWAIT ExpectScreenChange(0, PTR TTClick(":OK"));
  TestEq(TRUE, TTIsVisible("autocomplete"), "Autosuggest should still not appear here");
  TestEq("", SELECT AS STRING title FROM TT("autocomplete")->options WHERE rowkey = "username", "Username shouldn't appear in email's autocomplete list (unless we know usernames to be email through siteprl?)");
  TestEq(["No autofill", "User's email address","Custom 'autocomplete'"], SELECT AS STRING ARRAY title FROM TT("autocomplete")->options WHERE NOT isdivider);
  TestEq("", TT("autocomplete")->value, "No autofill should be initially selected");
  TestEq(FALSE, TTIsVisible("autocomplete_customvalue"));
  TT("autocomplete")->value := " custom ";
  TestEq(TRUE, TTIsVisible("autocomplete_customvalue"));

  TT("autocomplete_customvalue")->value := "neue_auto_complete";
  TT("title")->value := "Ëmail plz";
  TT("name")->value := "email";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  TestEq("Ëmail plz",TT("doceditor->questions")->selection.title);

  //Verify autocomplete persisted
  AWAIT ExpectScreenChange(+1, PTR TTClick("{item}:Edit"));
  TestEq(" custom ", TT("autocomplete")->value);
  TestEq("neue_auto_complete", TT("autocomplete_customvalue")->value);
  TT("autocomplete")->value := "email";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  AWAIT ExpectScreenChange(+1, PTR TTClick("{item}:Edit"));
  TestEq("email", TT("autocomplete")->value);
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  //Add another field with the same name (tag)
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Question", [menuitem := TRUE]));
  TT("builtincomponents")->selection := RECORD(SELECT * FROM TT("builtincomponents")->rows WHERE name = "Email");
  AWAIT ExpectScreenChange(0, PTR TTClick(":OK"));
  TT("title")->value := "Ëmail pretty plz";
  TT("name")->value := "email";
  AWAIT ExpectAndAnswerMessageBox("OK", PTR TTClick(":OK"), [ awaitcall := TRUE ]); //'tag already in use' message box
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Cancel"));

  //Delete the existing email field
  TestEq("Ëmail plz",TT("doceditor->questions")->selection.title);
  AWAIT ExpectAndAnswerMessageBox("YES", PTR TTClick(":Delete"), [ awaitcall := TRUE ]); //confirm deletion

  //Add another field with the same name (tag) again, should succeed now
  TT("doceditor->questions")->selection := RECORD[TT("doceditor->questions")->rows[0]]; //select first page
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Question", [menuitem := TRUE]));
  TT("builtincomponents")->selection := RECORD(SELECT * FROM TT("builtincomponents")->rows WHERE name = "Email");
  AWAIT ExpectScreenChange(0, PTR TTClick(":OK"));
  TT("title")->value := "Ëmail pretty plz";
  TT("name")->value := "email";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  TestEq("Ëmail pretty plz",TT("doceditor->questions")->selection.title);

  //Check that the question can be edited (it shouldn't fail on its own tag being there)
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit", [menuitem := TRUE]));
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  //Undelete the deleted question
  INSERT TT("doceditor->questions")->rows[END-1].rowkey INTO TT("doceditor->questions")->expanded AT END; //expand the last node (i.e. open the trash)
  ExecuteListDragDrop(TT("doceditor->questions"), VARIANT[TT("doceditor->questions")->rows[END-1].rowkey], TT("doceditor->questions"), [ target := TT("doceditor->questions")->rows[0].rowkey, droplocation := "appendchild" ]);
  TT("doceditor->questions")->selection := RECORD(SELECT * FROM TT("doceditor->questions")->rows WHERE title = "Ëmail plz");
  TestEq("Ëmail plz",TT("doceditor->questions")->selection.title);

  //Check that restoring the deleted question cleared the tag to avoid duplicate tags, and set a new name
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit", [menuitem := TRUE]));
  TestEq("", TT("name")->value);
  TT("name")->value := "otheremail";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  //Delete and undelete the question again
  AWAIT ExpectAndAnswerMessageBox("YES", PTR TTClick(":Delete"), [ awaitcall := TRUE ]); //confirm deletion
  ExecuteListDragDrop(TT("doceditor->questions"), VARIANT[TT("doceditor->questions")->rows[END-1].rowkey], TT("doceditor->questions"), [ target := TT("doceditor->questions")->rows[0].rowkey, droplocation := "appendchild" ]);
  TT("doceditor->questions")->selection := RECORD(SELECT * FROM TT("doceditor->questions")->rows WHERE title = "Ëmail plz");

  // Check that the tag isn't cleared now
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit", [menuitem := TRUE]));
  TestEq("otheremail", TT("name")->value);
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  //Delete the question again
  AWAIT ExpectAndAnswerMessageBox("YES", PTR TTClick(":Delete"), [ awaitcall := TRUE ]); //confirm deletion

  // Regression: Empty richtext on non-thankyou page would crash validation
  TT("doceditor->questions")->selection := RECORD[TT("doceditor->questions")->rows[0]]; //select first page
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Text", [menuitem := TRUE]));
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  AWAIT ExpectAndAnswerMessageBox("YES", PTR TTClick(":Publish"), [ awaitcall := TRUE ]); //confirm save and publish

  //Add the custom form field
  TT("doceditor->questions")->selection := RECORD[TT("doceditor->questions")->rows[0]]; //select first page
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Question", [menuitem := TRUE]));
  TT("type")->selectedtab := TT("custom");
  // Test if allowing questions by matchattribute works
  TestEq(FALSE, RecordExists(SELECT * FROM TT("customcomponents")->rows WHERE name = "MatchAttributes Type1"));
  TestEq(TRUE, RecordExists(SELECT * FROM TT("customcomponents")->rows WHERE name = "MatchAttributes Type2 true"));
  TestEq(FALSE, RecordExists(SELECT * FROM TT("customcomponents")->rows WHERE name = "MatchAttributes Type2 false"));
  TT("customcomponents")->selection := RECORD(SELECT * FROM TT("customcomponents")->rows WHERE name = "Custom Form Field");
  AWAIT ExpectScreenChange(0, PTR TTClick(":OK"));
  TT("title")->value := "Reverse";
  TT("name")->value := "reverse";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  TestEq("Reverse",TT("doceditor->questions")->selection.title);

  //Add a mail handler
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Form handler", [menuitem := TRUE]));
  TT("builtincomponents")->value := "http://www.webhare.net/xmlns/publisher/forms#mailfeedbackhandler";
  AWAIT ExpectScreenChange(0, PTR TTClick(":OK"));
  TT(":Send to")->selection := SELECT * FROM TT(":Send to")->options WHERE title = "Ëmail pretty plz";
  AWAIT ExpectScreenChange(+1, PTR (TT(":Email contents")->ProcessInboundMessage("buttonclick", [ button := "object-insert" ])));
  TestEq(2, Length(TT("contenttypes")->rows)); //expecting the merge field and the custom form widget
  TT("contenttypes")->selection := SELECT * FROM TT("contenttypes")->rows WHERE rowkey = "http://www.webhare.net/xmlns/publisher/formmergefield";
  TestEq(TRUE, RecordExists(TT("contenttypes")->selection));
  AWAIT ExpectScreenChange(0, PTR TTClick(":OK"));

  //Expecting "Email" to be offered, along with "Reverse" and "Reverse – Reversed" (the additional custom form field merge field)
  TestEq(3, Length(TT(":Field")->options));
  TestEq("Page 1 \u2013 Ëmail pretty plz", TT(":Field")->options[0].title);
  TestEq("Page 1 \u2013 Reverse", TT(":Field")->options[1].title);
  TestEq("Page 1 \u2013 Reverse \u2013 Reversed", TT(":Field")->options[2].title);
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  //Insert the object
  RECORD insertedobjectinstr := SELECT * FROM GetTestController()->GrabInstructions() WHERE instr="update" AND type="messages" AND messages[0].type="InsertEmbeddedObject";
  TestEq(TRUE, RecordExists(insertedobjectinstr), "Unable to find the InsertEmbeddedObject instruction in the outgoing instruction stream #1");
  RECORD insertdata := insertedobjectinstr.messages[0].data;
  TestEq(TRUE, insertdata.widget.canedit);
  TestEqLike("*&#203;mail pretty plz*", insertdata.widget.htmltext);

  TT(":Email contents")->TolliumWeb_FormUpdate('<html><body><p class="normal">Your email is: <span class="wh-rtd-embeddedobject" data-instanceref="' || EncodeValue(insertdata.widget.instanceref) || '"></span></p></body></html>');
  TT(":Email contents")->__debug_simulatedirty();
  TT(":Sender name")->value := "testformfile-formeditapp";
  TT(":Sender address")->value := "testformfile-formeditapp@beta.webhare.net";

  BLOB img := GetHarescriptResource("mod::system/web/tests/snowbeagle.jpg");
  TT("fragment1!attachments!files!addbutton")->action->ExecuteUpload([WrapBlob(img, "bob.jpg")]);
  TestEq(1, Length(TT("fragment1!attachments!files!list")->rows));

  AWAIT ExpectAndAnswerMessageBox("no", PTR TTClick(":OK"), [ messagemask := "*is not*required*" ]); //verify saying no doesn't crash
  AWAIT ExpectAndAnswerMessageBox("yes", PTR TTClick(":OK"), [ messagemask := "*is not*required*" ]);
  AWAIT ExpectScreenChange(-1, DEFAULT MACRO PTR);

  //Test adding a page and moving it to top. This crashed earlier once the form had handlers
  TTClick("doceditor->addpage");
  TestEq("Page 2", TT("doceditor->questions")->selection.title);

  //Make sure move actions dirty the form
  TestEq(TRUE, TTClick("save"));
  TestEq(FALSE, topscreen->frame->flags.isdirty, "Doc should now be dirty");

  TTClick("doceditor->movetotop");
  TestEq("Page 1", TT("doceditor->questions")->selection.title);

  TestEq(TRUE, topscreen->frame->flags.isdirty, "Doc should now be dirty");

  //Modify one of the questions
  TT("doceditor->questions")->selection := SELECT * FROM TT("doceditor->questions")->rows WHERE title = "Ëmail pretty plz";
  topscreen->frame->focused := TT("doceditor->questions");
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit", [menuitem := TRUE]));
  TT("title")->value := "Renamed email field";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  //See if the merge field list updated
  TT("doceditor->questions")->selection := SELECT * FROM TT("doceditor->questions")->rows WHERE title = "Email response to visitor";
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit", [menuitem := TRUE]));
  AWAIT ExpectScreenChange(+1, PTR (TT(":Email contents")->ProcessInboundMessage("buttonclick", [ button := "object-insert" ])));
  TT("contenttypes")->selection := SELECT * FROM TT("contenttypes")->rows WHERE rowkey = "http://www.webhare.net/xmlns/publisher/formmergefield";
  AWAIT ExpectScreenChange(0, PTR TTClick(":OK"));

  TestEq(3, Length(TT(":Field")->options));
  TestEq("Page 2 \u2013 Renamed email field", TT(":Field")->options[0].title);
  AWAIT ExpectScreenChange(-1, PTR TTEscape);
  AWAIT ExpectAndAnswerMessageBox("yes", PTR TTClick(":OK"), [ messagemask := "*is not*required*" ]);
  AWAIT ExpectScreenChange(-1, DEFAULT MACRO PTR);

  AWAIT ExpectAndAnswerMessageBox("YES", PTR TTClick(":Publish"), [ awaitcall := TRUE ]); //confirm save and publish

  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteCancel); //close form editor
}


ASYNC MACRO TestViewAsBart()
{
  testfw->SetTestUser("bart");

  OBJECT webtoolsfolder := OpenTestsuiteSite()->OpenByPath("webtools");

  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:edit", [ calltype := "direct", params := STRING[ webtoolsfolder->whfspath || "customform2" ], target := DEFAULT RECORD ]));
  TT("doceditor->questions")->selection := SELECT * FROM TT("doceditor->questions")->rows WHERE title = "Email response to visitor";
  TestEq(FALSE, TTClick("{item}:Edit", [ allowfailure := TRUE ]));
  TestEq(FALSE, TT("doceditor->resultsbuttons", [ findinvisible := TRUE] )->visible);

  AWAIT ExpectScreenChange(-1, PTR TTEscape); //close app
}


ASYNC MACRO TestEditAsLisa()
{
  testfw->SetTestUser("lisa");

  OBJECT webtoolsfolder := OpenTestsuiteSite()->OpenByPath("webtools");

  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:edit", [ calltype := "direct", params := STRING[ webtoolsfolder->whfspath || "customform2" ], target := DEFAULT RECORD ]));
  TestEq(TRUE, TT("doceditor->resultsbuttons")->visible);
  TT("doceditor->questions")->selection := SELECT * FROM TT("doceditor->questions")->rows WHERE title = "Email response to visitor";
  AWAIT ExpectScreenChange(+1, PTR TTClick("{item}:Edit"));
  AWAIT ExpectAndAnswerMessageBox("yes", PTR TTClick(":OK"), [ messagemask := "*is not*required*" ]);
  AWAIT ExpectScreenChange(-1, DEFAULT MACRO PTR);

  AWAIT ExpectAndAnswerMessageBox("YES", PTR TTClick(":Publish"), [ awaitcall := TRUE ]); //confirm save and publish
  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  //rename the customform to disable any mailtemplate, this triggered an error earlier because the radio was removed and a required field then triggered
  testfw->BeginWork();
  IF(ObjectExists(webtoolsfolder->OpenByName("customform-nomailtemplates")))
    webtoolsfolder->OpenByName("customform-nomailtemplates")->RecycleSelf();

  webtoolsfolder->OpenByName("customform2")->UpdateMetadata([name := "customform-nomailtemplates" ]);
  testfw->CommitWork();

  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:edit", [ calltype := "direct", params := STRING[ webtoolsfolder->whfspath || "customform-nomailtemplates" ], target := DEFAULT RECORD ]));
  TT("doceditor->questions")->selection := SELECT * FROM TT("doceditor->questions")->rows WHERE title = "Email response to visitor";
  AWAIT ExpectScreenChange(+1, PTR TTClick("{item}:Edit"));
  AWAIT ExpectAndAnswerMessageBox("yes", PTR TTClick(":OK"), [ messagemask := "*is not*required*" ]);
  AWAIT ExpectScreenChange(-1, DEFAULT MACRO PTR);

  AWAIT ExpectAndAnswerMessageBox("YES", PTR TTClick(":Publish"), [ awaitcall := TRUE ]); //confirm save and publish

  AWAIT ExpectScreenChange(+1, PTR TTClick(":Results")); //can open reuslts
  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

RunTestframework([ PTR TestBuildCustomForm
                 , PTR TestViewAsBart
                 , PTR TestEditAsLisa
                 ]
                ,[ testusers := [[ login := "sysop", grantrights := ["system:sysop"] ]
                                ,[ login := "lisa" ]
                                ,[ login := "bart" ]
                                ]
                 ]);
