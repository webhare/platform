<?wh

/* wh runtest publisher.forms.testformfile-editconditions
   https://my.webhare.dev/?app=publisher:edit(/WebHare%20testsuite%20site/webtools/conditionalform)
*/

LOADLIB "mod::tollium/lib/testframework.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/whfs.whlib";

LOADLIB "mod::publisher/lib/forms/api.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";


OBJECT conditionalformfile, checkboxesfield, radiobuttonsfield, checkboxfield;

MACRO TestBuildConditionalForm()
{
  testfw->BeginWork();

  // (Re)create the form file
  OBJECT webtoolsfolder := OpenTestsuiteSite()->OpenByPath("webtools");
  conditionalformfile := webtoolsfolder->OpenByName("conditionalform");
  IF (ObjectExists(conditionalformfile))
    conditionalformfile->RecycleSelf();
  conditionalformfile := webtoolsfolder->CreateFile([ name := "conditionalform", type := OpenWHFSType("http://www.webhare.net/xmlns/publisher/formwebtool")->id, publish := TRUE ]);

  // Fill the form through the form definition api
  OBJECT formdefs := CreateNewFormdefinitionsFile();
  OBJECT formdef := formdefs->CreateFormDefinition("webtoolform");
  OBJECT formpage := formdef->GetPage(0);

  // Add a checkboxes field
  checkboxesfield := formpage->AppendComponent("select");
  checkboxesfield->SetAttribute("name", "checkboxes");
  checkboxesfield->SetAttribute("title", "Checkboxes");
  checkboxesfield->SetAttribute("type", "checkbox");
  FOREVERY (INTEGER suffix FROM [1, 2, 3])
  {
    OBJECT checkboxopt := checkboxesfield->ownerdocument->CreateElementNS(checkboxesfield->namespaceuri, "option");
    checkboxopt->SetAttribute("rowkey", "copt" || suffix);
    checkboxopt->SetAttribute("title", "Checkbox Option " || suffix);
    checkboxesfield->AppendChild(checkboxopt);
  }

  // Add a radiobuttons field
  radiobuttonsfield := formpage->AppendComponent("select");
  radiobuttonsfield->SetAttribute("name", "radiobuttons");
  radiobuttonsfield->SetAttribute("title", "Radiobuttons");
  radiobuttonsfield->SetAttribute("type", "radio");
  FOREVERY (INTEGER suffix FROM [1, 2, 3])
  {
    OBJECT radioopt := radiobuttonsfield->ownerdocument->CreateElementNS(radiobuttonsfield->namespaceuri, "option");
    radioopt->SetAttribute("rowkey", "ropt" || suffix);
    radioopt->SetAttribute("title", "Radiobutton Option " || suffix);
    radiobuttonsfield->AppendChild(radioopt);
  }

  // Add a single checkbox field
  checkboxfield := formpage->AppendComponent("checkbox");
  checkboxfield->SetAttribute("name", "checkbox");
  checkboxfield->SetAttribute("title", "Checkbox");

  // Add a text field, for which we'll edit the conditions
  OBJECT firstnamefield := formpage->AppendComponent("textedit");
  firstnamefield->SetAttribute("name", "firstname");
  firstnamefield->SetAttribute("tid", "tollium:tilde.firstname");
  firstnamefield->SetAttribute("autocomplete", "given-name");

  // Save the file
  formdefs->SaveToWHFS(conditionalformfile);
  testfw->CommitWork();
}

ASYNC MACRO TestEditConditions()
{
  // Open the editor
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:edit", [ calltype := "direct", params := STRING[ conditionalformfile->whfspath ], target := DEFAULT RECORD ]));

  // Select and edit the textedit question
  TT("doceditor->questions")->selection := TT("doceditor->questions")->rows[4];
  TestEq("First name", TT("doceditor->questions")->selection.title);
  AWAIT ExpectScreenChange(+1, PTR TTClick("{item}:Edit"));

  TestEq(DEFAULT RECORD, TT("requiredcondition")->value);

  // Edit the required condition
  AWAIT ExpectScreenChange(+1, PTR TTClick("requiredcondition!edit"));
  // One single condition should be present, without add or delete buttons
  TestEq(FALSE, ObjectExists(TT("fragment1!addconditionbutton", [ allowmissing := TRUE ])));
  TestEq(FALSE, ObjectExists(TT("fragment1!deleteconditionbutton", [ allowmissing := TRUE ])));
  TestEq(FALSE, ObjectExists(TT("fragment2!field", [ allowmissing := TRUE ])));
  // Cannot close: By default the first field is selected, which is a checkboxes field, and by default no options are selected
  TestEq(checkboxesfield->GetAttribute("guid"), TT("fragment1!field")->value);
  AWAIT ExpectNoScreenChange(PTR TTClick(":OK"));
  // The 'checkboxes' field is selected, so the 'match' fields should be visible instead of the 'in' fields (which are used for single-select fields)
  TestEq(TRUE, ObjectExists(TT("fragment1!value_match", [ allowmissing := TRUE ])));
  TestEq(TRUE, ObjectExists(TT("fragment1!select_match", [ allowmissing := TRUE ])));
  TestEq(FALSE, ObjectExists(TT("fragment1!value_in", [ allowmissing := TRUE ])));
  TestEq(FALSE, ObjectExists(TT("fragment1!select_in", [ allowmissing := TRUE ])));
  TT("fragment1!field")->value := radiobuttonsfield->GetAttribute("guid");
  TestEq(FALSE, ObjectExists(TT("fragment1!value_match", [ allowmissing := TRUE ])));
  TestEq(FALSE, ObjectExists(TT("fragment1!select_match", [ allowmissing := TRUE ])));
  TestEq(TRUE, ObjectExists(TT("fragment1!value_in", [ allowmissing := TRUE ])));
  TestEq(TRUE, ObjectExists(TT("fragment1!select_in", [ allowmissing := TRUE ])));
  AWAIT ExpectScreenChange(+1, PTR TTClick("fragment1!select_in"));
  TT("value")->value := [ EncodeHSON("ropt1") ]; // Field rowkeys are always HSON encoded
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  // Unfortunately we cannot compare the field value with the value of an FC* function directly, as these functions return
  // the 'comparename' of referenced fields (for frontend use) where the field value uses raw guid's.
  TestEq([ field := radiobuttonsfield->GetAttribute("guid"), matchtype := "IN", value := VARIANT[ "ropt1" ] ], TT("requiredcondition")->value);

  // Edit the required condition again, it should still be there
  AWAIT ExpectScreenChange(+1, PTR TTClick("requiredcondition!edit"));
  TestEq(radiobuttonsfield->GetAttribute("guid"), TT("fragment1!field")->value);
  TestEq("Radiobutton Option 1", TT("fragment1!value_in")->value);

  // Change to an OR condition
  TT("fragment1!field")->value := ":OR";
  // The first condition is still not addable or deleteable
  TestEq(FALSE, ObjectExists(TT("fragment1!addconditionbutton", [ allowmissing := TRUE ])));
  TestEq(FALSE, ObjectExists(TT("fragment1!deleteconditionbutton", [ allowmissing := TRUE ])));
  // There should be a second condition now
  TestEq(TRUE, ObjectExists(TT("fragment2!field")));
  TestEq(FALSE, ObjectExists(TT("fragment3!field", [ allowmissing := TRUE ])));
  // Set the new condition value
  TestEq(checkboxesfield->GetAttribute("guid"), TT("fragment2!field")->value);
  TT("fragment2!field")->value := radiobuttonsfield->GetAttribute("guid");
  AWAIT ExpectScreenChange(+1, PTR TTClick("fragment2!select_in"));
  TT("value")->value := [ EncodeHSON("ropt1") ];
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  // The new condition can be deleted, in which case the OR condition is deleted and a new condition is initialized
  TTClick("fragment2!deleteconditionbutton");
  TestEq(checkboxesfield->GetAttribute("guid"), TT("fragment3!field")->value);
  TestEq(TRUE, ObjectExists(TT("fragment3!value_match", [ allowmissing := TRUE ])));
  TestEq(TRUE, ObjectExists(TT("fragment3!select_match", [ allowmissing := TRUE ])));
  TestEq(FALSE, ObjectExists(TT("fragment3!value_in", [ allowmissing := TRUE ])));
  TestEq(FALSE, ObjectExists(TT("fragment3!select_in", [ allowmissing := TRUE ])));
  TestEq("", TT("fragment3!value_match")->value);

  // Change it again to an OR condition
  TT("fragment3!field")->value := ":OR";
  // The first condition is still not addable or deleteable
  TestEq(FALSE, ObjectExists(TT("fragment3!addconditionbutton", [ allowmissing := TRUE ])));
  TestEq(FALSE, ObjectExists(TT("fragment3!deleteconditionbutton", [ allowmissing := TRUE ])));
  // There should be a second condition now
  TestEq(TRUE, ObjectExists(TT("fragment4!field")));
  TestEq(FALSE, ObjectExists(TT("fragment5!field", [ allowmissing := TRUE ])));
  // Set the new condition value
  TestEq(checkboxesfield->GetAttribute("guid"), TT("fragment4!field")->value);
  TT("fragment4!field")->value := radiobuttonsfield->GetAttribute("guid");
  AWAIT ExpectScreenChange(+1, PTR TTClick("fragment4!select_in"));
  TT("value")->value := [ EncodeHSON("ropt1") ];
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  // Add a second subcondition
  TTClick("fragment4!addconditionbutton");
  TT("fragment5!field")->value := checkboxfield->GetAttribute("guid");
  TT("fragment5!value_hasvalue")->value := TRUE;
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  TestEq(
      [ matchtype := "OR"
      , conditions :=
          [ [ field := radiobuttonsfield->GetAttribute("guid"), matchtype := "IN", value := VARIANT[ "ropt1" ] ]
          , [ field := checkboxfield->GetAttribute("guid"), matchtype := "HASVALUE", value := TRUE ]
          ]
      ], TT("requiredcondition")->value);

  // Edit the required condition again
  AWAIT ExpectScreenChange(+1, PTR TTClick("requiredcondition!edit"));
  TestEq(":OR", TT("fragment1!field")->value);
  TestEq(radiobuttonsfield->GetAttribute("guid"), TT("fragment2!field")->value);
  TestEq("Radiobutton Option 1", TT("fragment2!value_in")->value);
  TestEq(TRUE, ObjectExists(TT("fragment2!addconditionbutton")));
  TestEq(TRUE, ObjectExists(TT("fragment2!deleteconditionbutton")));
  TestEq(TRUE, ObjectExists(TT("fragment3!field")));
  TestEq(TRUE, ObjectExists(TT("fragment3!deleteconditionbutton")));
  TestEq(checkboxfield->GetAttribute("guid"), TT("fragment3!field")->value);
  TestEq(TRUE, TT("fragment3!value_hasvalue")->value);
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  TestEq(
      [ matchtype := "OR"
      , conditions :=
          [ [ field := radiobuttonsfield->GetAttribute("guid"), matchtype := "IN", value := VARIANT[ "ropt1" ] ]
          , [ field := checkboxfield->GetAttribute("guid"), matchtype := "HASVALUE", value := TRUE ]
          ]
      ], TT("requiredcondition")->value);

  // Save the textedit question
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  // Save and publish
  AWAIT ExpectAndAnswerMessageBox("YES", PTR TTClick("Publish"), [ awaitcall := TRUE ]); //confirm save and publish

  // Select and delete the 'radiobuttons' question
  TT("doceditor->questions")->selection := TT("doceditor->questions")->rows[2];
  AWAIT ExpectAndAnswerMessageBox("YES", PTR TTClick("{item}:Delete"));

  // Try to save and publish
  AWAIT ExpectScreenChange(+1, PTR TTClick("Publish"));
  STRING message := MemberExists(topscreen,'message') ? topscreen->message : topscreen->^message->value;
  TestEQLike("*sure*save*publish?", message, "The message in the messagebox does not match");
  AWAIT ExpectScreenChange(0, PTR GetCell(topscreen->buttonmap,"YES")->TolliumClick(), [ allowscreenchangeafter := TRUE ]);
  // Expect a 'condition source deleted' message
  message := MemberExists(topscreen,'message') ? topscreen->message : topscreen->^message->value;
  TestEQLike("*required*dependency*Page 1*First name*refers to a deleted field", message, "The message in the messagebox does not match");
  AWAIT ExpectScreenChange(-1, PTR GetCell(topscreen->buttonmap,"OK")->TolliumClick(), [ allowscreenchangeafter := TRUE ]);

  // Edit the textedit question
  TT("doceditor->questions")->selection := TT("doceditor->questions")->rows[3];
  TestEq("First name", TT("doceditor->questions")->selection.title);
  AWAIT ExpectScreenChange(+1, PTR TTClick("{item}:Edit"));
  // The required condition field should show a 'condition source deleted' message
  TestEqLike("*dependency refers*deleted field", TT("requiredcondition!condition")->value);
  TestEq(DEFAULT RECORD, TT("requiredcondition")->value); // value is cleared when condition is invalid
  // Clear the required condition
  TTClick("requiredcondition!clear");
  TestEq("", TT("requiredcondition!condition")->value);
  TestEq(DEFAULT RECORD, TT("requiredcondition")->value);
  // Save the question
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  // Save and publish
  AWAIT ExpectAndAnswerMessageBox("YES", PTR TTClick("Publish"), [ awaitcall := TRUE ]); //confirm save and publish

  // Close the editor
  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteCancel);
}

RunTestframework([ PTR TestBuildConditionalForm
                 , PTR TestEditConditions
                 ]
                ,[ testusers := [[ login := "sysop", grantrights := ["system:sysop"] ]
                                ]
                 ]);
