<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::ooxml/spreadsheet.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/testframework.whlib";

LOADLIB "mod::tollium/lib/testframework.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";


DATETIME lastcheck;

ASYNC MACRO Setup()
{
  EnsureUserMgmtStateClean(FALSE);

  INTEGER lisaunit := GetTestController()->userapi->CreateUnit([wrd_title := "Lisa's unit"]);

  testfw->SetTestUser("lisa");
  AWAIT ExpectAndAnswerMessageBox("ok", PTR TTLaunchApp("system:userrights"), [ messagemask := "You don't have access to this application*lisa@beta.webhare.net*" ]);

  //first we grant 'lisa' a right, but nothing that gives her broad usermgmt rights. this no longe gives you access to userrights
  testfw->BeginWork();
  testfw->GetUserObject("sysop")->UpdateGrant("grant", "webhare_testsuite:betatesting", 0, testfw->GetUserObject("lisa"), [ withgrantoption := TRUE ]);
  testfw->CommitWork();

  AWAIT ExpectAndAnswerMessageBox("ok", PTR TTLaunchApp("system:userrights"), [ messagemask := "You don't have access to this application*lisa@beta.webhare.net*" ]);

  testfw->BeginWork();
  testfw->GetUserObject("sysop")->UpdateGrant("grant", "system:manageunit", GetTestController()->userapi->GetUnit(lisaunit)->authobjectid, testfw->GetUserObject("lisa"), [ withgrantoption := TRUE ]);
  testfw->CommitWork();
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("system:userrights", [ calltype := "direct", params := DEFAULT STRING ARRAY, target := DEFAULT RECORD, message := DEFAULT RECORD ]));

  //Create a unit
  TT("unittree->unitlist")->selection := SELECT * FROM TT("unittree->unitlist")->rows WHERE title="Lisa's unit";
  AWAIT ExpectScreenChange(+1, PTR TTClick("unittree->newunit"));
  TT("wrd_title")->value := "Lisa's new unit";
  TT("usereditpolicy")->value := "webhare_testsuite:usereditpolicy";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  //Select the new unit
  TestEq("Lisa's new unit", TT("unittree->unitlist")->selection.title);

  //Verify edit works
  AWAIT ExpectScreenChange(+1, PTR TT("unittree->unitlist")->openaction->TolliumClick());
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  //Add a user
  AWAIT ExpectScreenChange(+1, PTR TT("unitcontents->newuser")->TolliumClick);

  TT("username")->value := "bart@example.net";
  DATETIME lastaction := GetCurrentDateTime();
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  TestEQMembers(
        [ [ type :=     "webhare_testsuite:usereditedtask"
          , result :=   [ wrdschema :=    GetTestController()->userapi->wrdschema->id
                        , id :=           GetTestController()->userapi->GetTolliumUserByLogin("bart@example.net")->entityid
                        , action :=       "add"
                        ]
          ]
        ], testfw->FlushTasks([ "webhare_testsuite:usereditedtask" ]), "*");

  lastaction := GetCurrentDateTime();
  testfw->BeginWork();
  GetTestController()->userapi->TriggerUserEditAfterEffects(GetTestController()->userapi->GetTolliumUserByLogin("bart@example.net")->entityid, [ action := "add" ]);
  testfw->CommitWork();

  TestEQMembers(
        [ [ type :=     "webhare_testsuite:usereditedtask"
          , result :=   [ wrdschema :=    GetTestController()->userapi->wrdschema->id
                        , id :=           GetTestController()->userapi->GetTolliumUserByLogin("bart@example.net")->entityid
                        , action :=       "add"
                        ]
          ]
        ], testfw->FlushTasks([ "webhare_testsuite:usereditedtask" ], [ createdafter := lastaction ]), "*");


  topscreen->frame->focused := TT("unitcontents->userandrolelist");
  AWAIT ExpectScreenChange(+1, PTR TTClick("edit"));
  lastaction := GetCurrentDateTime();
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  TestEQMembers(
        [ [ type :=     "webhare_testsuite:usereditedtask"
          , result :=   [ wrdschema :=    GetTestController()->userapi->wrdschema->id
                        , id :=           GetTestController()->userapi->GetTolliumUserByLogin("bart@example.net")->entityid
                        , action :=       "edit"
                        ]
          ]
        ], testfw->FlushTasks([ "webhare_testsuite:usereditedtask" ], [ createdafter := lastaction ]), "*");

  lastaction := GetCurrentDateTime();
  testfw->BeginWork();
  GetTestController()->userapi->TriggerUserEditAfterEffects(GetTestController()->userapi->GetTolliumUserByLogin("bart@example.net")->entityid, [ action := "edit" ]);
  testfw->CommitWork();

  TestEQMembers(
        [ [ type :=     "webhare_testsuite:usereditedtask"
          , result :=   [ wrdschema :=    GetTestController()->userapi->wrdschema->id
                        , id :=           GetTestController()->userapi->GetTolliumUserByLogin("bart@example.net")->entityid
                        , action :=       "edit"
                        ]
          ]
        ], testfw->FlushTasks([ "webhare_testsuite:usereditedtask" ], [ createdafter := lastaction ]), "*");

  //Select the new user
  TestEq(TRUE, RecordExists(SELECT FROM TT("unitcontents->userandrolelist")->rows WHERE email="bart@example.net"));
  TT("unitcontents->userandrolelist")->selection := SELECT * FROM TT("unitcontents->userandrolelist")->rows WHERE email="bart@example.net";
  AWAIT ExpectScreenChange(+1, PTR topscreen->unitcontents->move->TolliumClick);

  TT("unittree->unitlist")->selection := SELECT * FROM TT("unittree->unitlist")->rows WHERE title="Lisa's unit";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  OBJECT bartuser := GetTestController()->userapi->GetTolliumUserByLogin("bart@example.net");
  TestEq(TRUE, ObjectExists(bartuser));

  OBJECT lisaunitobject := GetTestController()->userapi->GetUnit(lisaunit);
  //test that authobjects was updated properly
  TestEq(lisaunitobject->authobjectid, SELECT AS INTEGER parent FROM system.authobjects WHERE id=bartuser->authobjectid);

  //Add a user with the same name. Should give an error, not crash
  AWAIT ExpectScreenChange(+1, PTR TT("unitcontents->newuser")->TolliumClick);
  TT("username")->value := "bart@example.net";
  AWAIT ExpectAndAnswerMessageBox("OK", PTR TTClick(":OK"), [ messagemask := "*not unique*" ]);
  TT("username")->value := "bart2@example.net";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));

  //Get its ID for later use
  INTEGER bart2id := testfw->GetWRDschema()->^wrd_person->Search("WRD_CONTACT_EMAIL", "bart2@example.net");
  TestEq(TRUE, bart2id != 0);

  //Delete the user
  topscreen->frame->focused := TT("unitcontents->userandrolelist");
  AWAIT ExpectAndAnswerMessageBox("Yes", PTR TTClick("delete"));
  TestEq(TRUE, testfw->GetWRDschema()->^wrd_person->GetEntityField(bart2id,"WRD_LIMITDATE") < GetCurrentDatetime());

  //Delete his unit
  topscreen->frame->focused := TT("unittree->unitlist");
  AWAIT ExpectAndAnswerMessageBox("Yes", PTR TTClick("delete"));

  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

ASYNC MACRO TestRightRoleGrants()
{
  testfw->SetTestUser("sysop");
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("system:userrights", [ calltype := "direct", params := DEFAULT STRING ARRAY, target := DEFAULT RECORD, message := DEFAULT RECORD ]));

  TTFillByTitle("unittree->unitlist", "Lisa's unit");
  TTFillByTitle("unitcontents->userandrolelist", "bart@example.net");

  // Grant right (global right)
  AWAIT ExpectScreenChange(+1, PTR TTClick("unitcontents->addrighttousers", [ menuitem := TRUE ]));
  {
    TTFillByTitle("objecttree!objectlist->thelist", "Miscellaneous");
    TTFillByTitle("rightslist", "Supervisor");
    TT("comment")->value := "Testsuite test";
    AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  }

  // Adjust admin option on grant
  TTFillByTitle("userrolerightslist!directpermissions", "Supervisor");
  TestEQMembers([ [ withgrantoption := "No" ] ], TT("userrolerightslist!directpermissions")->selection, "*");

  TT("frame")->focused := TT("userrolerightslist!directpermissions");
  AWAIT ExpectScreenChange(+1, PTR TTClick("userrolerightslist->edit"));
  {
    TestEQ("Testsuite test", TT("comment")->value);
    TestEQ(FALSE, TT("selectright!grantwithgrantoption")->value);
    TT("selectright!grantwithgrantoption")->value := TRUE;
    AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  }

  // Delete grant
  TestEQMembers([ [ withgrantoption := "Yes" ] ], TT("userrolerightslist!directpermissions")->selection, "*");
  AWAIT ExpectAndAnswerMessageBox("yes", PTR TTClick("userrolerightslist->revokefromusers"));
  TestEQ(RECORD[], TT("userrolerightslist!directpermissions")->rows);

  // Grant right (object right)
  AWAIT ExpectScreenChange(+1, PTR TTClick("unitcontents->addrighttousers", [ menuitem := TRUE ]));
  {
    TTFillByTitle("objecttree!objectlist->thelist", "webhare_testsuite.testsite", [ expandtreelevels := 3 ]);
    TTFillByTitle("rightslist", "Browse");
    TT("comment")->value := "Testsuite test 2";
    AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  }

  // Adjust admin option on grant
  TTFillByTitle("userrolerightslist!directpermissions", "Browse");
  TestEQMembers([ [ withgrantoption := "No" ] ], TT("userrolerightslist!directpermissions")->selection, "*");

  TT("frame")->focused := TT("userrolerightslist!directpermissions");
  AWAIT ExpectScreenChange(+1, PTR TTClick("userrolerightslist!edit"));
  {
    TestEQ("Testsuite test 2", TT("comment")->value);
    TestEQ(FALSE, TT("selectright!grantwithgrantoption")->value);
    TT("selectright!grantwithgrantoption")->value := TRUE;
    AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  }

  // Delete grant
  TestEQMembers([ [ withgrantoption := "Yes" ] ], TT("userrolerightslist!directpermissions")->selection, "*");
  AWAIT ExpectAndAnswerMessageBox("yes", PTR TTClick("userrolerightslist!revokefromusers"));
  TestEQ(RECORD[], TT("userrolerightslist!directpermissions")->rows);

  // Create new role
  AWAIT ExpectScreenChange(+1, PTR TTClick("unitcontents->newrole", [ menuitem := TRUE ]));
  {
    TT(":Name")->value := "New role";
    AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  }

  // Add user to role
  AWAIT ExpectScreenChange(+1, PTR TTClick("userrolerightslist!adduserstorole", [ menuitem := TRUE ]));
  {
    TTFillByTitle("selectuserrole!unitcontents!userandrolelist", "bart@example.net");
    TT(":Comments")->value := "Testsuite test 3";
    AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  }

  // Get properties via users tab
  TTFillByTitle("userrolerightslist!rightstabs", "Users");
  TT("frame")->focused := TT("userrolerightslist!userlistbyrole");
  TTFillByTitle("userrolerightslist!userlistbyrole", "bart@example.net");
  AWAIT ExpectScreenChange(+1, PTR TTClick("userrolerightslist!edit"));
  {
    TestEQ("Testsuite test 3", TT("comment")->value);
    AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  }

  // Delete user from role via users tab
  AWAIT ExpectAndAnswerMessageBox("yes", PTR TTClick("userrolerightslist!removeusersfromrole"));
  TestEQ(RECORD[], TT("userrolerightslist!userlistbyrole")->rows);

  // Add role to user via user
  TTFillByTitle("unitcontents->userandrolelist", "bart@example.net");
  AWAIT ExpectScreenChange(+1, PTR TTClick("unitcontents->addroletousers", [ menuitem := TRUE ]));
  {
    TTFillByTitle("selectuserrole!unitcontents!userandrolelist", "New role");
    TT(":Comments")->value := "Testsuite test 4";
    AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  }

  // Get role grant properties via rights tab
  TTFillByTitle("userrolerightslist!directpermissions", "New role");
  TT("frame")->focused := TT("userrolerightslist!directpermissions");
  AWAIT ExpectScreenChange(+1, PTR TTClick("userrolerightslist!edit"));
  {
    TestEQ("Testsuite test 4", TT("comment")->value);
    AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  }

  // Delete role grant via rights tab
  AWAIT ExpectAndAnswerMessageBox("yes", PTR TTClick("userrolerightslist!revokefromusers"));
  TestEQ(RECORD[], TT("userrolerightslist!directpermissions")->rows);

  // Add grant via objects list
  TTFillByTitle("viewmode", "Module and object rights");
  TTFillByTitle("objecttree!objectlist->thelist", "webhare_testsuite.testsite", [ expandtreelevels := 3 ]);
  TT("frame")->focused := TT("objecttree!objectlist->thelist");
  AWAIT ExpectScreenChange(+1, PTR TTClick("grant"));
  {
    TTFillByTitle("selectuserrole!unittree->unitlist", "Lisa's unit");
    TTFillByTitle("selectuserrole!unitcontents->userandrolelist", "bart@example.net");
    TTFillByTitle("{select}:Right", "Browse (Viewing rights on this filesystem object and all underlying objects)");
    TT("comment")->value := "Testsuite test 5";
    AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  }

  // Adjust admin option on grant
  TTFillByTitle("objectrightslist!authobjectlistbyobject", "bart@example.net");
  TestEQMembers([ [ grantable := "No" ] ], TT("objectrightslist!authobjectlistbyobject")->selection, "*");
  TT("frame")->focused := TT("objectrightslist!authobjectlistbyobject");

  AWAIT ExpectScreenChange(+1, PTR TTClick(":Properties"));
  {
    TestEQ("Testsuite test 5", TT("comment")->value);
    TestEQ(FALSE, TT("selectright!grantwithgrantoption")->value);
    TT("selectright!grantwithgrantoption")->value := TRUE;
    AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  }

  // Delete grant
  TestEQMembers([ [ grantable := "Yes" ] ], TT("objectrightslist!authobjectlistbyobject")->selection, "*");
  AWAIT ExpectAndAnswerMessageBox("yes", PTR TTClick(":Delete"));
  TestEQ(RECORD[], SELECT * FROM TT("objectrightslist!authobjectlistbyobject")->rows WHERE name = "bart@example.net");

  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

ASYNC MACRO TestExport()
{
  testfw->BeginWork();
  testfw->SetupTestWebsite(DEFAULT BLOB);

  OBJECT subfolder1 := testfw->GetTestSite()->rootobject->CreateFolder([ name := "visiblefolder" ]);
  OBJECT subfolder2 := testfw->GetTestSite()->rootobject->CreateFolder([ name := "invisiblefolder" ]);

  OBJECT visibleunit := testfw->userapi->GetUnit(testfw->userapi->CreateUnit([ wrd_title := "visibleunit" ]));
  OBJECT deeperunit := testfw->userapi->GetUnit(testfw->userapi->CreateUnit([ wrd_title := "deeperunit", wrd_leftentity := visibleunit->entityid ]));
  OBJECT invisibleunit := testfw->userapi->GetUnit(testfw->userapi->CreateUnit([ wrd_title := "invisibleunit" ]));

  OBJECT testuser := testfw->userapi->GetUser(testfw->userapi->CreateUser([ whuser_unit := visibleunit->entityid, wrd_contact_email := "testuser@beta.webhare.net", wrd_lastname := "testuser" ]));
  OBJECT visibleuser1 := testfw->userapi->GetUser(testfw->userapi->CreateUser([ whuser_unit := visibleunit->entityid, wrd_contact_email := "visibleuser1@beta.webhare.net", wrd_lastname := "visibleuser1" ]));
  OBJECT visibleuser2 := testfw->userapi->GetUser(testfw->userapi->CreateUser([ whuser_unit := deeperunit->entityid, wrd_contact_email := "visibleuser2@beta.webhare.net", wrd_lastname := "visibleuser2" ]));
  OBJECT visibleuser3 := testfw->userapi->GetUser(testfw->userapi->CreateUser([ whuser_unit := visibleunit->entityid, wrd_contact_email := "visibleuser3@beta.webhare.net", wrd_lastname := "visibleuser3" ]));
  OBJECT invisibleuser1 := testfw->userapi->GetUser(testfw->userapi->CreateUser([ whuser_unit := invisibleunit->entityid, wrd_contact_email := "invisibleuser1@beta.webhare.net", wrd_lastname := "invisibleuser1" ]));
  OBJECT invisibleuser2 := testfw->userapi->GetUser(testfw->userapi->CreateUser([ whuser_unit := invisibleunit->entityid, wrd_contact_email := "invisibleuser2@beta.webhare.net", wrd_lastname := "invisibleuser2" ]));
  OBJECT visiblerole := testfw->userapi->GetRole(testfw->userapi->CreateRole([ wrd_leftentity := visibleunit->entityid, wrd_title := "visiblerole" ]));
  OBJECT invisiblerole := testfw->userapi->GetRole(testfw->userapi->CreateRole([ wrd_leftentity := invisibleunit->entityid, wrd_title := "invisiblerole" ]));

  testfw->GetUserObject("sysop")->UpdateGrant("grant", "system:manageunit", visibleunit->authobjectid, testuser, [ withgrantoption := TRUE ]);
  testfw->GetUserObject("sysop")->UpdateGrant("grant", "system:fs_browse", subfolder1->id, testuser);

  testfw->GetUserObject("sysop")->UpdateGrant("grant", "system:fs_browse", subfolder1->id, visibleuser1); // visible right, visible user, visible object
  testfw->GetUserObject("sysop")->UpdateGrant("grant", "system:fs_browse", subfolder2->id, visibleuser1); // visible right, visible user, invisible object
  testfw->GetUserObject("sysop")->UpdateGrant("grant", "system:fs_fullaccess", subfolder1->id, visibleuser1); // invisible right, visible user, visible object
  testfw->GetUserObject("sysop")->UpdateGrant("grant", "system:fs_browse", subfolder1->id, invisibleuser1); // visible right, invisible user, visible object
  testfw->GetUserObject("sysop")->UpdateGrant("grant", "system:fs_browse", subfolder1->id, visiblerole); // visible right, visible user, visible object, via visible role
  testfw->GetUserObject("sysop")->UpdateRoleGrant("grant", visiblerole->authobjectid, visibleuser2);
  testfw->GetUserObject("sysop")->UpdateGrant("grant", "system:fs_browse", subfolder1->id, invisiblerole); // visible right, visible user, visible object, via invisible role
  testfw->GetUserObject("sysop")->UpdateRoleGrant("grant", invisiblerole->authobjectid, visibleuser3);

  testfw->CommitWork();

  __SetEffectiveUser(testuser);
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("system:userrights"));

  // Go to the 'Module and object rights' tab
  TTFillByTitle("viewmode", "Module and object rights");

  // Find the 'visiblefolder' object
  TTFillByTitle("objecttree->objectlist->thelist", "visiblefolder", [ expandtreelevels := 3 ]);

  // Export the object rights on this folder
  AWAIT ExpectScreenChange(+1, PTR TTClick("objecttree->exportobjectrights"));
  RECORD rec := AWAIT ExpectSentWebFile(PTR TTClick("download"));
  AWAIT ExpectScreenChange(-1, DEFAULT FUNCTION PTR);

  RECORD ARRAY rows := GetOOXMLSpreadsheetRows(rec.file.data);
  TestEqMembers(
      [ [ "object path"   :=  "/visiblefolder"
        , unit :=         "visibleunit"
        , username :=     "testuser@beta.webhare.net"
        , "full name" :=  "testuser"
        , right :=        "system:fs_browse"
        , "right title" :=        "Browse"
        , "via role" :=   ""
        , email :=        "testuser@beta.webhare.net"
        ]
      , [ "object path"   :=  "/visiblefolder"
        , username :=     "visibleuser1@beta.webhare.net"
        , "full name" :=  "visibleuser1"
        , right :=        "system:fs_browse"
        , "right title" :=        "Browse"
        , "via role" :=   ""
        , email :=        "visibleuser1@beta.webhare.net"
        ]
      , [ "object path"   :=  "/visiblefolder"
        , username :=     "visibleuser2@beta.webhare.net"
        , unit :=         "deeperunit"
        , "unit path" :=  "visibleunit/deeperunit"
        , "full name" :=  "visibleuser2"
        , right :=        "system:fs_browse"
        , "right title" :=        "Browse"
        , "via role" :=   "visibleunit/visiblerole"
        , email :=        "visibleuser2@beta.webhare.net"
        ]
      ], (SELECT * FROM rows ORDER BY username), "*");

  //Delete 'visiblerole'. This should eliminate it from the exportobjectrights
  testfw->BeginWork();
  testfw->userapi->wrdschema->^whuser_role->CloseEntity(visiblerole->entityid);
  testfw->CommitWork();

  AWAIT ExpectScreenChange(+1, PTR TTClick("objecttree->exportobjectrights"));
  rec := AWAIT ExpectSentWebFile(PTR TTClick("download"));
  AWAIT ExpectScreenChange(-1, DEFAULT FUNCTION PTR);

  rows := GetOOXMLSpreadsheetRows(rec.file.data);
  TestEqMembers(
      [ [ "object path"   :=       "/visiblefolder"
        , username :=     "testuser@beta.webhare.net"
        , "full name" :=  "testuser"
        , right :=        "system:fs_browse"
        , "right title" :="Browse"
        , "via role" :=   ""
        , email :=        "testuser@beta.webhare.net"
        ]
      , [ "object path"   :=       "/visiblefolder"
        , username :=     "visibleuser1@beta.webhare.net"
        , "full name" :=  "visibleuser1"
        , right :=        "system:fs_browse"
        , "right title" :="Browse"
        , "via role" :=   ""
        , email :=        "visibleuser1@beta.webhare.net"
        ]
      ], (SELECT * FROM rows ORDER BY username), "*");

  TTFillByTitle("unittree->unitlist", "visibleunit");
  AWAIT ExpectScreenChange(+1, PTR TTClick("unittree->exportusersinunit"));
  rec := AWAIT ExpectSentWebFile(PTR TTClick("download"));
  AWAIT ExpectScreenChange(-1, DEFAULT FUNCTION PTR);

  rows := GetOOXMLSpreadsheetRows(rec.file.data);
  TestEqMembers([ [ username := 'testuser@beta.webhare.net' , unit := "visibleunit" ]
                , [ username := 'visibleuser1@beta.webhare.net' , unit := "visibleunit" ]
                , [ username := 'visibleuser2@beta.webhare.net' , unit := "deeperunit" ]
                , [ username := 'visibleuser3@beta.webhare.net' , unit := "visibleunit" ]
                ], rows, "*");

  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  //Undelete 'visiblerole' for the next test to work
  testfw->BeginWork();
  testfw->userapi->wrdschema->^whuser_role->UpdateEntity(visiblerole->entityid, [ wrd_limitdate := MAX_DATETIME ]);
  testfw->CommitWork();
}

ASYNC MACRO TestExportSysop()
{
  testfw->SetTestUser("sysop");

  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("system:userrights"));
  AWAIT ExpectScreenChange(+1, PTR TTClick("exportallgrants"));
  RECORD rec := AWAIT ExpectSentWebFile(PTR TTClick("download"));
  AWAIT ExpectScreenChange(-1, DEFAULT FUNCTION PTR);

  RECORD ARRAY rows := GetOOXMLSpreadsheetRows(rec.file.data);
  //Test that ROLEGRANTs exist, they were a recent addition
  TestEq(TRUE, RecordExists(SELECT FROM rows WHERE type="ROLEGRANT" AND username="visibleuser2@beta.webhare.net" AND rows."object path" = "visiblerole"));
  //Test that we see the ROLE's grant. but no status as it's a role
  TestEqMembers([[ "account status" := "" ]],
                SELECT * FROM rows WHERE type="GRANT" AND username="visiblerole" AND rows."object path" = "webhare-tests; webhare_testsuite.testfolder; webhare_testsuite.site; visiblefolder", '*');

  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}


RunTestframework( [ PTR Setup
                  , PTR TestRightRoleGrants
                  , PTR TestExport
                  , PTR TestExportSysop
                  , PTR EnsureUserMgmtStateClean(TRUE)
                  ], [ testusers := [[ login := "sysop", grantrights := ["system:sysop"] ]
                                    ,[ login := "supervisor", grantrights := ["system:supervisor"] ]
                                    ,[ login := "unitmanager" ]
                                    ,[ login := "lisa"]
                                    ]
                     , testroles := [[ role := "powerplantmanagers" ]]
                     ]
                  );
