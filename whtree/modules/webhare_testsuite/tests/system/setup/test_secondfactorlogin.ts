import * as test from '@mod-tollium/js/testframework';
import { invokeSetupForTestSetup, type TestSetupData } from '@mod-webhare_testsuite/js/wts-testhelpers';
import { isTruthy } from '@webhare/std';

const webroot = test.getTestSiteRoot();
let setupdata: TestSetupData | null = null;
let pietje_resetlink = '';
let totpsecret = '';
let totpdata;
let totpbackupcodes = '';

test.runTests(
  [
    async function () {
      setupdata = await invokeSetupForTestSetup({ createsysop: true });
    },

    "create Pietje",
    async function () {
      await test.load(webroot + 'portal1/' + setupdata?.overridetoken + "&notifications=0&language=en");
      await test.wait('ui');

      // start usermgmt
      test.click(test.qSA('li li').filter(node => node.textContent?.includes("User Management"))[0]);
      await test.wait('ui');

      test.click(test.qSA('div.listrow').filter(node => node.textContent?.includes("webhare_testsuite.unit"))[0]);
      await test.wait('ui');

      // Create user pietje@allow2fa.test.webhare.net
      test.clickToddToolbarButton("Add", "New user");
      await test.wait('ui');

      test.setTodd('username', "pietje@allow2fa.test.webhare.net");
      test.clickToddButton('OK');
      await test.wait('ui');

      await test.selectListRow('unitcontents!userandrolelist', 'pietje');
      test.click(test.getMenu(['Create password reset link']));
      await test.wait('ui');
      test.clickToddButton('OK');
      await test.wait('ui');
      pietje_resetlink = test.getCurrentScreen().getValue("resetlink!previewurl");
      test.clickToddButton('Close');
      await test.wait('ui');
    },

    "set Pietje password",
    async function () {
      await test.load(pietje_resetlink);
      await test.wait('ui');

      test.eq("pietje@allow2fa.test.webhare.net", test.getCurrentScreen().getValue("username"));

      test.setTodd('password', "SECRET");
      test.setTodd('passwordrepeat', "SECRET");
      test.clickToddButton('OK');
      await test.wait('ui');

      // policy: password must have at least one lowercase character
      test.eq(/doesn't have/, test.getCurrentScreen().getNode()?.textContent);
      test.clickToddButton('OK');
      await test.wait('ui');

      test.setTodd('password', "xecret");
      test.setTodd('passwordrepeat', "xecret");
      test.clickToddButton('OK');
      await test.wait('ui');

      test.eq(/has been updated/, test.getCurrentScreen().getNode()?.textContent);
      test.clickToddButton('OK');

      // reloads to login window
      await test.wait('load');
      await test.wait('ui');
    },

    "login Pietje",
    async function () {
      test.setTodd('loginname', 'pietje@allow2fa.test.webhare.net');
      test.setTodd('password', 'xecret');
      test.clickToddButton('Login');
      await test.wait('ui');
    },

    "enable TOTP",
    async function enable2FA() {
      test.click("#dashboard-user-name");
      await test.wait('ui');
      test.click(test.qSA("button").filter(e => e.textContent === "Change")[1]);
      await test.wait('ui');

      // setup one-time access code
      test.clickToddButton('Setup');
      await test.wait('ui');

      // enter current password
      test.setTodd('password', "xecret");
      test.clickToddButton('OK');
      await test.wait('ui');

      test.click(test.qSA("t-text").filter(e => e.textContent === "Show the secret key")[0]);
      await test.wait('ui');

      totpsecret = test.getCurrentScreen().getValue("totpsecret");
      totpdata = await test.invoke('mod::webhare_testsuite/lib/tollium/login.whlib#GetTOTPCode', { secret: totpsecret, offset: -61 });

      test.setTodd('entercode', totpdata.code);
      test.click(test.qSA("button").filter(e => e.textContent?.startsWith("Next"))[0]);
      await test.wait('ui');

      test.eq(/your clock is -[69]0 seconds off/, test.getCurrentScreen().getNode()?.textContent);
      test.clickToddButton('OK');
      await test.wait('ui');
      totpdata = await test.invoke('mod::webhare_testsuite/lib/tollium/login.whlib#GetTOTPCode', { secret: totpsecret, offset: 0 });
      test.setTodd('entercode', totpdata.code);
      test.click(test.qSA("button").filter(e => e.textContent?.startsWith("Next"))[0]);
      await test.wait('ui');

      totpbackupcodes = test.getCurrentScreen().getValue("backupcodes_text").split("\n").filter(isTruthy);

      test.clickToddButton('Finish');
      await test.wait('ui');

      test.eq("Configured", test.getCurrentScreen().getValue("totp"));
      test.eq("Used 0 of 10 backup codes", test.getCurrentScreen().getValue("totpbackupcodes"));

      test.clickToddButton('Close');
      await test.wait('ui');

      test.clickToddButton('OK');
      await test.wait('ui');
      await test.sleep(100); // wait for dashboard to appear

      test.click("#dashboard-logout");
      await test.wait('ui');
      test.clickToddButton('Yes');
      await test.wait('load');
      await test.wait('ui');
    },

    "login Pietje with 2FA code",
    async function () {
      test.setTodd('loginname', 'pietje@allow2fa.test.webhare.net');
      test.setTodd('password', 'xecret');
      test.clickToddButton('Login');
      await test.wait('ui');

      // STORY: test an invalid code
      // gather a lot of valid codes
      const validcodes = [];
      for (let i = -90; i <= 120; i += 30) {
        totpdata = await test.invoke('mod::webhare_testsuite/lib/tollium/login.whlib#GetTOTPCode', { secret: totpsecret, offset: 0 });
        validcodes.push(totpdata.code);
      }

      // Get an invalid code
      let wrongcode = "000000";
      while (validcodes.includes(wrongcode))
        wrongcode = `00000${parseInt(wrongcode, 10) + 1}`.substr(-6);
      test.setTodd('totpcode', wrongcode);
      await test.wait('ui');
      test.click(test.compByName("secondfactorloginbutton"));
      await test.wait('ui');
      test.eq(/This code is not valid/, test.getCurrentScreen().getNode()?.textContent);
      test.clickToddButton('OK');

      // STORY: test an valid code (after using an invalid code)
      totpdata = await test.invoke('mod::webhare_testsuite/lib/tollium/login.whlib#GetTOTPCode', { secret: totpsecret, offset: 0 });
      test.setTodd('totpcode', totpdata.code);
      await test.wait('ui');

      test.click(test.compByName("secondfactorloginbutton"));
      await test.wait('ui');

      // should be logged in
      test.assert(Boolean(test.qS("#dashboard-logout")));

      // logout
      test.click("#dashboard-logout");
      await test.wait('ui');
      test.clickToddButton('Yes');
      await test.wait('load');
      await test.wait('ui');
    },

    "login Pietje with backup code",
    async function () {
      test.setTodd('loginname', 'pietje@allow2fa.test.webhare.net');
      test.setTodd('password', 'xecret');
      test.clickToddButton('Login');
      await test.wait('ui');

      test.setTodd('totpcode', totpbackupcodes[0]);
      await test.wait('ui');

      test.click(test.compByName("secondfactorloginbutton"));
      await test.wait('ui');

      // should be logged in
      test.assert(Boolean(test.qS("#dashboard-logout")));

      // logout
      test.click("#dashboard-logout");
      await test.wait('ui');
      test.clickToddButton('Yes');
      await test.wait('load');
      await test.wait('ui');
    },

    "login but first lock pietje",
    async function () {
      await test.invoke('mod::webhare_testsuite/lib/tollium/login.whlib#LockUser', 'pietje@allow2fa.test.webhare.net');
      test.setTodd('loginname', 'pietje@allow2fa.test.webhare.net');
      test.setTodd('password', 'xecret');
      test.clickToddButton('Login');
      await test.wait('ui');

      test.setTodd('totpcode', totpbackupcodes[1]);
      await test.wait('ui');

      test.click(test.compByName("secondfactorloginbutton"));
      await test.wait('ui');

      await test.waitForElement(["t-text", /Login failed: this user account is disabled/]);
      test.clickToddButton('OK');
      await test.wait('load'); //should reload

      // should be logged in
      test.assert(!test.qS("#dashboard-logout"));
    }

  ]);
