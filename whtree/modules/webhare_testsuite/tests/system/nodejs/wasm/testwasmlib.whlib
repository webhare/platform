<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::os.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::internal/wasm.whlib";
LOADLIB "wh::internal/interface.whlib";
LOADLIB "wh::promise.whlib";


PUBLIC MACRO ThrowIt()
{
  THROW NEW Exception("We're throwing it");
}

PUBLIC ASYNC MACRO ThrowItAsync()
{
  AWAIT CreateSleepPromise(1);
  THROW NEW Exception("We're async throwing it");
}

PUBLIC INTEGER FUNCTION ReentrantSyscall()
{
  RETURN WaitForPromise(EM_Syscall("executeInline", [ func := "return await 1", param := DEFAULT RECORD ]));
}

PUBLIC MACRO RunWASMEventTestHandler()
{
  INTEGER eventstream := __HS_EVENT_CREATESTREAM();
  __HS_EVENT_STREAMMODIFYSUBSCRIPTIONS(eventstream, [ "ipc-test.*" ], STRING[], FALSE);

  STRING id := GetConsoleArguments()[0];

  OBJECT link := ConnectToIPCPort("local:registration");
  link->DoRequest(CELL[ type := "register", id ]);
  link->Close();

  BroadcastEvent(`ipc-test.${id}`, DEFAULT RECORD);

  INTEGER gotevents;
  DATETIME wait_until := MAX_DATETIME;
  WHILE (TRUE)
  {
    RECORD wres := WaitUntil([ read := eventstream ], wait_until);
    IF (wres.type = "timeout")
      BREAK;
    RECORD rec := __HS_EVENT_STREAMREAD(eventstream);
    IF (RecordExists(rec))
    {
      gotevents := gotevents + 1;
      IF (gotevents = 4)
        wait_until := AddTimeToDate(100, GetCurrentDateTime());
      PRINT(`${id}: ${rec.name}\n`);
    }
  }

  PRINT(`${id}: final events: ${gotevents}\n`);
}

PUBLIC OBJECTTYPE TestObj
<
  STRING data;

  PUBLIC PROPERTY prop(GetProp, SetProp);

  STRING FUNCTION GetProp()
  {
    RETURN this->data;
  }

  MACRO SetProp(STRING x)
  {
    SWITCH(x)
    {
      CASE "ok", "ok2"
      {
        this->data := x;
        RETURN;
      }
      CASE "throw"
      {
        THROW NEW Exception("Throwing");
      }
      DEFAULT
      {
        ABORT(`Unexpected value '${x}'`);
      }
    }
  }
>;

PUBLIC OBJECTTYPE TestCallObj <
  PUBLIC FUNCTION PTR member_func;
  PUBLIC MACRO PTR member_macr;
  PUBLIC INTEGER member_int;

  PUBLIC PROPERTY property_int(member_int, member_int);
  PUBLIC PROPERTY property_macr(member_macr, member_macr);
  PUBLIC PROPERTY property_func(member_func, member_func);
  PUBLIC PROPERTY ^(gethat, -);

  MACRO NEW() {
    this->member_func := PTR this->method_func;
    this->member_macr := PTR this->method_macr;
  }

  PUBLIC INTEGER FUNCTION method_func(INTEGER x) {
    IF (x = 2)
      THROW NEW Exception("testthrow");
    IF (x = 3)
      ABORT("testabort");
    RETURN x;
  }
  PUBLIC MACRO method_macr(INTEGER x) {
    IF (x = 2)
      THROW NEW Exception("throw");
    IF (x = 3)
      ABORT("testabort");
  }

  VARIANT FUNCTION GetHat(STRING name) {
    IF (name = "^HAT_FUNC")
      RETURN this->member_func;
    IF (name = "^HAT_MACR")
      RETURN this->member_macr;
    IF (name = "^HAT_INT")
      RETURN this->member_int;
    ABORT(`Unknown property '${name}'`);
  }
>;

INTEGER counter;

PUBLIC INTEGER FUNCTION GetResetCounter() {
  INTEGER retval := counter;
  counter := 0;
  RETURN retval;
}

PUBLIC MACRO IncreaseCounter() {
  counter := counter + 1;
}
