<?wh

LOADLIB "mod::system/lib/keystore.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::socialite/lib/database.whlib";

MACRO TestAPIKeyLookup() //TODO move to more generic test, this doesn't need to depend on tag=external
{
  IF(RecordExists(SELECT FROM socialite.google_apikeys WHERE domain="*" AND keytype=0))
    ABORT("Cannot run TestAPIKEY if you already have a wildcard api key! Please make your '*' rule more specific");

  testfw->BeginWork();
  DELETE FROM socialite.google_apikeys WHERE domain LIKE "*.testframework.beta.webhare.net";
  INSERT INTO socialite.google_apikeys(domain,scopetype,apikey)
         VALUES("backend.testframework.beta.webhare.net", 2, "Key1");
  INSERT INTO socialite.google_apikeys(domain,scopetype,apikey)
         VALUES("any.testframework.beta.webhare.net", 0, "Key2");
  INSERT INTO socialite.google_apikeys(domain,scopetype,apikey)
         VALUES("client.testframework.beta.webhare.net", 1, "Key3");

  INSERT INTO socialite.google_apikeys(domain,scopetype,apikey)
         VALUES("*.wild.testframework.beta.webhare.net", 0, "Key4-0");
  INSERT INTO socialite.google_apikeys(domain,scopetype,apikey)
         VALUES("*.wild.testframework.beta.webhare.net", 1, "Key4-client");
  INSERT INTO socialite.google_apikeys(domain,scopetype,apikey)
         VALUES("*.wild.testframework.beta.webhare.net", 2, "Key4-server");

  INSERT INTO socialite.google_apikeys(domain,scopetype,apikey)
         VALUES("*.bla.wild.testframework.beta.webhare.net", 0, "Key5");
  INSERT INTO socialite.google_apikeys(domain,scopetype,apikey)
         VALUES("*.bla.wild.testframework.beta.webhare.net", 1, "Key5-client");
  INSERT INTO socialite.google_apikeys(domain,scopetype,apikey)
         VALUES("*.bla.wild.testframework.beta.webhare.net", 2, "Key5-server");

  INSERT INTO socialite.google_apikeys(domain,scopetype,apikey)
         VALUES("wild.testframework.beta.webhare.net", 2, "Key6-server");

  INSERT INTO socialite.google_apikeys(domain,scopetype,apikey,keytype)
         VALUES("*.testframework.beta.webhare.net", 0, "recaptcha",1);

  testfw->CommitWork();

  TestThrowsLike("Google cloud platform keys require *", PTR LookupAPIKey("google","any.testframework.beta.webhare.net"));
  TestThrowsLike("Google cloud platform keys require *", PTR LookupAPIKey("google","any.testframework.beta.webhare.net", [scope:="any"]));
  TestEq("Key2", LookupAPIKey("google","any.testframework.beta.webhare.net", [scope:="server"]).apikey);
  TestEq("Key2", LookupAPIKey("google","http://any.testframework.beta.webhare.net/", [scope:="server"]).apikey);
  TestThrowsLike("Unable to find*", PTR LookupAPIKey("google","x.testframework.beta.webhare.net", [scope:="server"]));
  TestEq(DEFAULT RECORD, LookupAPIKey("google","x.testframework.beta.webhare.net", [scope:="server", allowmissing   := TRUE ]));
  TestEq("Key4-server", LookupAPIKey("google","x.wild.testframework.beta.webhare.net", [scope:="server"]).apikey);
  TestEq("Key5-server", LookupAPIKey("google","x.bla.wild.testframework.beta.webhare.net", [scope:="server"]).apikey);
  TestEq("Key4-server", LookupAPIKey("google","https://x.wild.testframework.beta.webhare.net/", [scope:="server"]).apikey);
  TestEq("Key4-client", LookupAPIKey("google","https://x.wild.testframework.beta.webhare.net/", [scope:="client"]).apikey);
  TestEq("Key2", LookupAPIKey("google","x.any.testframework.beta.webhare.net", [scope:="server"]).apikey); //automatic subdomain matching (if all else failed)
  TestEq("Key4-server", LookupAPIKey("google","x.wild.testframework.beta.webhare.net", [scope:="server"]).apikey); //wildcard defeats subdomain matching

  TestEq("recaptcha", LookupAPIKey("google-recaptcha","any.testframework.beta.webhare.net").apikey); //recaptcha keys require no scope
}

RunTestframework([ PTR TestAPIKeyLookup
                 ]);
