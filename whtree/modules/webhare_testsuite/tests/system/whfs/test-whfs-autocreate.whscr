<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "mod::system/lib/testframework.whlib";

LOADLIB "mod::publisher/lib/siteapi.whlib";
LOADLIB "mod::publisher/lib/testframework.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/compiler.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/whfs.whlib";
LOADLIB "mod::system/lib/internal/whfs/contenttypes.whlib";

OBJECT testsite;

MACRO Prepare()
{
  testfw->BeginWork();

  OBJECT firstfolder := testfw->GetWHFSTestRoot();

  testsite := CreateSiteFromFolder(firstfolder->id);

  // Hard cleanup for all referenced types
  FOREVERY (STRING type FROM
      [ "http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest"
      , "http://www.webhare.net/xmlns/webhare_testsuite/autocreatefiletest"
      , "http://www.webhare.net/xmlns/webhare_testsuite/autocreatefoldertest"
      , "http://www.webhare.net/xmlns/webhare_testsuite/autocreatedynexectest"
      , "http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance1"
      , "http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance2"
      , "http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance3"
      ])
  {
    OBJECT whfs_type := OpenWHFSType(type, [ openorphans := TRUE]);
    IF (ObjectExists(whfs_type))
    {
      DELETE FROM system.fs_objects WHERE COLUMN type = whfs_type->id;
      DELETE FROM system.fs_types WHERE COLUMN id = whfs_type->id;
    }
  }

  //cleanup for a reliable repeatable test (without executerecomp whfs_types fails if executed twice in a row, FIXME?€)
  FlushUnreferredContentData();

  //GetWHFSCommitHandler()->TriggerSiteProfileRecompileOnCommit();

  testfw->CommitWork();
}

MACRO VerifyAutoCreateTestFile(OBJECT testfile, BOOLEAN after)
{
  OBJECT autocreatetesttype := OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest", [ openorphans := TRUE ]);
  RECORD result := autocreatetesttype->GetInstanceData(testfile->id, [ orphans := after ]);

  TestEq(FALSE, autocreatetesttype->isdynamicexecution);
  TestEq("String", result.str);
  TestEq(15, result.int);
  TestEq(StringToBlob("Dit is een test"), result.blub.data);
  TestEq(testfile->id, result.blub.source_fsobject);
  TestEq("", result.emptystr);
  TestEq(1.5m, result.price);
  TestEQ(TRUE, result.yesno);
  TestEQ(1.25f, result.afloat);
  TestEQ(TRUE, RecordExists(result.aninstance));
  TestEQ("http://example.com", result.url);
  TestEQ([ arbitrarydata := "val" ], result.arecord);
  IF(NOT after)
  {
    TestEQ("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance1", result.aninstance.whfstype);
    TestEQ("str1", result.aninstance.str1);
  }
  TestEQ(TRUE, RecordExists(result.rich));
  TestEQ(StringToBlob("htmltext"), result.rich.htmltext);
  TestEQ(1, LENGTH(result.rich.embedded));
  TestEqMembers([ contentid := "cid:1"
                , mimetype := "application/octet-stream"
                , data := StringToBlob("text")
                , width := 0
                , height := 0
                , hash := "mC2ePrmW9VnmM_TRlN7zdh2Qn1o7ZH0ahR_q1nwyydE"
                , filename := "cid:1"
                , extension := ""
                , rotation := 0
                , mirrored := FALSE
                , refpoint := DEFAULT RECORD
                , source_fsobject := testfile->id
                , dominantcolor := ""
                ], result.rich.embedded[0], "*,-__blobsource,-settingid,-link");

  TestEq(2, Length(result.rich.instances));
  IF(NOT after)
  {
    RECORD inp := [ instanceid := "TAG1"
                  , data :=
                         [ whfstype := "http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance1"
                         , str1 := "str3"
                         , whfssettingid := 0i64
                         , whfsfileid := 0
                         ]
                  ];
    TestEqStructure(inp, result.rich.instances[0]);
    TestEqMembers(inp, result.rich.instances[0], "INSTANCEID,DATA,WHFSTYPE,STR1");
  }

  RECORD inp := [ instanceid := "TAG3"
                 , data :=
                        [ whfstype := "http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance3"
                        , str3 := "str3"
                        , whfssettingid := 0i64
                        , whfsfileid := 0
                        ]
                 ];

  TestEqStructure(inp, result.rich.instances[1]);
  TestEqMembers(inp, result.rich.instances[1], "INSTANCEID,DATA,WHFSTYPE,STR3");
}

MACRO AutoCreateTypes()
{
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefiletest", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefoldertest", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatedynexectest", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance1", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance2", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance3", [ openorphans := TRUE ])));

  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest")));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefiletest")));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefoldertest")));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatedynexectest")));

  SetupTestModuleToLoadBaseProfile(Resolve("data/autocreatetest.siteprl.xml"));

  TestEq(FALSE, OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest")->isdynamicexecution);
  TestEq(TRUE,  OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatedynexectest")->isdynamicexecution);

  testfw->BeginWork();
  OBJECT testdatafolder := testsite->rootobject->CreateFolder(
            [ name:="testdata"
            , type := OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefoldertest")->id
            ]);

  OBJECT testfile := testdatafolder->CreateFile(
            [ name:="testfile"
            , type := OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefiletest")->id
            ]);

  OBJECT dynexecfile := testdatafolder->CreateFile(
            [ name:="dynexecfile"
            , type := OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatedynexectest")->id
            ]);

  // Test empty values
  testfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest",
      [ str := ""
      , int := 0
      , blub := DEFAULT RECORD
      , price := 0m
      , yesno := FALSE
      , rich := DEFAULT RECORD
      , afloat := 0f
      , aninstance := DEFAULT RECORD
      , url := ""
      , arecord := DEFAULT RECORD
      ]);

  RECORD result := testfile->GetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest");

  TestEq("", result.str);
  TestEq(0, result.int);
  TestEq(DEFAULT RECORD, result.blub);
  TestEq("", result.emptystr);
  TestEq(0m, result.price);
  TestEQ(FALSE, result.yesno);
  TestEq(DEFAULT RECORD, result.rich);
  TestEQ(0f, result.afloat);
  TestEq(DEFAULT RECORD, result.aninstance);
  TestEq("", result.url);
  TestEq(DEFAULT RECORD, result.arecord);

  // Test casting
  testfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest",
      [ str := ""
      , afloat := 0
      , price := 0
      ]);
  testfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest",
      [ str := ""
      , afloat := 0m
      ]);

  RECORD file_blub := WrapBlob(StringToBlob("Dit is een test"),"blub");
  file_blub.source_fsobject := testfile->id;
  RECORD file_embedded := WrapBlob(StringToBlob("text"),"text.txt");
  file_embedded.source_fsobject := testfile->id;

  // Normal data
  testfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest",
      [ str := "String"
      , int := 15
      , blub := file_blub
      , price := 1.5
      , yesno := TRUE
      , rich :=
          [ htmltext :=   StringToBlob("htmltext")
          , embedded :=   [MakeMergedRecord(file_embedded, [ contentid := "cid:1" ])]
          , instances := [ [ instanceid := "TAG1"
                           , data :=
                                [ whfstype := "http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance1"
                                , str1 := "str3"
                                ]
                           ]
                         , [ instanceid := "TAG3"
                           , data :=
                                [ whfstype := "http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance3"
                                , str3 := "str3"
                                ]
                           ]
                         ]
          ]
      , afloat := 1.25
      , aninstance :=
          [ whfstype := "http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance1"
          , str1 := "str1"
          ]
      , url := "http://example.com"
      , arecord := [ arbitrarydata := "val" ]
      ]);

  OBJECT deletedtestfile := testdatafolder->CreateFile([ name := "deletedtestfile" ]);
  deletedtestfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance2", [ str2 := "str2" ]);
  deletedtestfile->RecycleSelf();

  INTEGER64 ARRAY instanceids := SELECT AS INTEGER64 ARRAY id FROM system.fs_instances WHERE fs_object = testfile->id;

  VerifyAutoCreateTestFile(testfile, FALSE);

  // Sub-instances should not leak - we shouldn't be seeing the embedded instances
  TestEQ("", testfile->GetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance1").str1);
  TestEQ("", testfile->GetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance3").str3);

  OBJECT fstype_instance1 := OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance1");
  TestEQ(TRUE, testfile->id IN fstype_instance1->FindObjectsByMemberValue("STR1", "str1", TRUE));
  OBJECT fstype_instance3 := OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance3");
  TestEQ(TRUE, testfile->id IN fstype_instance3->FindObjectsByMemberValue("STR3", "str3", TRUE));

  // Test throw when setting invalid URL in URL field
  TestThrowsLike(`Incorrect value for field 'url'*`, PTR testfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest",
      [ url := "invalid_url"
      ]));

  // Test throw when forgetting whfstype field
  TestThrowsLike(`Missing field 'WHFSTYPE'*`, PTR testfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest",
      [ aninstance :=
          [ str2 := "str2"
          ]
      ]));

  // Update instance
  testfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest",
      [ aninstance :=
          [ whfstype := "http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance2"
          , str2 := "str2"
          ]
      ]);

  result := testfile->GetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest");
  TestEQ("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance2", result.aninstance.whfstype);
  TestEQ("str2", result.aninstance.str2);

  RECORD saverich := result.rich;

  OBJECT fstype_instance2 := OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance2");
  //verify legacy TRUE/FALSE flag, and that inactive objects aren't normally found
  TestEQ(FALSE, testfile->id IN fstype_instance1->FindObjectsByMemberValue("STR1", "str1", TRUE));
  TestEQ(TRUE, testfile->id IN fstype_instance2->FindObjectsByMemberValue("STR2", "str2", TRUE));
  TestEQ(FALSE, testfile->id IN fstype_instance2->FindObjectsByMemberValue("STR2", "STR2", TRUE));
  TestEQ(TRUE, testfile->id IN fstype_instance2->FindObjectsByMemberValue("STR2", "str2", FALSE));
  TestEQ(FALSE, deletedtestfile->id IN fstype_instance2->FindObjectsByMemberValue("STR2", "str2", TRUE));

  //test options-style call
  TestEQ(FALSE, testfile->id IN fstype_instance2->FindObjectsByMemberValue("STR2", "STR2"));
  TestEQ(TRUE, testfile->id IN fstype_instance2->FindObjectsByMemberValue("STR2", "STR2", [matchcase := FALSE]));
  TestEQ(FALSE, deletedtestfile->id IN fstype_instance2->FindObjectsByMemberValue("STR2", "str2", [matchcase := FALSE]));

  //test finding inactive files
  TestEQ(TRUE, testfile->id IN fstype_instance2->FindObjectsByMemberValue("STR2", "str2", [ findinprivate := TRUE ]));
  TestEQ(TRUE, deletedtestfile->id IN fstype_instance2->FindObjectsByMemberValue("STR2", "str2", [ findinprivate := TRUE ]));
  TestEQ(FALSE, deletedtestfile->id IN fstype_instance2->FindObjectsByMemberValue("STR2", "str2", [ findinprivate := FALSE ]));

  // Update instance
  testfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest",
      [ aninstance := DEFAULT RECORD ]);

  TestEQ(DEFAULT RECORD, testfile->GetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest").aninstance);

  TestEQ(FALSE, testfile->id IN fstype_instance1->FindObjectsByMemberValue("STR1", "str1", TRUE));
  TestEQ(FALSE, testfile->id IN fstype_instance2->FindObjectsByMemberValue("STR2", "str2", TRUE));

  DATETIME now := GetCurrentDateTime();
  Sleep(1);

  file_embedded := WrapBlob(StringToBlob("text"),"text.txt");
  file_embedded.source_fsobject := testfile->id;

  // overflow 4096 bytes storage of record
  testfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest",
      [ arecord := [ longdata := RepeatText("word", 16384)] ]);
  TestEQ([ longdata := RepeatText("word", 16384)], testfile->GetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest").arecord);

  // Restore to filling for archive test
  testfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest",
      [ aninstance :=
          [ whfstype := "http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance2"
          , str2 := "str2"
          ]
      , rich :=
          [ htmltext :=   StringToBlob("htmltext")
          , embedded :=   [ MakeMergedRecord(file_embedded, [ contentid := "cid:1" ]) ]
          , instances := [ [ instanceid := "TAG1"
                           , data :=
                                [ whfstype := "http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance2"
                                , str2 := "str2-2"
                                ]
                           ]
                         ]
                         CONCAT SELECT * FROM result.rich.instances WHERE instanceid="TAG3"
          ]
      , arecord := [ arbitrarydata := "val" ]
      ]);

  //TestEQ(TRUE, now < (SELECT AS DATETIME modificationdate FROM system.fs_objects WHERE id = testfile->id));

  //Create an archive out of this
  BLOB arc := testdatafolder->ExportFolder().files[0].data;

  testdatafolder->DeleteSelf();
  deletedtestfile->DeleteSelf();
  testfw->CommitWork();

  testfw->DeleteTestModule();
  RecompileSiteProfiles();

  testfw->BeginWOrk();
  FlushUnreferredContentData(); //needed to reliably test
  testfw->CommitWork();

  testfw->BeginWork();

  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefiletest", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefoldertest", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatedynexectest", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance1", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance2", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance3", [ openorphans := TRUE ])));

  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest")));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefiletest")));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefoldertest")));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatedynexectest")));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance1")));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance2")));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance3")));

  //Extract the archive again
  RECORD imp := ImportWHFSArchive(testsite->rootobject, arc, [ overwrite := TRUE ]);
  testfw->CommitWork();

  testdatafolder := testsite->rootobject->OpenByName("testdata");
  TestEq(TRUE, ObjectExists(testdatafolder));

  TestEq(TRUE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest", [ openorphans := TRUE ])));
  TestEq(TRUE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefiletest", [ openorphans := TRUE ])));
  TestEq(TRUE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefoldertest", [ openorphans := TRUE ])));
  TestEq(TRUE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatedynexectest", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance1", [ openorphans := TRUE ])));
  TestEq(TRUE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance2", [ openorphans := TRUE ])));
  TestEq(TRUE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance3", [ openorphans := TRUE ])));

  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest")));
  TestEq(TRUE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest", [ openorphans := TRUE ]))); //test the new API to OpenWHFSType
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefiletest")));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefoldertest")));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance1")));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance2")));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance3")));

  TestEq(FALSE, OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest", [ openorphans := TRUE ])->isdynamicexecution);
  TestEq(TRUE,  OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatedynexectest", [ openorphans := TRUE ])->isdynamicexecution);

  testfile := testdatafolder->OpenByName("testfile");
  TestEq(TRUE, ObjectExists(testfile));
  VerifyAutoCreateTestFile(testfile, TRUE);

  testfw->BeginWork();
  FlushUnreferredContentData(); //needed to reliably test
  testfw->CommitWork();

  testfw->BeginWork();
  result := OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest", [ openorphans := TRUE ])->__ExportInstanceData(testfile->id, DEFAULT OBJECT, TRUE);

  TestEq("String", result.str);
  TestEq(15, result.int);
  TestEq(StringToBlob("Dit is een test"), result.blub.data);
  TestEq(FALSE, CellExists(result,"emptystr")); //it DOESN'T know about emptystr...
  TestEQ("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance2", result.aninstance.whfstype);
  TestEQ("str2", result.aninstance.str2);
  TestEq(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefoldertest", [ openorphans := TRUE ])->id, testdatafolder->type);
  TestEq(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefiletest", [ openorphans := TRUE ])->id, testfile->type);

  testfw->CommitWork();

  //Associate the site profile again
  SetupTestModuleToLoadBaseProfile(Resolve("data/autocreatetest.siteprl.xml"));

  testfw->BeginWork();
  TestEq(TRUE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest")));
  result := testfile->GetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest");
  TestEq(TRUE, CellExists(result,"emptystr")); //it DOESN'T know about emptystr...

  //Create an outside dependence to check whether unused instances are not discarded
  testsite->rootobject->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest",
      [ aninstance :=
          [ whfstype := "http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance3"
          //no further content!
          ]
      ]);

  testfw->CommitWork();

  testfw->DeleteTestModule();
  RecompileSiteProfiles();

  testfw->WaitForPublishCompletion(testdatafolder->id);
  testfw->BeginWork();

  //Remove the stuff again and force a recompile directly (no way to trigger this indirectly)
  //It should make all the types go away again, except those referred by an instance
  testdatafolder->DeleteSelf();

  testfw->CommitWork();

  testfw->BeginWork();
  FlushUnreferredContentData(); //needed to reliably test
  testfw->CommitWork();

  TestEq(TRUE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefiletest", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefoldertest", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance1", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance2", [ openorphans := TRUE ])));
  TestEq(TRUE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance3", [ openorphans := TRUE ])));

  //Now remove the outside dependency
  testfw->BeginWork();
  OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest", [ openorphans := TRUE ])->SetInstanceData(testsite->rootobject->id , [ aninstance := DEFAULT RECORD ], [ orphans := TRUE ]);
  FlushUnreferredContentData(); //needed to reliably test
  testfw->CommitWork();

  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance3", [ openorphans := TRUE ])));

  //add the second site profile to try to force an internal conflict. this is what siteprofiledelaymode should fix
  SetupTestModuleToLoadBaseProfile(Resolve("data/autocreatetest_without_str.siteprl.xml"));

  testfw->BeginWork();
  OBJECT autocreatetesttype := OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest");

  TestEq(TRUE, ObjectExists(autocreatetesttype));
  TestEq(2, Length(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest")->members));
  TestEq([emptystr := "", int := 0], autocreatetesttype->defaultinstance);
  TestEq([emptystr := "", int := 0], autocreatetesttype->structure);

  imp := ImportWHFSArchive(testsite->rootobject, arc, [ overwrite := TRUE ]);
  testdatafolder := testsite->rootobject->OpenByName("testdata");
  TestEq(2, Length(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest")->members));
  TestEq(19, Length(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest")->allmembers));
  testfw->RollbackWork();

  testfw->DeleteTestModule();
  RecompileSiteProfiles();

  testfw->BeginWork();
  FlushUnreferredContentData(); //needed to reliably test
  testfw->CommitWork();

  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatetest", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefiletest", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreatefoldertest", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance1", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance2", [ openorphans := TRUE ])));
  TestEq(FALSE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/autocreate/instance3", [ openorphans := TRUE ])));
}


RunTestframework([ PTR Prepare
                 , PTR AutoCreateTypes
                 ]);
