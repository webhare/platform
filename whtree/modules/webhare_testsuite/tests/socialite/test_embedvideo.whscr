<?wh

/* To test locally try eg
   WEBHARE_SERVICES_HOSTS=webhare.moe.sf.webhare.dev wh run mod::webhare_testsuite/tests/socialite/test_embedvideo.whscr
   */

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::os.whlib";

LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::publisher/lib/embedvideo.whlib";

MACRO Prep()
{
  STRING override := GetEnvironmentVariable("WEBHARE_SERVICES_HOSTS");
  IF(override != "")
    testfw->MockRegistryKey("system.services.webhare.serviceshosts", override); //for local tests on MOE
}

MACRO TestNewVideoAPI()
{
  /*
  NOTES
  - YouTube supports ?t= both with and without s attached at the end
  - YouTube supports
      - ?t= for the normal URL, share(short) URL            (but not in the iframe embed src)
      - ?start= for the iframe embed src & share(short) URL (but not the YouTube video page URL)
  */
  RECORD ARRAY youtubetestdata :=
      [ [ title  := "YouTube"
        , test   := "https://www.youtube.com/watch?v=TRVaU9X0rOs"
        , out    := [ network := "youtube", videoid := "TRVaU9X0rOs", starttime := 0 ]
        ]

      , [ title  := "YouTube (typo with uppercase)"
        , test   := "https://www.yoUTUbe.com/watch?v=TRVaU9X0rOs"
        , out    := [ network := "youtube", videoid := "TRVaU9X0rOs", starttime := 0 ]
        ]

      , [ title  := "YouTube nocookie"
        , test   := "https://www.youtube-nocookie.com/watch?v=TRVaU9X0rOs"
        , out    := [ network := "youtube", videoid := "TRVaU9X0rOs", starttime := 0 ]
        ]

      , [ title  := "Share URL"
        , test   := "http://youtu.be/TRVaU9X0rOs"
        , out    := [ network := "youtube", videoid := "TRVaU9X0rOs", starttime := 0 ]
        ]

        // setting starttime using the 't' argument
      , [ title  := "Share URL with start time"
        , test   := "http://youtu.be/TRVaU9X0rOs?t=4s" // official way the YouTube share works
        , out    := [ network := "youtube", videoid := "TRVaU9X0rOs", starttime := 4 ]
        ]
      , [ title  := "Share URL with start time"
        , test   := "http://youtu.be/TRVaU9X0rOs?t=4" // leaving out the unit works (YouTube assumes seconds)
        , out    := [ network := "youtube", videoid := "TRVaU9X0rOs", starttime := 4 ]
        ]
      , [ title  := "Share URL with start time"
        , test   := "http://youtu.be/TRVaU9X0rOs?t=2m" // although not documented in the embed parameters, 'm' (minutes) is a working unit
        , out    := [ network := "youtube", videoid := "TRVaU9X0rOs", starttime := 120 ]
        ]

        // setting starttime using the 'start' argument
      , [ title  := "Share URL with start time"
        , test   := "http://youtu.be/TRVaU9X0rOs?start=4s" // official way the YouTube share works
        , out    := [ network := "youtube", videoid := "TRVaU9X0rOs", starttime := 4 ]
        ]
      , [ title  := "Share URL with start time"
        , test   := "http://youtu.be/TRVaU9X0rOs?start=4" // leaving out the unit works (YouTube assumes seconds)
        , out    := [ network := "youtube", videoid := "TRVaU9X0rOs", starttime := 4 ]
        ]
      , [ title  := "Share URL with start time"
        , test   := "http://youtu.be/TRVaU9X0rOs?start=2m" // although not documented in the embed parameters, 'm' (minutes) is a working unit
        , out    := [ network := "youtube", videoid := "TRVaU9X0rOs", starttime := 120 ]
        ]


      , [ title  := "YouTube embed"
        , test   := '<iframe width="560" height="315" src="//www.youtube.com/embed/TRVaU9X0rOs" frameborder="0" allowfullscreen></iframe>'
        , out    := [ network := "youtube", videoid := "TRVaU9X0rOs", starttime := 0 ]
        ]

      , [ title  := "YouTube old URL schema"
        , test   := '<iframe width="560" height="315" src="//www.youtube.com/v/TRVaU9X0rOs" frameborder="0" allowfullscreen></iframe>'
        , out    := [ network := "youtube", videoid := "TRVaU9X0rOs", starttime := 0 ]
        ]

      , [ title := "Regressed"
        , test :=   'https://www.youtube.com/watch?v=U3NTNdbj8yg&list=PLLi0G31kZGUkv_UYVqcPCFBkA_GAgNZZC'
        , out :=  [ network := "youtube", videoid := "U3NTNdbj8yg", starttime := 0 ]
        ]

      , [ title := "URL stripped from old object embed with variable in URL"
        , test  := "http://www.youtube.com/v/Sv5wi369eS4&feature"
        , out   := [ network := "youtube", videoid := "Sv5wi369eS4", starttime := 0 ]
        ]
      ];

  RECORD ARRAY vimeotestdata :=
      [ [ test:= 'http://vimeo.com/channels/staffpicks/100426447'
        , out := [ network := "vimeo", videoid := '100426447', starttime := 0 ]
        ]
      , [ test:= 'http://vimeo.com/channels/'
        , out := DEFAULT RECORD
        ]
      , [ test:= 'http://b-lex.com/'
        , out := DEFAULT RECORD
        ]
      , [ test:= '//vimeo.com/100426447/'
        , out := [ network := "vimeo", videoid := '100426447', starttime := 0 ]
        ]
      , [ test:= '//vimeo.com/channels/staffpicks/1006447/'
        , out := [ network := "vimeo", videoid := '1006447', starttime := 0 ]
        ]
      , [ test:= '<iframe src="//player.vimeo.com/video/100426447?title=0&amp;byline=0&amp;portrait=0&amp;badge=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="http://vimeo.com/100426447">Beautiful Scotland</a> from <a href="http://vimeo.com/johnduncan">John Duncan</a> on <a href="https://vimeo.com">Vimeo</a>.</p>'
        , out := [ network := "vimeo", videoid := '100426447', starttime := 0 ]
        ]

      , [ test:= 'vimeo.com/100426447' // no protocol, no trailing slash
        , out := [ network := "vimeo", videoid := '100426447', starttime := 0 ]
        ]
      , [ test:= 'vimeo.com/100426447/' // no protocol
        , out := [ network := "vimeo", videoid := '100426447', starttime := 0 ]
        ]

        // ADDME: test with starttime in the hash -> #t=2m10s
      ];

  FOREVERY(RECORD testdata FROM (youtubetestdata CONCAT vimeotestdata))
  {
    RECORD result := GuessEmbeddedVideoFromCode(testdata.test);
    TestEq(testdata.out, result);
  }
}

MACRO TestVimeoProvider()
{
  OBJECT prov := GetVideoEmbedProvider("vimeo");
  TestEQ(TRUE, ObjectExists(prov));

  RECORD ARRAY searchres := prov->Search("",3);
  TestEq(0, Length(searchres));

  /* Vimeo currently seems to be blocking searches from the EU. Error code 5451: This resource is restricted in your region
     I'm pretty sure that's not how you should interpret the GDPR, but too bad. no more vimeo searches!

     https://www.reddit.com/r/vimeo/comments/1gh1kc0/comment/luujuf1/
     "The bad news is we can confirm that it is not a bug. Unfortunately, this feature will not be restored in the future and will no longer be available in some regions as part of our compliance with local regulations.
      We are extremely sorry for the inconvenience."
  */
  TestEqLike("Vimeo has disabled search for legal reasons", prov->Search2("Beagle").error);

/*
  searchres := prov->Search("Beagle",3);
  TestEq(3, Length(searchres));
*/
  //get video specifically - this is a public video
  searchres := prov->Search("50788492",1);
  TestEq(1,Length(searchres));
  Testeq(MakeDateTime(2012,10,4,20,35,49), searchres[0].creationdate);
  TestEqLike("*the txtr beagle*", searchres[0].description);
  TestEq(110, searchres[0].duration);
  TestEqFloat(1.77, searchres[0].playratio,0.05);
  TestEq("txtr beagle", searchres[0].title);
  TestEq("50788492", searchres[0].videoid);
  TestEQ(DEFAULT RECORD, searchres[0].location);

  //Vimeo suddenly started repoter 720px heigh instead of the real 544 - stop testing height
  TestEq(960, searchres[0].biggestthumbnail.width);
  //and vimeo started to drop the jpg extension. but for us all that matters is that we get something that looks like a url!
  TestEqLike('https://*', searchres[0].biggestthumbnail.url);

  TestEq(960, searchres[0].thumbnail.width);
  TestEqLike('https://*', searchres[0].thumbnail.url);

  RECORD describeres := prov->DescribeVideo("50788492");
  TestEq(searchres[0], describeres);

/* TODO for unlisted videos we probably need to support full urls or the has format?
  searchres := prov->Search("217139057",3);
  TestEq(TRUE, RecordExists(searchres), 'expected result for hidden vimeo video');
  TestEq("217139057", searchres[0].videoid);
  Testeq(MakeDateTime(2017,5,12,4,20,56), searchres[0].creationdate);
  TestEq("", searchres[0].description);
  TestEq(430, searchres[0].duration);
  TestEqFloat(1.77, searchres[0].playratio,0.05);
  TestEq("Pitch of Health stories for domain, impact, infrastructure, and scientific contribution", searchres[0].title);
  TestEQ(DEFAULT RECORD, searchres[0].location);
  TestEq(640, searchres[0].biggestthumbnail.width);
  TestEq(640, searchres[0].thumbnail.width);

  describeres := prov->DescribeVideo("217139057");
  TestEq(searchres[0], describeres);

  TestEq(1, Length(searchres));
*/
}

MACRO TestYoutubeProvider()
{
 // __webbrowser_debugall := TRUE;
  OBJECT prov := GetVideoEmbedProvider("youtube");
  TestEQ(TRUE, ObjectExists(prov));

  RECORD ARRAY searchres := prov->Search("",3);
  TestEq(0, Length(searchres));

  searchres := prov->Search("BEAGLE",3);
  TestEq(TRUE, LEngth(searchres)> 1);

  //DumpValue(searchres[0],'tree');
  RECORD videores := prov->DescribeVideo(searchres[0].videoid);
  TestEq(TRUE, RecordExists(videores));
  TestEq(searchres[0].videoid, videores.videoid);
  TestEq(TRUE, videores.duration > 0);

  videores := prov->DescribeVideo("4rPNWYD8su8"); //video with location
  TestEq(TRUE, RecordExists(videores));
  IF(RecordExists(videores.location)) //if location info is present... (dropped 27 feb?)
  {
    TestEq(TRUE, RecordExists(videores.location));
    TestEq(TYPEID(FLOAT), TypeID(videores.location.lat));
    TestEq(TYPEID(FLOAT), TypeID(videores.location.long));
  }

  videores := prov->DescribeVideo("dCnyLx4fQ-8"); //video with location that crashed earlier (it only supplies )
  TestEq(TRUE, RecordExists(videores));
  TestEq(FALSE, RecordExists(videores.location));

  //invisible video
  searchres := prov->Search("hAp2lrclF3w",2);
  TestEq(1, Length(searchres));
  TestEQ("hAp2lrclF3w", searchres[0].videoid);
  TestEq(TRUE, RecordExists(searchres[0].thumbnail));

  //video with PT49M duration (without seconds)
  videores := prov->DescribeVideo("5AwdkGKmZ0I");
  TestEq(TRUE, RecordExists(videores));
  TestEq(49*60, videores.duration);

  //video with PT1H10M59S duration (with hours)
  videores := prov->DescribeVideo("KR0g-1hnQPA");
  TestEq(TRUE, RecordExists(videores));
  TestEq(1*60*60+10*60+59, videores.duration);
}


RunTestFramework([ PTR Prep
                 , PTR TestNewVideoAPI
                 , PTR TestYoutubeProvider
                 , PTR TestVimeoProvider
                 ]);
