<?wh

/** @short Fetch
    @long Fetching resources over the internet via HTTP
    @topic internet/webbrowser
*/

LOADLIB "wh::files.whlib";
LOADLIB "wh::promise.whlib";
LOADLIB "wh::internal/interface.whlib";
LOADLIB "mod::system/lib/services.whlib";

OBJECT fetchpool;

STRING FUNCTION SanitizeBody(VARIANT body)
{
  STRING bodytext := TYPEID(body) = TYPEID(BLOB) ? BlobToString(body, 5000) : body;
  bodytext := LimitUTF8Bytes(bodytext, 5000);
  bodytext := Substitute(bodytext,"\r", " ");
  bodytext := Substitute(bodytext,"\n", " ");
  RETURN Bodytext;
}

STATIC OBJECTTYPE FetchResponseHeaders
<
  RECORD ARRAY headers;
  MACRO NEW(VARIANT ARRAY headers)
  {
    FOREVERY(VARIANT hdr FROM headers)
      INSERT CELL[ name  := hdr[0], value := hdr[1] ] INTO this->headers AT END;
  }
  PUBLIC STRING FUNCTION get(STRING headername)
  {
    RETURN SELECT AS STRING value FROM this->headers WHERE ToUppercase(name) = ToUppercase(headername);
  }
  PUBLIC RECORD ARRAY FUNCTION Entries()
  {
    RETURN SELECT "key" := name, value FROM this->headers;
  }
>;

STATIC OBJECTTYPE FetchResponse
<
  RECORD fetchresult;
  OBJECT __headers;

  PUBLIC PROPERTY status(fetchresult.status, -);
  PUBLIC PROPERTY statustext(fetchresult.statustext, -);
  PUBLIC PROPERTY status_text(fetchresult.statustext, -); //snakecase alias
  // A boolean indicating whether the response was successful (status in the range 200 – 299) or not.
  PUBLIC PROPERTY ok(fetchresult.ok, -);
  PUBLIC PROPERTY headers(__headers, -);

  MACRO NEW(RECORD fetchresult)
  {
    this->fetchresult := fetchresult;
    this->__headers := NEW FetchResponseHeaders(fetchresult.headers);
  }
  PUBLIC ASYNC FUNCTION JSON()
  {
    RETURN DecodeJSONBlob(this->fetchresult.body);
  }
  PUBLIC ASYNC FUNCTION Text()
  {
    RETURN BlobToString(this->fetchresult.body);
  }
  PUBLIC ASYNC FUNCTION AsBlob()
  {
    RETURN this->fetchresult.body;
  }
>;

STATIC OBJECTTYPE SyncFetchResponse
<
  OBJECT asyncresponse;

  MACRO NEW(OBJECT asyncresponse)
  {
    this->asyncresponse := asyncresponse;
  }

  PUBLIC PROPERTY status(asyncresponse->status, -);
  PUBLIC PROPERTY statustext(asyncresponse->statustext, -);
  PUBLIC PROPERTY status_text(asyncresponse->statustext, -); //snakecase alias
  PUBLIC PROPERTY ok(asyncresponse->ok, -);
  PUBLIC PROPERTY headers(asyncresponse->headers, -);

  PUBLIC VARIANT FUNCTION JSON()
  {
    RETURN WaitForPromise(this->asyncresponse->JSON());
  }
  PUBLIC VARIANT FUNCTION Text()
  {
    RETURN WaitForPromise(this->asyncresponse->Text());
  }
  PUBLIC VARIANT FUNCTION AsBlob()
  {
    RETURN WaitForPromise(this->asyncresponse->AsBlob());
  }
>;


STRING FUNCTION GetResponseSummary(OBJECT response)
{
  STRING ARRAY toks;
  IF(response->headers->Get("Content-Type") != "")
    INSERT response->headers->Get("Content-Type") INTO toks AT END;
  IF(response->headers->Get("Content-Length") != "")
    INSERT response->headers->Get("Content-Length") || " bytes" INTO toks AT END;
  IF(response->headers->Get("Transfer-Encoding") != "")
    INSERT response->headers->Get("Transfer-Encoding") INTO toks AT END;
  IF(response->headers->Get("Content-Encoding") != "")
    INSERT response->headers->Get("Content-Encoding") INTO toks AT END;

  RETURN Length(toks) > 0 ? `(${Detokenize(toks,", ")})` : "";
}



/** Fetch a HTTP resource
    @param uri URI of the resource
    @param options Fetch options
    @param pooloptions Pool options
    @return Promise resvoling to a Response
*/
PUBLIC ASYNC FUNCTION Fetch(STRING uri, RECORD options DEFAULTSTO DEFAULT RECORD, RECORD pooloptions DEFAULTSTO DEFAULT RECORD)
{
  IF(NOT ObjectExists(fetchpool))
    fetchpool := OpenWebHareService("platform:fetchpool");

  options := ValidateOptions(
      [ method :=         "GET"
      , headers :=        DEFAULT RECORD
      , body :=           DEFAULT BLOB
      , redirect :=       "follow"
      ],
      options,
      [ enums :=        [ redirect := [ "follow", "error", "manual" ]
                        ]
      , optional :=     [ "body" ]
      , notypecheck  := [ "body" ]
      ]);

  pooloptions := ValidateOptions(
      [ timeout :=  -1 //fetch would use (abort) signal
      , debug :=    FALSE //debug from pool to console
      , reject_unauthorized := FALSE
      ], pooloptions);

  IF(CellExists(options,'body'))
  {
    IF(TYPEID(options.body) = TYPEID(STRING))
      options := CELL[ ...options, body := StringToBlob(options.body) ];
    ELSE IF(TYPEID(options.body) != TYPEID(BLOB))
      THROW NEW Exception(`BODY must be either a BLOB or a STRING`);
  }

  IF(NOT RecordExists(options.headers))
    options.headers := CELL[];

  STRING debugrequestid;
  BOOLEAN debug := IsDebugTagEnabled("wrq");
  IF(debug)
  {
    debugrequestid := GenerateUFS128BitId();
    STRING method := ToUppercase(Length(options.method) < 7 ? Left(options.method || "      ",7) : options.method);

    Print(`[wrq] ${debugrequestid} ${method} ${uri}\n`);
    Print(`[wrq] ${debugrequestid} headers ${EncodeJSON(options.headers)}\n`);
    IF(CellExists(options,'body'))
      Print(`[wrq] ${debugrequestid} body    ${SanitizeBody(options.body)}\n`);
  }

  VARIANT fetchresult;
  OBJECT deadline := CreateSleepPromise(pooloptions.timeout > 0 ? pooloptions.timeout : 5 * 60 * 1000); //5 minute timeout so we eventually abort. fetch() has this timeout internally too
  OBJECT fetchpromise;
  TRY
  {
    fetchpromise := (AWAIT fetchpool)->goFetch(uri, options, pooloptions);
    fetchresult := AWAIT CreatePromiseRace([fetchpromise, deadline]);
  }
  CATCH(OBJECT<ServiceDisconnectException> e)
  {
    IF(e->beforerequest OR ToUppercase(options.method) in ["GET", "HEAD"]) //it should be safe to retry GET and HEAD, and it *is* safe to retry if the fetchpool can never have received our request
    {
      IF(debug)
        Print(`[wrq] ${debugrequestid} ServiceDisconnectException, retrying once\n`);

      fetchpool := OpenWebHareService("platform:fetchpool");
      fetchpromise := (AWAIT fetchpool)->goFetch(uri, options, pooloptions);
      fetchresult := AWAIT CreatePromiseRace([fetchpromise, deadline]);
    }
    ELSE
    {
      IF(debug)
        Print(`[wrq] ${debugrequestid} ServiceDisconnectException, unsafe to retry\n`);

      THROW NEW Exception("Fetch pool connection was lost, fetch request status is unknown");
    }
  }

  IF(TYPEID(fetchresult) = TYPEID(INTEGER)) {
    IF(debug)
      Print(`[wrq] ${debugrequestid} Client-side timeout!\n`);

    // The fetchpool itself should have timed out too, we're just protecting ourselves here
    THROW NEW Exception("The operation was aborted due to timeout");
  }

  OBJECT response := NEW FetchResponse(fetchresult);
  IF(debug)
  {
    Print(`[wrq] ${debugrequestid} result  ${fetchresult.status} ${fetchresult.statustext} ${GetResponseSummary(response)}\n`);
    Print(`[wrq] ${debugrequestid} headers ${EncodeJSON(fetchresult.headers)}\n`);
    IF(Length(fetchresult.body) > 0)
      Print(`[wrq] ${debugrequestid} body    ${SanitizeBody(fetchresult.body)}\n`);
  }

  RETURN response;
}

/** Synchronous fetch implementation */
PUBLIC OBJECT FUNCTION FetchSync(STRING uri, RECORD options DEFAULTSTO DEFAULT RECORD, RECORD pooloptions DEFAULTSTO DEFAULT RECORD)
{
  RETURN NEW SyncFetchResponse(WaitForPromise(Fetch(uri, options, pooloptions)));
}
