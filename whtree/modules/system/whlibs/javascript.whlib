<?wh
/* This library intends to enables cross engine (eg both native, wasm and native-shared-postgres) JavaScript support and
   is intended to ease the transition from HareScript to JavaScript by allowing you to start building new code in JS and
   still interface from existing HareScript applications.

   For now we're simply based on CallAsync but we hope to improve on this in the feature (eg using a JS CoVM inside either
   the bridge or directly linked to the current HSVM hosting process like CallAsync. And in WASM mode we should be able
   to directly invoke JS through syscalls without any IPC)
*/
LOADLIB "wh::internal/wasm.whlib" EXPORT IsWasm;
LOADLIB "wh::internal/interface.whlib";
LOADLIB "mod::system/lib/internal/whcore_interface.whlib";
LOADLIB "mod::system/lib/internal/services.whlib";
LOADLIB "wh::regex.whlib";
LOADLIB "wh::ipc.whlib";

RECORD FUNCTION GetDiskFileProperties(STRING filename) __ATTRIBUTES__(EXTERNAL);

RECORD ARRAY libcache; //cache opened instances

STRING FUNCTION SnakeIt(STRING match, STRING ARRAY params, INTEGER pos, STRING input)
{
  RETURN "_" || ToLowercase(match);
}
PUBLIC STRING FUNCTION ToSnakeCase(STRING camelcasetext)
{
  RETURN NEW RegEx("[A-Z]","g")->ReplaceCallback(camelcasetext, PTR SnakeIt);
}

STRING FUNCTION CamelCaseIt(STRING match, STRING ARRAY params, INTEGER pos, STRING input)
{
  RETURN ToUppercase(Substring(match,1));
}
PUBLIC STRING FUNCTION ToCamelCase(STRING camelcasetext)
{
  RETURN NEW RegEx("_[a-z]","g")->ReplaceCallback(camelcasetext, PTR CamelCaseIt);
}

OBJECTTYPE ImportedLibrary
<
  STRING __lib;

  MACRO NEW(STRING lib, RECORD descr)
  {
    this->__lib := lib;

    FOREVERY(RECORD exp FROM descr.exports)
    {
      IF(MemberExists(this, exp.name))
        CONTINUE; //ambiguous? ignore
      IF(exp.name LIKE "_*")
        CONTINUE; //probably an internal api. don't offer this (but if needed we might add/expose something like _Invoke to resolve ambiguous names?)

      IF(exp.type = "function")
      {
        FUNCTION PTR rebound := __HS_REBINDFUNCTIONPTR2(
            PTR this->_Invoke,
            [[ source := 0, value := exp.name ]],
            0,
            TRUE);

        MemberInsert(this, exp.name, FALSE, rebound);
      }
      ELSE
      {
        //Ignore things we can't process yet.
      }
    }
  }

  PUBLIC VARIANT FUNCTION _Invoke(STRING name, VARIANT ARRAY args) __ATTRIBUTES__(VARARG)
  {
    IF(IsWasm())
      RETURN EM_Syscall("importCall", CELL[ lib := this->__lib, name, args ]);

    FUNCTION PTR CallAsync := MakeFunctionPtr("mod::system/lib/internal/tasks/callasync.whlib#CallAsync");
    RECORD result := WaitForPromise(CallAsync("@mod-platform/js/typescript/callasync.ts#callExport", VARIANT[this->__lib, name, args]));
    IF(CellExists(result,'promiseid'))
      RETURN CallAsync("@mod-platform/js/typescript/callasync.ts#awaitPromise", VARIANT[ result.promiseid ]);

    RETURN result.retval;
  }

  PUBLIC VARIANT FUNCTION _Get(STRING prop)
  {
    RETURN this->_Invoke("^^get", prop);
  }

>;

BOOLEAN FUNCTION IsResolvablePath(STRING name)
{
  IF(name LIKE "./*" OR name LIKE "../*") //relative paths
    RETURN TRUE;
  IF (name LIKE "/*")
    RETURN FALSE;
  IF(name NOT LIKE "*::*" AND name NOT LIKE "@mod-*" AND name NOT LIKE "@webhare/*") //try node_modules like resolution
    RETURN TRUE;

  RETURN FALSE;
}

STRING FUNCTION ResolvePath(STRING name, STRING caller)
{
  IF(name LIKE "./*" OR name LIKE "../*") //relative paths
  {
    INTEGER lastslash := SearchLastSubstring(caller,'/');
    name := __HS_INTERNAL_RESOLVEABSOLUTELIBRARY(caller, "relative::" || name);
  }
  ELSE//try node_modules like resolution
  {
    //Attempt an upwards search (but do not leave the module!)
    STRING ourdir := __HS_INTERNAL_RESOLVEABSOLUTELIBRARY(caller, "relative::.");
    IF(ourdir LIKE "mod::*")
    {
      STRING ARRAY toks := Tokenize(ourdir,'/');
      STRING modpart := toks[0];
      toks[0] := GetModuleInstallationRoot(Substring(modpart, 5));
      IF(toks[0] != "")
        FOR(INTEGER numtoks := Length(toks); numtoks>0; numtoks := numtoks - 1) //shorten th path until we find a match (TODO node_module style extension appending)
        {
          STRING trydiskpath := Detokenize(ArraySlice(toks, 0, numtoks),'/') || "/node_modules/" || name;
          IF(trydiskpath != "")
          {
            RECORD props := GetDiskFileProperties(trydiskpath);
            IF(RecordExists(props))
            {
              //Rebuild that path using mod:: to cut back on potential aliassing
              name := modpart || Detokenize(ArraySlice(toks, 1, numtoks-1),'/') || "/node_modules/" || name;
              BREAK;
            }
          }
      }
    }
  }
  RETURN name;
}

PUBLIC OBJECT FUNCTION ImportJS(STRING name)
{
  IF(IsResolvablePath(name))
    name := ResolvePath(name, GetCallingLibrary(TRUE));

  OBJECT lib := SELECT AS OBJECT libcache.lib FROM libcache WHERE libcache.name = VAR name;
  IF(NOT ObjectExists(lib))
  {
    //TODO cache descriptions, eventmasks on the file should suffice unless we really need to support 'export' ?
    RECORD descr;
    IF(IsWasm())
      descr := WaitForPromise(EM_Syscall("importDescribe", CELL[name]));
    ELSE
    {
      FUNCTION PTR CallAsync := MakeFunctionPtr("mod::system/lib/internal/tasks/callasync.whlib#CallAsync");
      descr := WaitForPromise(CallAsync("@mod-platform/js/typescript/callasync.ts#describe", [ name ]));
    }
    lib := NEW ImportedLibrary(name, descr);
    INSERT CELL [ lib, name ] INTO libcache AT END;
  }

  RETURN lib;
}

OBJECT calljsservice;
OBJECT calljseventmgr;

/** Invoke a specific JavaScript function. Through a central service in Native mode and
    directly in WASM mode (with reentrancy disallowed)
    @param name lib#name to call. Case sensitive!
    @return The result of the function call. Promises are evaluated immediately
    */
PUBLIC VARIANT FUNCTION CallJS(STRING name, VARIANT ARRAY args) __ATTRIBUTES__(VARARG)
{
  STRING ARRAY toks := Tokenize(name,'#');
  STRING lib := Length(toks) = 1 ? "" : toks[0];
  IF(IsResolvablePath(lib))
    lib := ResolvePath(lib, GetCallingLibrary(TRUE));
  IF(Length(toks) = 2)
    name := toks[1];

  IF(IsWasm())
    RETURN WaitForPromise(EM_Syscall("jsCall", CELL[ lib, name, args ]));

  IF(NOT ObjectExists(calljseventmgr)) {
    calljseventmgr := NEW EventManager; //RegisterEventCallback won't do, that one waits for microtasks but CallJS avoids running those
    calljseventmgr->RegisterInterest("system:webhareservice.platform:calljs.start");
  } ELSE IF(calljseventmgr->ReceiveEvent(DEFAULT DATETIME).status NOT IN ["timeout","gone"]) {
    //calljs has restarted, reconnect it
    calljsservice->CloseService();
    calljsservice := DEFAULT OBJECT;

    WHILE(calljseventmgr->ReceiveEvent(DEFAULT DATETIME).status NOT IN ["timeout","gone"])
      /* Flush any other pending events */;
  }
  IF(NOT ObjectExists(calljsservice))
    calljsservice := __OpenSynchronousWebHareService("platform:calljs");

  RETURN calljsservice->Invoke(lib,name,args);
}
