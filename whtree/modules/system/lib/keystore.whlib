<?wh
/** @topic modules/services */

LOADLIB "wh::adhoccache.whlib";
LOADLIB "wh::crypto.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::internet/urls.whlib";
LOADLIB "wh::util/algorithms.whlib";
LOADLIB "wh::internal/asn1.whlib";
LOADLIB "wh::internal/ber.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::filetypes/pkcs.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/whfs.whlib";
LOADLIB "mod::system/lib/remoting/whservice.whlib";
LOADLIB "mod::system/lib/internal/whfs/base.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

LOADLIB "mod::tollium/lib/gettid.whlib";

LOADLIB "mod::socialite/lib/google/apikey.whlib";

/** @private Creates the DN hash needed to find intermediate at the webhare certificate store service
    @param readablename Readable name
    @return DN hash
*/
PUBLIC STRING FUNCTION CreateWebhareDNHash(STRING readablename)
{
  RETURN ToLowercase(EncodeBase16(GetSHA1Hash(readablename)));
}

/** A keypair contains a private key and a certificate chain
    @public
*/
OBJECTTYPE KeyPair
< // ---------------------------------------------------------------------------
  //
  // Variables
  //

  /// Id of the keypair folder
  INTEGER pvt_id;

  /** Cached folderdata
      @cell name Key name
      @cell title Key title
      @cell description Key description
  */
  RECORD pvt_folderdata;

  // ---------------------------------------------------------------------------
  //
  // Properties
  //

  /// @type(integer) Id of the keypair
  PUBLIC PROPERTY id(pvt_id, -);

  /// @type(string) Name of the keypair
  PUBLIC PROPERTY name(pvt_folderdata.name, -);

  /// @type(string) Title of the keypair
  PUBLIC PROPERTY title(pvt_folderdata.title, -);

  /// @type(string) Description of the keypair
  PUBLIC PROPERTY description(pvt_folderdata.description, -);

  /// @type(blob) Private key (should be PEM encoded)
  PUBLIC PROPERTY privatekey(GetPrivateKey, -);

  /// @type(blob) Certificate chain (X509 encoded)
  PUBLIC PROPERTY certificatechain(GetCertificateChain, -);

  /// @type(blob) First certificate in chain
  PUBLIC PROPERTY certificate(GetCertificate, -);

  // ---------------------------------------------------------------------------
  //
  // Constructor
  //

  MACRO NEW(INTEGER id)
  {
    this->pvt_id := id;
    this->Reload();
  }

  // ---------------------------------------------------------------------------
  //
  // Getters & setters
  //

  BLOB FUNCTION GetPrivateKey()
  {
    RETURN
        SELECT AS BLOB data
          FROM system.fs_objects
         WHERE parent = this->pvt_id
           AND name = "privatekey.pem";
  }

  BLOB FUNCTION GetCertificateChain()
  {
    RETURN
        SELECT AS BLOB data
          FROM system.fs_objects
         WHERE parent = this->pvt_id
           AND name = "certificatechain.pem";
  }

  BLOB FUNCTION GetCertificate()
  {
    // Get the first certificate from the chain
    STRING chain := BlobToString(this->GetCertificateChain());
    STRING endtag := "-----END CERTIFICATE-----";
    INTEGER pos := SearchSubString(chain, endtag);
    IF (pos = -1)
      RETURN DEFAULT BLOB;

    RETURN StringToBlob(Left(chain, pos + LENGTH(endtag)));
  }

  // ---------------------------------------------------------------------------
  //
  // Helper functions
  //

  MACRO Reload()
  {
    this->pvt_folderdata :=
        SELECT name
             , title
             , description
          FROM system.fs_objects
         WHERE id = this->pvt_id AND NOT IsRecycleOrHistoryWHFSPath(whfspath);

    IF (NOT RecordExists(this->pvt_folderdata)) // Deleted?
    {
      // Prevent crashes
      this->pvt_id := 0;
      this->pvt_folderdata := [ name := "", title := "", description := "" ];
    }
  }

  OBJECT FUNCTION GetFolderObject()
  {
    // Can't loadlib whfs.whlib here
    RETURN MakeFunctionPtr("mod::system/lib/whfs.whlib#OPENWHFSOBJECT")(this->pvt_id);
  }

  // ---------------------------------------------------------------------------
  //
  // Public API
  //

  ///Suggested default renewal warning period in days
  PUBLIC INTEGER FUNCTION GetDefaultRenewalWarningPeriod()
  {
    RETURN this->name LIKE "certbot-*" ? whconstant_autorenewed_warnexpirydays : whconstant_default_warnexpirydays;
  }

  /** Update keypair metadata
      @param newmetadata New metadata values
      @cell(string) newmetadata.name Key name
      @cell(string) newmetadata.title Key title
      @cell(string) newmetadata.description Key description
      @cell(blob) newmetadata.privatekey Private key
      @cell(blob) newmetadata.certificatechain Certificate chain
      @cell(blob) newmetadata.certbotoutput Output from certbot
      @cell(boolean) newmetadata.skipvalidation If TRUE, don't validate the private key (use when the private key is encrypted)
  */
  PUBLIC MACRO UpdateMetadata(RECORD newmetadata)
  {
    newmetadata := ValidateOptions( [ name := ""
                                    , title := ""
                                    , description := ""
                                    , privatekey := DEFAULT BLOB
                                    , certificatechain := DEFAULT BLOB
                                    , certbotoutput := DEFAULT BLOB
                                    , skipvalidation := FALSE
                                    ], newmetadata, [ optional := ["name","title","description","privatekey","certificatechain","certbotoutput"]]);

    RECORD folder_changes;
    IF (CellExists(newmetadata, "NAME"))
      INSERT CELL name := newmetadata.name INTO folder_changes;
    IF (CellExists(newmetadata, "TITLE"))
      INSERT CELL title := newmetadata.title INTO folder_changes;
    IF (CellExists(newmetadata, "DESCRIPTION"))
      INSERT CELL description := newmetadata.description INTO folder_changes;

    OBJECT obj := this->GetFolderObject();
    obj->UpdateMetadata(folder_changes);

    IF (CellExists(newmetadata, "PRIVATEKEY"))
    {
      IF(NOT newmetadata.skipvalidation)
      {
        RECORD decoded := DecodePEMFile(BlobToString(newmetadata.privatekey));
        IF (decoded.type NOT IN [ "private key", "rsa private key" ])
          THROW NEW Exception("Private key file is not a private key, type: " || decoded.type);
      }

      OBJECT keyfile := obj->OpenByPath("privatekey.pem");
      IF (ObjectExists(keyfile))
        keyfile->UpdateMetadata([ data := newmetadata.privatekey ]);
      ELSE
        obj->CreateFile([ name := "privatekey.pem", data := newmetadata.privatekey ]);
    }

    IF (CellExists(newmetadata, "CERTIFICATECHAIN"))
    {
      OBJECT certfile := obj->OpenByPath("certificatechain.pem");

      IF (LENGTH(newmetadata.certificatechain) != 0)
      {
        BLOB fixeddata;
        // Check the certificate chain for validity
        RECORD decoded := DecodePEMFile(BlobToString(newmetadata.certificatechain));
        IF (decoded.type = "multiple")
        {
          FOREVERY (RECORD part FROM decoded.parts)
            IF (part.type != "certificate")
              THROW NEW Exception("Certificatechain file contains a non-certificate: " || part.type);
        }
        ELSE IF (decoded.type != "certificate")
          THROW NEW Exception("Certificatechain file is not a certificate (chain), type: " || decoded.type);

        fixeddata := StringToBlob(EncodePEMFileFromRawSource(decoded));

        IF (ObjectExists(certfile))
          certfile->UpdateMetadata([ data := fixeddata ]);
        ELSE
          obj->CreateFile([ name := "certificatechain.pem", data := fixeddata ]);
      }
      ELSE IF(Objectexists(certfile))
      {
        certfile->RecycleSelf();
      }
    }

    IF (CellExists(newmetadata, "CERTBOTOUTPUT"))
      obj->EnsureFile([ name := "certbotoutput.txt" ])->UpdateData(newmetadata.certbotoutput);

    this->Reload();
  }

  /** Move the keypair to the recycle bin
  */
  PUBLIC MACRO RecycleSelf()
  {
    this->GetFolderObject()->RecycleSelf();
  }

  /** Generate a certificate signing request
      @cell options.c Value for the `c` certificate field
      @cell options.st Value for the `st` certificate field
      @cell options.l Value for the `l` certificate field
      @cell options.o Value for the `o` certificate field
      @cell options.cn Value for the `cn` certificate field
      @cell options.ou Value for the `ou` certificate field
      @cell options.dnsaltnames DNS altnames for the certificate
      @return Certificate signing request
  */
  PUBLIC BLOB FUNCTION GenerateCSR(RECORD options)
  {
    RECORD ARRAY params;
    STRING altnames;

    FOREVERY (RECORD opt FROM UnpackRecord(options))
    {
      IF (opt.name IN [ "C", "ST", "L", "O", "CN", "OU" ])
        INSERT [ field := opt.name, value := opt.value ] INTO params AT END;
      ELSE IF (opt.name = "DNSALTNAMES")
        altnames := Detokenize((SELECT AS STRING ARRAY "DNS:" || host FROM ToRecordArray(opt.value, "HOST")), ", ");
      ELSE
        THROW NEW Exception("Unknown option '" || opt.name || "'");
    }

    // Order params
    params := SELECT * FROM params ORDER BY SearchElement([ "C", "ST", "L", "O", "CN" ], field);

    STRING keydata := BlobToString(this->privatekey);
    OBJECT evpkey2 := MakeCryptoKey(keydata);
    IF(NOT Objectexists(evpkey2))
      THROW NEW Exception("Unable to read private key");

    STRING csr := evpkey2->GenerateCSR(params, altnames);
    RETURN StringToBlob(csr);
  }

  /** Signs a certificate request, generating a new certificate
      @param csr Certificate request
      @cell(boolean) options.selfsign Whether this is a self-signing
      @cell(string) options.passphrase Passphrase to decode the private key
      @cell(string) options.validuntil Expiration date
      @return Certificate
      @cell(blob) return.certificate Certificate
      @cell(blob) return.certificatechain Whole certificate chain
  */
  PUBLIC RECORD FUNCTION SignCertificateRequest(BLOB csr, RECORD options)
  {
    options := ValidateOptions(
        [ selfsign :=     FALSE
        , passphrase :=   ""
        , validuntil :=   DEFAULT DATETIME
        ], options);

    RECORD csrdata := DecodePEMFile(BlobToString(csr));
    IF (NOT RecordExists(csrdata) OR csrdata.type != "certificate request")
      THROW NEW Exception("CSR is not a valid certificate request");

    RECORD csr_info := csrdata.rawdecode.value.certificationrequestinfo;

    // Check if the CSR is self-consistent (signed with the private key from corresponding to the subject public key)
    IF (NOT CheckPKCSSignature(csrdata).success)
      THROW NEW Exception("CSR signature validation failed");

    BLOB certificatechain;

    // Extra attributes requested?
    RECORD ext_attrs :=
        SELECT *
          FROM csr_info.attributes
         WHERE type = "1.2.840.113549.1.9.14";

    RECORD ARRAY csr_extensions;
    IF (RecordExists(ext_attrs))
    {
      csr_extensions :=
        SELECT AS RECORD ARRAY data[0]
          FROM ext_attrs."values"
         WHERE RecordExists(data);
    }

    IF (NOT RecordExists(SELECT FROM csr_extensions WHERE extnid = "2.5.29.14")) // SubjectKeyIdentifier
    {
      RECORD pkey := DecodePEMFile(csrdata.subjectpublickey);
      INSERT
          [ extnid := "2.5.29.14"
          , extnvalue :=  [ __datatype := "SubjectKeyIdentifier"
                          , data := GetSHA1Hash(pkey.publickey.data)
                          ]
          ] INTO csr_extensions AT END;
    }

    RECORD signcert_info;
    IF (options.selfsign)
    {
      signcert_info := csr_info;
      INSERT CELL extensions := csr_extensions INTO signcert_info;
    }
    ELSE
    {
      BLOB cert := this->certificate;
      IF (LENGTH(this->certificate) = 0)
        THROW NEW Exception("No certificate present, can't sign with this key");

      certificatechain := this->certificatechain;
      RECORD decoded_cert := DecodePEMFile(BlobToString(this->certificate));
      signcert_info := decoded_cert.rawdecode.value.tbscertificate;
    }

    DATETIME now := GetRoundedDateTime(GetCurrentDateTime(), 1000);
    DATETIME until := options.validuntil != DEFAULT DATETIME
        ? GetRoundedDateTime(options.validuntil, 1000)
        : AddYearsToDate(1, now);

    RECORD signpubkeyext := SELECT * FROM signcert_info.extensions WHERE extnid = "2.5.29.14"; // SubjectKeyIdentifier
    IF (RecordExists(signpubkeyext))
    {
      INSERT
          [ extnid := "2.5.29.35"
          , extnvalue :=  [ __datatype := "AuthorityKeyIdentifier"
                          , data :=
                                [ keyIdentifier := signpubkeyext.extnvalue.data
                                ]
                          ]
          ] INTO csr_extensions AT END;
    }

    RECORD newcert :=
        [ tbscertificate :=
              [ serialnumber :=   "00" || EncodeBase16(DecodeUFS(GenerateUFS128BitId()))
              , version :=        2
              , issuer :=         signcert_info.subject
              , subject :=        csr_info.subject
              , signature :=      [ algorithm := "1.2.840.113549.1.1.11" // RSA-SHA256
                                  , parameters := ""
                                  ]
              , subjectPublicKeyInfo := csr_info.subjectpublickeyinfo
              , validity :=       [ notafter :=   [ utctime := until ]
                                  , notbefore :=  [ utctime := now ]
                                  ]
              , extensions :=     csr_extensions
              ]
        , signaturealgorithm :=   [ algorithm := "1.2.840.113549.1.1.11" // RSA-SHA256
                                  , parameters := ""
                                  ]
        , signature :=            DEFAULT RECORD
        ];

    OBJECT parser := GetASN1Parser();
    parser->ParseDocument(BlobToString(GetHarescriptResource("mod::system/whres/asn1/x509.asn1"), -1));
    parser->ParseDocument(BlobToString(GetHarescriptResource("mod::system/whres/asn1/certsupport.asn1"), -1));

    OBJECT decoder := GetASN1BERDecoder(parser->GetDescription());

    OBJECT signeddata_encoder := NEW BerEncoder;
    decoder->EncodeData(signeddata_encoder, "TBSCertificate", newcert.tbscertificate);
    STRING signeddata := signeddata_encoder->GetRequest();

    STRING signature := MakeCryptoKey(BlobToString(this->privatekey))->Sign(signeddata, "SHA-256");// TODO fix passphrase support in MakeCryptoKey but noone was using it right now

    newcert.signature :=
        [ bits :=   LENGTH(signature) * 8
        , data :=   signature
        ];

    OBJECT encoder := NEW BerEncoder;
    decoder->EncodeData(encoder, "Certificate", newcert);
    STRING finalcert := encoder->GetRequest();

    BLOB finalcertblob := StringToBlob(EncodeRawPEMFile("CERTIFICATE", finalcert));

    RETURN
        [ certificate :=        finalcertblob
        , certificatechain :=   MakeComposedBlob([ [ data := finalcertblob ], [ data := certificatechain ] ])
        ];
  }

  /** Tests if a certificate is valid
      @param certdata Certificate data
      @return Verification result
      @cell(boolean) return.success Whether the certificate is valid
      @cell(string) return.message Translated error message when  the certificate is invalid
      @cell(blob) return.finalkey Final certificate chain (missing certificates may be added)
  */
  PUBLIC RECORD FUNCTION TestCertificate(BLOB certdata)
  {
    RECORD chaindata;
    STRING certtext := BlobToString(certdata);
    TRY chaindata := DecodePEMFile(certtext);
    CATCH ;
    IF (NOT RecordExists(chaindata) OR chaindata.type != "multiple")
    {
      // Might be self-signed certificate
      IF (NOT RecordExists(chaindata) OR chaindata.type != "certificate")
      {
        RETURN [ success := FALSE
               , message := GetTid("system:tolliumapps.config.keystore.main.noncertificateinchain")
               ];
      }
      chaindata := [ parts := [ chaindata ] ];
    }

    RECORD keydata := DecodePEMFile(BlobToString(this->privatekey));
    IF (keydata.algorithm != "RSA encryption" OR TypeID(keydata.privatekey.modulus) != TypeID(STRING))
      keydata := DEFAULT RECORD;

    IF (NOT RecordExists(keydata) OR keydata.privatekey.modulus != chaindata.parts[0].subjectpublickeymodulus)
    {
      RETURN [ success := FALSE
             , message := GetTid("system:tolliumapps.config.keystore.main.certificatenotforthiskey")
             ];
    }

    RECORD ARRAY keyparts := chaindata.parts;
    WHILE(keyparts[END-1].subject != keyparts[END-1].issuer)
    {
      STRING getissuer := keyparts[END-1].issuer;
      IF(Length(keyparts)>10)
        THROW NEW Exception("Keychain too long looking for " || getissuer);

      OBJECT browser := GetWebbrowserForWHServiceURL("certificatestore/" || CreateWebHareDNHash(getissuer) || ".pem");
      IF(NOT Objectexists(browser))
        RETURN [ success := FALSE
               , message := GetTid("system:tolliumapps.config.keystore.main.missingcertificate", getissuer)
               ];

      INSERT DecodePEMFile(BlobToString(browser->content)) INTO keyparts AT END;
      IF(keyparts[END-1].subject != keyparts[END-1].issuer) //not the terminating cert yet, then add it to our full chain
        certtext := certtext || "\r\n" || BlobToString(browser->content);
    }

    STRING certfile := BlobToString(this->certificatechain);

    // Single, self-signed certificate?
    RECORD signingcert;
    IF (LENGTH(keyparts) = 1 AND NOT CheckPKCSSignature(chaindata.parts[0], [ signingcert := keyparts[0] ]).success)
    {
      RETURN [ success := FALSE
             , message := GetTid("system:tolliumapps.config.keystore.main.missingcertificatechain")
             ];
    }

    //If we get here, we've got a chain of certificates
    FOR (INTEGER i := LENGTH(keyparts) - 1; i >= 0; i := i - 1)
    {
      RECORD keycertdata := keyparts[i];
      IF (keycertdata.type != "certificate")
      {
        RETURN [ success := FALSE
               , message := GetTid("system:tolliumapps.config.keystore.main.noncertificateinchain")
               ];
      }

      IF (RecordExists(signingcert))
      {
        RECORD checkstatus := CheckPKCSSignature(keycertdata, [ signingcert := signingcert ]);
        IF (NOT checkstatus.success)
        {
          SWITCH (checkstatus.code)
          {
            CASE "NOTISSUER", "KEYIDENTMISMATCH"
            {
              RETURN [ success := FALSE
                     , message := GetTid("system:tolliumapps.config.keystore.main.certificatedoesnotmatchissuer")
                     ];
            }
            CASE "SIGNFAILURE"
            {
              RETURN [ success := FALSE
                     , message := GetTid("system:tolliumapps.config.keystore.main.signatureverificationfailed")
                     ];
            }
            CASE "UNSUPPORTEDALGORITHM"
            {
              RETURN [ success := FALSE
                     , message := GetTid("system:tolliumapps.config.keystore.main.unsupportedsignaturealgorithm", chaindata.signaturealgorithm)
                     ];
            }
            DEFAULT
            {
              THROW NEW Exception("Unhandled verification status '" || checkstatus.code || "'");
            }
          }
        }
      }
      signingcert := keycertdata;
    }
    RETURN [ success := TRUE
           , finalkey := StringToBlob(certtext)
           ];
  }

  /** Returns the invalidation event masks for this key
      @return Event masks
  */
  PUBLIC STRING ARRAY FUNCTION GetEventMasks()
  {
    RETURN [ "system:whfs.folder." || this->id ];
  }
>;


/** List all keypairs
    @cell(boolean) options.requirecert Return only keys with certificates (defaults to true)
    @return A list of keypairs
    @cell(integer) return.id Key ids
    @cell(integer) return.name Key name
    @cell(integer) return.title Title
    @cell(integer) return.has_certificate True if this key has a certificate
*/
PUBLIC RECORD ARRAY FUNCTION ListKeyPairs(RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([requirecert := TRUE ], options);

  INTEGER keystore_folder := LookupWHFSObject(0, "/webhare-private/system/keystore");
  IF (keystore_folder = -1)
    RETURN DEFAULT RECORD ARRAY;

  RECORD ARRAY keys :=
      SELECT id
           , name
           , title
        FROM system.fs_objects
       WHERE parent = keystore_folder;

  INTEGER ARRAY keyids := SELECT AS INTEGER ARRAY id FROM keys;
  INTEGER ARRAY keys_with_cert :=
      SELECT AS INTEGER ARRAY parent
        FROM system.fs_objects
       WHERE parent IN keyids
         AND name = "certificatechain.pem";

  RETURN
      SELECT *
           , has_certificate :=   id IN keys_with_cert
        FROM keys
       WHERE options.requirecert ? id IN keys_with_cert : TRUE;
}

/** Get a specific keypair by id
    @param id Keypair id
    @return(object %KeyPair) Keypair object (DEFAULT OBJECT if not found)
*/
PUBLIC OBJECT FUNCTION OpenKeyPair(INTEGER id)
{
  OBJECT kp := NEW KeyPair(id);
  IF(kp->id = 0) //deleted?
    RETURN DEFAULT OBJECT;
  RETURN kp;
}

/** Get a keypair by name
    @param keypairname Keypair name (case insensitive)
    @return(object %KeyPair)  Keypair object (DEFAULT OBJECT if not found)
*/
PUBLIC OBJECT FUNCTION OpenKeyPairByName(STRING keypairname)
{
  INTEGER keystore_folder := LookupWHFSObject(0, "/webhare-private/system/keystore");
  IF (keystore_folder = -1)
    RETURN DEFAULT OBJECT;

  INTEGER keypair_id :=
      SELECT AS INTEGER id
        FROM system.fs_objects
       WHERE parent = keystore_folder
         AND ToUppercase(name) = ToUppercase(keypairname);

  RETURN keypair_id = 0 ? DEFAULT OBJECT : NEW KeyPair(keypair_id);
}

/** Create a new keypair
    @param name Name for the key
    @param privatekey The key to add
    @cell(blob) options.certificatechain Certificate chain
    @cell(string) options.title Key title
    @cell(string) options.description Key description
    @cell(boolean) options.generateuniquename If TRUE, generate a unique name instead of using the name parameter
    @return(object %KeyPair) New keypair object
*/
PUBLIC OBJECT FUNCTION CreateKeyPair(STRING name, BLOB privatekey, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  OBJECT systemfolder := OpenWHFSObject(whconstant_whfsid_private_system);
  OBJECT keystorefolder := systemfolder->OpenByName("keystore") ?? systemfolder->CreateFolder([name := "keystore"]);

  IF(CellExists(options,'generateuniquename') AND options.generateuniquename)
    name := keystorefolder->GenerateUniqueName(name);

  DELETE CELL generateuniquename FROM options;

  OBJECT keyfolder := keystorefolder->CreateFolder([ name := name ]);
  TRY
  {
    OBJECT keypair := OpenKeyPair(keyfolder->id);
    keypair->UpdateMetadata(MakeMergedRecord( [ privatekey := privatekey ], options));
    RETURN keypair;
  }
  CATCH (OBJECT e)
  {
    keyfolder->DeleteSelf();
    THROW e;
  }
}

RECORD FUNCTION GetCachableKeyList()
{
  RECORD ARRAY keypairs := ListKeyPairs();

  INTEGER keystore_folder := LookupWHFSObject(0, "/webhare-private/system/keystore");
  IF (keystore_folder = -1)
    RETURN [ value := RECORD[], ttl := 1000 ];

  STRING ARRAY eventmasks := [ "system:whfs.folder." || keystore_folder ];

  RECORD ARRAY certificates;
  FOREVERY (RECORD keypair_rec FROM ListKeyPairs([ requirecert := FALSE ]))
  {
    OBJECT keypair := OpenKeyPair(keypair_rec.id);
    eventmasks := eventmasks CONCAT keypair->GetEventMasks();

    IF(Length(keypair->certificate)=0)
      CONTINUE;

    TRY
    {
      RECORD certdata := DecodePEMFile(BlobToString(keypair->certificate));
      IF (NOT RecordExists(certdata))
        CONTINUE;

      INSERT CELL
          [ keypair_rec.id
          , certdata := CELL
                [ certdata.valid_until
                , certdata.servernames
                ]
          ] INTO certificates AT END;
    }
    CATCH (OBJECT e)
    {
      LogHarescriptException(e);
    }
  }

  RETURN CELL
      [ ttl :=        60 * 60 * 1000
      , eventmasks := GetSortedSet(eventmasks)
      , value :=      certificates
      ];
}

/** Returns the ID of the best keypair for a specific hostname
    @param hostname Hostname to get the best certificate for
    @return ID of the best certificate (0 if no match found)
*/
PUBLIC INTEGER FUNCTION GetBestKeyPairForHostName(STRING hostname)
{
  RECORD ARRAY certificates := GetAdhocCached([ type := "allkeypairs" ], PTR GetCachableKeyList);

  RECORD best_cert;
  STRING name := ToLowercase(hostname);
  FOREVERY (RECORD cert FROM certificates)
  {
    BOOLEAN is_match := IsCertificateForHostname(cert.certdata, name);
    IF (is_match)
    {
      IF (NOT RecordExists(best_cert) OR best_cert.certdata.valid_until < cert.certdata.valid_until)
        best_cert := cert;
    }
  }
  RETURN RecordExists(best_cert) ? best_cert.id : 0;
}

/** @short Obtain an API key
    @param type Key type you're looking up. Must be "google" or "google-recaptcha" at this moment
    @param requestdomain Domain or URL for which you're requesting a key
    @cell options.scope Target use for the key. `client` for an apikey you can send to the user (eg javascript),
                        `server` for an apikey to use when directly communicating to the api. 'any' if you don't care
    @cell options.allowmissing If true, returns a default record if not found. Otherwise, the function will throw
    @return The api keys, or throws if no match is found (unless allowmissing is set)
    @cell return.apikey The API key
    @cell return.privatekey The private key, if set
*/
PUBLIC RECORD FUNCTION LookupAPIKey(STRING type, STRING requestdomain, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  IF(type NOT IN ["google", "google-recaptcha"])
    THROW NEW Exception(`Unsupported API key '${type}'`);

  options := ValidateOptions([ scope := "any", allowmissing := FALSE ], options, [ enums := [ scope := [ "any", "client", "server" ]]]);

  IF(type = "google" AND options.scope NOT IN ["client","server"])
    THROW NEW Exception(`Google cloud platform keys require you to explicitly indicate whether you're looking for a client or server key`);

  //If we receive a URL, extract the requestdomain
  IF(requestdomain LIKE "http://*" OR requestdomain LIKE "https://*")
    requestdomain := UnpackURL(requestdomain).host;

  RECORD keyrec := __GetGoogleAPIKey(requestdomain, type="google" ? 0 : 1, SearchElement(["any","client","server"], options.scope));
  IF(NOT RecordExists(keyrec))
  {
    IF(NOT options.allowmissing)
      THROW NEW Exception(`Unable to find the proper API key for '${requestdomain}' (scope=${options.scope}, type=${type})`);
    RETURN DEFAULT RECORD;
  }
  RETURN CELL[ apikey := keyrec.apikey, privatekey := keyrec.privatekey ];
}
