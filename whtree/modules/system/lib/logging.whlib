<?wh
/** @topic modules/services */

LOADLIB "wh::ipc.whlib";

LOADLIB "wh::javascript.whlib";
LOADLIB "mod::system/lib/internal/whcore_interface.whlib";
LOADLIB "mod::system/lib/internal/cluster/logging.whlib" EXPORT OpenWebHareLogStream, OpenWebHareLogStreamFromFiles, LogHarescriptException, ModuleLog, UpdateAuditContext;
LOADLIB "mod::system/lib/internal/cluster/logsettings.whlib";
LOADLIB "mod::system/lib/internal/cluster/logwriter.whlib" EXPORT LogToJSONLog;

/** Flushes a specific module log
    @param name Name of the log (module:logname) to flush, or "*" to flush all log files
    @return Whether the flush succeeded
*/
PUBLIC BOOLEAN FUNCTION FlushModuleLog(STRING name)
{
  RETURN __SYSTEM_FLUSHREMOTELOG(name);
}

/** @short Log an audit event by the current effective user
    @param logsource Source or call of the error message
    @param data Event data */
PUBLIC MACRO LogAuditEvent(STRING logsource, RECORD data)
{
  LogAuditEventBy(logsource, DEFAULT RECORD, data);
}

MACRO LogNotice(STRING logsource, STRING channel, STRING message, RECORD data)
{
  IF(logsource="")
    THROW NEW Exception("A log source must be specified");
  IF(message="")
    THROW NEW Exception("A message must be specified");

  message := Left(message,4096);

  STRING logline := EncodeJava(logsource) || "\t" || EncodeJava(channel) || "\t" || GetCurrentGroupId() || "\t" || EncodeJava(GetExternalSessionData()) || "\t" || EncodeJava(message);
  STRING encodeddata := EncodeHSON(data);

  IF(Length(encodeddata) > 127*1024)
    LogToJSONLog("system:notice", CELL[ source := logsource, groupid := GetCurrentGroupId(), type := channel, message, session := GetExternalSessionData(), overflow := Length(encodeddata) ]);
  ELSE
    LogToJSONLog("system:notice", CELL[ source := logsource, groupid := GetCurrentGroupId(), type := channel, message, session := GetExternalSessionData(), data ]);
}


/** @short Log an error to the 'notice' log
    @long Log an important message indicating trouble that should be handled as soon as possible ("ACT NOW")
    @param logsource Source or call of the error message
    @param message Error message in text, should be understandable by sysop users
    @param data System-parseable error context */
PUBLIC MACRO LogError(STRING logsource, STRING message, RECORD data DEFAULTSTO DEFAULT RECORD)
{
  LogNotice(logsource, "error", message, data);
}

/** @short Log a warning to the 'notice' log
    @long Log a message indicating an important issue, that should probably be dealt with when convenient ("ACT LATER")
    @param logsource Source or call of the error message
    @param message Error mesage in text, should be understandable by sysop users
    @param data System-parseable error context */
PUBLIC MACRO LogWarning(STRING logsource, STRING message, RECORD data DEFAULTSTO DEFAULT RECORD)
{
  LogNotice(logsource, "warning", message, data);
}

/** @short Log an informational message to the 'notice' log
    @long Log a message indicating an issue or odd event, that is probably not the system's fault, but may be relevant when investing issues ("FYI")
    @param logsource Source or call of the error message
    @param message Error mesage in text, should be understandable by sysop users
    @param data System-parseable error context */
PUBLIC MACRO LogInfo(STRING logsource, STRING message, RECORD data DEFAULTSTO DEFAULT RECORD)
{
  LogNotice(logsource, "info", message, data);
}

/** @short Log a list of Harescript errors to the notice log (via a HarescriptErrorException)
    @param errors Errors to log
    @param options Options @includecelldef LogHarescriptException.options
*/
PUBLIC MACRO LogHarescriptErrors(RECORD ARRAY errors, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  LogHarescriptException(NEW HarescriptErrorException(errors), options);
}

/** @short Log RPC traffic
    @param logsource Source or call of the rpc action (eg: webshop:payment.buckaroo, or statsserver:service.regsiter)
    @param transport Transport method or destination
    @param outgoing Set to true for outgoing traffic
    @param sourcetracker Orginal requestor (IP address or trackingstamp if possible)
    @param transactionid RPC transaction id, to join request and response together
    @param data Data to log. A request/reponse body should be logged into the cell 'body' */
PUBLIC MACRO LogRPCTraffic(STRING logsource, STRING transport, BOOLEAN outgoing, STRING sourcetracker, STRING transactionid, RECORD data)
{
  IF(logsource="")
    THROW NEW Exception("A log source must be specified");
  IF(transport="")
    THROW NEW Exception("A transport must be specified");
  IF(NOT IsRPCTrafficLogged(logsource))
    RETURN;

  IF(sourcetracker="")
    sourcetracker := "-";
  IF(transactionid ="")
    transactionid := "-";

  STRING hsondata := EncodeHSON(data);
  IF(Length(hsondata)>128*1024)
    hsondata := EncodeHSON([__notlogged := "Not logging " || Length(hsondata) || " bytes of encoded data"]); //we can't throw, it would often break RPCs

  ModuleLog("system:rpc", EncodeJava(logsource) || "\t" || GetCurrentGroupId() || "\t" || EncodeJava(GetExternalSessionData()) || "\t" || EncodeJava(sourcetracker) || (outgoing ? "\t>" : "\t<") || EncodeJava(transport) || "\t" || EncodeJava(transactionid) || "\t" || hsondata);
}


MACRO WBTrafficHook(STRING logsource, STRING sourcetracker, BOOLEAN outgoing, STRING method, STRING url, STRING transaction, RECORD data)
{
  LogRPCTraffic(logsource, method || " " || url, outgoing, sourcetracker, transaction, data);
}

/** @short Register RPC logging for a webbrowser connection
    @param logsource Source or call of the rpc action (eg: webshop:payment.buckaroo, or statsserver:service.regsiter)
    @param sourcetracker Original requestor (IP address or trackingstamp if possible)
    @param webbrowser Browser which may need to log */
PUBLIC MACRO LogRPCForWebbrowser(STRING logsource, STRING sourcetracker, OBJECT webbrowser)
{
  IF (logsource = "")
    THROW NEW Exception("Logsource is required");
  IF (logsource NOT LIKE "*:*")
    THROW NEW Exception("Logsource must have the format <modulename>:<rpcname>");
  IF(NOT IsRPCTrafficLogged(logsource))
    RETURN;

  //ADDME nicer register api, check whether logsource has been disabled, etc..
  INSERT PTR WBTrafficHook(logsource, sourcetracker, #1,#2,#3,#4,#5) INTO webbrowser->pvt_traffichooks AT END;
}

/** @short Register RPC logging for a SOAP client connection
    @param logsource Source or call of the rpc action (eg: webshop:payment.buckaroo, or statsserver:service.regsiter)
    @param sourcetracker Orginial requestor (IP address or trackingstamp if possible)
    @param webbrowser Browser which may need to log */
PUBLIC MACRO LogRPCForSoapClient(STRING logsource, STRING sourcetracker, OBJECT soapclient)
{
  LogRPCForWebbrowser(logsource, sourcetracker, soapclient->__GetBrowser());
}

/** @short Write a marker with optional text to all primary logfiles
    @param text Text to write with the mark */
PUBLIC MACRO WriteLogMarker(STRING text)
{
  STRING mark := `--- MARK${text != "" ? `: ${text}` : ''} ---`;
  LogDebug("system:debug", mark);
  LogInfo("system:debug", mark, DEFAULT RECORD);
  ModuleLog("system:rpc", EncodeJava("system:debug") || "\t" || GetCurrentGroupId() || "\t" || EncodeJava(mark));
  ModuleLog("system:servicemanager", EncodeJava("system:debug") || "\t" || GetCurrentGroupId() || "\t" || EncodeJava(mark));
}

PUBLIC RECORD ARRAY FUNCTION ReadJSONLogLines(STRING logname, DATETIME start, VARIANT until DEFAULTSTO DEFAULT RECORD)
{
  FlushModuleLog(logname); //flush entries in *our* vm..
  VARIANT ARRAY args := VARIANT[ logname, start ];
  RETURN WaitForPromise(ImportJS("@mod-system/js/internal/logging.ts")->readJSONLogLines(logname, start, until));
}
