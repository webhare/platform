﻿<?wh

LOADLIB "wh::ipc.whlib";

LOADLIB "mod::tollium/lib/dialogs.whlib";
LOADLIB "mod::tollium/lib/screenbase.whlib";

LOADLIB "mod::system/lib/screens/userrights/commondialogs-api.whlib";
LOADLIB "mod::system/lib/internal/userrights/exports.whlib";


PUBLIC OBJECTTYPE ObjectTree EXTEND TolliumFragmentBase
< // ---------------------------------------------------------------------------
  //
  // Variables
  //

  INTEGER cbid;

  // ---------------------------------------------------------------------------
  //
  // Public variables
  //

  PUBLIC FUNCTION PTR onselect;

  PUBLIC PROPERTY value(this->objectlist->value, this->objectlist->value);

  /// Boolean that indicates that multiple rightsgroups/objecttypes have been selected
  PUBLIC PROPERTY typesmixed(GetTypesMixed, -);

  /// List of rights that are relevant for the currenty selected rightsgroup/object(type)
  PUBLIC PROPERTY relevantrights(GetRelevantRights, -);

  /// Selectmode, one of 'single', 'multiple'
  PUBLIC PROPERTY selectmode(GetSelectMode, SetSelectMode);

  /// Borders
  PUBLIC PROPERTY borders(this->objectlist->borders, this->objectlist->borders);

  // ---------------------------------------------------------------------------
  //
  // Constructor & tollium registration
  //

  PUBLIC UPDATE MACRO StaticInit(RECORD def)
  {
    TolliumFragmentBase::StaticInit(def);
  }

  UPDATE MACRO PreInitComponent()
  {
    this->cbid := RegisterMultiEventCallback("system:unit.change.*", PTR this->OnUpdateEvent);

    this->objectlist->filteritems := PTR this->FilterObjectTreeItems;
    this->objectlist->EnsurePreInit();
    this->objectlist->ReloadList();
  }

  PUBLIC UPDATE MACRO OnUnloadComponent()
  {
    IF (this->cbid != 0)
      UnregisterCallback(this->cbid);
    this->cbid := 0;
    TolliumFragmentBase::OnUnloadComponent();
  }

  // ---------------------------------------------------------------------------
  //
  // Getters & setters
  //

  BOOLEAN FUNCTION GetTypesMixed()
  {
    RETURN this->objectlist->typesmixed;
  }

  RECORD ARRAY FUNCTION GetRelevantRights()
  {
    RETURN this->objectlist->relevantrights;
  }

  STRING FUNCTION GetSelectMode()
  {
    RETURN this->objectlist->selectmode;
  }

  MACRO SetSelectMode(STRING selectmode)
  {
    this->objectlist->selectmode := selectmode;
  }

  // ---------------------------------------------------------------------------
  //
  // Callbacks
  //

  MACRO OnUpdateEvent(STRING event, RECORD ARRAY data)
  {
    this->objectlist->ReloadList();
  }

  MACRO OnSelectHandler()
  {
    IF (this->onselect != DEFAULT FUNCTION PTR)
      this->onselect();
  }

  // ---------------------------------------------------------------------------
  //
  // Helper functions
  //

  RECORD ARRAY FUNCTION OnExtendObjectTreeChildren(RECORD ARRAY children, RECORD parent)
  {
    IF (NOT RecordExists(parent) OR parent.objecttypename = "")
    {
      // These are global rights
      children :=
          SELECT relevant_rights := (SELECT AS STRING ARRAY name FROM this->objectlist->GetRelevantRightsForRow(sel))
               , *
            FROM children AS sel;

      children :=
          SELECT root_grantable_objects := this->owner->tolliumuser->GetRootObjectsForGrantableRights(relevant_rights)
               , *
            FROM children AS sel;

      children :=
          SELECT cangrantany := 0 IN root_grantable_objects
               , *
            FROM children AS sel;
    }
    ELSE
    {
      children :=
          SELECT relevant_rights := parent.relevant_rights
               , root_grantable_objects := parent.root_grantable_objects
               , cangrantany := parent.cangrantany OR objectid IN parent.root_grantable_objects
               , *
            FROM children;
    }

    RETURN children;
  }

  RECORD ARRAY FUNCTION FilterObjectTreeItems(STRING objecttypename, RECORD ARRAY initems)
  {
    SWITCH (objecttypename)
    {
      CASE "system:units"
      {
        RETURN this->FilterItemsForWRDSchema(initems);
      }
    }
    RETURN initems;
  }

  RECORD ARRAY FUNCTION FilterItemsForWRDSchema(RECORD ARRAY initems)
  {
    /// FIXME: This should be done in the UnitObjecttypeDescriber, but that will have to wait until we've redone the rightsAPI
    INTEGER ARRAY existing := SELECT AS INTEGER ARRAY obj->authobjectid
                                FROM ToRecordArray(this->contexts->userapi->GetObjectsByAuthobjectID(SELECT AS INTEGER ARRAY id FROM initems), "obj");
    RETURN
        SELECT * FROM initems
                WHERE id IN existing;
  }

  // ---------------------------------------------------------------------------
  //
  // Actions
  //

  MACRO DoGrantRightOnObject()
  {
    RECORD val := this->objectlist->value;

    IF (val.rightsgroup != "")
      MakeAddGrantOnRightsGroupDialog(this->owner, val.rightsgroup)->RunModal();
    ELSE
    {
      // Exectly one object is selected (min=1,max=1 on enableon)
      INTEGER objectid := this->objectlist->selectmode = "single"
          ? val.objectid
          : val.objectids[0];

      MakeAddGrantOnObjectDialog(this->owner, val.objecttypename, objectid)->RunModal();
    }
  }

  MACRO DoExportObjectRights()
  {
    RECORD val := ^objectlist->value;
    RECORD exportfile := GetObjectTreeRightsExport(this->contexts, val.objecttypename, val.objectid);
    RunColumnFileExportDialog(this, exportfile);
  }

  MACRO DoReload()
  {
    this->objectlist->ReloadList();
  }
>;
