<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::ipc.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/internal/backend/apps.whlib";

LOADLIB "mod::tollium/lib/dialogs.whlib";
LOADLIB "mod::tollium/lib/gettid.whlib";
LOADLIB "mod::tollium/lib/screenbase.whlib";
LOADLIB "mod::tollium/lib/towl.whlib";
LOADLIB "mod::tollium/lib/internal/towl.whlib";

LOADLIB "mod::system/lib/screens/userrights/support.whlib";

LOADLIB "mod::wrd/lib/auth/passwordpolicy.whlib";
LOADLIB "mod::wrd/lib/internal/auth/support.whlib";


PUBLIC STATIC OBJECTTYPE UserPreferences EXTEND TolliumScreenBase
< // -----------------------------------------------------------------------------
  //
  // Global variables and such
  //

  RECORD policy;

  FUNCTION PTR notificationsubmit;

  // -----------------------------------------------------------------------------
  //
  // Tollium init
  //

  MACRO NEW()
  {
    this->contexts->wrdschema := this->contexts->userapi->wrdschema;
  }

  MACRO Init(RECORD data)
  {
    this->policy := GetPolicyForUser(this->contexts, this->contexts->user->entityid);

    //app passwords currently only make sense for webdav, so hide the option if you're not allowed to do that
    ^openapppasswords->enabled := this->contexts->user->HasRight("system:webdav");
    ^appaccounts->visible := ^openapppasswords->enabled;

    this->ConfigurePrefsTab();
    this->LoadFavorites();

    ^username->value := this->contexts->user->login;
    ^username->readonly := TRUE;
    this->frame->title := GetTid("system:userrights.edituser.changeownsettings");

    ConfigurePasswordFieldForPolicy(^authenticationsettings, this->policy);
    ^authenticationsettings->LoadEntityField(this->contexts->user->entity, this->contexts->userapi->wrdschema->accountpasswordtag);
    ^authenticationsettings->allowsecondfactor := TRUE;

    ^entitylistener->masks := this->contexts->userapi->wrdschema->accounttype->GetEventMasks();

    IF(this->contexts->userapi->wrdschema->accountemailtag = ""
       OR this->contexts->userapi->wrdschema->accountlogintag = this->contexts->userapi->wrdschema->accountemailtag)
    {
      ^username->composition := DEFAULT OBJECT;
      ^username->visible := FALSE;
    }
    ELSE
    {
      ^username->cellname := this->contexts->userapi->wrdschema->accountemailtag;
    }

    ^userobj->LoadEntity(this->contexts->userapi->wrdschema->accounttype, this->contexts->user->entityid);

    RECORD unitinfo;

    RECORD locale := this->contexts->user->GetLocale();

    ^language->SetValueIfValid(locale.language);
    ^timezone->SetValueIfValid(locale.timezone);
    ^decimalsep->SetValueIfValid(locale.decimalseparator);
    ^thousandsep->SetValueIfValid(locale.thousandseparator);
    ^browsertitleprefix->value := this->contexts->user->GetRegistryKey("system.prefs.browsertitleprefix", "");

    ^showdeveloperapps->visible := this->contexts->user->HasRight("system:sysop");
    ^showdeveloperapps->value := this->contexts->user->GetRegistryKey("system.prefs.showdeveloperapps", GetDtapStage() = "development");

    // These settings (decimal sep, thousand sep and background) can only be changed by the sysop and the user himself
    OBJECT dlg := this->LoadScreen(".notificationsettings", [ parentframe := this->frame ]);
    ^notificationstab->contents := dlg;
    this->notificationsubmit := PTR dlg->Submit;
    this->frame->onmessage := PTR this->OnMessage;

    this->RefreshWarnings();
  }

  // -----------------------------------------------------------------------------
  //
  // Callbacks
  //

  MACRO GotInterval()
  {
    IF (ObjectExists(^notificationstab->contents))
      ^notificationstab->contents->GotInterval();
  }

  MACRO OnMessage(RECORD message)
  {
    IF (CellExists(message, "opentab"))
    {
      OBJECT tab := GetMember(this, `^${message.opentab}`);
      IF (ObjectExists(tab))
        ^usertabs->selectedtab := tab;
    }
  }

  MACRO GotEntityEvents(RECORD ARRAY data)
  {
    this->RefreshWarnings();
  }

  // -----------------------------------------------------------------------------
  //
  // Actions
  //

  MACRO RefreshWarnings()
  {
    BOOLEAN passwordexpired, passwordabouttoexpire, passwordrequire2fa;
    TRY
    {
      RECORD usereditpolicy := this->contexts->controller->wrdauthplugin->GetUserEditPolicyForUser(^userobj->wrdentity->id);
      IF (usereditpolicy.passwordvalidationchecks != "")
      {
        RECORD authsettings := ^userobj->wrdentity->GetField(this->contexts->controller->wrdauthplugin->wrdschema->accountpasswordtag);

        IF (IsValueSet(authsettings)
            AND IsValueSet(authsettings.passwords)
            AND authsettings.passwords[END-1].passwordhash NOT IN [ "", "*" ])
        {
          RECORD ARRAY parsedchecks := ParsePasswordChecks(usereditpolicy.passwordvalidationchecks);

          // ADDME: warn when password is becoming too old
          STRING expire_duration := SELECT AS STRING duration FROM parsedchecks WHERE check = "maxage";
          IF (expire_duration != "")
          {
            RECORD expiryduration := ParseDuration(expire_duration);

            DATETIME expiredate := AddUnpackedDurationToDate(expiryduration, authsettings.passwords[END-1].validfrom);
            IF (expiredate < GetCurrentDateTime())
              passwordexpired := TRUE;
            ELSE
            {
              RECORD warnduration := expiryduration;
              warnduration.years := 0;
              warnduration.months := warnduration.months + 12 * warnduration.years;

              IF (warnduration.months > 1)
                warnduration.months := warnduration.months - 1; // warn one month before expiry
              ELSE IF (warnduration.months = 1 OR warnduration.days > 7)
                warnduration.days := warnduration.days - 7;
              ELSE
                warnduration.days := 0;


              DATETIME warndate := AddUnpackedDurationToDate(warnduration, authsettings.passwords[END-1].validfrom);
              IF (warndate < GetCurrentDateTime())
              {
                DATETIME expirydate := AddUnpackedDurationToDate(expiryduration, authsettings.passwords[END-1].validfrom);
                RECORD diff := GetDateTimeDifference(GetCurrentDateTime(), expirydate);

                passwordabouttoexpire := TRUE;
                ^warning_passwordabouttoexpire->value := this->GetTid(".passwordabouttoexpire", ToString(diff.days));
              }
            }
          }

          IF (RecordExists(SELECT FROM parsedchecks WHERE check = "require2fa") AND NOT RecordExists(authsettings.totp))
            passwordrequire2fa := TRUE;
        }

      }
    }
    CATCH (OBJECT e)
      LogHarescriptException(e);

    ^warningpanel->visible := passwordexpired OR passwordabouttoexpire OR passwordrequire2fa;
    ^warning_passwordexpired->visible := passwordexpired;
    ^warning_passwordabouttoexpire->visible := passwordabouttoexpire;
    ^warning_passwordrequire2fa->visible := passwordrequire2fa;
  }

  // -----------------------------------------------------------------------------
  //
  // Actions
  //

  MACRO DoOpenAppPassWords()
  {
    this->RunScreen("#appaccounts");
  }

  BOOLEAN FUNCTION Submit()
  {
    OBJECT work := this->BeginUnvalidatedWork();
    ^userinfopage->ValidateValue(work);

    IF (^decimalsep->value = ^thousandsep->value)
      work->AddError(GetTid("system:sysmgmt.decimalsepandthousandsepmustdiffer"));

    ^userobj->StoreEntity(work);

    this->contexts->user->SetRegistryKey("system.prefs.browsertitleprefix", ^browsertitleprefix->value);

    STRING oldlanguage := this->contexts->user->GetLocale().language;
    this->contexts->user->SetLocale([ language := ^language->value
                                    , timezone := ^timezone->value
                                    , decimalseparator := ^decimalsep->value
                                    , thousandseparator := ^thousandsep->value
                                    ]);
    this->contexts->user->SetRegistryKey("system.prefs.browsertitleprefix", ^browsertitleprefix->value);

    IF (^notificationstab->visible)
      this->notificationsubmit();

    IF(^language->value != oldlanguage)
      work->AddWarning(GetTidForLanguage(^language->value, "system:userrights.dialogues.languagechanged"));

    IF(^showdeveloperapps->visible)
      this->contexts->user->SetRegistryKey("system.prefs.showdeveloperapps", ^showdeveloperapps->value);

    //get the original rows, but overwrite the title inside them
    RECORD ARRAY shortcuts := SELECT AS RECORD ARRAY CELL[...row, title] FROM ^shortcuts->value;
    this->contexts->user->SetRegistryKey("tollium.dashboard.shortcuts", shortcuts);

    IF(NOT work->Finish())
      RETURN FALSE;

    BroadcastEvent("system:unit.change." || this->contexts->user->GetParentUnitId(), DEFAULT RECORD);
    BroadcastEvent("tollium:userprefs." || this->contexts->user->entityid, DEFAULT RECORD);
    this->contexts->user->BroadcastToClient([ type := "tollium:shell.refreshmenu" ]);

    RETURN TRUE;
  }

  // -----------------------------------------------------------------------------
  //
  // Helper functions
  //

  MACRO LoadFavorites()
  {
    RECORD ARRAY favorites := this->contexts->user->GetRegistryKey("tollium.dashboard.shortcuts", RECORD[]);
    RECORD ARRAY allapps := GetAllApplications(this->contexts->user);

    ^shortcuts->value := SELECT app := (SELECT AS STRING title FROM allapps WHERE name = favorites.app) ?? favorites.app
                                   , title
                                   , row := favorites //just save the original nrow
                                FROM favorites
                               WHERE app != "";
  }


  // -----------------------------------------------------------------------------
  //
  // Public API
  //

  PUBLIC MACRO ConfigurePrefsTab()
  {
    RECORD prefs := CollectUserPreferenceOptions(this->tolliumcontroller);

    ^language->options := prefs.languages;
    //FIXME this->timeformat->options := prefs.timeformats;
    //FIXME this->dateformat->options := prefs.dateformats;
    ^decimalsep->options := prefs.decimalseps;
    ^thousandsep->options := prefs.thousandseps;
  }

  MACRO DoResetWarnings()
  {
    IF (this->RunSimpleScreen("confirm", this->GetTid(".confirmresetwarnings")) != "yes")
      RETURN;

    OBJECT work := this->BeginWork();
    this->contexts->user->DeleteRegistryNode("tollium.dialogresult");
    work->AddWarning(this->GetTid(".settingswerereset"));
    work->Finish();
  }

  MACRO DoResetSavedStates()
  {
    IF (this->RunSimpleScreen("confirm", this->GetTid(".confirmresetsavedstates")) != "yes")
      RETURN;

    OBJECT work := this->BeginWork();
    this->contexts->user->DeleteRegistryNode("tollium.savestate");
    work->AddWarning(this->GetTid(".settingswerereset"));
    work->Finish();
  }
>;

PUBLIC OBJECTTYPE NotificationSettings EXTEND TolliumScreenBase
<
  BOOLEAN embedded;
  OBJECT towlservice;

  /** Permission state
      - unknown: initial state
      - '': browser does not support notifications
      - default: not yet permitted/denied
      - granted: permission granted
      - denied: permission denied
  */
  STRING lastpermissionstate;
  BOOLEAN querypending;

  MACRO Init(RECORD data)
  {
    this->embedded := CellExists(data, "parentframe") AND ObjectExists(data.parentframe);
    this->towlservice := NEW TowlServiceObject;
    this->lastpermissionstate := "unknown";

    this->listener->masks := [ "tollium:towl.notificationsettings." || this->contexts->user->entityid ];

    this->timeout->value := this->towlservice->GetUserTimeout(this->contexts->user) / 1000;
    INTEGER userpriority := this->towlservice->GetNotificationPriority(this->contexts->user, "");
    this->onlypriority->value := userpriority != -4;
    this->showpriority->value := userpriority != -4 ? userpriority : 0;
    this->showon->SetValueIfValid(this->contexts->user->GetRegistryKey("system.prefs.shownotifications", "desktop"));

    this->buttons->visible := NOT this->embedded;

    this->RefreshNotificationsList();
    //ADDME: Can we still somehow register for receiving towl broadcasts?

    this->OnShowOnChange();
    this->QueryPermissionState();

    this->listener->EnsurePostInit();
  }

  MACRO GotNewNotificationEvents(RECORD ARRAY events)
  {
    this->RefreshNotificationsList();
  }

  /// ADDME: Embedded screens should have interval too
  PUBLIC MACRO GotInterval()
  {
    this->QueryPermissionState();
  }

  MACRO QueryPermissionState()
  {
    IF (this->lastpermissionstate = "" OR this->querypending) // not supported?
      RETURN;

    this->querypending := TRUE;
    this->tolliumcontroller->ExecuteClientCall("GetNotificationPermissionState")->Then(
          PTR this->ProcessPermissionState,
          PTR this->ShowException);
  }

  MACRO ProcessPermissionState(STRING state)
  {
    this->querypending := FALSE;
    this->lastpermissionstate := state;
    this->OnShowOnChange();
  }

  MACRO ShowException(OBJECT e)
  {
    this->querypending := FALSE;
    this->lastpermissionstate := ""; // Treat as not supported
    RunExceptionReportDialog(this, e);
  }

  MACRO OnShowOnChange()
  {
    this->permissiontabs->visible := this->showon->value = "desktop";
    SWITCH (this->lastpermissionstate)
    {
      CASE "unknown", "default"     { this->permissiontabs->selectedtab := this->requestpermissiontab; }
      CASE ""                       { this->permissiontabs->selectedtab := this->notsupportedtab; }
      CASE "granted"                { this->permissiontabs->selectedtab := this->permissiongrantedtab; }
      CASE "denied"                 { this->permissiontabs->selectedtab := this->permissiondeniedtab; }
    }
  }

  PUBLIC BOOLEAN FUNCTION Submit()
  {
    OBJECT work;
    IF (NOT this->embedded)
      work := this->BeginWork();

    this->contexts->user->SetRegistryKey("system.prefs.shownotifications", this->showon->value);
    this->towlservice->SetUserTimeout(this->contexts->user, this->timeout->value * 1000);
    this->towlservice->SetNotificationPriority(this->contexts->user, "", this->onlypriority->value ? this->showpriority->value : -4);

    IF (ObjectExists(work))
      RETURN work->Finish();
    RETURN TRUE;
  }

  STRING FUNCTION GetNotificationStatusIcon(INTEGER priority)
  {
    SWITCH (priority)
    {
      CASE -3              { RETURN "tollium:status/checked"; }
      CASE -2, -1, 0, 1, 2 { RETURN "tollium:status/unknown"; }
      CASE 3               { RETURN "tollium:status/negative"; }
    }
    RETURN "";
  }

  STRING FUNCTION GetNotificationStatusText(INTEGER priority)
  {
    SWITCH (priority)
    {
      CASE -3 { RETURN GetTid("system:userrights.editnotification.shownotification-always"); }
      CASE -2 { RETURN GetTid("system:userrights.editnotification.verylow"); }
      CASE -1 { RETURN GetTid("system:userrights.editnotification.low"); }
      CASE 0  { RETURN GetTid("system:userrights.editnotification.normal"); }
      CASE 1  { RETURN GetTid("system:userrights.editnotification.high"); }
      CASE 2  { RETURN GetTid("system:userrights.editnotification.veryhigh"); }
      CASE 3  { RETURN GetTid("system:userrights.editnotification.shownotification-never"); }
    }
    RETURN "";
  }

  MACRO RefreshNotificationsList()
  {
    RECORD ARRAY notifications := SELECT towl_notifications.*
                                       , modulename := modules.name
                                    FROM system.towl_notifications
                                       , system.modules
                                   WHERE towl_notifications.module = modules.id;
    notifications := SELECT *
                          , userpriority := this->towlservice->GetNotificationPriority(this->contexts->user, modulename || ":" || name)
                       FROM notifications;
    notifications := SELECT *
                          , statustext := this->GetNotificationStatusText(userpriority)
                          , statusicon := this->GetNotificationStatusIcon(userpriority)
                       FROM notifications
                      WHERE userpriority > -4;

    this->notifications->rows := SELECT rowkey := id
                                      , name := modulename || ":" || name
                                      , title := GetTid(modulename || ":" || name)
                                      , module := modulename
                                      , status := statustext
                                      , statusicon := this->notifications->GetIcon(statusicon)
                                      , userpriority
                                   FROM notifications;
  }

  MACRO OnNotificationBroadcast(RECORD data)
  {
    // The data record contains a cell 'data', which contains the JSON-encoded message, which should have a 'type' cell with
    // the value "tollium:towl.event".
    data := DecodeJSON(data.data);
    IF (CellExists(data, "message") AND CellExists(data.message, "type") AND data.message.type = "tollium:towl.event")
      this->RefreshNotificationsList();
  }

  MACRO DoRequestPermission()
  {
    this->frame->QueueOutboundMessage("requestpermission", [ type := "notifications" ]);
  }

  MACRO DoTestNotification()
  {
    ShowTowlNotification( "tollium:notifications.testnotification"
                        , [ webhare_users := [ GetEffectiveUserId() ] ]
                        , [ timeout := this->timeout->value * 1000
                          , priority := priority_internal
                          ]);
  }

  MACRO DoEditNotification()
  {
    RECORD notification := this->notifications->selection;
    OBJECT dlg := this->LoadScreen(".editnotification", notification);
    IF (dlg->RunModal() = "ok")
    {
      OBJECT work := this->BeginWork();
      this->towlservice->SetNotificationPriority(this->contexts->user, notification.name, dlg->priority);
      work->Finish();
    }
  }

  MACRO DoDeleteNotification()
  {
    RECORD notification := this->notifications->selection;
    IF (this->RunSimpleScreen("confirm", this->GetTid(".confirmdeletenotification", notification.title)) = "yes")
    {
      OBJECT work := this->BeginWork();
      this->towlservice->SetNotificationPriority(this->contexts->user, notification.name, -4);
      work->Finish();
    }
  }
>;


PUBLIC OBJECTTYPE EditNotification EXTEND TolliumScreenBase
<
  PUBLIC PROPERTY priority(GetPriority, -);

  MACRO Init(RECORD data)
  {
    this->frame->title := GetTid("system:userrights.editnotification.title", data.title);

    IF (data.userpriority = -3)
      this->shownotification->value := "always";
    ELSE IF (data.userpriority = 3)
      this->shownotification->value := "never";
  }

  BOOLEAN FUNCTION Submit()
  {
    RETURN this->BeginWork()->Finish();
  }

  INTEGER FUNCTION GetPriority()
  {
    SWITCH (this->shownotification->value)
    {
      CASE "never"
      {
        RETURN 3;
      }
      CASE "always"
      {
        RETURN -3;
      }
    }
    RETURN -4;
  }
>;

//*/

PUBLIC OBJECTTYPE AppAccounts EXTEND TolliumScreenBase
< OBJECT user;

  MACRO Init(RECORD data)
  {
    this->appaccounts->wrdtype := this->contexts->userapi->wrdschema->GetType("WHUSER_APPACCOUNT");
    this->appaccounts->entitycontext := [ wrd_leftentity := this->contexts->user->entityid ];
  }

  BOOLEAN FUNCTION Submit()
  {
    RETURN TRUE;
  }
>;

PUBLIC OBJECTTYPE AppAccount EXTEND TolliumScreenBase
<
  RECORD entitycontext;
  INTEGER newid;

  MACRO Init(RECORD data)
  {
    this->entitycontext := data.entitycontext;
  }
  BOOLEAN FUNCTION OnDevicePageNext()
  {
    OBJECT work := this->BeginUnvalidatedWork();
    work->Validate(this->devicepage);
    IF(work->HasFailed())
      RETURN work->Finish();

    RECORD res := this->contexts->user->CreateAppPassword(this->scope->value, this->device->value);
    this->login->value := this->contexts->user->login;
    this->password->value := res.password;
    this->newid := res.id;

    RETURN work->Finish();
  }

  MACRO Onpasswordpageinit(RECORD data)
  {
    this->wizard->hidecancel := TRUE;
    this->wizard->ClearPageHistory();
  }

  PUBLIC INTEGER FUNCTION GetNewEntityId()
  {
    RETURN this->newid;
  }
>;

PUBLIC MACRO RunUserPreferences(OBJECT parent, RECORD options)
{
  IF(NOT ObjectExists(parent->contexts->userapi))
  {
    STRING error := GetTid("system:userrights.edituser.unavailable");
    IF(GetDtapStage() = "development")
      error := error || "\n\nFor developers: there was no 'userapi' in the current context.\n\nThere may be an issue with the currently cached siteprofiles and you may need to perfom a `wh softreset --sp` or to restart the webserver (or WebHare entirely)";
    parent->RunSimpleScreen("error", error);
    RETURN;
  }
  IF(NOT ObjectExists(parent->contexts->user->entity))
  {
    // This means we're logged in as an override user; let's tell the user and exit:
    parent->RunSimpleScreen("info", GetTid("system:userrights.edituser.noprefsforoverride"));
    RETURN;
  }
  parent->RunScreen("mod::system/screens/userrights/dialogs/userpreferences.xml#userpreferences");
}
