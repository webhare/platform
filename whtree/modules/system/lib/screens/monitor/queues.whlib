﻿<?wh

LOADLIB "wh::datetime.whlib";

LOADLIB "mod::consilio/lib/database.whlib";
LOADLIB "mod::consilio/lib/internal/fetcher_queue.whlib";
LOADLIB "mod::consilio/lib/internal/contentsources.whlib";
LOADLIB "mod::consilio/lib/internal/queuemgmt.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/services.whlib";
LOADLIB "mod::system/lib/screenbase.whlib";
LOADLIB "mod::system/lib/whfs.whlib";

LOADLIB "mod::tollium/lib/gettid.whlib";


DATETIME FUNCTION DecodeDateTimeString(STRING dt)
{
  STRING ARRAY parts := Tokenize(dt,"\t");
  IF (Length(parts) != 2)
    RETURN DEFAULT DATETIME;
  RETURN MakeDateFromParts(ToInteger(parts[0],0),ToInteger(parts[1],0));
}

STRING FUNCTION DescribeFile(INTEGER objid)
{
  STRING path := SELECT AS STRING whfspath FROM system.fs_objects WHERE id=objid;
  IF(path="")
    path := "#" || objid;
  RETURN path;
}

STRING FUNCTION DescribeConsilioTask(RECORD worker, RECORD ARRAY contentproviders)
{
  STRING workerstatus;
  IF (RecordExists(worker.curitem))
  {
    STRING command := GetTid("system:monitor.queues.workerstatus.unknown");
    STRING ARRAY additional;// := [ STRING(this->tolliumuser->FormatTimespan(0, worker.curitem.workingtime)) ];
    IF (worker.curitem.numpages > 0)
      INSERT GetTid("system:monitor.queues.workerstatus.workerpages", ToString(worker.curitem.numpages)) INTO additional AT 0;

    RECORD data := GetQueueDataFromCommand(worker.curitem.command);
    IF (RecordExists(data))
    {
      STRING indextag := CellExists(data, "indexid") ? SELECT AS STRING name FROM consilio.catalogs WHERE id = data.indexid : "";
      STRING contentsource;
      IF(CellExists(data, "contentsourceid"))
        contentsource := SELECT AS STRING tag FROM consilio.contentsources WHERE id=VAR data.contentsourceid;

      STRING groupname := CellExists(data, "groupid") ? data.groupid : "";
      INTEGER groupid_int := ToInteger(groupname, 0);
      IF(groupid_int != 0)
      {
        OBJECT obj := OpenWHFSObject(groupid_int);
        IF(ObjectExists(obj))
          groupname := obj->GetResourceName();
      }

      STRING objectname := CellExists(data, "objectid") ? data.objectid : "";

      SWITCH (data.action)
      {
        CASE "CHECKINDEXSTATE"
        {
          command := GetTid("system:monitor.queues.workerstatus.checkindexstate");
        }
        CASE "CHECKINDEX"
        {
          IF(contentsource="")
            command := GetTid("system:monitor.queues.workerstatus.checkindex", indextag);
          ELSE
            command := GetTid("system:monitor.queues.workerstatus.checkindexsource", indextag, contentsource);
        }
        CASE "UPDATEINDEX"
        {
          IF(contentsource="")
            command := GetTid("system:monitor.queues.workerstatus.updateindex", indextag);
          ELSE
            command := GetTid("system:monitor.queues.workerstatus.updateindexsource", indextag, contentsource);
        }
        CASE "FASTCHECKGROUP"
        {
          command := GetTid("system:monitor.queues.workerstatus.fastcheckgroup", groupname, indextag);
        }
        CASE "CHECKGROUP"
        {
          command := GetTid("system:monitor.queues.workerstatus.checkgroup", groupname, indextag);
        }
        CASE "UPDATEGROUP"
        {
          command := GetTid("system:monitor.queues.workerstatus.updategroup", groupname, indextag);
        }
        CASE "DEACTIVATEGROUP"
        {
          command := GetTid("system:monitor.queues.workerstatus.deactivategroup", groupname, indextag);
        }
        CASE "DELETEGROUP"
        {
          command := GetTid("system:monitor.queues.workerstatus.deletegroup", groupname, indextag);
        }
        CASE "CHECKOBJECT"
        {
          command := GetTid("system:monitor.queues.workerstatus.checkobject", objectname, indextag);
        }
        CASE "DELETEOBJECT"
        {
          command := GetTid("system:monitor.queues.workerstatus.deleteobject", objectname, indextag);
        }
        CASE "CLEANUPINDEX"
        {
          command := GetTid("system:monitor.queues.workerstatus.cleanupindex", indextag);
        }
      }
    }
    workerstatus := GetTid("system:monitor.queues.workerstatus.workertask", command, Detokenize(additional, ", "));
  }
  ELSE IF (NOT worker.available)
    workerstatus := GetTid("system:monitor.queues.workerstatus.unavailable");
  ELSE
    workerstatus := GetTid("system:monitor.queues.workerstatus.idle");
  RETURN workerstatus;
}

PUBLIC OBJECTTYPE Queues EXTEND DashboardPanelBase
<
  UPDATE PUBLIC MACRO RefreshDashboardPanel()
  {
    this->workers->rows := this->GetConsilioWorkers() CONCAT this->GetPublisherWorkers();
  }

  RECORD ARRAY FUNCTION GetConsilioWorkers()
  {
    RECORD ARRAY contentproviders := SELECT *
                                         , title := GetTid(title)
                                      FROM GetContentProviders();

    RECORD status := __ConsilioGetQueueManagerStatus();
    IF(NOT RecordExists(status))
      RETURN DEFAULT RECORD ARRAY;


    //Print("QueueManager status:\n" || AnyToString(status, "tree"));
    this->consilioqueuelength->value := ToString(status.queuelength);

    RECORD contentsourceinfo :=
        SELECT total := Sum(contentsources.status >= 0 ? 1 : 0)
             , idle := Sum(contentsources.status = 0 ? 1 : 0)
             , disabled := Sum(contentsources.status < 0 ? 1 : 0)
          FROM consilio.contentsources
         WHERE tag NOT LIKE "$consilio$deleted$*";
    IF (RecordExists(contentsourceinfo))
      this->contentsources->value := GetTid("system:monitor.queues.contentsources-desc", ToString(contentsourceinfo.total), ToString(contentsourceinfo.idle), ToString(contentsourceinfo.disabled));

    RECORD ARRAY workerlist := SELECT rowkey := "consilio" || #workers
                                    , name := "consilio " || #workers + 1
                                    , currenttask := DescribeConsilioTask(workers, contentproviders)
                                    , currenttaskhint := RecordExists(curitem) ? curitem.command : ""
                                    , time := RecordExists(curitem) ? this->tolliumuser->FormatTimespan(0,curitem.workingtime) : ""
                                    , starttime := RecordExists(curitem) ? this->tolliumuser->FormatTime(curitem.starttime, "seconds", TRUE) : ""
                                    , deadline := RecordExists(curitem) ? this->tolliumuser->FormatTime(curitem.maxtime, "seconds", TRUE) : ""
                                 FROM status.workers;
    RETURN workerlist;
  }

  DATETIME FUNCTION DecodeDateTimeString(STRING dt)
  {
    STRING ARRAY parts := Tokenize(dt,"\t");
    IF (Length(parts) != 2)
      RETURN DEFAULT DATETIME;
    RETURN MakeDateFromParts(ToInteger(parts[0],0),ToInteger(parts[1],0));
  }

  RECORD ARRAY FUNCTION GetPublisherWorkers()
  {
    RECORD ARRAY workerlist;

    RECORD res := DEFAULT RECORD;
    TRY
    {
      OBJECT service := WaitForPromise(OpenWebHareService("publisher:publication"));
      res := WaitForPromise(service->GetState());
      service->CloseService();
    }
    CATCH;

    IF (RecordExists(res))
    {
      this->queuelength->value := ToString(LENGTH(res.running) + res.runnable + res.timedwait);

      DATETIME now := GetCurrentDatetime();
      workerlist :=
          SELECT TEMPORARY diff := GetDatetimeDifference(date_running, now)
               , rowkey := "publisher" || #running
               , name := "publisher " || #running+1
               , currenttask := DescribeFile(taskdata.id)
               , currenttaskhint := "#" || taskdata.id
               , time := this->tolliumuser->FormatTimespan(diff.days, diff.msecs)
               , starttime := this->tolliumuser->FormatTime(date_running, "seconds", TRUE)
               , deadline := ""
            FROM res.running;

      IF (res.expectedtimetocompletion = 0)
        this->estimatedcompletion->value := GetTid("system:monitor.queues.estimate_unavailable");
      ELSE
      {
        // Add a few seconds to completion
        DATETIME eta := AddTimeToDate(res.expectedtimetocompletion + 10 * 1000, GetCurrentDatetime());
        IF(GetDayCount(this->tolliumuser->UTCToLocal(eta)) != GetDayCount(this->tolliumuser->UTCToLocal(now))) //tomorrow? (from user's perspective)
          this->estimatedcompletion->value := this->tolliumuser->FormatDatetime(eta, "seconds", TRUE, TRUE);
        ELSE
          this->estimatedcompletion->value := this->tolliumuser->FormatTime(eta, "seconds", TRUE);
      }

    }
    ELSE
    {
      this->queuelength->value := "";
      this->estimatedcompletion->value := "";
    }

    res := DEFAULT RECORD;
    TRY
    {
      OBJECT service := WaitForPromise(OpenWebHareService("publisher:outputanalyzer"));
      res := WaitForPromise(service->GetState());
      service->CloseService();
    }
    CATCH;

    IF (RecordExists(res))
    {
      this->analyzerqueuelength->value := ToString(LENGTH(res.running) + res.runnable + res.timedwait);
    }
    ELSE
    {
      this->analyzerqueuelength->value := "";
    }

    RETURN workerlist;
  }
>;
