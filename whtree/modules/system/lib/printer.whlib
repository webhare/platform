<?wh
/** @topic witty/api */

LOADLIB "wh::files.whlib";
LOADLIB "wh::internet/urls.whlib";
LOADLIB "wh::graphics/canvas.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/internal/asynctools.whlib";
LOADLIB "mod::system/lib/internal/browsers/chrome/pdfapi.whlib";
LOADLIB "mod::system/lib/internal/services/javaservice.whlib";


/** Get a screenshot of a page
    @param url URL to take a screenshot of
    @cell(boolean) options.debug Enable debug mode
    @cell(string) options.media Media to use - 'screen' (default) or 'print'
    @cell(integer) options.delay How much milliseconds to wait after setting the url before taking a screenshot
    @cell(boolean) options.getlog Return the browser log
    @cell(integer) options.screenheight Override the screen height (default: 1080)
    @cell(integer) options.screenwidth Set the screen width (default: 1920)
    @cell(boolean) options.withalpha Take two screenshots, with different background colors on the body element. Calculates the
        alpha channel from the difference.
    @cell(record array) options.cutouts Cutouts
    @cell(string) options.cutouts.selector CSS Selector
    @cell(integer) options.timeout Timeout (in milliseconds) for PDF generation. Defaults to 30 seconds
    @return Requested screenshot, logs and any requested cutouts
    @cell return.screenshot Wrapped blob with the screenshot
    @cell return.cutouts Informatation about the cutouts
    @cell return.cutouts.selector Selector
    @cell(float) return.cutouts.x When found, the x-position of the element
    @cell(float) return.cutouts.y When found, the y-position of the element
    @cell(float) return.cutouts.width When found, the width of the element
    @cell(float) return.cutouts.height When found, the heigth of the element
    @cell(boolean) return.cutouts.found Wether the element was found
    @cell(record) return.cutouts.image Wrapped blob with image (if the coordinates are (partially) within the screenshot)
    @cell return.log Browser log (if requested via options.getlog)
    @cell(string) return.log.level 'INFO', 'WARNING'
    @cell(string) return.log.message
    @cell(datetime) return.log.timestamp
*/
PUBLIC RECORD FUNCTION GenerateBrowserScreenshot(STRING url, RECORD options DEFAULTSTO CELL[])
{
  options := ValidateOptions([ cutouts := RECORD[]
                             , getlog := FALSE
                             , delay := 0
                             , withalpha := FALSE
                             , media := "screen"
                             , debug := FALSE
                             , screenwidth := 1920
                             , screenheight := 1080
                             , timeout := 30 * 1000
                             ], options);

  options.cutouts := SELECT AS RECORD ARRAY
      ValidateOptions([ selector := "" ]
                      , cutouts, [ title := `options.cutout[${#cutouts}]`
                                 , required := ["selector"]
                                 ]) FROM options.cutouts;

  IF(NOT IsAbsoluteURL(url))
    THROW NEW Exception("Invalid URL to screenshot: " || url);
  BOOLEAN withalpha := options.withalpha;
  BOOLEAN getlog :=  options.getlog;
  IF (withalpha)
    INSERT CELL adjustbackground := TRUE INTO options;

  OBJECT pdfgen := NEW ChromePDFAPI([ debug := options.debug]);
  RECORD response := WaitForPromise(
                       WrapPromiseInTimeout(
                         pdfgen->GenerateScreenshotFromURL(url, CELL[ options.media
                                                                    , options.delay
                                                                    , options.cutouts
                                                                    , options.screenwidth
                                                                    , options.screenheight
                                                                    , printbackground := NOT withalpha
                                                                    ]), options.timeout));

  OBJECT finalimage;
  RECORD retval :=
      [ screenshot :=   DEFAULT RECORD
      , cutouts :=      DEFAULT RECORD ARRAY
      , log :=          DEFAULT RECORD ARRAY
      ];

  finalimage := CreateCanvasFromBlob(response.screenshot.data);
  IF(NOT ObjectExists(finalimage))
    THROW NEW Exception("Unable to open final image");
  retval.screenshot := response.screenshot;

  IF (Length(options.cutouts) > 0)
  {
    RECORD ARRAY cutouts := response.cutouts;

    FOREVERY (RECORD rec FROM cutouts)
    {
      RECORD image;
      IF (rec.found)
      {
        // Don't pass coordinates outside the canvas, CreateCroppedCanvas doesn't like that
        INTEGER x := INTEGER(rec.x);
        INTEGER y := INTEGER(rec.y);
        INTEGER rightlimit := INTEGER(rec.x + rec.width);
        INTEGER bottomlimit := INTEGER(rec.y + rec.height);

        IF (x < 0) x := 0;
        IF (y < 0) y := 0;
        IF (rightlimit >= finalimage->width)
          rightlimit := finalimage->width;
        IF (bottomlimit >= finalimage->height)
          bottomlimit := finalimage->height;

        IF (x < finalimage->width AND rightlimit > x AND y < finalimage->height AND bottomlimit > y)
        {
          OBJECT cutoutcanvas := finalimage->CreateCroppedCanvas(x, y, rightlimit, bottomlimit);
          image := WrapBlob(cutoutcanvas->ExportAsPNG(FALSE), "cutout_" || #rec || ".png");
          cutoutcanvas->Close();
        }
        ELSE
          rec.message := "Element is not visible within the viewport";
      }
      INSERT CELL image := image INTO cutouts[#rec];
    }

    retval.cutouts := cutouts;
  }

  IF (getlog)
    retval.log := response.log;

  finalimage->Close();

  // Done
  RETURN retval;
}

/** @short Generate a PDF from a witty record
    @long Header and footer templates can use `<span class="pageNumber"></span>` and `<span class="totalPages"></span>` to show the page number and total number of pages respectively, but due to a Chrome limitation
          these are only shown if you also set the page margins to not intersect with these elements
    @param data Witty data
    @cell options.pdfengine PDF Engine to use. 'chrome' (default)
    @cell options.printbackground Print backgrounds too? (defaults to true)
    @cell options.filename Output filename, will be genrated from the wittypath if not set
    @cell options.wittypath Path to witty template to use. If not set but the data contains a 'wittypath' variable, that one is used instead
    @cell options.getsource Get a full dump of the data that will be used for the PDF. Used for debugging, may be removed in the future
    @cell(string) options.media Media to use - 'screen' or 'print' (default)
    @cell(boolean) options.debug Enable debug mode
    @cell(integer) options.delay How much milliseconds to wait after setting the url before taking a screenshot
    @cell(record) options.margin Page marginn (eg `[ top := "0.5cm", bottom := "0.5cm", left := "1cm", right := "1cm" ]`). Defaults to no margins
    @cell(boolean) options.displayheaderfooter Display header and footer. Defaults to FALSE
    @cell(string) options.headertemplate Header template HTML code
    @cell(string) options.footertemplate Header template HTML code
    @cell(string) options.invokeifdone Function to invoke when rendering is done
    @cell(integer) options.timeout Timeout (in milliseconds) for PDF generation. Defaults to 30 seconds
    @return Wrapped file containing the PDF (or ZIP, if getsource is used). if 'getlog' is used, the wrapped file wil be in the "PDF" member
*/
PUBLIC RECORD FUNCTION GeneratePDF(RECORD data, RECORD options DEFAULTSTO CELL[])
{
  options := ValidateOptions([ filename := ""
                             , getlog := FALSE
                             , pdfengine := "chrome"
                             , wittypath := ""
                             , debug := FALSE
                             , printbackground := TRUE
                             , media := "screen"
                             , getsource := FALSE
                             , delay := 0
                             , lookupresource := DEFAULT FUNCTION PTR
                             , displayheaderfooter := FALSE
                             , headertemplate := ""
                             , footertemplate := ""
                             , margin := DEFAULT RECORD
                             , timeout := 30 * 1000
                             , devtoolsurl := ""
                             , invokeifdone := ""
                             ], options, [ enums := [ pdfengine := ["chrome"]
                                                    , media := ["print","screen"]
                                                    ]
                                         ]);

  IF(options.wittypath = "" AND NOT CellExists(data,'wittypath'))
    THROW NEW Exception(`Wittypath was not explicitly specified and not passed as part of the data record either`);

  STRING wittypath := options.wittypath ?? data.wittypath;
  STRING extension := options.getsource ? ".zip" : ".pdf";
  IF(options.filename = "")
  {
    options.filename := GetSafeFileName(GetNameFromPath(wittypath)) || extension;
  }
  ELSE IF(options.filename NOT LIKE `*${extension}`)
  {
    options.filename := GetSafeFileName(options.filename) || extension;
  }

  IF(options.pdfengine = "chrome")
  {
    OBJECT pdfgenerator := NEW ChromePDFAPI(CELL[ options.debug
                                                , options.printbackground
                                                , options.devtoolsurl
                                                ]);
    pdfgenerator->LoadRichBody(wittypath,"text/html");
    pdfgenerator->mergerecord := data;
    pdfgenerator->lookupresource := options.lookupresource;

    BLOB filedata;
    RECORD ARRAY log;
    VARIANT ifdoneresult := DEFAULT RECORD;
    IF(options.getsource)
    {
      filedata := pdfgenerator->GenerateSource();
    }
    ELSE
    {
      RECORD result := WaitForPromise(
                    WrapPromiseInTimeout(
                      pdfgenerator->GeneratePDF(CELL[ options.displayheaderfooter
                                                    , options.headertemplate
                                                    , options.footertemplate
                                                    , options.margin
                                                    , options.delay
                                                    , options.invokeifdone
                                                    ]), options.timeout));
      filedata := result.pdf;
      log := result.log;
      ifdoneresult := result.ifdoneresult;
    }

    RECORD pdf := WrapBlob(filedata, options.filename);
    IF(NOT options.getlog)
      RETURN pdf;

    RETURN CELL[ pdf
               , log
               , ifdoneresult
               ];
  }

  THROW NEW Exception("GeneratePDF failed");
}

RECORD FUNCTION __GeneratePDFFrom(STRING link, RECORD wrappedfile, RECORD options)
{
  options := ValidateOptions(CELL[ pdfengine := "chrome"
                                 , ...pdf_defaultsettings
                                 , media := "print"
                                 , filename := "output.pdf"
                                 , delay := 0
                                 , timeout := 30 * 1000
                                 , cookies := RECORD[]
                                 , embeddedfiles := RECORD[]
                                 , getlog := FALSE
                                 , invokeifdone := ""
                                 ], options, [ enums := [ pdfengine := ["chrome"]
                                                        , media := ["print","screen"]
                                                        ]
                                             , optional := pdf_defaultsettings_optionals
                                             ]);


  OBJECT pdfgen := NEW ChromePDFAPI();
  RECORD result := WaitForPromise(WrapPromiseInTimeout(pdfgen->GeneratePDFFrom(link, wrappedfile, CELL
      [ ...options
      , DELETE getlog
      , DELETE filename
      , DELETE pdfengine
      , DELETE timeout
      ]), options.timeout));
  RECORD pdf := WrapBlob(result.pdf, options.filename);
  IF(NOT options.getlog)
    RETURN pdf;

  RETURN CELL[ pdf
             , result.log
             ];
}

/** @short Generate a PDF from a URL
    @param link URL to generate a PDF for
    @cell options.pdfengine PDF Engine to use. Defaults to 'chrome' and is currently the only available one
    @cell options.printbackground Print backgrounds too? (defaults to true)
    @cell(string) options.media Media to use - 'screen' or 'print' (default)
    @cell options.filename Output filename.
    @cell(integer) options.delay How much milliseconds to wait after setting the url before taking a screenshot
    @cell(integer) options.timeout Timeout (in milliseconds) for PDF generation. Defaults to 30 seconds
    @return Wrapped file containing the PDF
*/
PUBLIC RECORD FUNCTION GeneratePDFFromURL(STRING link, RECORD options DEFAULTSTO CELL[])
{
  RETURN __GeneratePDFFrom(link, DEFAULT RECORD, options);
}

/** @short Generate a PDF from a file
    @param wrappedfile Wrapped file record to generate a PDF for
    @param options @includecelldef #GeneratePDFFromURL.options
    @return Wrapped file containing the PDF
*/
PUBLIC RECORD FUNCTION GeneratePDFFromFile(RECORD wrappedfile, RECORD options DEFAULTSTO CELL[])
{
  IF(NOT RecordExists(wrappedfile))
    THROW NEW Exception("No file passed to GeneratePDFFromFile");

  RETURN __GeneratePDFFrom("", wrappedfile, options);
}

// https://pdfbox.apache.org/2.0/commandline.html
PUBLIC RECORD FUNCTION __RunPDFBox(STRING ARRAY instructions)
{
  RETURN RunJavaServiceApp([GetInstallationRoot() || "/libexec/pdfbox-app.jar", ...instructions]);
}

/** Export each page of the PDF to an image
    @param inpdf PDF to read
    @return The images in wrapped blobs, one for each page */
PUBLIC RECORD ARRAY FUNCTION ExportPDFToImages(BLOB inpdf)
{
  STRING pdftemp := GenerateTemporaryPathname();
  StoreDiskFile(pdftemp, inpdf, [ exclusive := TRUE ]);
  STRING outputbase := GenerateTemporaryPathname() || "-";
  RECORD res := __RunPDFBox(["PDFToImage", "-dpi", "96", "-outputPrefix", outputbase, pdftemp]);

  RECORD ARRAY diskfiles := ReadDiskDirectory(GetDirectoryFromPath(outputbase), GetNameFromPath(outputbase) || "*");
  //order by the number between '-' and '.extension' (first take everything behind the last '-', then remove the dot)
  diskfiles := SELECT * FROM diskfiles ORDER BY ToInteger(Tokenize(Tokenize(name,'-')[END-1],'.')[0],0);

  RECORD ARRAY images;
  IF(res.exitcode = 0)
  {
    images   := SELECT data := StringToBlob(BlobToString(GetDiskResource(path))) FROM diskfiles;
    images := SELECT AS RECORD ARRAY WrapBlob(data, "") FROM images;
    images := SELECT *, filename := "page-" || (#images+1) || extension FROM images;
  }

  DeleteDiskFile(pdftemp);
  FOREVERY (RECORD file FROM diskfiles)
    DeleteDiskFile(file.path);

  IF(res.exitcode > 0)
    THROW NEW Exception(`PDF image extraction failed with exitcode ${res.exitcode}`);

  RETURN images;
}

/** @short Overlay one PDF on top of another
    @param inputpdf Original PDF
    @param overlaypdf PDF to overlay (will become the background)
    @return Combined pdf */
PUBLIC BLOB FUNCTION ApplyOverlayToPDF(BLOB inputpdf, BLOB overlaypdf)
{
  STRING inputpdfpath := GenerateTemporaryPathname();
  StoreDiskFile(inputpdfpath, inputpdf, [ exclusive := TRUE ]);

  STRING overlaypdfpath := GenerateTemporaryPathname();
  StoreDiskFile(overlaypdfpath, overlaypdf, [ exclusive := TRUE ]);

  STRING outputpath := GenerateTemporaryPathname();
  RECORD res := __RunPDFBox(["OverlayPDF", inputpdfpath, "-position", "background", overlaypdfpath, outputpath]);
  BLOB result;

  IF(res.exitcode = 0)
    result := StringToBlob(BlobToString(GetDiskResource(outputpath)));

  DeleteDiskFile(inputpdfpath);
  DeleteDiskFile(overlaypdfpath);
  DeleteDiskFile(outputpath);

  IF(res.exitcode > 0)
    THROW NEW Exception(`Applying overlay to PDF failed with exitcode ${res.exitcode}`);

  RETURN result;
}

/** @short Concatenate PDFs together
    @param pdfs PDFs to merge
    @return Combined PDFs */
PUBLIC BLOB FUNCTION ConcatenatePDFs(BLOB ARRAY pdfs)
{
  IF(Length(pdfs) = 0)
    THROW NEW Exception(`Empty PDF list passed to ConcatenatePDFs`);

  IF(Length(pdfs) = 1) //let's not bother with that
    RETURN pdfs[0];

  STRING ARRAY inpaths;
  FOREVERY(BLOB pdf FROM pdfs)
  {
    STRING inputpdfpath := GenerateTemporaryPathname();
    StoreDiskFile(inputpdfpath, pdf, [ exclusive := TRUE ]);
    INSERT inputpdfpath INTO inpaths AT END;
  }

  STRING outputpath := GenerateTemporaryPathname();
  RECORD res := __RunPDFBox(["PDFMerger", ...inpaths, outputpath]);
  BLOB result;

  IF(res.exitcode = 0)
    result := StringToBlob(BlobToString(GetDiskResource(outputpath)));

  DeleteDiskFile(outputpath);
  FOREVERY(STRING path FROM inpaths)
    DeleteDiskFile(path);

  IF(res.exitcode > 0)
    THROW NEW Exception(`Concatenating PDFs failed with exitcode ${res.exitcode}`);

  RETURN result;
}
