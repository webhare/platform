<?wh
/** @topic testframework/validation */

LOADLIB "wh::adhoccache.whlib";
LOADLIB "wh::javascript.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::util/algorithms.whlib";
LOADLIB "wh::regex.whlib";
LOADLIB "wh::os.whlib";

LOADLIB "mod::tollium/lib/gettid.whlib";
LOADLIB "mod::tollium/lib/internal/gettid.whlib";
LOADLIB "mod::tollium/lib/internal/icons.whlib";
LOADLIB "mod::tollium/lib/internal/language.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/internal/harescript/libscan.whlib";
LOADLIB "mod::system/lib/internal/validation/config.whlib" EXPORT GetModuleValidationConfig;
LOADLIB "mod::system/lib/internal/validation/harescript.whlib";
LOADLIB "mod::system/lib/internal/validation/javascript.whlib";
LOADLIB "mod::system/lib/internal/validation/svg.whlib";
LOADLIB "mod::system/lib/internal/validation/witty.whlib";
LOADLIB "mod::system/lib/internal/validation/xml.whlib";
LOADLIB "mod::system/lib/internal/validation/support.whlib" EXPORT XMLValidatorBase;
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

CONSTANT STRING ARRAY validate_using_js_extensions := [".yml",".yaml"];

BOOLEAN FUNCTION IsHareScriptFile(STRING path)
{
  FOREVERY (STRING ext FROM harescriptextensions)
    IF(ToLowercase(path) LIKE ext)
      RETURN TRUE;

  RETURN FALSE;
}

RECORD FUNCTION ValidateTidsSyntax(RECORD ARRAY intids)
{
  RECORD ARRAY validtids, errors, warnings;
  FOREVERY(RECORD tid FROM intids)
  {
    IF(NOT IsAbsoluteTid(tid.tid))
    {
      INSERT CELL[ tid.resourcename, tid.line, tid.col, message := `Invalid tid, missing module name: '${tid.tid}'` ] INTO errors AT END;
      CONTINUE;
    }
    ELSE IF(tid.tid LIKE "*:*" AND tid.tid NOT LIKE "*.*")
    {
      INSERT CELL[ tid.resourcename, tid.line, tid.col, message := `Invalid tid, no group name: '${tid.tid}'` ] INTO errors AT END;
      CONTINUE;
    }
    ELSE IF(tid.tid LIKE "*::*" AND tid.tid NOT LIKE "*.*") //site:: tids existed at one point, ensure they're really really gone
    {
      INSERT CELL[ tid.resourcename, tid.line, tid.col, message := `Invalid legacy tid '${tid.tid}'` ] INTO errors AT END;
      CONTINUE;
    }
    ELSE IF(tid.tid LIKE "tollium:common.buttons.*" OR tid.tid LIKE "tollium:common.actions.*" OR tid.tid LIKE "tollium:common.labels.*")
    {
      STRING propertid := "~" || Detokenize(ArraySlice(Tokenize(tid.tid,'.'), 2),'.');
      INSERT CELL[ tid.resourcename, tid.line, tid.col, message := `Legacy tid '${tid.tid}' should be replaced by ${propertid} (dev module can rewrite these in XML files)` ] INTO warnings AT END;
      CONTINUE;
    }
    ELSE IF(tid.tid LIKE "* *" OR ToLowercase(tid.tid)!=tid.tid)
    {
      INSERT CELL[ tid.resourcename, tid.line, tid.col, message := `Invalid tid, must have no spaces, be lowercase: '${tid.tid}'` ] INTO errors AT END;
      CONTINUE;
    }

    INSERT tid INTO validtids AT END;
  }
  RETURN CELL[ validtids, errors, warnings ];
}

BOOLEAN FUNCTION IsTidTranslatedAnywhere(STRING tid)
{
  STRING canonicaltid := GetCanonicalTid(tid);
  IF(GetTid(tid) != `(cannot find text: ${canonicaltid})`)
    RETURN TRUE;

  //Try all the other languages, for validating, if just one matches it's fine with us. cross-reference checks of language files are not our problem
  FOREVERY(STRING trylang FROM GetSupportedLanguageCodes(Tokenize(canonicaltid,':')[0]))
    IF(GetTidForLanguage(trylang, canonicaltid) != `(cannot find text: ${canonicaltid})`)
      RETURN TRUE;

  RETURN FALSE;
}

/** Returns warnings for missing or invalid tid references
    @param tids List of tids to check
    @cell(string) tids.resource Resource where the tid is defined
    @cell(string) tids.tid Tid to check
    @cell(integer) tids.line Line number
    @cell(integer)  tids.col Column number
    @return List of warnings
    @cell(string) return.resourcename Resource name
    @cell(string)  return.message Warning message
    @cell(integer) return.line Relevant line number
    @cell(integer) return.col Relevant column
*/
RECORD ARRAY FUNCTION ValidateTidsExistence(RECORD ARRAY tids)
{
  RECORD ARRAY warnings;

  FOREVERY (RECORD tid FROM tids)
    IF(NOT IsTidTranslatedAnywhere(tid.tid))
    {
      STRING attr := CellExists(tid, "ATTRNAME") ? tid.attrname : "";
      INSERT
          [ resourcename :=   tid.resourcename
          , message :=    "Missing tid" || (attr != "" ? " from attribute '" || attr || "'" : "") || ": '" || tid.tid || "'"
          , line :=       tid.line
          , col :=        tid.col
          ] INTO warnings AT END;
    }

  RETURN warnings;
}

RECORD ARRAY FUNCTION ValidateIcons(RECORD ARRAY icons)
{
  RECORD ARRAY warnings;

  FOREVERY(RECORD icon FROM icons)
    FOREVERY(STRING iconpart FROM Tokenize(icon.icon,'+'))
    {
      OBJECT exc;
      TRY
      {
        IF (RecordExists(GetImage(iconpart, 24, 24, "", "b", [ nobroken := TRUE ])))
          CONTINUE;
      }
      CATCH (OBJECT e)
        exc := e;

      STRING attr := CellExists(icon, "ATTRNAME") ? icon.attrname : "";
      INSERT
          [ resourcename :=   icon.resourcename
          , message :=    "Missing icon" || (attr != "" ? " from attribute '" || attr || "'" : "") || ": '" || iconpart || "'" || (ObjectExists(exc) ? ` (exception: ${exc->what})` : "")
          , line :=       icon.line
          , col :=        icon.col
          ] INTO warnings AT END;
    }

  RETURN warnings;
}

/** Returns the list of tids in a module
    @param validationconfig @includecelldef #GetModuleValidationConfig.return
    @param module Module to scan
    @return List of tids and warnings
    @cell return.tids @includecelldef #ValidateSingleFile.return.tids
    @cell return.warnings @includecelldef #ValidateSingleFile.return.warnings
*/
PUBLIC RECORD FUNCTION ScanModuleTids(RECORD validationconfig, STRING module)
{
  __EnableTidCacheInvalidation();

  STRING ARRAY scanlist := GetValidatableFiles(validationconfig, module);
  RECORD scanresult := [ tids := RECORD[], warnings := RECORD[] ];
  RECORD ARRAY warnings;

  FOREVERY(STRING toscan FROM scanlist)
  {
    RECORD results      := ValidateSingleFile(toscan, [ onlytids := TRUE ]);
    scanresult.tids     := scanresult.tids     CONCAT results.tids;
    scanresult.warnings := scanresult.warnings CONCAT results.warnings;
  }
  RETURN scanresult;
}

STRING ARRAY FUNCTION RecurseGetValidatableFiles(STRING basepath, RECORD validateconfig, MACRO PTR onskippedfile, STRING filemask)
{
  RECORD ARRAY dir_contents := ReadWebHareResourceFolder(basepath);
  STRING ARRAY files;

  FOREVERY(RECORD entry FROM dir_contents)
  {
    IF(entry.name LIKE ".*" OR entry.name="node_modules" OR entry.name="vendor")
      CONTINUE;

    STRING resname := basepath || "/" || entry.name;
    IF(entry.isfolder)
    {
      files := files CONCAT RecurseGetValidatableFiles(resname, validateconfig, onskippedfile, filemask);
      CONTINUE;
    }

    IF(ToUppercase(entry.name) NOT LIKE ToUppercase(filemask))
      CONTINUE;

    BOOLEAN skip;
    STRING resnameformasks := Substring(resname,SearchSubString(resname,'/')+1);

    FOREVERY(RECORD exclusion FROM validateconfig.excludemasks)
      IF(ToUppercase(resnameformasks) LIKE ToUppercase(exclusion.mask))
      {
        IF(exclusion.permanent = FALSE AND onskippedfile != DEFAULT MACRO PTR)
          onskippedfile(resname, exclusion.why);
        skip := TRUE;
        BREAK;
      }

    IF(NOT skip)
      INSERT resname INTO files AT END;
  }
  RETURN files;
}

/** Gathers the list of files that must be validated
    @param validateconfig @includecelldef #GetModuleValidationConfig.return
    @param modulename Name of the module to scan
    @cell(macro ptr) options.onskippedfile Called for all skipped files. Signature: MACRO onskippedfile(STRING resourcename, STRING reason)
    @return List of validatable files
*/
PUBLIC STRING ARRAY FUNCTION GetValidatableFiles(RECORD validateconfig, STRING modulename, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ onskippedfile := DEFAULT MACRO PTR
                             , filemask := "*"
                             ], options);

  STRING root := modulename = "jssdk"
      ? "direct::" || GetWebhareConfiguration().installationroot || "jssdk"
      : "mod::" || modulename;

  RETURN RecurseGetValidatableFiles(root, validateconfig, options.onskippedfile, options.filemask);
}

BOOLEAN FUNCTION IsSVGIconFile(STRING resourcename)
{
  RETURN ToUppercase(resourcename) LIKE "*/WEB/IMG/*.SVG";
}

//can we validate this file? shortcircuit resource grabbing/adhoc cache usage
BOOLEAN FUNCTION CanValidateFile(STRING resourcename, BOOLEAN onlytids)
{
  IF(IsHareScriptFile(resourcename))
    RETURN TRUE;
  IF(IsSVGIconFile(resourcename))
    RETURN onlytids ? FALSE : TRUE;
  IF(GetExtensionFromPath(resourcename) IN STRING[...whconstant_javascript_extensions, ...validate_using_js_extensions])
    RETURN TRUE;
  STRING ext := ToUppercase(GetExtensionFromPath(resourcename));
  IF(ext IN [".WITTY",".XML",".XSD",".SITEPRL",".IXO"])
    RETURN TRUE;
  RETURN FALSE;
}

RECORD FUNCTION RunValidatorForFile(BLOB doc_blob, STRING resourcename, RECORD options)
{
  __EnableTidCacheInvalidation();

  //README: When adding a validator or tidscanner, use `wh validate [--tids] <path>` to test
  IF(IsHareScriptFile(resourcename))
    RETURN RunHSValidator(doc_blob, resourcename, options);

  IF(ToUppercase(resourcename) LIKE "*/WEB/IMG/*.SVG")
    RETURN RunSVGIconValidator(doc_blob, resourcename, options);
  IF(GetExtensionFromPath(resourcename) IN whconstant_javascript_extensions)
    RETURN RunJSValidator(doc_blob, resourcename, options);
  IF(GetExtensionFromPath(resourcename) IN validate_using_js_extensions)
    RETURN CallJS("@mod-platform/js/devsupport/validation.ts#runJSBasedValidator", doc_blob, resourcename, options);

  STRING ext := ToUppercase(GetExtensionFromPath(resourcename));
  SWITCH(ext)
  {
    CASE ".WITTY"
    {
      RETURN RunWittyValidator(doc_blob, resourcename, options);
    }
    CASE ".XML", ".XSD", ".SITEPRL", ".IXO"
    {
      RETURN RunXMLValidator(doc_blob, resourcename, options);
    }

    //README: new extensions should also be added to CanValidateFile
    DEFAULT
    {
      THROW NEW Exception(`Missing validator for '${resourcename}' but we thought we could validate it`);
    }
  }
  RETURN DEFAULT RECORD;
}

RECORD defaultvalidaterecord := [ warnings     := RECORD[]
                                , errors       := RECORD[]
                                , hints        := RECORD[]
                                , tids         := RECORD[]
                                , eventmasks   := STRING[]
                                , icons        := RECORD[]
                                ];

RECORD FUNCTION ValidateDocument(STRING resourcename, RECORD options)
{
  IF(NOT IsAbsoluteResourcePath(resourcename))
    THROW NEW Exception(`Invalid resource path ${resourcename}`);

  BLOB doc_blob := CellExists(options,'overridedata') ? options.overridedata : GetWebhareResource(resourcename);
  RECORD result := RunValidatorForFile(doc_blob, resourcename, options);

  result := ValidateOptions(defaultvalidaterecord, result);
  INSERT CELL resourcename := resourcename INTO result;

  //Sanity checks until validate works everywhere
  FOREVERY(STRING checklist FROM ["tids","warnings","errors","hints","icons"])
  {
    RECORD ARRAY list := GetCell(result,checklist);
    IF(RecordExists(SELECT FROM list WHERE NOT CellExists(list,'resourcename')))
    {
      DumpValue(list,'boxed');
      THROW NEW Exception(`Member ${checklist} has rows without resourcename (validating ${resourcename})`);
    }
    IF(RecordExists(SELECT FROM list WHERE CellExists(list,'filename')))
    {
      DumpValue(list,'boxed');
      THROW NEW Exception(`Member ${checklist} has rows which still have filename (validating ${resourcename})`);
    }
    IF(resourcename != "" AND RecordExists(SELECT FROM list WHERE list.resourcename=""))
    {
      DumpValue(list,'boxed');
      THROW NEW Exception(`Member ${checklist} has rows which have an empty resource name even though a resourcename was set (validating ${resourcename})`);
    }
  }

  RECORD validatedtids := ValidateTidsSyntax(result.tids);
  result.tids := validatedtids.validtids;
  result.errors := result.errors CONCAT validatedtids.errors;
  result.warnings := result.warnings CONCAT validatedtids.warnings;

  IF(NOT options.onlytids)
  {
    RECORD ARRAY tidissues := ValidateTidsExistence(result.tids);
    IF(options.nomissingtids)
      result.errors := result.errors CONCAT tidissues;
    ELSE
      result.warnings := result.warnings CONCAT tidissues;

    RECORD ARRAY iconissues := ValidateIcons(result.icons);
    result.errors := result.errors CONCAT iconissues;
  }

  FOREVERY(RECORD toignore FROM SELECT * FROM options.ignoremessages WHERE VAR resourcename LIKE COLUMN mask)
  {
    OBJECT theregex := NEW RegEx(toignore.regex, "i");
    DELETE FROM result.errors WHERE theregex->Test(message);
    DELETE FROM result.warnings WHERE theregex->Test(message);
    DELETE FROM result.hints WHERE theregex->Test(message);
  }

  STRING ARRAY eventmasks;
  IF (NOT CellExists(options,'overridedata'))
  {
    //also gather -other- resources which are triggering errors for us
    STRING ARRAY files := SELECT AS STRING ARRAY DISTINCT COLUMN resourcename FROM (result.warnings CONCAT result.errors CONCAT result.tids);
    IF(resourcename != "" AND resourcename NOT IN files)
      INSERT resourcename INTO files AT END;

    eventmasks := GetResourceEventMasks(files);
  }

  eventmasks := ArrayUnion(eventmasks, result.eventmasks);
  DELETE CELL eventmasks FROM result;

  //Normalize so noone relies on 'extra' cells
  result.errors   :=
      SELECT TEMPORARY source := CellExists(errors, "source") ? COLUMN source : "unknown"
           , COLUMN resourcename
           , line
           , col
           , message
           , source :=        source
           , metadata :=      source != "unknown" AND CellExists(errors, "metadata") ? metadata : DEFAULT RECORD
        FROM result.errors
    ORDER BY COLUMN resourcename, line, col, message;

  result.warnings :=
      SELECT TEMPORARY source := CellExists(warnings, "source") ? COLUMN source : "unknown"
           , COLUMN resourcename
           , line
           , col
           , message
           , source :=        source
           , metadata :=      source != "unknown" AND CellExists(warnings, "metadata") ? metadata : DEFAULT RECORD
        FROM result.warnings
    ORDER BY COLUMN resourcename, line, col, message;

  result.hints :=
      SELECT TEMPORARY source := CellExists(hints, "source") ? COLUMN source : "unknown"
           , COLUMN resourcename
           , line
           , col
           , message
           , source :=        source
           , metadata :=      source != "unknown" AND CellExists(hints, "metadata") ? metadata : DEFAULT RECORD
        FROM result.hints
    ORDER BY COLUMN resourcename, line, col, message;

  result.tids :=
      SELECT COLUMN resourcename
           , line
           , col
           , tid
           , attrname := CellExists(tids,'attrname') ? tids.attrname : ""
        FROM result.tids
    ORDER BY COLUMN resourcename, line, col, tid;

  RETURN [ ttl := 60*60*1000
         , value := result
         , eventmasks := eventmasks
         ];
}

/** Validates a single file
    @param resourcename resource to validate
    @cell(boolean) options.onlytids If TRUE, only scan and validate tids
    @cell(blob) options.overridedata If present, use this data instead of the data from disk
    @cell(boolean) options.perfectcompile @includecelldef #GetModuleValidationConfig.return.perfectcompile
    @cell(boolean) options.nomissingtids  @includecelldef #GetModuleValidationConfig.return.nomissingtids
    @cell(boolean) options.nowarnings  @includecelldef #GetModuleValidationConfig.return.nowarnings
    @cell(boolean) options.documentation  @includecelldef #GetModuleValidationConfig.return.documentation
    @cell(string array) options.eslintmasks  @includecelldef #GetModuleValidationConfig.return.eslintmasks
    @return Validation results
    @cell(record array) return.hints List of hints
    @cell(string) return.hints.resourcename Resource name for this message
    @cell(integer) return.hints.line Line number for this message
    @cell(integer) return.hints.col Column number for this message
    @cell(string) return.hints.source Source for this message
    @cell(string) return.hints.message Message
    @cell(record) return.hints.metadata metadata (only when source != "unknown")
    @cell(record array) return.warnings List of warnings
    @cell(string) return.warnings.resourcename Resource name for this message
    @cell(integer) return.warnings.line Line number for this message
    @cell(integer) return.warnings.col Column number for this message
    @cell(string) return.warnings.source Source for this message
    @cell(string) return.warnings.message Message
    @cell(record) return.warnings.metadata metadata (only when source != "unknown")
    @cell(record array) return.errors List of errors @includecelldef #ValidateSingleFile.return.warnings
    @cell(record array) return.tids List of tids
    @cell(string) return.tids.resourcename Resource where this tid was defined
    @cell(integer) return.tids.line Line number for this message
    @cell(integer) return.tids.col Column number for this message
    @cell(string) return.tids.tid Tid
    @cell(string) return.tids.attrname Attribute where this tid was defined
    @cell(string array) return.eventmasks Event masks for invalidation of this validation result
*/
PUBLIC RECORD FUNCTION ValidateSingleFile(STRING resourcename, RECORD options DEFAULTSTO CELL[])
{
  options := ValidateOptions([ onlytids := FALSE
                             , overridedata := DEFAULT BLOB
                             , ...defaultvalidationoptions
                             ], options, [ optional := ["overridedata"] ]);

  IF(resourcename != "" AND NOT IsAbsoluteResourcePath(resourcename))
    THROW NEW Exception(`Invalid resource path '${resourcename}'`);
  IF(NOT CanValidateFile(resourcename, options.onlytids))
    RETURN CELL [ ...defaultvalidaterecord, resourcename ];

  IF(CellExists(options,"overridedata")) //uncacheable
    RETURN ValidateDocument(resourcename, options).value;
  ELSE
    RETURN GetAdhocCached(CELL[ resourcename, options.onlytids, options.documentation ], PTR ValidateDocument(resourcename, options));
}

/** Returns the string for a validation error from an XML (without column number)
    @param msg Message record
    @cell(string) msg.message Error message
    @cell(string) msg.resourcename Resource name
    @cell(integer) msg.line Line number
    @return Formatted error message
*/
PUBLIC STRING FUNCTION FormatValidationError(RECORD msg)
{
  RETURN msg.resourcename = ""
      ? `Error: ${msg.message}`
      : `${msg.resourcename}:${msg.line}: Error: ${msg.message}`;
}

/** Returns the string for a validation warning from an XML (without column number)
    @param msg Message record
    @cell(string) msg.message Warning message
    @cell(string) msg.resourcename Resource name
    @cell(integer) msg.line Line number
    @return Formatted warning message
*/
PUBLIC STRING FUNCTION FormatValidationWarning(RECORD msg)
{
  RETURN msg.resourcename = ""
      ? `Warning: ${msg.message}`
      : `${msg.resourcename}:${msg.line}: Warning: ${msg.message}`;
}

PUBLIC STRING FUNCTION FormatValidationMessageWithType(RECORD msg)
{
  //Formats ValidationMessageWithType from mod::platform/js/devsupport/validation.ts and matches formatValidationMessage from there
  RETURN `${msg.resourcename}:${msg.line}:${msg.col}: ${ToUppercase(Left(msg.type,1))}${Substring(msg.type, 1)}: ${msg.message}`;
}

PUBLIC MACRO PrintValidationMessagesToConsoleWithType(RECORD ARRAY messages)
{
  //matches logValidationMessagesToConsole
  messages := SELECT * FROM messages ORDER BY resourcename, line, col;
  FOREVERY(RECORD msg FROM messages)
    Print(FormatValidationMessageWithType(msg) || "\n");
}

/** Prints (colored) validation errors and warnings to the console
    @param file Warnings and errors
    @cell(record array) file.errors List of errors
    @cell(string) file.errors.message Error message
    @cell(string) file.errors.resourcename Resource name
    @cell(integer) file.errors.line Line number
    @cell(integer) file.errors.col Column number
    @cell(record array) file.warnings List of warnings
    @cell(string) file.warnings.message Warning message
    @cell(string) file.warnings.resourcename Resource name
    @cell(integer) file.warnings.line Line number
    @cell(integer) file.warnings.col Column number
*/
PUBLIC MACRO PrintValidationMessages(RECORD file)
{
  FOREVERY (RECORD error FROM (SELECT * FROM file.errors ORDER BY resourcename, line, col))
    Print(`${AnsiCmd("red")}${error.resourcename = "" ? "" : `${error.resourcename}:${error.line}: `}Error: ${error.message}${AnsiCmd("reset")}\n`);

  FOREVERY (RECORD warning FROM (SELECT * FROM file.warnings ORDER BY resourcename, line, col))
    Print(`${warning.resourcename = "" ? "" : `${warning.resourcename}:${warning.line}:${warning.col}: `}Warning: ${warning.message}\n`);

  FOREVERY (RECORD hint FROM (SELECT * FROM file.hints ORDER BY resourcename, line, col))
    Print(`${hint.resourcename = "" ? "" : `${hint.resourcename}:${hint.line}:${hint.col}: `}Hint: ${hint.message}\n`);
}


/** Validate a single file using the module configuration for validation options
    @param resname resource name
    @cell(boolean) options.documentation Validate documentation, defaults to TRUE
    @cell(blob) options.overridedata If present, use this data as the source data
    @return %includecelldef #ValidateSingleFile.return
*/
PUBLIC RECORD FUNCTION ValidateSingleFileAdhoc(STRING resname, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions( [ documentation := FALSE
                              , overridedata := DEFAULT BLOB
                              ], options, [ optional := ["overridedata"] ]);

  TRY
  {
    RECORD validationoptions;
    TRY
    {
      validationoptions := GetValidationOptions(resname);
    }
    CATCH(OBJECT<RetrieveResourceException> e)
    {
      /* unless the source of the error is the moduledef we're validating, this aborts the validation.
         fix a chicken-and-egg when the moduledef itself is broken and we can't read the options */
      IF(e->resourcename != resname)
        THROW;
    }
    RETURN ValidateSingleFile(resname, CELL[ ...validationoptions, ...options ]);
  }
  CATCH(OBJECT e)
  {
    RETURN [ errors := [[ line := 0
                        , col :=0
                        , resourcename := resname
                        , source := "exception"
                        , message := "Unable to validate: " || e->what
                       ]]
           , warnings := RECORD[]
           , hints := RECORD[]
           , tids := RECORD[]
           ];
  }
}
