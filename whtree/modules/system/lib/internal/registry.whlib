﻿<?wh

/** @topic modules/config
*/

LOADLIB "wh::adhoccache.whlib";
LOADLIB "wh::crypto.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::regex.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/internal/typecoder.whlib";

RECORD ARRAY anonymousregistry;
INTEGER nextanonymousid;
RECORD ARRAY mockedregistry;
OBJECT regkeyregex;

BOOLEAN FUNCTION IsAnonymousConfKey(STRING confkey)
{
  RETURN confkey="<anonymous>" OR confkey LIKE "<anonymous>.*";
}

INTEGER FUNCTION FindModuleColon(STRING confkey)
{
  INTEGER firstcolon := SearchSubstring(confkey, ':', confkey LIKE "<wrd:*" ? 5 : 0);
  INTEGER firstdot := SearchSubstring(confkey, '.', confkey LIKE "<wrd:*" ? 39 : 0); //skip over <wrd:00001111222233334444555566667777>. - especially that dot
  RETURN firstcolon >= 0 AND (firstcolon < firstdot OR firstdot = -1) ? firstcolon : -1;
}

PUBLIC STRING FUNCTION FixupRegKey(STRING confkey)
{
  INTEGER usescolon := FindModuleColon(confkey);
  IF(usescolon >= 0)
  { /* This will be the recommended module-style format but also a chance to become stricter...
       We want '(<userprefix>.)module:key.name
       ([a-z0-9][-a-z0-9_]*[a-z0-9]) is the module name regex (we have to permit things like '4tu')
       TODO also prevent double dots in the regex, incorrect trailing dots etc
       */
    IF(NOT ObjectExists(regkeyregex))
      regkeyregex := NEW JSRegEx("^(<overrideuser>\\.|<anonymous>\\.|<wrd:[a-f0-9]{32}>\\.)?[a-z0-9][-a-z0-9_]*[a-z0-9]:[a-z0-9_\\.]+$");
    IF(NOT regkeyregex->Test(confkey))
      THROW NEW Exception(`Invalid registry key name '${confkey}' - you should use the format '(<userprefix>.)module:key.name'`);

    confkey := Left(confkey, usescolon) || '.' || Substring(confkey, usescolon+1); //in the database we'll keep it as a '.'
  }

  IF(confkey LIKE "<*" AND NOT NEW JSRegEx("^(<overrideuser>\\.|<anonymous>\\.|<wrd:[a-f0-9]{32}>\\.)")->Test(confkey))
    THROW NEW Exception(`Invalid userprefix on registry key name '${confkey}'`);

  confkey := ToLowercase(confkey);
  IF(confkey NOT LIKE "system.modules.*" AND confkey NOT LIKE "modules.*")
    RETURN confkey;

  STRING dtapstage := MakeFunctionPtr("mod::system/lib/internal/whconfig.whlib#GetDTAPStage")();
  IF(dtapstage = "development") //2019-10-04: time to deprecate these old names
    THROW NEW Exception(`Invalid registry key name '${confkey}' - you should no longer prefix registry keys with 'system.modules.' or 'modules.'`);

  IF(confkey LIKE "system.modules.*")
    confkey := Substring(confkey,15);
  ELSE IF(confkey LIKE "modules.*")
    confkey := Substring(confkey,8);

  RETURN confkey;
}

STRING FUNCTION GetNodeFromKey(STRING name)
{
  RETURN Left(name,SearchLastSubstring(name,'.'));
}

PUBLIC MACRO __MockRegistryKey(STRING confkey, VARIANT value) //used by the test framework
{
  RECORD keyinfo := __GetRegistryKey(confkey, FALSE, 0, 0);
  STRING newvalue := EncodeHSON(value);
  STRING newdata := Length(newvalue) <= 4096 ? newvalue : "";
  BLOB newblob := Length(newvalue) <= 4096 ? DEFAULT BLOB : StringToBlob(newvalue);

  DELETE FROM mockedregistry WHERE name = keyinfo.name;
  nextanonymousid := nextanonymousid - 1;
  INSERT INTO mockedregistry(id, data, blobdata, name, namehash, nodehash, modificationdate)
         VALUES(nextanonymousid, newdata, newblob, keyinfo.name, keyinfo.namehash, keyinfo.nodehash, GetCurrentDatetime())
         AT END;
}

/** @short Get the event masks to use to listen to specific registry keys in adhoc caches
    @public
    @loadlib mod::system/lib/configure.whlib
    @param keys List of registry keys
    @return A list of event mask(s) */
PUBLIC STRING ARRAY FUNCTION GetRegistryKeyEventMasks(STRING ARRAY keys)
{
  RETURN SELECT AS STRING ARRAY DISTINCT "system:registry." || GetNodeFromKey(keyname)
           FROM ToRecordArray(keys, "keyname");
}

PUBLIC RECORD FUNCTION __GetRegistryKey(STRING confkey, BOOLEAN loadkey, INTEGER cachettl, INTEGER negative_cachettl)
{
  confkey := FixupRegKey(confkey);

  STRING nodename := GetNodeFromKey(confkey);
  STRING hash := GetSHA1Hash(confkey);

  RECORD curkey;
  IF(IsAnonymousConfKey(confkey)) //we simulate the <anonymous> registry
    curkey := SELECT * FROM anonymousregistry WHERE namehash=hash;
  ELSE
  {
    IF(Length(mockedregistry) > 0)
      curkey := SELECT * FROM mockedregistry WHERE namehash=hash;
    IF(NOT RecordExists(curkey))
      curkey := SELECT * FROM system.flatregistry WHERE namehash=hash;
  }

  RECORD result := [ id := RecordExists(curkey) ? curkey.id : 0
                   , eventname := "system:registry." || nodename
                   , name := confkey
                   , nodehash := GetSHA1Hash(nodename)
                   , namehash := hash
                   ];

  IF(RecordExists(curkey) AND loadkey)
  {
    INSERT CELL value := ReadAnyFromDatabase(curkey.data, curkey.blobdata)
           INTO result;
  }

  IF(cachettl > 0)
  {
    BOOLEAN gotvalue := CellExists(result, 'value') AND NOT IsDefaultValue(result.value);
    RETURN [ value := result
           , ttl := gotvalue ? cachettl : negative_cachettl
           , eventmasks := GetRegistryKeyEventMasks([confkey])
           ];
  }

  RETURN result;
}

PUBLIC MACRO __SetRegistryKey(RECORD keyinfo, VARIANT value)
{
  STRING newvalue := EncodeHSON(value);
  STRING newdata := Length(newvalue) <= 4096 ? newvalue : "";
  BLOB newblob := Length(newvalue) <= 4096 ? DEFAULT BLOB : StringToBlob(newvalue);

  IF(IsAnonymousConfKey(keyinfo.name))
  {
    IF(keyinfo.id != 0)
      UPDATE anonymousregistry SET data := newdata, blobdata := newblob, modificationdate := GetCurrentDatetime() WHERE id = keyinfo.id;
    ELSE
    {
      nextanonymousid := nextanonymousid - 1;
      INSERT INTO anonymousregistry(id, data, blobdata, name, namehash, nodehash, modificationdate)
             VALUES(nextanonymousid, newdata, newblob, keyinfo.name, keyinfo.namehash, keyinfo.nodehash, GetCurrentDatetime())
             AT END;
    }
  }
  ELSE
  {
    IF(keyinfo.id != 0)
      UPDATE system.flatregistry SET data := newdata, blobdata := newblob, modificationdate := GetCurrentDatetime() WHERE id = keyinfo.id;
    ELSE
      INSERT INTO system.flatregistry(data, blobdata, name, namehash, nodehash, modificationdate)
             VALUES(newdata, newblob, keyinfo.name, keyinfo.namehash, keyinfo.nodehash, GetCurrentDatetime());
  }
  GetPrimary()->BroadcastOnCommit(keyinfo.eventname, DEFAULT RECORD);
}

/** @short Get a registry key. Module registry keys can be read using the key "modulename.registrykey...".
    @public
    @loadlib mod::system/lib/configure.whlib
    @param confkey Key name
    @cell options.fallback Fallback value (returned when key does not exist, and used for type validation)
    @return Value of registry key. Throws if the key does not exist and no fallback has been provided.
    @see %WriteRegistryKey %DeleteRegistryKey %ReadRegistryNode
*/
PUBLIC VARIANT FUNCTION ReadRegistryKey(STRING confkey, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions( [ fallback := "", cachettl := 0 ], options, [ optional := ["fallback"], notypecheck := ["fallback" ]]);
  IF(confkey LIKE "<*" AND NOT CellExists(options,'fallback'))
    THROW NEW Exception(`Reading a user registry requires you to set a fallback value`); // as you can't initialize it

  RECORD keyinfo;
  IF(options.cachettl > 0) //with cachettl set, __GetRegistryKey returns a GetAdhocCached record
    keyinfo := GetAdhocCached(CELL[confkey], PTR __GetRegistryKey(confkey, TRUE, options.cachettl, options.cachettl));
  ELSE
    keyinfo :=  __GetRegistryKey(confkey, TRUE, 0, 0);

  IF(keyinfo.id = 0)
    IF(CellExists(options, 'fallback'))
      RETURN options.fallback;
    ELSE
      THROW NEW Exception(`No such registry key '${confkey}' - you may need to 'wh apply registry'`);

  IF(CellExists(options, 'fallback') AND TYPEID(options.fallback) != TYPEID(keyinfo.value))
    THROW NEW Exception(`Invalid type in registry for registry key '${confkey}', got ${GetTypeName(TYPEID(keyinfo.value))} but expected ${GetTypeName(TYPEID(options.fallback))}`);

  RETURN keyinfo.value;
}


/** @short Read registry keys by mask
    @public
    @loadlib mod::system/lib/configure.whlib
    @param keymask Mask to use (to search the temporary anonymous registry, the mask must look like an anonymous key ie start with <anonymous>.)
    @return Registry keys
    @cell(string) node.name Registry key name
    @cell(variant) node.value Registry key value */
PUBLIC RECORD ARRAY FUNCTION ReadRegistryKeysByMask(STRING keymask)
{
  INTEGER usescolon := FindModuleColon(keymask);
  IF(usescolon >= 0)
    keymask := left(keymask,usescolon) || '.' || Substring(keymask,usescolon+1);

  RECORD ARRAY result;
  IF(IsAnonymousConfKey(keymask)) //we simulate the <anonymous> registry
  {
    result := SELECT name, value := ReadAnyFromDatabase(data, blobdata)
                FROM anonymousregistry
               WHERE ToUppercase(name) LIKE ToUppercase(keymask);
  }
  ELSE
  {
    result := SELECT name, value := ReadAnyFromDatabase(data, blobdata)
                FROM system.flatregistry
               WHERE ToUppercase(name) LIKE ToUppercase(keymask);
  }

  IF(usescolon >= 0)
    UPDATE result SET name := Left(name,usescolon) || ':' || Substring(name,usescolon+1);
  RETURN result;
}

/** @short Deletes a registry key. Module registry keys can be read using the key "modulename.registrykey...".
    @public
    @loadlib mod::system/lib/configure.whlib
    @param confkey Key to delete
    @see %ReadRegistryKey %WriteRegistryKey %ReadRegistryNode %DeleteRegistryNode
*/
PUBLIC MACRO DeleteRegistryKey(STRING confkey)
{
  RECORD keyinfo := __GetRegistryKey(confkey, FALSE, 0, 0);
  IF(keyinfo.id != 0)
  {
    IF(IsAnonymousConfKey(keyinfo.name))
      DELETE FROM anonymousregistry WHERE id=keyinfo.id;
    ELSE
      DELETE FROM system.flatregistry WHERE id=keyinfo.id;
  }

  GetPrimary()->BroadcastOnCommit(keyinfo.eventname, DEFAULT RECORD);
}

/** @short Deletes a registry node. Module registry keys can be read using the key "modulename.registrykey...".
    @public
    @loadlib mod::system/lib/configure.whlib
    @param confkey Node to delete
    @see %ReadRegistryKey %WriteRegistryKey %ReadRegistryNode %DeleteRegistryKey
*/
PUBLIC MACRO DeleteRegistryNode(STRING confkey)
{
  confkey := FixupRegKey(confkey);
  IF(IsAnonymousConfKey(confkey))
    DELETE FROM anonymousregistry WHERE name LIKE confkey || ".*";
  ELSE
    DELETE FROM system.flatregistry WHERE name LIKE confkey || ".*";
}

PUBLIC RECORD FUNCTION SplitConfKey(STRING parts)
{
  INTEGER lastdot := SearchLastSubstring(parts,'.');
  RETURN [ node := Left(parts,lastdot), keyname := Substring(parts,lastdot+1) ];
}

PUBLIC MACRO __RegistryFastInsert(RECORD ARRAY registry)
{
  STRING ARRAY seenkeys;
  DATETIME now := GetCurrentDatetime();
  FOREVERY(RECORD reg FROM registry)
  {
    reg.name := ToLowercase(reg.name);
    STRING hash := GetSHA1Hash(ToLowercase(reg.name));
    IF(hash IN seenkeys)
      CONTINUE;

    INSERT hash INTO seenkeys AT END;
    RECORD parts := PrepareAnyForDatabase(reg.data);
    INSERT INTO system.flatregistry(name, namehash, nodehash, data, blobdata, modificationdate)
           VALUES(ToLowercase(reg.name), hash, GetSHA1Hash(Left(reg.name,SearchLastSubstring(reg.name,'.'))), parts.stringpart, parts.blobpart, now);
  }
}

/** @short Get all keys in a node
    @public
    @loadlib mod::system/lib/configure.whlib
    @param confkey Registry node name
    @return List of registry keys
    @cell return.fullname Full name of the key
    @cell return.subkey Local name of the key within the node
    @cell return.data Value stored in this key
*/
PUBLIC RECORD ARRAY FUNCTION ReadRegistryNode(STRING confkey)
{
  INTEGER usescolon := FindModuleColon(confkey);
  confkey := FixupRegKey(confkey);
  STRING gethash := GetSHA1Hash(confkey);

  RECORD ARRAY result;
  IF(IsAnonymousConfKey(confkey))
  {
    result :=
           SELECT fullname := name
                , subkey := Substring(name, Length(confkey)+1)
                , data := data != "" ? DecodeHSON(data) : DecodeHSONBlob(blobdata)
             FROM anonymousregistry
            WHERE nodehash = gethash;
  }
  ELSE
  {
    result :=
           SELECT fullname := name
                , subkey := Substring(name, Length(confkey)+1)
                , data := data != "" ? DecodeHSON(data) : DecodeHSONBlob(blobdata)
             FROM system.flatregistry
            WHERE nodehash = gethash;
  }

  IF(usescolon >= 0)
    UPDATE result SET fullname := Left(fullname,usescolon) || ':' || Substring(fullname,usescolon+1);
  RETURN result;
}

/** @short Set a registry key if it exists. Module registry keys can be written using the key "modulename.registrykey...".
    @public
    @loadlib mod::system/lib/configure.whlib
    @param confkey Key name
    @param newval New value (must be of the same type as the existing key)
    @cell options.createifneeded Create the registry key if it doesn't exist yet.
    @cell options.initialcreate is the initial create (don't overwrite, only create)
    @see ReadRegistryKey DeleteRegistryKey ReadRegistryNode
*/
PUBLIC MACRO WriteRegistryKey(STRING confkey, VARIANT newval, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions( [ createifneeded := FALSE
                              , initialcreate := FALSE
                              ], options);

  IF(confkey LIKE "<*" AND NOT options.createifneeded AND NOT options.initialcreate)
    THROW NEW Exception(`Writing a user registry requires you to set either createifneeded or initialcreate`); // as you can't initialize it

  RECORD keyinfo := __GetRegistryKey(confkey, TRUE, 0, 0);
  IF(options.initialcreate AND keyinfo.id != 0)
    RETURN;
  IF(keyinfo.id = 0 AND NOT options.createifneeded AND NOT options.initialcreate)
    THROW NEW Exception(`No such registry key '${confkey}' - you may need to 'wh apply registry'`);

  //No type promotion! It would make your code racy, depending on first value ever written
  IF(keyinfo.id != 0 AND TypeID(keyinfo.value) != TypeID(newval))
    THROW NEW Exception(`Invalid type for registry key '${confkey}', got ${GetTypeName(TYPEID(newval))} but expected ${GetTypeName(TYPEID(keyinfo.value))}`);

  __SetRegistryKey(keyinfo, newval);
}
