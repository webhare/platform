﻿<?wh
LOADLIB "wh::files.whlib";

LOADLIB "mod::publisher/lib/siteapi.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/whfs.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";
LOADLIB "mod::system/lib/internal/whconfig.whlib";

MACRO GotBackendSiteChanged(BOOLEAN committed)
{
  IF (committed) //we only need the config.json backend value to be updated
    ApplyWebHareConfiguration([ subsytems := ["config.base"], source := "GotBackendSiteChanged" ]);
}

PUBLIC OBJECT FUNCTION EnsureSystemBackendSite()
{
  OBJECT target := OpenWHFSObject(whconstant_whfsid_webharebackend);
  IF(NOT ObjectExists(target))
    target := OpenWHFSRootObject()->CreateFolder([ id := whconstant_whfsid_webharebackend, name := "WebHare backend" ]);
  ELSE IF(target->name != "WebHare backend")
    target->UpdateMetadata( [ name := "WebHare backend" ]);

  IF(NOT RecordExists(SELECT FROM system.sites WHERE id=whconstant_whfsid_webharebackend))
  {
    GetPrimary()->RegisterCommitHandler("system:updatewebhareconfigjsonfile", PTR GotBackendSiteChanged);
    CreateSiteFromFolder(whconstant_whfsid_webharebackend);
  }

  RETURN OpenSite(whconstant_whfsid_webharebackend);
}

PUBLIC MACRO UpdateSystemBackendSite()
{
  OBJECT backendsite := EnsureSystemBackendSite();
  OBJECT target := backendsite->rootobject;

  /* The WebHare backend is marked as noindex/nofollow. But if we set a robots.txt, Google will not be able to see the
     noindex so existing Webhares will *remain* in the index. Now that google works that way, keeping the robots.txt is
     counterproductive */
  OBJECT robotstxt := target->OpenByName("robots.txt");
  IF(ObjectExists(robotstxt) AND BlobToString(robotstxt->data,1024) = "# No need to index the WebHare backend\n\nUser-agent: *\nDisallow: /\n")
    robotstxt->RecycleSelf();

  OBJECT portalfile := target->EnsureFile( [ name := "portal", typens := "http://www.webhare.net/xmlns/tollium/applicationportal", publish := TRUE ]);
  target->UpdateMetadata([indexdoc := portalfile->id ]);

  //create a testportal file for external CI tests. notice that even if published, this file will be not work on non-development dtap stages
  target->EnsureFile( [ name := "testportal", typens := "http://www.webhare.net/xmlns/tollium/applicationportal", publish := GetDtapStage() = "development" ]);

  OBJECT webharefolder := target->EnsureFolder( [ name := "webhare" ]);
  OBJECT checkfolder := webharefolder->EnsureFolder( [ name := "check" ]);
  //the 'testoutput' folder is reserved for CI tests
  OBJECT testfwfolder := backendsite->rootobject->EnsureFolder([ name := "testoutput", type := 1 ]);
  //the 'documentation' folder is reserved as a common point for documentation subsites. they should preferably be one extra level deeper in this folder
  backendsite->rootobject->EnsureFolder([ name := "documentation", type := 1 ]);

  //TODO Stop injecting code directly into WHFS. But we should probably replace this with an official oauth-ed WebHare REST API anyway
  OBJECT webhareindex := webharefolder->EnsureFile(
    [ name := "index.shtml"
    , type := whconstant_whfstype_shtmlfile
    , data := StringToBlob('<?wh LOADLIB "wh::internet/urls.whlib";LOADLIB "mod::system/lib/webserver.whlib"; Redirect(UpdateURLVariables("../", [ app := GetWebVariable("app") ] ));')
    , publish := TRUE
    ]);
  webharefolder->UpdateMetadata([indexdoc := webhareindex->id]);
  OBJECT checkindex := checkfolder->EnsureFile(
    [ name := "index.shtml"
    , type := whconstant_whfstype_shtmlfile
    , data := StringToBlob('<?wh LOADLIB "mod::tollium/webdesigns/webinterface/pages/check/check.whlib" EXPORT RunDynamicPage;')
    , publish := TRUE
    ]);
  checkfolder->UpdateMetadata([indexdoc := checkindex->id]);

  UPDATE system.sites
     SET name := "WebHare backend"
   WHERE id = whconstant_whfsid_webharebackend;

  //Ensure it points to an interface webserver
  INTEGER setoutputweb := backendsite->outputweb;
  RECORD destoutput := SELECT * FROM system.webservers WHERE type = 1 AND id = setoutputweb;
  IF(NOT RecordExists(destoutput))
  {
    RECORD bestmatch; //just pick any interface server
    bestmatch := SELECT * FROM system.webservers WHERE type=1 ORDER BY id;

    IF(RecordExists(bestmatch))
      setoutputweb := bestmatch.id;
  }

  backendsite->UpdateSiteMetadata(
      [ sitedesign := "tollium:webinterface"
      , outputfolder := "/"
      , outputweb := setoutputweb
      , locked := FALSE
      ]);
}
