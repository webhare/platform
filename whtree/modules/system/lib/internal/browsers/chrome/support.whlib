<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::float.whlib";

STRING FUNCTION RenderVariable(RECORD arg)
{
  STRING text;
  IF (arg.type = "string" )
    text := `${SubString(EncodeHSON(arg.value), 5)}`;
  ELSE IF (arg.type = "number")
  {
    IF (TypeID(arg.type) = TypeID(INTEGER))
      text := `${arg.value}`;
    ELSE IF(CellExists(arg,'value'))
    {
      text := FormatFloat(arg.value, 12);
      WHILE (text LIKE "*.*0" OR text LIKE "*.")
        text := Left(text, LENGTH(text) - 1);
    }
    ELSE IF(CellExists(arg,"unserializablevalue"))
    {
      text := arg.unserializablevalue;
    }
    ELSE
    {
      RETURN "<unable to decode variable>";
    }
  }
  ELSE IF (arg.type IN [ "boolean" ])
    text := `${arg.value ? "true" : "false"}`;
  ELSE IF (arg.type = "object")
  {
    IF (CellExists(arg, "subtype") AND arg.subtype = "array" AND CellExists(arg, "preview"))
    {
      STRING str := "[";
      INTEGER i := 0;
      FOR (; i < 5; i := i + 1)
      {
        RECORD prop := SELECT * FROM RECORD ARRAY(arg.preview.properties) WHERE name = ToString(i);
        IF (RecordExists(prop))
          str := str || (i = 0 ? " " : ", ") || prop.value;
        ELSE
          BREAK;
      }
      IF (RecordExists(SELECT * FROM RECORD ARRAY(arg.preview.properties) WHERE name = ToString(i)))
        str := str || ", ...";
      RETURN str || (i = 0 ? "]" : " ]");
    }
    ELSE IF (CellExists(arg, "subtype") AND arg.subtype = "error")
    {
      RETURN `[Object Error ("${UCTruncate(EncodeJava(arg.description), 80)}")]`;
    }
    ELSE IF (CellExists(arg, "CLASSNAME"))
      text := `[Object ${arg.classname}]`;
    ELSE IF (CellExists(arg, "subtype") AND arg.subtype = "null")
      text := `null`;
    ELSE
      text := `[Object]`;
  }
  ELSE
  {
    text := `?${arg.type}`;
  }
  RETURN text;
}

PUBLIC RECORD FUNCTION FormatChromeConsoleEntry(RECORD event)
{
  STRING line;
  FOREVERY (RECORD arg FROM event.args)
    line := `${line} ${RenderVariable(arg)}`;

  RECORD callframe := event.stacktrace.callframes[0];
  line := line || ` (${Tokenize(callframe.url, "/")[END-1]}:${callframe.linenumber})`;
  RETURN [ level := event.type
         , message := line
         , timestamp := MakeDatetimeFromUnixTimestamp(MONEY(event.timestamp / 1000))
         ];
}

PUBLIC RECORD FUNCTION FormatChromeExceptionEntry(RECORD event)
{
  event := EnforceStructure([ timestamp := 0i64
                            , exceptiondetails := [ "exception" := DEFAULT RECORD ]
                            ], event);

  RECORD exinfo := EnforceStructure([ description := "", value := "" ], event.exceptiondetails."exception");
  RETURN [ level := "exception"
         , message := exinfo.description ?? exinfo.value //uncauhgt promise rejections seem to use value, not description
         , timestamp := MakeDatetimeFromUnixTimestamp(MONEY(event.timestamp / 1000))
         ];
}
