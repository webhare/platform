<?wh
LOADLIB "wh::witty.whlib";
LOADLIB "wh::internet/urls.whlib";

LOADLIB "mod::publisher/lib/webdesign.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/webserver.whlib";
LOADLIB "mod::system/lib/internal/webserver/errorhandler-bare.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

STATIC OBJECTTYPE HarescriptErrorPage EXTEND DynamicPageBase
<
  RECORD harescriptinfo;
  STRING interfaceurl;

  MACRO NEW(RECORD harescriptinfo)
  {
    this->harescriptinfo := harescriptinfo;
    this->interfaceurl := GetPrimaryWebhareInterfaceURL();
  }

  RECORD FUNCTION GetResourceRef(RECORD resourceref)
  {
    //TODO we can probably simplify the PrintHSErrors&Co and remove this callback entirely now, as long as we don't leak it to non-dtapstage development
    RETURN resourceref;
  }

  UPDATE PUBLIC MACRO PrepareForRendering(OBJECT webdesign)
  {
    STRING ARRAY resources := this->harescriptinfo.resources CONCAT
        SELECT AS STRING ARRAY filename
          FROM this->harescriptinfo.allerrors
         WHERE filename LIKE "mod::*";

    webdesign->isstandardcontentpage := TRUE;
    webdesign->PrepareErrorPage(500, this->harescriptinfo, GetRequestURL());
    webdesign->pagetitle := "HareScript error";
    webdesign->pageconfig.favicon :=
        [ touchicon := GetModuleResourceURL("mod::tollium/webdesigns/webinterface/web/img/erroricon/apple-touch-icon.png")
        , icon32 := GetModuleResourceURL("mod::tollium/webdesigns/webinterface/web/img/erroricon/favicon-32x32.png")
        , icon16 := GetModuleResourceURL("mod::tollium/webdesigns/webinterface/web/img/erroricon/favicon-16x16.png")
        , maskicon := GetModuleResourceURL("mod::tollium/webdesigns/webinterface/web/img/erroricon/safari-pinned-tab.svg")
        , webmanifest := GetModuleResourceURL("mod::tollium/webdesigns/webinterface/web/img/erroricon/site.webmanifest")
        , browserconfig := GetModuleResourceURL("mod::tollium/webdesigns/webinterface/web/img/erroricon/browserconfig.xml")
        , iconcolor := "#e40015"
        , themecolor := "#4a4a4a"
        ];
    webdesign->AddResourceDependencies(resources);
  }

  UPDATE PUBLIC MACRO RunBody(OBJECT webdesign)
  {
    FUNCTION PTR getresourceinfo;
    IF(GetDtapStage() = "development")
      getresourceinfo := PTR this->GetResourceRef;
    RECORD wittydata := CELL[ ...this->harescriptinfo
                            , errors := PTR PrintHSErrors(this->harescriptinfo.allerrors, getresourceinfo)
                            , debuglink := "/.publisher/common/debug/?back=/" || EncodeURL(UnpackURL(GetRequestURL()).urlpath)
                            , isglobal := IsDebugTagEnabled("etr")
                            ];
    LoadWittylibrary(Resolve("mod::publisher/lib/internal/builtinpages/harescripterror.witty"),"HTML-NI")->Run(wittydata);
  }
>;

PUBLIC MACRO GenerateHarescriptErrorPage(RECORD harescriptinfo)
{
  NEW HarescriptErrorPage(harescriptinfo)->RunPageForWHFSId(whconstant_whfsid_webharebackend);
}
