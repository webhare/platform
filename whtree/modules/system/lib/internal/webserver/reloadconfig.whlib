﻿<?wh

LOADLIB "wh::crypto.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::internet/urls.whlib";
LOADLIB "wh::internet/webbrowser.whlib";
LOADLIB "wh::os.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/services.whlib";
LOADLIB "mod::system/lib/tasks.whlib";
LOADLIB "mod::system/lib/internal/dbschema.whlib";
LOADLIB "mod::system/lib/internal/webserver/config.whlib";
LOADLIB "mod::system/lib/internal/nginx/config.whlib";
LOADLIB "mod::system/lib/internal/whcore_interface.whlib";
LOADLIB "mod::system/lib/internal/webserver/confighelpers.whlib";

RECORD FUNCTION FixedAuthHandler(STRING password, STRING realm, STRING url)
{
  RETURN
      [ username := "webhare"
      , password := password
      ];
}

RECORD FUNCTION LoadConfig(RECORD configuration, BOOLEAN isuptodate)
{
  IF(NOT RecordExists(configuration))
    THROW NEW Exception("No valid configuration received");

  //Enforce the structure expected by the webserver fore more stability between upgrades - TODO also enforce structure for other subarrays than datastorage
  configuration := EnforceStructure(GetBaseWebserverConfigRecord(), configuration);

  FOREVERY(RECORD rule FROM configuration.rules)
  {
    rule := CELL[ ...__whs_baserulerecord, ...rule ];
    FOREVERY(RECORD datastorage FROM rule.datastorage)
      rule.datastorage[#datastorage] := CELL [ ...__whs_basedatastoragerecord, ...datastorage ];

    configuration.rules[#rule] := rule;
  }

  RECORD useconfiguration := configuration;
  IF(GetEnvironmentVariable("WEBHARE_WEBSERVER") = "node") //Get out of node's way
    DELETE FROM useconfiguration.ports WHERE id != whwebserverconfig_hstrustedportid;

  RECORD reloadstatus := ConfigureWebserver(useconfiguration);
  //count entries to give an impression how 'complete' this configuration set was
  reloadstatus := CELL[ ...reloadstatus
                      , numports := Length(useconfiguration.ports)
                      , numhosts := Length(useconfiguration.hosts)
                      , numrules := Length(useconfiguration.rules)
                      , numtypes := Length(useconfiguration.types)
                      ];

  //Do NOT just blindly talk to proxies when restarting - they should already be working from our previous config
  IF(isuptodate)
  {
    GetPrimary()->BeginWork();
    ScheduleTimedTask("system:update_proxies", [ allowmissing := TRUE]); //allowmissing as we may be invoked during WebHare's bootstrap
    GetPrimary()->CommitWOrk();
  }

  BroadcastEvent("system:internal.webserver.didconfigreload", DEFAULT RECORD);
  RETURN CELL [...configuration, version := GetWebhareVersionNumber(), reloadstatus];
}

/** Reloads the webserver configuration. Broadcasts are done in the getconfig.whscr runner
*/
PUBLIC RECORD FUNCTION ReloadConfig()
{
  OBJECT trans := OpenPrimary([ clientname := "webserver reloadconfig" ]);
  IF (IsDatabaseWritable())
    trans->BeginWork();
  RECORD config := DownloadWebserverConfig();
  IF (IsDatabaseWritable())
    trans->RollbackWork();

  RETURN LoadConfig(config, TRUE);
}

// Reloads a previously saved configuration
PUBLIC RECORD FUNCTION ReloadPreviousConfig(RECORD config)
{
  RETURN LoadConfig(config, FALSE);
}

PUBLIC MACRO ReconfigureProxies()
{
  IF (NOT IsDatabaseWritable())
    RETURN;

  OBJECT trans := GetPrimary();
  OBJECT lock := OpenLockManager()->LockMutex("system:webserver.proxyconfig");
  TRY
  {
    RECORD config := DownloadWebserverConfig();

    // Generate a new verification code for this registration round
    STRING data := GenerateUFS128BitId();
    trans->BeginWork();
    WriteRegistryKey("system.webserver.global.proxyservers.verificationcode", data);
    trans->CommitWork();

    STRING verificationurl := AddVariableToURL(GetPrimaryWebhareInterfaceURL(), "proxy_test", data);

    IF(RecordExists(SELECT FROM system_internal.proxies))
    {
      FOREVERY (RECORD rec FROM SELECT id, url, password, reverseaddress, hostswhitelist, lastset FROM system_internal.proxies ORDER BY Random(1, 1000))
      {
        RECORD reg := GenerateNginxProxyRegistrationRequest(config, rec.reverseaddress, verificationurl, rec.hostswhitelist, rec.id, rec.lastset);

        trans->BeginWork();
        OBJECT browser := NEW WebBrowser;
        LogRPCForWebbrowser("system:webservers.proxies", "register", browser);

        browser->onauth := PTR FixedAuthHandler(rec.password, #1, #2);
        browser->timeout := 120000;
        RECORD result := browser->InvokeJSONRPC(ResolveToAbsoluteURL(rec.url, "/admin/rpc"), "registerProxyClient", [ reg ]);
        IF (browser->GetHTTPStatusCode() IN [ 403, 404 ] AND NOT RecordExists(result))
        {
          // Support old URL scheme too
          result := browser->InvokeJSONRPC(ResolveToAbsoluteURL(rec.url, "/rpc"), "registerProxyClient", [ reg ]);
        }

        STRING type := RecordExists(result)
            ? result.success
                  ? "ok"
                  : "error"
            : browser->GetHTTPStatusCode() = 403
                  ? "password"
                  : "connecterror";

        UPDATE system_internal.proxies
           SET status :=                  type
             , lastregistration :=        GetCurrentDateTime()
             , lasterror :=               CellExists(result,'error') ? Left(result.error.message,4096) : ''
             , registrationresultdata :=  browser->content
             , proxy_ips :=               type != "ok" ? proxy_ips : Detokenize(STRING ARRAY(result.result.local_ips), ",")
         WHERE id = rec.id;

        trans->CommitWork();
        BroadcastEvent("system:webserver.proxies", DEFAULT RECORD);
        browser->Close();
      }
    }
  }
  FINALLY
  {
    lock->Close();
  }
}
