<?wh

LOADLIB "wh::adhoccache.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::filetypes/yaml.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/resources.whlib";

LOADLIB "mod::tollium/lib/componentbase.whlib";
LOADLIB "mod::tollium/lib/gettid.whlib";


RECORD FUNCTION LoadScreenDefs(STRING mod)
{
  STRING resname := `mod::${mod}/modulescreens.yml`;
  RECORD data := DecodeYAML(BlobToString(GetWebHareResource(resname)));
  RETURN [ value := data, ttl := 30 * 60 * 1000, eventmasks := GetResourceEventMasks([resname]) ];
}

RECORD FUNCTION GetScreenDef(STRING screenname)
{
  STRING mod := Tokenize(screenname,':')[0];
  RECORD screendefs := GetAdhocCached(CELL[mod], PTR LoadScreenDefs(mod));

  STRING name := Tokenize(screenname,':')[1];
  IF(NOT CellExists(screendefs, name))
    THROW NEW Exception(`Screen '${name}' not found in builtinscreens.yml`);

  RECORD screendef := Getcell(screendefs, name);
  IF(RecordExists(screendef))
  {
    screendef := EnforceStructure(
      [ extendtabs := [ component := ""
                      , insertpoints := STRING[]
                      , extendcomponents := STRING[]
                      ]
      , actioncategories := DEFAULT RECORD
      ], screendef);

    IF(RecordExists(screendef.actioncategories))
      screendef.actioncategories := RepackRecord(SELECT COLUMN name, value := EnforceStructure(CELL[ insertmenus := STRING[] ], value) FROM UnpackRecord(screendef.actioncategories));
  }
  RETURN screendef;
}

RECORD ARRAY FUNCTION FixAddActions(STRING resourcename, RECORD ARRAY addactions)
{
  addactions := EnforceStructure([[ title := "", onexecute := "", category := "" ]], addactions);
  UPDATE addactions
         SET title := title != "" ? ":" || title : ""
           , onexecute := MakeAbsoluteResourcePath(resourcename, onexecute);
  RETURN addactions;
}

RECORD FUNCTION StructureModuleYamlDefSettings(STRING resourcename, RECORD def)
{
  //This is getting a bit convoluted *especially* as long as we're only dealing with one field here.. we should start making an extract of this and deal with all trouble in JS
  IF(CellExists(def,'backendhooks') AND TYPEID(def.backendhooks) = TYPEID(RECORD ARRAY))
  {
    //This is the WH5.3 backendhook structure
    RECORD newbackendhooks;
    FOREVERY(RECORD row FROM def.backendhooks)
      IF(CellExists(row,"tag"))
        newbackendhooks := CellInsert(row,row.tag,CellDelete(row,"tag"));
     ELSE
        newbackendhooks := CellInsert(row,"hook" || #row,row);
    def := CELL[...def, backendhooks := newbackendhooks];
  }

  def := EnforceStructure(
    CELL[ backendhooks := DEFAULT RECORD
        ], def);

  RECORD ARRAY thehooks := EnforceStructure(
        [[ name := ""
         , value := [ screen := ""
                    , tabsextension := ""
                    , addactions := RECORD[]
                    ]
         ]
        ], UnpackRecord(def.backendhooks));

  FOREVERY(RECORD hook FROM thehooks)
  {
    thehooks[#hook].value.tabsextension := MakeAbsoluteResourcePath(resourcename, thehooks[#hook].value.tabsextension);
    thehooks[#hook].value.addactions := FixAddActions(resourcename, thehooks[#hook].value.addactions);
    INSERT CELL module := GetModuleNameFromResourcePath(resourcename) INTO thehooks[#hook].value;
  }

  def.backendhooks := RepackRecord(thehooks);

  RETURN CELL[ def.backendhooks ];
}

RECORD FUNCTION GetAllHooks()
{
  RECORD ARRAY allhooks;

  FOREVERY(STRING modulename FROM GetInstalledModuleNames())
  {
    STRING path := `mod::${modulename}/moduledefinition.yml`;
    RECORD def;

    BLOB moduledef := GetWebHareResource(path, [ allowmissing := TRUE ]);
    IF(Length(moduledef) != 0)
    {
      TRY
      {
        def := DecodeYAML(BlobToString(moduledef));
      }
      CATCH(OBJECT e) //Throwing on parse failures would cause vital screens to break. Some check/validation needs to pick this up instead
      {
        LogHarescriptException(e);
        CONTINUE;
      }
    }

    IF(NOT CellExists(def, 'backendhooks'))
      CONTINUE;

    def := StructureModuleYamlDefSettings(path, def);
    allhooks := allhooks CONCAT SELECT tag := name, ...value FROM UnpackRecord(def.backendhooks);
  }

  RETURN [ ttl := 24 * 60 * 60 * 1000
         , value := allhooks
         , eventmasks := ["system:modulesupdate"]
         ];
}

BOOLEAN FUNCTION MatchWhen(STRING module, RECORD when, OBJECT contexts)
{
  IF(CellExists(when,'wrdschema'))
  {
    IF(NOT ObjectExists(contexts->wrdschema) OR TYPEID(when.wrdschema) != TYPEID(STRING))
      RETURN FALSE;

    STRING schemaname := when.wrdschema;
    IF(schemaname NOT LIKE "*:*")
      schemaname := module || ":" || when.wrdschema;
    RETURN contexts->wrdschema->tag = schemaname;
  }

  RETURN TRUE;
}

RECORD ARRAY FUNCTION GetHooksFor(OBJECT contexts, STRING screenname)
{
  RECORD ARRAY allhooks := GetAdhocCached(CELL["allhooks"], PTR GetAllHooks);
  RETURN SELECT *
           FROM allhooks
          WHERE allhooks.screen = VAR screenname
                AND (CellExists(allhooks, 'when') ? MatchWhen(allhooks.module, allhooks.when, contexts) : TRUE);
}

MACRO InvokeWithContext(OBJECT contexts, MACRO PTR toinvoke)
{
  toinvoke(contexts);
}

PUBLIC MACRO SetupBackendScreenHooks(OBJECT screen, RECORD options)
{
  options := ValidateOptions(
    [ screenname := ""
    ], options, [ required := ["screenname" ]]);

  //Is anyone hooking this screen definition? Otherwise we don't have to set up anything
  //TODO but we should still do some static validation of hookdefs vs screedefs as not all hooks may be actually tested by the testsuite.
  RECORD ARRAY hooks := GetHooksFor(screen->contexts, options.screenname);
  IF(Length(hooks) = 0)
    RETURN;

  STRING ARRAY errors;

  RECORD screendef := GetScreenDef(options.screenname); //made an option, we might at some point just derive the screenname straight from builtinscreens
  IF(RecordExists(screendef.extendtabs) AND RecordExists(SELECT FROM hooks WHERE tabsextension != ""))
  {
    OBJECT tabcomp := GetMember(screen, "^" || screendef.extendtabs.component);
    FOREVERY(STRING insertpoint FROM screendef.extendtabs.insertpoints)
    {
      IF(NOT MemberExists(screen, "^" || insertpoint))
        THROW NEW Exception(`Insert point '${insertpoint}' not found in screen '${options.screenname}' - fix the screen or its modulescreens.yml definition`);
      INSERT [ name := insertpoint, component := GetMember(screen, "^" || insertpoint) ] INTO tabcomp->insertpoints AT END;
    }
    FOREVERY(STRING extendcomponent FROM screendef.extendtabs.extendcomponents)
    {
      IF(NOT MemberExists(screen, "^" || extendcomponent))
        THROW NEW Exception(`Extend component '${extendcomponent}' not found in screen '${options.screenname}' - fix the screen or its modulescreens.yml definition`);
      INSERT [ name := extendcomponent, component := GetMember(screen, "^" || extendcomponent) ] INTO tabcomp->extendcomponents AT END;
    }

    /* TODO
    IF(config.insertbefore != DEFAULT OBJECT)
    {
      INTEGER pos := SearchElement(tabs->pages, config.insertbefore);
      IF(pos = -1)
        THROW NEW Exception(`insertbefore tab is not a page of the tab`);

      tabs->extendposition := pos;
    }*/

    FOREVERY(RECORD hook FROM hooks)
      IF(hook.tabsextension != "")
        tabcomp->LoadTabsExtension(hook.tabsextension);
  }

  IF(RecordExists(screendef.actioncategories) AND RecordExists(SELECT FROM hooks WHERE Length(addactions) > 0))
  {
    FOREVERY(RECORD category FROM UnpackRecord(screendef.actioncategories))
    {
      OBJECT ARRAY insertmenus;
      FOREVERY(STRING insertmenu FROM category.value.insertinto)
        INSERT GetMember(screen, "^" || insertmenu) INTO insertmenus AT END;

      FOREVERY(RECORD hook FROM hooks)
        FOREVERY(RECORD action FROM hook.addactions)
        {
          IF(action.category = ToLowercase(category.name))
          {
            OBJECT actioncomp := screen->CreateTolliumComponent("action");
            FUNCTION PTR onexecute;
            TRY
            {
              onexecute := MakeFunctionPtr(action.onexecute);
            }
            CATCH(OBJECT e)
            {
              //TODO it would be nice to signal sysops on production servers without bothering all users
              LogHarescriptException(e);
              INSERT `Unable to register hook ${hook.module}:${ToLowercase(hook.tag)}: ${e->what}` INTO errors AT END;
              CONTINUE;
            }
            actioncomp->onexecute := PTR InvokeWithContext(screen->contexts, onexecute);

            FOREVERY(OBJECT insertmenu FROM insertmenus)
            {
              //Find the menu owning the inserted items TODO tollium should just track it.  and why does it allow item multi-insertion anyway ?
              OBJECT itemcomp := screen->CreateTolliumComponent("item");
              itemcomp->title := GetTid(action.title);
              itemcomp->action := actioncomp;

              OBJECT intomenu;
              FOREVERY(OBJECT item FROM screen->tolliumscreenmanager->GetSpecialComponents())
                IF(item->componenttype = "menuitem" AND RecordExists(SELECT FROM item->items WHERE menuitem=insertmenu))
                {
                  intomenu := item;
                  BREAK;
                }

              IF(NOT ObjectExists(intomenu))
                THROW NEW TolliumException(insertmenu, `Cannot find menu owning this item`);

              intomenu->InsertMenuItemBefore(itemcomp, insertmenu, FALSE);
            }
          }
        }
    }
  }

  IF(Length(errors) > 0 AND NOT IsDtapLive())
    screen->RunSimpleScreen("error", Detokenize(errors,'\n'));
}
