﻿<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::promise.whlib";
LOADLIB "wh::dbase/transaction.whlib";
LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::consilio/lib/internal/fetcher_linkcheck.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/services.whlib";
LOADLIB "mod::system/lib/internal/fsctypes.whlib";
LOADLIB "mod::system/lib/internal/services.whlib";
LOADLIB "mod::system/lib/internal/whfs/support.whlib";
LOADLIB "mod::system/lib/internal/whfs/compilables.whlib";

INTEGER counter;

STATIC OBJECTTYPE WHFSCommitHandler EXTEND TransactionFinishHandlerBase
< // ---------------------------------------------------------------------------
  //
  // Variables
  //

  RECORD ARRAY items;
  RECORD ARRAY completions;
  INTEGER ARRAY emptyupdates;
  INTEGER ARRAY reindexes; // reindex these objects explicitly, e.g. for invisible updates
  RECORD ARRAY analyzertasks;

  PUBLIC RECORD ARRAY historyitems;
  BOOLEAN checksitesettings;
  BOOLEAN scancompilables;

  INTEGER64 ARRAY linkcheckedremovedsettings;
  INTEGER64 ARRAY linkcheckedsettings;

  RECORD ARRAY deferredindexpromises;
  INTEGER eventcallback;

  // ---------------------------------------------------------------------------
  //
  // Helper functions
  //

  INTEGER FUNCTION GetEntry(INTEGER id, BOOLEAN ispub)
  {
    RECORD pos := RecordLowerBound(this->items, [ folder := id, ispub := ispub ], [ "FOLDER", "ISPUB" ]);
    IF (NOT pos.found)
    {
      INSERT
          [ folder :=           id
          , ispub :=            ispub
          , files :=            DEFAULT RECORD ARRAY
          , events :=           DEFAULT STRING ARRAY
          , sites :=            DEFAULT INTEGER ARRAY
          ] INTO this->items AT pos.position;
    }
    RETURN pos.position;
  }


  MACRO AddEvent(INTEGER site, INTEGER folderid, INTEGER fileid, STRING filemetatype, STRING event)
  {
    IF(IsDebugTagEnabled("que"))
      LogDebug("system:whfscommithandler", event, CELL[ site, folderid, fileid, trace := GetStackTrace() ]);

    // 'pub' events don't trigger publisher:site events, and have a different broadcast name
    INTEGER itempos := this->GetEntry(folderid, event = "pub");
    RECORD rec := this->items[itempos];

    IF(site != 0 AND site NOT in rec.sites AND event != "pub")
      INSERT site INTO this->items[itempos].sites AT END;

    IF (folderid = fileid)
    {
      IF (event NOT IN rec.events)
        INSERT event INTO this->items[itempos].events AT END;
    }
    ELSE
    {
      RECORD fpos := RecordLowerBound(rec.files, [ file := fileid ], [ "FILE" ]);
      IF (NOT fpos.found)
      {
        BOOLEAN isfolder;
        IF (IsValueSet(filemetatype))
          isfolder := filemetatype = "folder";
        ELSE
          isfolder := SELECT AS BOOLEAN COLUMN isfolder FROM system.fs_objects WHERE id = fileid;

        INSERT CELL[ file := fileid, isfolder, events := [ event ] ] INTO this->items[itempos].files AT fpos.position;
      }
      ELSE
        IF (event NOT IN rec.files[fpos.position].events)
          INSERT event INTO this->items[itempos].files[fpos.position].events AT END;
    }
  }

  MACRO AddAnalyzerTask(INTEGER folderid, BOOLEAN recursive)
  {
    IF(IsDebugTagEnabled("que"))
      LogDebug("system:whfscommithandler", "analyze", CELL[ folderid, recursive, trace := GetStackTrace() ]);

    RECORD pos := RecordLowerBound(this->analyzertasks, CELL[ folderid ], [ "FOLDERID" ]);
    IF (NOT pos.found)
      INSERT CELL[ action := "SCAN", folderid, recursive ] INTO this->analyzertasks AT pos.position;
    ELSE IF (recursive AND NOT this->analyzertasks[pos.position].recursive)
      this->analyzertasks[pos.position].recursive := TRUE;
  }

  // ---------------------------------------------------------------------------
  //
  // Overridden functions
  //

  UPDATE PUBLIC MACRO OnPreCommit(OBJECT transaction)
  {
    FOREVERY (INTEGER id FROM this->emptyupdates)
      PerformEmptyWHFSMetadataUpdate(id, [ republish := TRUE ]);
    IF (LENGTH(this->linkcheckedremovedsettings) != 0)
      DeleteCheckedObjectLinks(INTEGER64[], this->linkcheckedremovedsettings);
    IF (LENGTH(this->linkcheckedsettings) != 0)
      ProcessLinkCheckedSettings(this->linkcheckedsettings);

    IF(this->scancompilables)
      ScanAllCompilables(FALSE);
  }

  UPDATE PUBLIC MACRO OnRollback()
  {
    IF(IsDebugTagEnabled("que"))
      LogDebug("system:whfscommithandler", "rollback", CELL[ trace := GetStackTrace() ]);
    this->checksitesettings := FALSE;
  }

  OBJECT firstexception;
  RECORD ARRAY to_reindex;
  INTEGER ARRAY to_republish_ids;
  INTEGER ARRAY to_republish_parents;

  /** Called when the transaction has committed
  */
  UPDATE PUBLIC MACRO OnCommitBroadcasts()
  {
    IF(IsDebugTagEnabled("que"))
      LogDebug("system:whfscommithandler", "commitbroadcasts", CELL[trace := GetStackTrace() ]);

    //Script update completions must be handled before anything that could synchronously use them, and ideally, before even directly scheduled republished can run
    TRY
      ScanLimitedCompilables(SELECT AS INTEGER ARRAY DISTINCT id FROM this->completions WHERE type IN ["addfile","replacefile"]);
    CATCH (OBJECT e)
      this->firstexception := e;

    IF (uncommitted_fstypes_changes)
    {
      BroadcastEvent("system:whfs.types", DEFAULT RECORD);
      uncommitted_fstypes_changes := FALSE;
    }

    INTEGER ARRAY sites;
    FOREVERY (RECORD rec FROM this->items)
    {
      RECORD data :=
          [ folder :=   rec.folder
          , events :=   rec.events
          , files :=    rec.files
          ];

      IF (rec.ispub)
      {
        BroadcastEvent("publisher:publish.folder." || rec.folder, data);
      }
      ELSE
      {
        BroadcastEvent("system:whfs.folder." || rec.folder, data);
        sites := sites CONCAT rec.sites;

        IF ("del" IN data.events)
          INSERT
              [ id := data.folder
              , isfolder := TRUE
              , isdelete := TRUE
              , events := data.events
              ] INTO this->to_reindex AT END;

        IF ("fullrep" IN data.events)
          INSERT rec.folder INTO this->to_republish_parents AT END;

        FOREVERY (RECORD obj FROM data.files)
        {
          IF (ArraysIntersect(obj.events, [ "create", "update", "rename", "move", "del" ]))
            INSERT
                [ id := obj.file
                , isfolder := obj.isfolder
                , isdelete := "del" IN obj.events
                , events := obj.events
                ] INTO this->to_reindex AT END;

          IF("rep" IN obj.events AND NOT ArrayIsSetEqual(obj.events, [ "rep", "del" ]))
            INSERT obj.file INTO this->to_republish_ids AT END;
        }
      }
    }

    FOREVERY(INTEGER site FROM GetSortedSet(sites))
      BroadcastEvent("system:whfs.site." || site, DEFAULT RECORD);
  }

  /** Called when the transaction has committed
  */
  UPDATE PUBLIC MACRO OnCommit()
  {
    IF(IsDebugTagEnabled("que"))
      LogDebug("system:whfscommithandler", "commit", CELL[trace := GetStackTrace() ]);

    RECORD ARRAY to_republish;
    IF (IsValueSet(this->to_republish_parents))
    {
      to_republish := to_republish CONCAT
        SELECT id
             , priority :=    published%100000
             , lastpublishtime
          FROM system.fs_objects
         WHERE parent IN this->to_republish_parents
           AND IsQueuedForPublication(published);
    }

    IF (IsValueSet(this->to_republish_ids))
    {
      to_republish := to_republish CONCAT
        SELECT id
             , priority :=    published%100000
             , lastpublishtime
          FROM system.fs_objects
         WHERE id IN this->to_republish_ids
           AND IsQueuedForPublication(published);
    }

    IF(Length(to_republish) > 0)
    {
      OBJECT service := __OpenSynchronousWebHareService("publisher:publication");
      service->ScheduleMultiple(to_republish);
      service->CloseService();
    }

    IF(Length(this->completions) > 0 OR this->checksitesettings)
    {
      OBJECT eventcompletionlink := ConnectToGlobalIPCPort("system:eventcompletion");
      IF(ObjectExists(eventcompletionlink))
      {
        eventcompletionlink->SendMessage( CELL[ type := "newcompletions",  data := this->completions, this->checksitesettings ]);
        eventcompletionlink->Close();
      }

    }

    IF (Length(this->analyzertasks) > 0)
    {
      OBJECT service;
      TRY
      {
        service := __OpenSynchronousWebHareService("publisher:outputanalyzer");
        service->ScheduleMultiple(this->analyzertasks);
      }
      CATCH(OBJECT e)
      {
        //TODO we need never-failing persistent queues but until then, we may have races especially when WebHare is starting and outputanalyzer should self-recovery anyway if it misses something
        LogHarescriptException(e);
      }
      FINALLY
      {
        IF(ObjectExists(service))
          service->CloseService();
      }
    }

    IF (Length(this->to_reindex) > 0 OR Length(this->reindexes) > 0)
    {
      INTEGER ARRAY additional := ArrayDelete(this->reindexes, SELECT AS INTEGER ARRAY id FROM this->to_reindex);
      IF (Length(additional) > 0)
        this->to_reindex := this->to_reindex CONCAT
            SELECT id
                 , isfolder
                 , isdelete := FALSE
                 , events := [ "update" ]
              FROM system.fs_objects
             WHERE id IN additional
             ORDER BY id;

      counter := counter + 1;
      STRING eventid := `${GetCurrentGroupId()}.${counter}`;
      IF (Length(this->deferredindexpromises) > 0)
        this->eventcallback := RegisterEventCallback(`system:whfs.index.response.${eventid}`, PTR this->ResolveIndexPromises);

      // Refresh if there are waiters
      BroadcastEvent(`system:whfs.index.request.${eventid}`,
          [ to_reindex := this->to_reindex
          , refresh := Length(this->deferredindexpromises) > 0
          ]);
    }
    ELSE
    {
      FOREVERY (RECORD deferred FROM this->deferredindexpromises)
        deferred.resolve(DEFAULT RECORD);
    }

    IF (ObjectExists(this->firstexception))
      THROW this->firstexception;
  }

  MACRO ResolveIndexPromises(STRING event, RECORD msg)
  {
    IF(this->eventcallback != 0)
    {
      UnregisterCallback(this->eventcallback);
      this->eventcallback := 0;
    }
    FOREVERY (RECORD deferred FROM this->deferredindexpromises)
      deferred.resolve(msg);
  }


  // ---------------------------------------------------------------------------
  //
  // 'Public' API - (code outside core modules is still not supposed to interact with the commit handler)
  //

  PUBLIC MACRO FilePublicationFinished(INTEGER parentsite, INTEGER folderid, INTEGER fileid)
  {
    this->AddEvent(parentsite, folderid, fileid, "file", "pub");
  }

  PUBLIC MACRO FolderRepublish(INTEGER parentsite, INTEGER objectid)
  {
    this->AddEvent(parentsite, objectid, objectid, "folder", "fullrep");
  }

  PUBLIC MACRO FileRepublish(INTEGER parentsite, INTEGER folderid, INTEGER fileid)
  {
    this->AddEvent(parentsite, folderid, fileid, "file", "rep");
  }

  PUBLIC MACRO ObjectCreate(INTEGER parentsite, INTEGER folderid, INTEGER objectid)
  {
    this->AddEvent(parentsite, folderid, objectid, "", "create");
  }

  PUBLIC MACRO ObjectUpdate(INTEGER parentsite, INTEGER folderid, INTEGER objectid)
  {
    this->AddEvent(parentsite, folderid, objectid, "", "update");
  }

  PUBLIC MACRO ObjectRename(INTEGER parentsite, INTEGER folderid, INTEGER objectid)
  {
    BOOLEAN isfolder := SELECT AS BOOLEAN COLUMN isfolder FROM system.fs_objects WHERE id = objectid;
    this->AddEvent(parentsite, folderid, objectid, isfolder ? "folder" : "file", "rename");
    this->AddAnalyzerTask(folderid, isfolder);
  }

  PUBLIC MACRO ObjectMove(INTEGER parentsite, INTEGER folderid, INTEGER objectid)
  {
    BOOLEAN isfolder := SELECT AS BOOLEAN COLUMN isfolder FROM system.fs_objects WHERE id = objectid;
    this->AddEvent(parentsite, folderid, objectid, isfolder ? "folder" : "file", "move");
    this->AddAnalyzerTask(folderid, isfolder);
  }

  PUBLIC MACRO ObjectMoved(INTEGER parentsite, INTEGER folderid, INTEGER objectid)
  {
    this->AddEvent(parentsite, folderid, objectid, "", "moved");
  }

  PUBLIC MACRO ObjectDelete(INTEGER parentsite, INTEGER folderid, INTEGER objectid)
  {
    BOOLEAN isfolder := SELECT AS BOOLEAN COLUMN isfolder FROM system.fs_objects WHERE id = objectid;
    this->AddEvent(parentsite, folderid, objectid, isfolder ? "folder" : "file", "del");
    this->AddAnalyzerTask(folderid, isfolder);
  }

  PUBLIC MACRO Unpublished(INTEGER parentsite, INTEGER folderid, INTEGER objectid)
  {
    this->AddEvent(parentsite, folderid, objectid, "", "unp");
    this->AddAnalyzerTask(folderid, FALSE);
  }

  PUBLIC MACRO ObjectReordered(INTEGER parentsite, INTEGER folderid, INTEGER objectid)
  {
    this->AddEvent(parentsite, folderid, objectid, "", "order");
  }

  PUBLIC MACRO FolderIndexDocUpdated(INTEGER folderid)
  {
    this->AddAnalyzerTask(folderid, FALSE);
  }

  PUBLIC MACRO SiteUpdated(INTEGER siteid)
  {
    this->AddAnalyzerTask(siteid, TRUE);
  }

  PUBLIC MACRO AddCompletionEvent(RECORD data)
  {
    INSERT data INTO this->completions AT END;
  }

  PUBLIC MACRO TriggerSiteSettingsCheckOnCommit()
  {
    IF(this->checksitesettings)
      RETURN; //already set

    IF(IsDebugTagEnabled("que"))
      LogDebug("system:whfscommithandler", "triggersitesettingscheckoncommit", CELL[trace := GetStackTrace() ]);

    this->checksitesettings := TRUE;
  }
  PUBLIC MACRO TriggerScanCompilables()
  {
    this->scancompilables := TRUE;
  }

  PUBLIC MACRO RemoveLinkCheckedSettings(INTEGER64 ARRAY settingids)
  {
    this->linkcheckedremovedsettings := this->linkcheckedremovedsettings CONCAT settingids;
  }

  PUBLIC MACRO AddLinkCheckedSettings(INTEGER64 ARRAY settingids)
  {
    this->linkcheckedsettings := this->linkcheckedsettings CONCAT settingids;
  }

  PUBLIC MACRO FSTypesChanged()
  {
    uncommitted_fstypes_changes := TRUE;
  }

  PUBLIC OBJECT FUNCTION WaitForChangesIndexed()
  {
    RECORD deferred := CreateDeferredPromise();
    INSERT deferred INTO this->deferredindexpromises AT END;
    RETURN deferred.promise;
  }

  PUBLIC MACRO TriggerEmptyUpdateOnCommit(INTEGER fsobj)
  {
    RECORD pos := LowerBound(this->emptyupdates, fsobj);
    IF (NOT pos.found)
      INSERT fsobj INTO this->emptyupdates AT pos.position;
  }

  PUBLIC MACRO TriggerReindexOnCommit(INTEGER fsobj)
  {
    RECORD pos := LowerBound(this->reindexes, fsobj);
    IF (NOT pos.found)
      INSERT fsobj INTO this->reindexes AT pos.position;
  }

  PUBLIC MACRO DoDeleteOrRecycle(INTEGER parentsite, INTEGER folderid, INTEGER objectid, BOOLEAN is_recycle)
  {
    this->ObjectDelete(parentsite, folderid, objectid);

    IF(is_recycle)
    {
      //Create the recycle action FIXME break recursive dependency so we can can just invoke it

      RECORD res := MakeFunctionPtr(Resolve("service.whlib#RecycleWHFSObjects"))([ objectid ], "");
      FOREVERY(RECORD repub FROM res.repubbedfiles)
        this->FileRepublish(repub.parentsite, repub.parent, repub.file);
    }
    ELSE
    {
      DELETE FROM system.fs_objects WHERE id = objectid;
    }
  }
>;

PUBLIC OBJECT FUNCTION GetWHFSCommitHandler()
{
  OBJECT this_trans := __INTERNAL_GetSystemSchemaBinding();

  OBJECT handler := this_trans->GetFinishHandler("system:whfs");
  IF (NOT ObjectExists(handler))
  {
    handler := NEW WHFSCommitHandler;
    this_trans->SetFinishHandler("system:whfs", handler);
  }
  RETURN handler;
}

PUBLIC MACRO PerformEmptyWHFSMetadataUpdate(INTEGER fsobj, RECORD options)
{
  options := ValidateOptions(
      [ republish :=      FALSE
      ], options);

  RECORD oldversion :=
      SELECT id
           , parentsite
           , parent
           , published
           , type
           , isfolder
        FROM system.fs_objects
       WHERE id = fsobj;

  IF(NOT RecordExists(oldversion))
    RETURN;

  OBJECT commithandler := GetWHFSCommitHandler();

  INTEGER newpublished := oldversion.published;
  IF (IsPublish(oldversion.published) AND options.republish) //may need to republish
  {
    newpublished := ConvertToWillpublish(newpublished, FALSE, FALSE, PubPrio_DirectEdit);
    commithandler->FileRepublish(oldversion.parentsite, oldversion.parent, fsobj);
  }

  RECORD newmetadata :=
      [ modificationdate := GetCurrentDatetime()
      , modifiedby :=       GetEffectiveUserID()
      , published :=        newpublished
      ];

  UPDATE system.fs_objects SET RECORD newmetadata WHERE id = fsobj;
  commithandler->ObjectUpdate(oldversion.parentsite, oldversion.parent, fsobj);

  IF (NOT oldversion.isfolder)
    commithandler->AddCompletionEvent( [ type := "replacefile", id := fsobj ]);
}
