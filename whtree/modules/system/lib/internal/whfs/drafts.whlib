<?wh
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/internal/dbschema.whlib";
LOADLIB "mod::system/lib/internal/whfs/support.whlib" ;
LOADLIB "mod::system/lib/internal/whfs/contenttypes.whlib" ;
LOADLIB "mod::system/lib/internal/whfs/events.whlib";
LOADLIB "mod::system/lib/internal/whfs/objects.whlib" ;
LOADLIB "mod::system/lib/internal/whfs/history.whlib" ;
LOADLIB "mod::system/lib/internal/webhareconstants.whlib" ;

RECORD FUNCTION StringVersionToParts(STRING stringversion)
{
  STRING ARRAY toks := Tokenize(stringversion, '.');
  IF(Length(toks) != 2)
    RETURN DEFAULT RECORD;

  RETURN CELL[ major := ToInteger(toks[0], 0)
             , minor := ToInteger(toks[1], 0)
             ];
}

RECORD FUNCTION GetLastSnapshotVersion(INTEGER source)
{
  RECORD ARRAY currentversions := SELECT version, userdata
                                    FROM system_internal.fs_history
                                   WHERE fs_object = VAR source
                                         AND version != "";
  currentversions := SELECT *, parts := StringVersionToParts(version) FROM currentversions;
  RETURN (SELECT AS RECORD CELL[ parts.major
                               , parts.minor
                               , userdata := userdata LIKE "hson:*" ? DecodeHSON(userdata) : DEFAULT RECORD
                               ] FROM currentversions
                                WHERE RecordExists(parts)
                                ORDER parts.major DESC, parts.minor DESC
                                LIMIT 1)
         ?? [ userdata := DEFAULT RECORD, major := 0, minor := 0 ];
}


RECORD FUNCTION QuickCopy(INTEGER srcid, INTEGER instancesource, INTEGER destid, INTEGER versionparent, RECORD updates)
{
  RECORD orginfo := SELECT * FROM system.fs_objects WHERE id = srcid;
  RECORD currentinfo := MakeReplacedRecord(orginfo, updates);

  INSERT INTO system.fs_objects(id, parent, name
                , creationdate, modificationdate, type, filelink
                , isfolder, indexdoc, data, title, description
                , modifiedby, externallink, keywords, ispinned)
       VALUES(destid, versionparent, ToString(destid)
               , currentinfo.creationdate, currentinfo.modificationdate, currentinfo.type, currentinfo.filelink
               , currentinfo.isfolder, currentinfo.indexdoc, currentinfo.data, currentinfo.title, currentinfo.description
               , currentinfo.modifiedby, currentinfo.externallink, currentinfo.keywords, currentinfo.ispinned);

  BOOLEAN isindex := RecordExists(
        SELECT
          FROM system.fs_objects
         WHERE id = orginfo.parent
           AND indexdoc = srcid);

  FOREVERY(INTEGER cancopy FROM GetCopyableInstanceTypes(instancesource))
  {
    OBJECT typeobj := OpenWHFSTypeById(cancopy);
    typeobj->SetInstanceData(destid, typeobj->GetInstanceData(instancesource), [ ifreadonly := "update" ]);
  }

  OBJECT typeobj := OpenWHFSType("http://www.webhare.net/xmlns/publisher/draftmetadata");
  typeobj->SetInstancedata(destid,
      [ name :=       orginfo.name
      , parent :=     orginfo.isactive ? orginfo.parent : 0
      , flags :=      (IsPublish(orginfo.published) ? (DraftFlag_Publish + DraftFlag_WasPublished) : 0) +
                      (isindex ? DraftFlag_WasIndex : 0)
      ], [ ifreadonly := "update" ]);

  RETURN currentinfo;
}

MACRO RefreshDraftStatus(INTEGER fsobjectid)
{
  INTEGER publicdraft := SELECT AS INTEGER id FROM system.fs_objects WHERE filelink = fsobjectid AND parent = whconstant_whfsid_drafts;
  INTEGER curpublished := SELECT AS INTEGER published FROM system.fs_objects WHERE id = fsobjectid;
  IF((publicdraft != 0) != TestFlagFromPublished(curpublished, PublishedFlag_HasPublicDraft))
  {
    RECORD file := SELECT id, parent, parentsite FROM system.fs_objects WHERE id = fsobjectid;
    IF (RecordExists(file))
    {
      UPDATE system.fs_objects
         SET published := SetFlagsInPublished(published, PublishedFlag_HasPublicDraft, publicdraft != 0)
       WHERE id = fsobjectid;

      GetWHFSCommitHandler()->ObjectUpdate(file.parentsite, file.parent, fsobjectid);
    }
  }
}


/** @param requestinguser User requesting the drafts
    @param forsource For which source file (if 0, requesting all drafts for current user) */
RECORD ARRAY FUNCTION GetSomeDrafts(INTEGER requestinguser, INTEGER forsource)
{
  INTEGER me := GetEffectiveUserId();
  RECORD ARRAY versions := SELECT id
                                , ispublic := parent = whconstant_whfsid_drafts
                                , parent
                                , user := modifiedby
                                , mine := modifiedby = VAR me
                                , creationdate
                             FROM system.fs_objects
                            WHERE (forsource != 0 ? filelink = forsource : modifiedby = requestinguser)
                                  AND parent IN [whconstant_whfsid_autosaves,whconstant_whfsid_drafts]
                         ORDER BY creationdate;
  RETURN versions;
}


PUBLIC OBJECT FUNCTION OpenWHFSDraft(INTEGER draftid)
{
  RECORD versioninfo := SELECT id, parent, filelink FROM system.fs_objects WHERE id = draftid AND parent IN [whconstant_whfsid_autosaves,whconstant_whfsid_drafts,17];
  IF(NOT RecordExists(versioninfo))
    RETURN DEFAULT OBJECT;

  RETURN NEW WHFSDraft(versioninfo.id, versioninfo.parent = whconstant_whfsid_drafts, versioninfo.filelink);
}
PUBLIC RECORD ARRAY FUNCTION ListUserWHFSDrafts(INTEGER userid)
{
  RETURN GetSomeDrafts(userid, 0);
}

PUBLIC OBJECT FUNCTION __CloneDraftasPublicDraft(INTEGER draftid)
{
  INTEGER destid := MakeAutonumber(system.fs_objects, "id");
  RECORD currentinfo := SELECT * FROM system.fs_objects WHERE id = draftid;

  INSERT INTO system.fs_objects(id, parent, name
                , creationdate, modificationdate, type, filelink
                , isfolder, indexdoc, data, title, description
                , modifiedby, externallink, keywords, ispinned)
       VALUES(destid, whconstant_whfsid_drafts, ToString(destid)
               , currentinfo.creationdate, currentinfo.modificationdate, currentinfo.type, currentinfo.filelink
               , currentinfo.isfolder, currentinfo.indexdoc, currentinfo.data, currentinfo.title, currentinfo.description
               , currentinfo.modifiedby, currentinfo.externallink, currentinfo.keywords, currentinfo.ispinned);

  RECORD ARRAY copyableinstances := SELECT fs_types.id
                                      FROM system.fs_instances, system.fs_types
                                     WHERE fs_instances.fs_type = fs_types.id
                                           AND fs_instances.fs_object = draftid
                                           AND fs_types.cloneoncopy = TRUE;

  FOREVERY(RECORD cancopy FROM copyableinstances) //ADDME should orphan data be cloned too ?
  {
    OBJECT typeobj := OpenWHFSTypeById(cancopy.id);
    typeobj->SetInstanceData(destid, typeobj->GetInstanceData(draftid), [ ifreadonly := "update" ]);
  }

  OBJECT typeobj := OpenWHFSType("http://www.webhare.net/xmlns/publisher/draftmetadata");
  typeobj->SetInstancedata(destid, typeobj->GetInstanceData(draftid), [ ifreadonly := "update" ]);

  RETURN OpenWHFSDraft(destid);
}

STATIC OBJECTTYPE WHFSDraft
<
  OBJECT pvt_fsobject;
  BOOLEAN pvt_ispublic;
  INTEGER pvt_source;

  PUBLIC PROPERTY ispublic(pvt_ispublic, -);

  //TODO get rid of when, user and fsobject. They're caches and already hid some bugs in the versioning code, but we should probably wait until all Versioning code is removed - 90% of fsobject references are there.
  PUBLIC PROPERTY fsobject(pvt_fsobject,-);

  PUBLIC PROPERTY id(pvt_fsobject->id, -);

  PUBLIC PROPERTY source(pvt_source, -);

  PUBLIC PROPERTY when(this->pvt_fsobject->creationdate, -);

  PUBLIC PROPERTY user(this->pvt_fsobject->modifiedby, -);

  MACRO NEW(INTEGER draftid, BOOLEAN ispublic, INTEGER source)
  {
    this->pvt_ispublic := ispublic;
    this->pvt_fsobject := OpenWHFSObject(draftid);
    this->pvt_source := source;
  }
  MACRO UnlinkThis()
  {
    this->pvt_ispublic := FALSE;
    this->pvt_fsobject := DEFAULT OBJECT;
    this->pvt_source := 0;
  }

  RECORD FUNCTION GetFilteredMetadata(INTEGER fileid, RECORD options)
  {
    RECORD basefields :=
        SELECT name, title, keywords, description, externallink
             , type, data
          FROM system.fs_objects
         WHERE id = fileid;

    IF (CellExists(options, "BASEFIELDS"))
    {
      RECORD fields;
      FOREVERY (STRING name FROM options.basefields)
        fields := CellInsert(fields, name, GetCell(basefields, name));
      basefields := fields;
    }

    RECORD ARRAY instances :=
        SELECT TEMPORARY type := OpenWHFSTypeById(fs_types.id)
             , type :=        type
             , data :=        type->__ExportInstanceData(fileid, DEFAULT OBJECT, TRUE)
             , namespace :=   namespace
          FROM system.fs_instances
             , system.fs_types
         WHERE fs_instances.fs_object = fileid
           AND fs_types.cloneoncopy = TRUE
           AND fs_types.id = fs_instances.fs_type
           AND (NOT CellExists(options, "CONTENTTYPES") OR namespace IN options.contenttypes)
      ORDER BY namespace;

    RETURN
        [ basefields :=   basefields
        , instances :=    instances
        ];
  }


  /** @short Save this draft to a new public draft
      @cell(string array) options.basefields If present, list of cells to save from fs_object
      @cell(string array) options.contenttypes If present, list of contenttypes to save
      @return The public draft into which this draft was merged */
  PUBLIC MACRO SaveAsNewPublicDraft(OBJECT user, RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    options := ValidateOptions([ basefields := STRING[]
                               , contenttypes := STRING[]
                               ], options);//, [ optional := ["BASEFIELDS", "CONTENTTYPES"] ]);

    IF(this->pvt_source = 0)
      THROW NEW Exception("This draft no longer exists");
    IF(this->ispublic)
      THROW NEW Exception("This draft is already public");

    INTEGER snapshotparent := EnsureSnapshotsFolder(SELECT AS INTEGER parentsite FROM system.fs_objects WHERE id = this->source);
    DATETIME now := GetCurrentDatetime();
    //Move older drafts to the snapshot folder for safe keeping
    UPDATE system.fs_objects SET parent := snapshotparent
                           WHERE filelink = this->source
                                 AND parent = whconstant_whfsid_drafts
                                 AND id != this->id;
    //Move our version to the draft folder
    UPDATE system.fs_objects SET parent := whconstant_whfsid_drafts
                               , modificationdate := now
                           WHERE id = this->id;
    DELETE FROM system.fs_objects WHERE parent = whconstant_whfsid_autosaves
                                    AND filelink = this->source;

    //Copy the unmanaged instances into the draft (as the autosave may be behind on those props)
    //Which types should not be copied?
    INTEGER ARRAY managedtypes;
    FOREVERY(STRING ns FROM options.contenttypes)
      INSERT OpenWHFSType(ns)->id INTO managedtypes AT END;

    //Merge the unmanaged copyable fields from the source
    FOREVERY(INTEGER cancopy FROM GetCopyableInstanceTypes(this->source))
      IF(cancopy NOT IN managedtypes)
      {
        OBJECT typeobj := OpenWHFSTypeById(cancopy);
        typeobj->SetInstanceData(this->id, typeobj->GetInstanceData(this->source), [ isvisibleedit := FALSE ]);
      }

    //And reapply managed fields
    RECORD sourcedata := SELECT data, title, description, keywords FROM system.fs_objects WHERE id = this->source;
    UPDATE system.fs_objects SET RECORD CELL[...sourcedata, ...PickCells(sourcedata, options.basefields), modificationdate := now ] WHERE id = this->id;

    //TODO require a locked transaction?
    //Get next version#
    RECORD curversion := GetLastSnapshotVersion(this->source);

    INSERT CELL [ fs_object := this->source
                , user := user->authobjectid
                , userdata := EncodeHSON(user->GetUserDataForLogging())
                , type := whconstant_historytype_saved
                , when := now
                , snapshot := this->id
                , version := `${curversion.major}.${curversion.minor + 1}`
                ] INTO system_internal.fs_history;

    RefreshDraftStatus(this->source);
    this->pvt_ispublic := TRUE;
  }

  /** @short Save this draft to a public draft
      @param options
      @cell(stringarray) options.basefields If present, list of cells to save from fs_object
      @cell(stringarray) options.contenttypes If present, list of contenttypes to save
      @return The public draft into which this draft was merged */
  PUBLIC OBJECT FUNCTION SaveToPublicDraft(RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    options := ValidateOptions([ basefields := STRING[]
                               , contenttypes := STRING[]
                               ], options, [ optional := ["BASEFIELDS", "CONTENTTYPES"] ]);
    IF(this->pvt_source = 0)
      THROW NEW Exception("This draft no longer exists");

    OBJECT publicdraft :=
        SELECT AS OBJECT OpenWHFSDraft(id)
          FROM system.fs_objects
         WHERE parent = whconstant_whfsid_drafts
           AND filelink = this->pvt_source;

    IF (NOT ObjectExists(publicdraft))
      THROW NEW Exception("No public draft exists"); // FIXME: should we create one?

    //Copy our data back to the draft object object
    OBJECT final := publicdraft->fsobject;
    RECORD new_metadata := this->GetFilteredMetadata(this->id, options);
    RECORD old_metadata := this->GetFilteredMetadata(final->id, options);
    final->UpdateMetadata(new_metadata.basefields, [ ifreadonly := "update" ]);
    FOREVERY (RECORD instance FROM new_metadata.instances)
    {
      instance.type->__ImportInstanceData(final->id, instance.data, DEFAULT OBJECT, TRUE, [ ifreadonly := "update" ]);
    }

    //delete copyable instance not present in the old list
    FOREVERY (RECORD instance FROM old_metadata.instances)
      IF (NOT RecordLowerBound(new_metadata.instances, instance, [ "NAMESPACE" ]).found)
      {
        instance.type->SetInstancedata(final->id, instance.type->defaultinstance, [ ifreadonly := "update" ]);
      }

    //Remove all drafts, except the public draft
    DELETE FROM system.fs_objects WHERE filelink = this->source AND parent IN [whconstant_whfsid_autosaves,whconstant_whfsid_drafts] AND id != publicdraft->id;
    RefreshDraftStatus(this->source);

    this->UnlinkThis(); //disable this object
    RETURN publicdraft;
  }

  /** @short Save this draft as the final version
      @param options
      @cell(stringarray) options.basefields If present, list of cells to save from fs_object
      @cell(stringarray) options.contenttypes If present, list of contenttypes to save */
  PUBLIC MACRO SaveAsFinal(OBJECT user, RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    IF(this->pvt_source = 0)
      THROW NEW Exception("This draft no longer exists");

    options := ValidateOptions(
        [ basefields :=       DEFAULT STRING ARRAY
        , contenttypes :=     DEFAULT STRING ARRAY
        , keeppublicdraft :=  FALSE
        ],
        options);

    DATETIME now := GetCurrentDatetime();
    //Move the draft and any older public drafts to snapshots for safe keeping
    INTEGER snapshotparent := EnsureSnapshotsFolder(SELECT AS INTEGER parentsite FROM system.fs_objects WHERE id = this->source);
    IF (NOT options.keeppublicdraft)
      UPDATE system.fs_objects SET parent := snapshotparent
                                 , modificationdate := now
                             WHERE id = this->id;

    UPDATE system.fs_objects SET parent := snapshotparent
                           WHERE filelink = this->source
                                 AND parent IN [ whconstant_whfsid_drafts, whconstant_whfsid_autosaves ];

    //Copy managed data from this the draft to the final
    INTEGER ARRAY managedtypes;
    FOREVERY(STRING ns FROM options.contenttypes)
    {
      OBJECT typeobj := OpenWHFSType(ns);
      typeobj->SetInstanceData(this->source, typeobj->GetInstanceData(this->id), [ isvisibleedit := FALSE ]);
    }

    //And reapply managed fields
    RECORD sourcedata := SELECT data, title, description, keywords FROM system.fs_objects WHERE id = this->id;
    RECORD updates := PickCells(sourcedata, options.basefields);
    OpenWHFSObject(this->source)->UpdateMetadata(CELL[...updates, modificationdate := now ]);

    RefreshDraftStatus(this->source);

    //Update version info (this is a copy of the other version code, but technically it should usually suffice to only look at the source file?)
    OBJECT versioninfotype := OpenWHFSType("http://www.webhare.net/xmlns/publisher/version");
    RECORD curversion := GetLastSnapshotVersion(this->source);
    STRING nextversion := `${curversion.major + 1}.0`;
    RECORD userinfo := user->GetUserDataForLogging();

    OpenWHFSType("http://www.webhare.net/xmlns/publisher/version")->SetInstancedata(this->source,
      [ version := nextversion
      , editor := curversion.userdata
      , publisher := userinfo
      , published := now
      ], [ isvisibleedit := FALSE ]);

    //Add history record
    INSERT CELL [ fs_object := this->source
                , user := user->authobjectid
                , userdata := EncodeHSON(userinfo)
                , type := whconstant_historytype_approved
                , when := now
                , version := nextversion
                , snapshot := this->id
                ] INTO system_internal.fs_history;

    IF (NOT options.keeppublicdraft OR NOT this->ispublic)
      this->UnlinkThis(); //disable this object
  }

  PUBLIC MACRO DeleteSelf()
  {
    IF(this->pvt_source = 0)
      THROW NEW Exception("This draft no longer exists");

    INTEGER sourceid;
    IF(this->ispublic)
      sourceid := SELECT AS INTEGER filelink FROM system.fs_objects WHERE id = this->id;

    DELETE FROM system.fs_objects WHERE id = this->id;
    IF(sourceid != 0)
      RefreshDraftStatus(sourceid);

    this->UnlinkThis();
  }

  /** Returns the event masks for broadcasts fired for this object
  */
  PUBLIC STRING ARRAY FUNCTION GetEventMasks()
  {
    RETURN this->fsobject->GetEventMasks();
  }
>;


/** Returns all drafts for this file
    @return List of drafts
    @cell(integer) return.id Id of the draft
    @cell(boolean) return.ispublic Whether the draft is a public draft
    @cell(integer) return.parent Parent folder of the draft
    @cell(integer) return.user User that created the draft
    @cell(boolean) return.mine Whether the current user created the draft
    @cell(datetime) return.creationdate Creation date of the draft
*/
PUBLIC RECORD ARRAY FUNCTION GetDrafts(INTEGER objid)
{
  RETURN GetSomeDrafts(GetEffectiveUserID(), objid);
}

PUBLIC OBJECT FUNCTION OpenDraft(INTEGER objid, INTEGER draftid)
{
  OBJECT draft := OpenWHFSDraft(draftid);
  RETURN ObjectExists(draft) AND draft->source = objid ? draft : DEFAULT OBJECT;
}

PUBLIC OBJECT FUNCTION CreateNewTemporary(OBJECT owner, INTEGER newparent, RECORD metadata)
{
  INTEGER destid := MakeAutonumber(system.fs_objects, "id");
  OBJECT tempfile := OpenWHFSObject(whconstant_whfsid_autosaves)->CreateFile(
    CELL[ ...metadata, name := ToString(destid), id := destid, modifiedby := owner->authobjectid, publish := FALSE ]);
  tempfile->SetInstanceData("http://www.webhare.net/xmlns/publisher/draftmetadata", [ parent := newparent ]);
  RETURN tempfile;
}

/** @short Create a new draft from this object
    @param whfsobject Object to which the draft is linked
    @param instancesource ID from which we should copy existing contenettype data, Usually whfsobject->id */
PUBLIC OBJECT FUNCTION CreateDraft(OBJECT whfsobject, INTEGER instancesource, BOOLEAN publicdraft, OBJECT __user DEFAULTSTO DEFAULT OBJECT)
{
  IF(whfsobject->isfolder)
    THROW NEW Exception("Cannot create a draft version from a folder");
  IF(NOT ObjectExists(__user))
    __user := GetEffectiveUser();
  IF(NOT ObjectExists(__user))
    THROW NEW Exception(`CreateDraft requires a user parameter if not available through GetEffectiveUser`);

  INTEGER versioning_site;
  IF (publicdraft)
  {
    // Test for public draft existence
    IF ((SELECT AS INTEGER id FROM system.fs_objects WHERE filelink = whfsobject->id AND parent = whconstant_whfsid_drafts) != 0)
      THROW NEW Exception("Cannot create a second public draft");

    RECORD file :=
        SELECT parent
             , parentsite
             , published
          FROM system.fs_objects
         WHERE id = whfsobject->id;

    IF (NOT TestFlagFromPublished(file.published, PublishedFlag_HasPublicDraft))
    {
      UPDATE system.fs_objects
         SET published := SetFlagsInPublished(published, PublishedFlag_HasPublicDraft, TRUE)
       WHERE id = whfsobject->id;

      GetWHFSCommitHandler()->ObjectUpdate(file.parentsite, file.parent, whfsobject->id);
    }
  }

  INTEGER destid := MakeAutonumber(system.fs_objects, "id");
  QuickCopy(whfsobject->id, instancesource, destid, publicdraft ? whconstant_whfsid_drafts : whconstant_whfsid_autosaves, [ modifiedby := __user->authobjectid, filelink := whfsobject->id, creationdate := GetCurrentDatetime() ]);

  IF (versioning_site != 0)
    GetPrimary()->BroadcastOnCommit("system:versionevent." || versioning_site || "." || whfsobject->id, [ type := "createdraft", live_file := whfsobject->id, draft := destid ]);

  OBJECT draft := NEW WHFSDraft(destid, publicdraft, whfsobject->id);
  RETURN draft;
}

/** @short Returns the public draft for this object, if it exists
    @param objid Id of the object
    @return(object %WHFSDraft) Public draft, if it exists
    @topic sitedev/whfs
    @public
    @loadlib mod::system/lib/whfs.whlib
*/
PUBLIC OBJECT FUNCTION GetPublicDraft(INTEGER objid)
{
  INTEGER draftid := SELECT AS INTEGER id FROM system.fs_objects WHERE filelink = objid AND parent = whconstant_whfsid_drafts;
  RETURN draftid = 0 ? DEFAULT OBJECT : NEW WHFSDraft(draftid, TRUE, objid);
}

/** Cleanup private drafts made by others */
PUBLIC MACRO CleanupOtherPrivateDrafts(INTEGER objid)
{
  DELETE FROM system.fs_objects WHERE filelink = VAR objid AND parent = whconstant_whfsid_autosaves AND modifiedby != GetEffectiveUserID();
}
/** Cleanup my drafts */
PUBLIC MACRO CleanupMyPrivateDrafts(INTEGER objid)
{
  DELETE FROM system.fs_objects WHERE filelink = VAR objid AND parent = whconstant_whfsid_autosaves AND modifiedby = GetEffectiveUserID();
}
