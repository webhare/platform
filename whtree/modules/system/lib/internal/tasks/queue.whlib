<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/tasks.whlib";
LOADLIB "mod::system/lib/internal/taskqueue.whlib";
LOADLIB "mod::system/lib/internal/whconfig.whlib";
LOADLIB "mod::system/lib/internal/tasks/recordslogfile.whlib";


PUBLIC CONSTANT INTEGER debuglevel_none := 0;
PUBLIC CONSTANT INTEGER debuglevel_error := 1;
PUBLIC CONSTANT INTEGER debuglevel_queue := 2;
PUBLIC CONSTANT INTEGER debuglevel_comm := 3;
PUBLIC CONSTANT INTEGER debuglevel_task := 4;
PUBLIC CONSTANT INTEGER debuglevel_all := 5;


PUBLIC INTEGER64 FUNCTION GetTimestamp()
{
  RETURN GetUnixTimestampMsecs(GetCurrentDateTime());
}

/** Work items for the persistent queue
*/
OBJECTTYPE PersistentQueueItem EXTEND QueueItem
< /// Task data
  PUBLIC RECORD taskdata;

  /// Id for the taskdata content
  PUBLIC STRING taskcontentid;

  /** Create a new work item
      @param queuekeeper Queue keeper for this item
      @param data Queue item data @includecelldef #QueueItem::NEW.initialdata
      @cell data.taskdata Queue task data
      @cell data.taskcontentid Queue task data content id (for deduplication)
  */
  MACRO NEW(OBJECT queuekeeper, RECORD data)
  : QueueItem(queuekeeper, CELL[ data.priority, data.queuename, data.scheduledate, data.onfinished ])
  {
    this->taskdata := data.taskdata;
    this->taskcontentid := data.taskcontentid;
  }
>;

/** Persistent, disk-based queue (saves the current state to disk)
*/
PUBLIC STATIC OBJECTTYPE PersistentDiskQueue
< // ---------------------------------------------------------------------------
  //
  // Variables
  //

  /// @type(object #QueueKeeper) Queue keeper
  OBJECT queue;

  /// Name for the used queue in the queue keeper
  STRING queuename;

  /// Task type
  STRING tasktype;

  /// Debug level
  INTEGER debuglevel;

  /// Whether to print debug messages to stdout
  BOOLEAN debugprint;

  /// Whether currently scheduling new work items
  BOOLEAN assigningwork;

  /// Running actions (promises for ephemeral tasks)
  OBJECT ARRAY running_actions;

  /** Active queue items
      @cell(string) taskcontentid Task content id
      @cell(integer64) id Item id
      @cell(object) item Item object
  */
  RECORD ARRAY queueitems;

  /// Rewrite the persistent log this nr of msecs after the first item finish
  INTEGER rewriteinterval;

  /// Callback id for rewrite
  INTEGER rewritecb;

  /// Object that keeps the persistent records log
  OBJECT recordslog;

  /// Whether the can accept new items
  BOOLEAN pvt_canaccept;

  /// Whether currently processing items
  BOOLEAN pvt_isactive;

  /// Statistics
  RECORD stats;

  /// @includecelldef schedulinginfo
  RECORD pvt_schedulinginfo;

  /** @type(record) Scheduling info
      @cell(integer) concurrenttasks Nr of tasks to run concurrently
      @cell(integer) timeout Timeout override (-1 to keep task default)
  */
  PUBLIC PROPERTY schedulinginfo(pvt_schedulinginfo, -);

  // ---------------------------------------------------------------------------
  //
  // Initialization
  //

  /** Create a new persistent queue
      @param tasktype Type of tasks (executed by ephemeral queue)
      @cell options.debuglevel Debugging level
      @cell(boolean) options.debugprint Print to stdout instead of debug log
      @cell(boolean) options.resetqueue If TRUE, ignore the stored queue when starting up
  */
  MACRO NEW(STRING tasktype, RECORD options)
  {
    options := ValidateOptions(
        [ debuglevel := debuglevel_none
        , debugprint := FALSE
        , resetqueue := FALSE
        ], options, [ passthrough := TRUE ]);

    this->queue := NEW QueueKeeper;
    this->queue->ongotrunnable := PTR this->AssignWork();
    this->queuename := tasktype;

    this->debuglevel := options.debuglevel;
    this->debugprint := options.debugprint;

    //FIXME: Check if there's no other PersistentQueue for this tasktype!
    this->tasktype := tasktype;

    this->rewriteinterval := 60 * 60 * 1000;

    STRING recordslogdir := GetModuleStorageRoot("system") || "persistentqueues/" || Substitute(this->tasktype, ":", "/") || "/";
    IF (NOT CreateDiskDirectoryRecursive(recordslogdir, TRUE))
      THROW NEW Exception(`Could not create persistent queue directory '${recordslogdir}'`);

    this->recordslog := NEW RecordsLogFile(MergePath(recordslogdir, "queuelog"));
    IF (options.resetqueue)
    {
      IF (this->debuglevel >= debuglevel_comm)
        this->DebugLog(`  Deleting record log file for reset`);
      this->recordslog->ClearLog();
    }

    this->SetSchedulingInfo([ concurrenttasks := 1 ]);
  }

  MACRO ActivateProcessing()
  {
    this->BeforeSchedulingStart();

    this->pvt_isactive := TRUE;
    this->AssignWork();
  }

  MACRO SetSchedulingInfo(RECORD data)
  {
    this->pvt_schedulinginfo := ValidateOptions(
        [ concurrenttasks :=  1
        , timeout := -1
        ], data);

    this->AssignWork();
  }

  MACRO GotIntervalRewrite()
  {
    this->rewritecb := 0;

    IF (NOT this->pvt_isactive)
      RETURN;

    this->RewriteRecordsLog();
  }

  MACRO GotItemFinished(OBJECT item)
  {
    IF (this->debuglevel >= debuglevel_all)
      this->DebugLog("  Item #" || item->id || " marked as finished");

    RECORD pos := RecordLowerBound(this->queueitems, CELL[ item->taskcontentid, item->id ], [ "TASKCONTENTID", "ID" ]);
    IF (pos.found)
      DELETE FROM this->queueitems AT pos.position;

    this->recordslog->AppendRecords([ CELL[ type := "finished", item->id, item->taskcontentid, item->taskdata ] ]);
    // Schedule rewrite after an hour
    IF (this->rewritecb = 0)
      this->rewritecb := RegisterTimedCallback(AddTimeToDate(this->rewriteinterval, GetCurrentDateTime()), PTR this->GotIntervalRewrite);

    this->AssignWork();
    this->HandleFinishedItemStats(item);
  }

  /// Check for new work, assign if work/workers available
  MACRO AssignWork()
  {
    IF (this->assigningwork OR NOT this->pvt_isactive)
      RETURN;
    this->assigningwork := TRUE;

    IF (this->debuglevel >= debuglevel_all)
      this->DebugLog("  Assigning work, currently active: " || LENGTH(this->running_actions));

    WHILE (LENGTH(this->running_actions) < this->pvt_schedulinginfo.concurrenttasks)
    {
      OBJECT item := this->queue->GetNextRunnable(this->queuename);
      IF (NOT ObjectExists(item))
        BREAK;

      item->stage := "running";
      this->RunTask(item);
    }

    this->assigningwork := FALSE;
  }

  ASYNC MACRO RunTask(OBJECT item)
  {
    OBJECT promise;
    TRY
    {
      IF (this->debuglevel >= debuglevel_queue)
        this->DebugLog(`> Running task #${item->id}`, EncodeHSON(item->taskdata));

      RECORD taskdata := CELL
          [ debug := this->debuglevel > debuglevel_task
          , task := item->taskdata
          ];
      RECORD taskoptions;

      IF (this->pvt_schedulinginfo.timeout >= 0)
        INSERT CELL timeout := this->pvt_schedulinginfo.timeout INTO taskoptions;

      promise := RunInSeparatePrimary(PTR ScheduleEphemeralTask(this->tasktype, taskdata, taskoptions));

      // No AWAITs before this point, this instruction needs to run synchronously with function invokation!
      INSERT promise INTO this->running_actions AT END;

      RECORD result := AWAIT promise;

      // Ignore everything when shutting down
      IF (NOT this->pvt_isactive)
        RETURN;

      item->stage := "finished";

      IF (this->debuglevel >= debuglevel_queue)
        this->DebugLog(`< Have result for task with id ${item->id}: ${EncodeHSON(result)}`);
    }
    CATCH (OBJECT error)
    {
      // Ignore everything when shutting down
      IF (NOT this->pvt_isactive)
        RETURN;

      item->stage := "finished";
      IF (this->debuglevel >= debuglevel_error)
        this->DebugLog(`! Error for task with id ${item->id}: ${error->what}`);

      this->HandleTaskError(item, error);
    }

    INTEGER pos := SearchElement(this->running_actions, promise);
    IF (pos >= 0)
      DELETE FROM this->running_actions AT pos;
  }

  // ---------------------------------------------------------------------------
  //
  // Helper functions
  //

  MACRO DebugLog(STRING message, VARIANT ARRAY ...debugargs)
  {
    IF (this->debugprint)
    {
      FOREVERY (VARIANT v FROM debugargs)
        IF (TypeID(v) = TypeID(STRING))
          message := message || ` ${v}`;
        ELSE
          message := message || ` ${EncodeHSON(v)}`;
      Print(message || "\n");
    }
    ELSE
      CallMacroPtrVA(PTR LogDebug, VARIANT[ "system:persistentqueue", this->tasktype, message, ...debugargs ]);
  }

  // ---------------------------------------------------------------------------
  //
  // Persistent queue
  //

  /** Import all records from the persistent queue
      @return Whether queue was succesfully read
  */
  BOOLEAN FUNCTION ReadQueue()
  {
    RECORD ARRAY items;
    RECORD stats;
    BOOLEAN readall;
    OBJECT itr := this->recordslog->ReadAllRecords();
    WHILE (TRUE)
    {
      RECORD rec;
      TRY
        rec := itr->Next();
      CATCH (OBJECT e)
      {
        LogHareScriptException(e);
        BREAK;
      }
      IF (rec.done)
      {
        readall := rec.value;
        BREAK;
      }

      IF(NOT CellExists(rec.value, "id"))
        DumpValue(rec);

      RECORD pos := RecordLowerBound(items, rec.value, [ "ID" ]);
      SWITCH (rec.value.type)
      {
        CASE "new"
        {
          IF (NOT CellExists(rec.value, "scheduledate"))
            INSERT CELL scheduledate := DEFAULT DATETIME INTO rec.value;
          INSERT CELL[ rec.value.taskcontentid, rec.value.taskdata, rec.value.id, rec.value.priority, rec.value.scheduledate ] INTO items AT pos.position;
        }
        CASE "update"
        {
          IF (pos.found)
            items[pos.position].priority := rec.value.priority;
        }
        CASE "finished"
        {
          IF (pos.found)
            DELETE FROM items AT pos.position;
        }
        CASE "stats"
        {
          stats := rec.value.stats;
        }
      }
    }

    IF (this->debuglevel >= debuglevel_all)
      this->DebugLog(`  Read ${LENGTH(items)} items from persistent records log\n`);

    this->ImportStats(stats);

    FOREVERY (RECORD rec FROM items)
      this->ImportTask(rec.taskdata, CELL[ rec.priority, writetolog := FALSE, rec.scheduledate ]);

    RETURN readall;
  }

  /// Rewrite the record log with only the current items
  MACRO RewriteRecordsLog()
  {
    RECORD ARRAY items :=
        SELECT type := "new"
             , id := item->id
             , taskcontentid := item->taskcontentid
             , taskdata := item->taskdata
             , priority := item->priority
          FROM this->queueitems;

    INSERT CELL[ type := "stats", id := 0i64, this->stats ] INTO items AT END;

    this->recordslog->Rewrite(items);
  }

  MACRO WriteUpdatedStats()
  {
    this->recordslog->AppendRecords([ CELL[ type := "stats", id := 0i64, this->stats ] ]);
  }

  // ---------------------------------------------------------------------------
  //
  // Update functions
  //

  /** Called just before scheduling starts, service state 'started' has been reached.
  */
  MACRO BeforeSchedulingStart()
  {
  }

  /** Returns scheduling info
      @return Scheduling info
      @cell(integer) return.concurrenttasks Concurrent tasks to run
      @cell(string) return.cluster Task cluster to run tasks on
  */
  RECORD FUNCTION GetSchedulingInfo()
  {
    RETURN
        [ maxtasks :=     1
        , cluster :=      ""
        ];
  }

  /** Returns a string that identifies the contents task (for coalescing re-schedules)
      @param task Task record
      @return Task content id
  */
  STRING FUNCTION GetTaskContentId(RECORD task)
  {
    RETURN `${task.id}`;
  }

  /** Update to try to rebuild the queue after a failure to correctly restore
      (if possible)
  */
  MACRO TryRebuildPersistentQueue()
  {
  }

  /** Update for custom error handling
      @param(object #PersistentQueueItem) item Queue item
      @param(object %Exception) error Exception
  */
  MACRO HandleTaskError(OBJECT item, OBJECT error)
  {
  }

  /** Import a task from the log. For processing when task structure has changed
      @param taskdata @includecelldef #PushTask.taskdata
      @param options @includecelldef #PushTask.options
  */
  MACRO ImportTask(RECORD taskdata, RECORD options)
  {
    this->PushTask(taskdata, options);
  }

  /** Import stats from the log. For processing when stats structure has changed
      @param stats New stats record
  */
  MACRO ImportStats(RECORD stats)
  {
    this->stats := stats;
  }

  MACRO HandleFinishedItemStats(OBJECT item)
  {
  }

  // ---------------------------------------------------------------------------
  //
  // Public API
  //

  /** Read the persistent queue and start processing
  */
  PUBLIC MACRO StartProcessing()
  {
    BOOLEAN queue_ok := this->ReadQueue();

    this->pvt_canaccept := TRUE;
    IF (NOT queue_ok)
      this->TryRebuildPersistentQueue();

    //Queues should be functional after 'started' and may not depend on any poststart initialization (that would cause WaitForPublishCompletion in poststartscripts to race)
    CreateWHServiceStatePromise("started")->Then(PTR this->ActivateProcessing);
  }

  /** Pushes a new task for processing
      @param taskdata Task data
      @cell options.writetolog Record this task in the log
      @cell options.priority Task prioriry
      @cell options.scheduledate Scheduled date
      @return Whether a new task was pushed (FALSE if only updated)
  */
  PUBLIC BOOLEAN FUNCTION PushTask(RECORD taskdata, RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    options := ValidateOptions(
          [ writetolog :=   TRUE // internal use only
          , priority :=     0
          , scheduledate := DEFAULT DATETIME
          ], options);

    IF (options.writetolog AND NOT this->pvt_canaccept)
      THROW NEW Exception(`Cannot accept new items yet!`);

    STRING taskcontentid := this->GetTaskContentId(taskdata);

    OBJECT ARRAY blockers;

    RECORD ARRAY items := RecordRange(this->queueitems, CELL[ taskcontentid ], [ "TASKCONTENTID" ]);
    FOREVERY (RECORD rec FROM items)
    {
      IF (rec.item->stage = "scheduled")
      {
        IF (this->debuglevel >= debuglevel_all)
          this->DebugLog(`  Found an scheduled item #${rec.item->id}, updating that item instead`);

        IF (rec.item->priority > options.priority)
        {
          rec.item->priority := options.priority;
          this->recordslog->AppendRecords([ CELL[ type := "update", rec.item->id, rec.item->taskcontentid, rec.item->priority ] ]);
        }
        RETURN FALSE;
      }
      INSERT rec.item INTO blockers AT END;
    }

    OBJECT item := NEW PersistentQueueItem(this->queue, CELL
        [ taskdata
        , options.priority
        , taskcontentid
        , queuename :=      this->queuename
        , options.scheduledate
        , onfinished :=     PTR this->GotItemFinished
        , stage :=          "new"
        ]);

    FOREVERY (OBJECT b FROM blockers)
      INSERT item INTO b->blocks AT END;

    item->stage := "scheduled";

    IF (this->debuglevel >= debuglevel_all)
    {
      STRING ARRAY blockerids := SELECT AS STRING ARRAY ToString(blocker->id) FROM ToRecordArray(blockers, "blocker");
      this->DebugLog("  Scheduled item #" || item->id || (IsValueSet(blockers) ? `, must run after ${Detokenize(blockerids, ", ")}` : ""));
    }

    RECORD rec := CELL
        [ taskcontentid
        , item
        , item->id
        ];
    INSERT rec INTO this->queueitems AT RecordUpperBound(this->queueitems, rec, [ "TASKCONTENTID", "ID" ]);

    IF (options.writetolog)
      this->recordslog->AppendRecords([ CELL[ type := "new", item->id, item->taskcontentid, item->taskdata, item->priority, item->scheduledate ] ]);

    RETURN TRUE;
  }

  /** Returns the current queue state
      @return State
      @cell(object array) return.running Running items
      @cell(object array) return.runnable Runnable items
      @cell(object array) return.timedwait Items scheduled in the future
  */
  PUBLIC RECORD FUNCTION GetState()
  {
    RECORD state := this->queue->GetState();
    RECORD queuedata :=
        SELECT runnable :=      (SELECT AS OBJECT ARRAY item FROM runnable)
             , timedwait :=     (SELECT AS OBJECT ARRAY item FROM timedwait)
          FROM state.queues
         WHERE queuename = this->queuename;

    RETURN CELL
        [ state.running
        , runnable :=         OBJECT[]
        , timedwait :=        OBJECT[]
        , ...queuedata
        , this->stats
        , queuestats :=       state.stats
        ];
  }

  /// Shutdown the service
  PUBLIC MACRO Shutdown()
  {
    IF (NOT this->pvt_isactive)
      RETURN;

    IF (this->debuglevel >= debuglevel_all)
      this->DebugLog(`  Shutting down`);

    this->pvt_canaccept := FALSE;
    this->pvt_isactive := FALSE;
    FOREVERY (OBJECT action FROM this->running_actions)
      action->Cancel();
    TerminateScript();
  }
>;

PUBLIC STATIC OBJECTTYPE PersistentQueueControllerBase
< OBJECT queue;

  MACRO NEW(OBJECT queue)
  {
    this->queue := queue;
  }

  /** Schedules an items
      @param taskdata Data for task to schedule @includecelldef #ScheduleMultiple.tasks
  */
  PUBLIC MACRO Schedule(RECORD taskdata)
  {
    this->ScheduleMultiple([ taskdata ]);
  }

  /** Schedules items
      @param tasks Data for tasks to schedule
      @cell(integer) tasks.priority Task priority. Defaults to 100, lower numbers will get executed first.
      @return Schedule result
      @cell return.scheduled Task data for items that added to the schedule
      @cell return.updated Task data for items that were already scheduled
  */
  PUBLIC RECORD FUNCTION ScheduleMultiple(RECORD ARRAY tasks)
  {
    RECORD retval := [ scheduled := RECORD[], updated := RECORD[] ];
    FOREVERY (RECORD taskdata FROM tasks)
    {
      taskdata := CELL[ priority := 100, ...taskdata ];
      BOOLEAN scheduled := this->queue->PushTask(CELL[ ...taskdata, DELETE priority ], CELL[ taskdata.priority ]);
      IF (scheduled)
        INSERT taskdata INTO retval.scheduled AT END;
      ELSE
        INSERT taskdata INTO retval.updated AT END;
    }
    RETURN retval;
  }

  PUBLIC RECORD FUNCTION GetState()
  {
    RECORD state := this->queue->GetState();
    RETURN
        [ status :=     "ok"
        , running :=    (SELECT AS RECORD ARRAY CELL[ item->taskdata, item->date_running ] FROM ToRecordArray(state.running, "ITEM"))
        , runnable :=   LENGTH(state.runnable)
        , timedwait :=  LENGTH(state.timedwait)
        , queuestats := state.queuestats
        ];
  }

  /** Shuts down the queue
  */
  PUBLIC MACRO Shutdown()
  {
    this->queue->Shutdown();
    TerminateScript();
  }
>;
