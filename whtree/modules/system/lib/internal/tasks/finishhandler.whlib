<?wh

LOADLIB "wh::dbase/transaction.whlib";
LOADLIB "wh::promise.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/services.whlib";

PUBLIC ASYNC MACRO AsyncExecuteEphemeralTasks(RECORD ARRAY tohandle)
{
  OBJECT taskservice;
  TRY
    taskservice := AWAIT OpenWebHareService("system:managedqueuemgr", [ arguments := VARIANT[ -1 ] ]);
  CATCH (OBJECT e)
  {
    FOREVERY(RECORD task FROM tohandle)
      task.reject(e);
    RETURN;
  }

  FOREVERY(RECORD task FROM tohandle)
  {
    RECORD data := task.taskdata;
    IF(CellExists(task.options,'AUXDATA'))
    {
      data := CELL[...data, ...task.options.auxdata];
      DELETE CELL auxdata FROM task.options;
    }

    taskservice->RunEphemeralTask(task.tasktype, data, task.priority, task.options)->Then(task.resolve, task.reject);
  }

  CreatePromiseAllSettled(SELECT AS OBJECT ARRAY promise FROM tohandle)->Then(PTR taskservice->CloseService());
}

STATIC OBJECTTYPE TasksFinishHandler EXTEND TransactionFinishHandlerBase
<
  RECORD ARRAY scheduledephemeraltasks;

  UPDATE PUBLIC MACRO OnRollback()
  {
    RECORD ARRAY tohandle := this->scheduledephemeraltasks;
    this->scheduledephemeraltasks := RECORD[];

    FOREVERY(RECORD task FROM tohandle)
      task.reject(NEW Exception("The transaction in which this task was queued did not commit"));
  }

  UPDATE PUBLIC MACRO OnCommit()
  {
    RECORD ARRAY tohandle := this->scheduledephemeraltasks;
    IF(Length(tohandle) = 0)
      RETURN;

    this->scheduledephemeraltasks := RECORD[];
    AsyncExecuteEphemeralTasks(tohandle);
  }

  /** Schedule an ephemeral task to run
      @param task Task data
      @cell task.tasktype Type of the task
      @cell task.taskdata Task data
      @cell task.priority Task priority
      @cell task.options @includecelldef mod::system/scripts/internal/managedqueuemgr.whscr#QueueMgrClientConnection::RunEphemeralTask.options
      @cell task.resolve Called with result
      @cell task.reject Called with exeception
  */
  PUBLIC MACRO AddEphemeralTask(RECORD task)
  {
    INSERT task INTo this->scheduledephemeraltasks AT END;
  }
>;

PUBLIC OBJECT FUNCTION GetTasksFinishHandler()
{
  OBJECT handler := GetPrimary()->GetFinishHandler("system:tasks");
  IF (NOT ObjectExists(handler))
  {
    handler := NEW TasksFinishHandler;
    GetPrimary()->SetFinishHandler("system:tasks", handler);
  }
  RETURN handler;
}
