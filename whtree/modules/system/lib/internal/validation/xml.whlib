<?wh

LOADLIB "wh::files.whlib";
LOADLIB "wh::xml/dom.whlib";
LOADLIB "wh::xml/internal/xpath.whlib";
LOADLIB "wh::xml/xsd.whlib";
LOADLIB "wh::util/algorithms.whlib";


LOADLIB "mod::system/lib/internal/validation/support.whlib";
LOADLIB "mod::system/lib/internal/resources.whlib";
LOADLIB "mod::system/lib/resources.whlib";

LOADLIB "mod::tollium/lib/internal/gettid.whlib";

RECORD ARRAY schemata;
RECORD ARRAY captured_tids;

MACRO OnTid(RECORD tiddata)
{
  INSERT tiddata INTO captured_tids AT END;
}

PUBLIC RECORD ARRAY FUNCTION RunAndExtractTids(FUNCTION PTR tocall)
{
  RECORD ARRAY save_captured_tids := captured_tids;
  MACRO PTR save_capture := onparsedtid;
  TRY
  {
    captured_tids := RECORD[];
    onparsedtid := PTR OnTid;
    tocall();
    RETURN captured_tids;
  }
  FINALLY
  {
    captured_tids := save_captured_tids;
    onparsedtid := save_capture;
  }
}


RECORD ARRAY FUNCTION GetXMLValidators()
{
  OBJECT resolver := NEW IndependentXPathNSResolver;
  resolver->AddNS("m", "http://www.webhare.net/xmlns/system/moduledefinition");

  RECORD ARRAY xmlfiletypes;
  FOREVERY (STRING module FROM GetInstalledModuleNames())
  {
    OBJECT doc;
    TRY
      doc := GetModuleDefinitionXML(module);
    CATCH
      CONTINUE;

    OBJECT ARRAY xmltypes := doc->GetEvaluatedElements(resolver, "/m:module/m:development/m:xmlfiletype | /m:module/m:validation/m:xmlfiletype");
    FOREVERY (OBJECT node FROM xmltypes)
    {
      RECORD filetype := [ module :=         module
                         , rootnodes :=      ParseXSList(node->GetAttribute("rootnodes"))
                         , namespaceuri :=   node->GetAttribute("namespaceuri")
                         , schemalocation := MakeAbsoluteResourcePath("mod::"||module||"/", node->GetAttribute("schemalocation"))
                         , validator :=      MakeAbsoluteResourcePath("mod::"||module||"/", node->GetAttribute("validator"))
                         ];

      IF(filetype.validator = "" AND node->GetAttribute("objectname") != "")
        filetype.validator := MakeAbsoluteResourcePath("mod::"||module||"/", node->GetAttribute("library") || "#" || node->GetAttribute("objectname"));

      INSERT filetype INTO xmlfiletypes AT END;
    }
  }
  RETURN xmlfiletypes;
}

PUBLIC STATIC OBJECTTYPE LegacyTestInfoValidator EXTEND XMLValidatorBase
<
  UPDATE PUBLIC MACRO ValidateDocument()
  {
    this->AddError(this->xml->documentelement, `You should add xmlns="http://www.webhare.net/xmlns/system/testinfo" to the root node of the tests`);
  }
>;

PUBLIC STATIC OBJECTTYPE TestInfoValidator EXTEND XMLValidatorBase
<
  UPDATE PUBLIC MACRO ValidateDocument()
  {
    IF(this->onlytids)
      RETURN;

    RECORD ARRAY unreferred := SELECT *
                                 FROM ReadWebHareResourceFolder(GetDirectoryFromPath(this->respath))
                                WHERE name != GetNameFromPath(this->respath)
                                      AND name NOT IN ["testdata","data","package.json","package-lock.json","node_modules"]
                            ORDER BY ToUppercase(name);

    FOREVERY(STRING toignore FROM ParseXSList(this->xml->documentelement->GetAttribute("ignoremasks")))
      DELETE FROM unreferred WHERE ToUppercase(name) LIKE ToUppercase(toignore);

    /* Quick and dirty filename gathering. It would be nicer if a real parser existed for testfiles, but runtest.whlib just
       enumerates nodes directly without parsing first (and we probably want to ignore Applicable anyway) */
    FOREVERY(OBJECT checknode FROM this->xml->GetElements("test[script]"))
      DELETE FROM unreferred WHERE name = checknode->GetAttribute("script");
    FOREVERY(OBJECT checknode FROM this->xml->GetElements("jstest[file]"))
      DELETE FROM unreferred WHERE name = checknode->GetAttribute("file");

    FOREVERY(OBJECT checknode FROM this->xml->GetElements("test[path]"))
    {
      STRING strippath := checknode->GetAttribute("path");
      IF(strippath LIKE "?*/")
        strippath := Left(strippath, Length(strippath)-1); //delete trailing slash
      DELETE FROM unreferred WHERE name = strippath;
      IF (NOT RecordExists(
          SELECT
            FROM ReadWebHareResourceFolder(MergePath(GetDirectoryFromPath(this->respath), strippath))
           WHERE name = "testinfo.xml"))
        this->AddError(checknode, `File ${strippath}/testinfo.xml does not exist`);
    }

    FOREVERY(RECORD entry FROM unreferred)
      this->AddError(this->xml->documentelement, `Directory entry '${entry.name}' not referred in tests and not on ignoremasks list`);
  }
>;

PUBLIC RECORD FUNCTION DetermineXMLFileValidationType(OBJECT doc, STRING name)
{
  IF (LENGTH(schemata) = 0)
    schemata := GetXMLValidators();

  RETURN SELECT *
           FROM schemata
          WHERE doc->documentelement->namespaceuri = namespaceuri
                AND (doc->documentelement->localname ?? doc->documentelement->nodename) IN rootnodes;
}

PUBLIC RECORD FUNCTION RunXMLValidator(BLOB doc_blob, STRING resourcename, RECORD options)
{
  OBJECT doc := resourcename LIKE "*.xsd" ? MakeXMLSchema(doc_blob) : MakeXMLDocument(doc_blob);

  IF(Length(doc->GetParseErrors())>0)
  {
    RETURN [ errors := (SELECT message
                             , resourcename :=  COLUMN filename ?? VAR resourcename
                             , localname
                             , namespaceuri
                             , line
                             , col :=       0
                        FROM doc->GetParseErrors()) ];
  }

  RECORD srec := DetermineXMLFileValidationType(doc, resourcename);
  IF(NOT RecordExists(srec))
    RETURN DEFAULT RECORD; //can't validate anything else

  RECORD schemarec;
  RECORD ARRAY errors;
  STRING ARRAY eventmasks;
  IF(srec.schemalocation != "")
  {
    TRY
    {
      STRING useschema := srec.schemalocation;

      IF(useschema = "mod::system/whres/xml/xmlschema.xsd") //we might need a specialized parser for tollium or form component XSDs, and we can't detect that just by looking at the root element
      {
        STRING targetns := doc->documentelement->GetAttribute("targetNamespace");
        RECORD matchingcomponentsource := SELECT * FROM GetComponentSources("") WHERE namespace = targetns;
        IF(RecordExists(matchingcomponentsource))
        {
          IF(matchingcomponentsource.type = "tolliumcomponents")
            useschema := "mod::tollium/data/tolliumcomponents.xsd.xsd";
          ELSE IF(matchingcomponentsource.type = "formcomponents")
            useschema := "mod::publisher/data/forms/formcomponents.xsd.xsd";
        }
      }

      schemarec := __RetrieveCachedXMLSchema(useschema);
      eventmasks := GetResourceEventMasks(SELECT AS STRING ARRAY path FROM schemarec.files);
    }
    CATCH(OBJECT e)
    {
      //Blame XSD issues on the document element
      INSERT [[ message := e->what, line := doc->documentelement->linenum, col := 0, resourcename := VAR resourcename ]] INTO errors AT END;
    }
  }

  OBJECT validator := srec.validator != "" ? MakeObject(srec.validator, doc, resourcename) : NEW XMLValidatorBase(doc, resourcename);
  IF(ObjectExists(validator) AND NOT validator EXTENDSFROM XMLValidatorBase)
    THROW NEW Exception(`Validator '${srec.validator}' must derive from XMLValidatorBase`);

  IF(RecordExists(schemarec))
  {
    errors := schemarec.doc->ValidateDocument(doc);
    IF(Length(errors)>0)
    {
      RETURN [ errors := SELECT message
                              , line
                              , col :=     0
                              , resourcename := resourcename
                           FROM SimplifyXMLValidationErrors(errors, doc->documentelement)
             ];
    }
  }

  validator->onlytids := options.onlytids;

  RECORD ARRAY tids := RunAndExtractTids(PTR validator->ValidateDocument());
  errors := errors CONCAT validator->errors;

  FOREVERY(RECORD conflict FROM SELECT * FROM tids WHERE Length(conflicting_attributes) > 0)
    FOREVERY(STRING attr FROM conflict.conflicting_attributes)
      INSERT CELL[ conflict.resourcename, conflict.line, conflict.col, message := `Conflicting translation attributes '${conflict.attrname}' and '${attr}'` ] INTO errors AT END;

  tids := SELECT DISTINCT COLUMN resourcename, line, col, attrname, tid FROM tids ORDER BY line, col;
  RETURN CELL[ errors
             , warnings := validator->warnings
             , hints := validator->hints
             , icons := validator->icons
             , tids
             , eventmasks
             ];
}

PUBLIC STATIC OBJECTTYPE TopicValidator EXTEND XMLValidatorBase
<
  UPDATE PUBLIC MACRO ValidateDocument()
  {
    IF(this->onlytids)
      RETURN;

    STRING ARRAY docorder := ParseXSList(this->xml->documentelement->GetAttribute("docorder"));

    RECORD ARRAY realdocs :=
        SELECT name
             , basename :=      GetBasenameFromPath(name)
          FROM ReadWebHareResourceFolder(Left(this->respath, SearchLastSubString(this->respath, "/")))
         WHERE name LIKE "*.md"
           AND name != "intro.md";

    STRING ARRAY missing := GetSortedSet(ArrayDelete(docorder, SELECT AS STRING ARRAY basename FROM realdocs));
    STRING ARRAY notordered := SELECT AS STRING ARRAY DISTINCT basename FROM realdocs WHERE basename NOT IN docorder ORDER BY name;

    IF (LENGTH(missing) != 0)
      this->AddError(this->xml->documentelement, `The following documents mentioned in docorder could not be found: ${Detokenize(missing, ", ")}`);
    IF (LENGTH(notordered) != 0)
      this->AddError(this->xml->documentelement, `The following documents are not mentioned in docorder: ${Detokenize(notordered, ", ")}`);
  }
>;
