<?wh
LOADLIB "wh::datetime.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/mailer.whlib";

LOADLIB "mod::wrd/lib/internal/dbschema.whlib";
LOADLIB "mod::wrd/lib/internal/support.whlib";

CONSTANT STRING ARRAY badalgorithms := [ "MD5", "NETASP-SHA1", "LCR"];

STRING FUNCTION GetBadPWDAlgorithm(STRING rawdata)
{
  IF(rawdata LIKE "hson:*") //authsetting
  {
    //we only need to validate the last password (that's what VerifyPassword checked)
    RECORD authdata := EnforceStructure([ passwords := [[passwordhash := ""]]], DecodeHSON(rawdata));
    IF(Length(authdata.passwords) = 0)
      RETURN "";

    STRING alg := Tokenize(authdata.passwords[END-1].passwordhash, ':')[0];
    RETURN alg IN badalgorithms ? alg : "";
  }
  STRING alg := Tokenize(rawdata,':')[0];
  RETURN alg IN badalgorithms ? alg : "";
}

STRING FUNCTION DescribeAttribute(INTEGER attributeid)
{
  RECORD info := SELECT attributetag := attrs.tag, typetag := types.tag, schematag := schemas.name
                   FROM wrd.schemas, wrd.attrs, wrd.types
                  WHERE attrs.id = attributeid
                        AND attrs.type = types.id
                        AND types.wrd_schema = schemas.id;
  RETURN RecordExists(info) ? Tolowercase(`${info.schematag}.${info.typetag}.${info.attributetag}`) : `attr #${attributeid}`;
}

PUBLIC RECORD FUNCTION GetDeprecationScanReport()
{
  DATETIME now := GetCurrentDatetime();
  RECORD ARRAY date_fs_members := SELECT * FROM system.fs_members WHERE type = 4;
  STRING maxdatetime := GetDayCount(MAX_DATETIME) || ":" || GetMsecondCount(MAX_DATETIME);
  RECORD ARRAY fs_maxdatetime_settings := SELECT fs_member, num:=count(*)
                                            FROM system.fs_settings
                                           WHERE setting = maxdatetime
                                        GROUP BY fs_member;
  fs_maxdatetime_settings := SELECT date_fs_members.name, fs_maxdatetime_settings.num, namespace := (SELECT AS STRING namespace FROM system.fs_types WHERE id = date_fs_members.fs_type)
                               FROM date_fs_members, fs_maxdatetime_settings
                              WHERE fs_maxdatetime_settings.fs_member = date_fs_members.id;


  RECORD ARRAY emailmembers := SELECT attrs.id
                                 FROM wrd.attrs
                                WHERE attributetype = wrd_attributetype_email;

  RECORD ARRAY wrd_brokenemails := SELECT rawdata, attribute, num := COUNT(*)
                                     FROM wrd.entity_settings
                                    WHERE attribute IN (SELECT AS INTEGER ARRAY id FROM emailmembers)
                                          AND NOT IsValidModernEmailAddress(rawdata)
                                 GROUP BY rawdata, attribute;

  wrd_brokenemails := SELECT *, attributetag := DescribeAttribute(attribute) FROM wrd_brokenemails;

  RECORD ARRAY passwordmembers := SELECT id
                                    FROM wrd.attrs
                                   WHERE attributetype IN [ wrd_attributetype_password, wrd_attributetype_authenticationsettings ];

  RECORD ARRAY wrd_brokenpasswords := SELECT alg := GetBadPWDAlgorithm(ANY(rawdata)), attribute, num := COUNT(*)
                                        FROM wrd.entity_settings
                                       WHERE attribute IN (SELECT AS INTEGER ARRAY id FROM passwordmembers)
                                             AND GetBadPWDAlgorithm(rawdata) != ""
                                    GROUP BY GetBadPWDAlgorithm(rawdata), attribute;

  wrd_brokenpasswords := SELECT *, attributetag := DescribeAttribute(attribute) FROM wrd_brokenpasswords;

  RECORD ARRAY addressmembers := SELECT id
                                   FROM wrd.attrs
                                  WHERE attributetype IN [ wrd_attributetype_address ];

  RECORD ARRAY wrd_brokenaddresses := SELECT attribute, num := COUNT(*)
                                       FROM wrd.entity_settings
                                      WHERE attribute IN (SELECT AS INTEGER ARRAY id FROM addressmembers)
                                             AND rawdata LIKE "*\n*"
                                    GROUP BY attribute;

  wrd_brokenaddresses := SELECT *, attributetag := DescribeAttribute(attribute) FROM wrd_brokenaddresses;

  RECORD ARRAY datesmembers := SELECT id
                                   FROM wrd.attrs
                                  WHERE attributetype IN [ wrd_attributetype_datetime ];

  RECORD ARRAY wrd_brokendates := SELECT attribute, num := COUNT(*)
                                    FROM wrd.entity_settings
                                   WHERE attribute IN (SELECT AS INTEGER ARRAY id FROM datesmembers)
                                         AND rawdata ="2147483647,86399999"
                                GROUP BY attribute;

  wrd_brokendates := SELECT *, attributetag := DescribeAttribute(attribute) FROM wrd_brokendates;

  RETURN CELL[ creationdate := now
             , versioninfo := GetWebHareVersionInfo()
             , name :=        GetServerName()
             , fs_maxdatetime_settings
             , wrd_brokenemails
             , wrd_brokenpasswords
             , wrd_brokenaddresses
             , wrd_brokendates
             , numissues := Length(fs_maxdatetime_settings)
                            + Length(wrd_brokenemails)
                            + Length(wrd_brokenpasswords)
                            + Length(wrd_brokenaddresses)
                            + Length(wrd_brokendates)
             ];
}

PUBLIC MACRO PrintDeprecationScanReport(RECORD report)
{
  Print(`Deprecation scan generated ${EncodeJSON(report.creationdate)} on ${report.name} (${report.versioninfo.version}). ${report.numissues > 0 ? report.numissues || " issues found." : "No known deprecation issues"}\n`);

  IF(Length(report.fs_maxdatetime_settings) > 0)
    FOREVERY(RECORD toreport FROM report.fs_maxdatetime_settings)
      Print(`* ${toreport.namespace}#${toreport.name} has ${toreport.num} instances of MAX_DATETIME - this will not be supported in JavaScript. You should use DEFAULT DATETIME or 9999-12-31 as a workaround\n`);
  ELSE
    Print(`- Good! No fs_settings with a MAX_DATETIME value\n`);

  IF(Length(report.wrd_brokenemails) > 0)
    FOREVERY(RECORD wrd_brokenemail FROM report.wrd_brokenemails)
      Print(`* WRD Attribute #${wrd_brokenemail.attribute} (${wrd_brokenemail.attributetag}) contains ${wrd_brokenemail.num} instance(s) of a future unacceptable email address '${wrd_brokenemail.rawdata}'\n`);
  ELSE
    Print(`- Good! No WRD Email attribute values with an unacceptable email address\n`);

  IF(Length(report.wrd_brokenpasswords) > 0)
    FOREVERY(RECORD wrd_brokenpassword FROM report.wrd_brokenpasswords)
      Print(`* WRD Attribute #${wrd_brokenpassword.attribute} (${wrd_brokenpassword.attributetag}) contains ${wrd_brokenpassword.num} instance(s) of a future unacceptable password algorithm '${wrd_brokenpassword.alg}'\n`);
  ELSE
    Print(`- Good! No WRD Password attribute values with a broken hash algorithm\n`);

  IF(Length(report.wrd_brokendates) > 0)
    FOREVERY(RECORD wrd_brokendate FROM report.wrd_brokendates)
      Print(`* WRD Attribute #${wrd_brokendate.attribute} (${wrd_brokendate.attributetag}) contains ${wrd_brokendate.num} instance(s) of MAX_DATETIME - this will not be supported in JavaScript. You should use DEFAULT DATETIME or 9999-12-31 as a workaround\n`);
  ELSE
    Print(`- Good! No WRD attribute values with a MAX_DATETIME value\n`);

  IF(Length(report.wrd_brokenaddresses) > 0)
    FOREVERY(RECORD wrd_brokenaddress FROM report.wrd_brokenaddresses)
      Print(`* WRD Attribute #${wrd_brokenaddress.attribute} (${wrd_brokenaddress.attributetag}) contains ${wrd_brokenaddress.num} instance(s) of an old-style address. Run fixaddressfields?\n`);
  ELSE
    Print(`- Good! No old WRD Address attribute values\n`);
}
