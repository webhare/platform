﻿<?wh

/* The services in this file are protected by maxservertype="development" in the <service> definition
*/

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::devsupport.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::promise.whlib";
LOADLIB "wh::javascript.whlib";
LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::system/lib/internal/webhareconstants.whlib";
LOADLIB "mod::system/lib/testfw/runtest.whlib";
LOADLIB "mod::system/lib/internal/whconfig.whlib";
LOADLIB "mod::system/lib/internal/cluster/logwriter.whlib";


PUBLIC RECORD FUNCTION RPC_SubmitReport(STRING reportid, RECORD data)
{
  BroadcastEvent("system:jstest.report." || (reportid ?? "??"), data);

  //LogDebug("jstest-service", "report on " || reportid, data.finished);

  RETURN
      [ success := TRUE
      ];
}

PUBLIC RECORD FUNCTION RPC_SyncDevToolsRequest(STRING reportid, RECORD data)
{
  OBJECT eventmgr := NEW EventManager;
  eventmgr->RegisterInterest("system:jstest.devresponse." || reportid);

  BroadcastEvent("system:jstest.devtools." || (reportid ?? "??"), data);

  RECORD evt := WaitForPromise(eventmgr->AsyncReceiveEvent(AddTimeToDate(60000, GetCurrentdatetime())));
  RETURN
      [ success := TRUE
      ];
}

PUBLIC RECORD FUNCTION RPC_ReportJSError(RECORD details)
{
  IF (CellExists(details, "__wh_jstestinfo"))
  {
    OBJECT testfw := NEW RunTests;
    RECORD ARRAY tests := testfw->GatherAllTests([ STRING(details.__wh_jstestinfo.testname) ], DEFAULT STRING ARRAY);
    DELETE FROM tests WHERE type != "jstest";
    IF (LENGTH(tests) != 0)
    {
      STRING scriptpath := tests[0].testscript.scriptpath;
      INSERT CELL testscript := scriptpath INTO details;
    }
  }

  LogNoticeScriptErrorFromRecord("system:jstests", details);
  RETURN [ success := TRUE ];
}

PUBLIC RECORD FUNCTION RPC_JSTestLog(STRING reportid, RECORD ARRAY lines)
{
  BroadcastEvent("system:jstestlog", CELL[ reportid, lines ]);
  RETURN DEFAULT RECORD;
}

PUBLIC VARIANT FUNCTION RPC_Invoke(STRING libfunc, VARIANT ARRAY params)
{
  IF(GetDtapStage() != "development")
    THROW NEW Exception(`RPC_Invoke is only available on servers in development mode`);

  INTEGER ARRAY intypes;
  FOREVERY(VARIANT param FROM params)
    INSERT TypeID(param) INTO intypes AT END;

  STRING ARRAY toks := Tokenize(libfunc,'#');
  IF(toks[0] LIKE "*.ts")
  {
    RETURN CallJS("@mod-system/js/internal/tests/helpers#testInvoke", libfunc, params);
  }

  FUNCTION PTR funcptr := MakeFunctionPtr(toks[0] || "#TestInvoke_" || toks[1], -1, intypes);
  VARIANT result := CallAnyPtrVA(funcptr, params);
  IF (TypeID(result) = TypeID(OBJECT) AND result EXTENDSFROM PromiseBase)
    result := WaitForPromise(result);
  IF (TypeID(result) = TypeID(RECORD))
  {
    // Filter out anything inside the builtinmodules
    STRING ARRAY resources :=
        SELECT AS STRING ARRAY liburi
          FROM GetAllUsedHarescriptLibraries()
         WHERE liburi LIKE "mod::*/*"
           AND SubString(Tokenize(liburi, "/")[0], 5) NOT IN VAR whconstant_builtinmodules;

    result := CELL[ ...result
                  ,  __outputtoolsdata := CELL[ resources := GetSortedSet(resources)
                                              ]
                  ];
  }
  RETURN result;
}
