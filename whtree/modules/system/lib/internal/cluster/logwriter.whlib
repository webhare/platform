<?wh
(*ISSYSTEMLIBRARY*)

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::internal/interface.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";
LOADLIB "mod::system/lib/internal/whcore_interface.whlib";
LOADLIB "wh::system.whlib";

/** @short Log to a JSON log file
    @param logname Target log. Ensure it's configured with timestamps=false!
    @param data Data to log. The `@timestamp` will be added automatically */
PUBLIC MACRO LogToJSONLog(STRING logname, RECORD data)
{
  __SYSTEM_REMOTELOG(logname, EncodeJSON(CELL[ ...data, "@timestamp" := GetCurrentDatetime() ])); //avoid overwriting timestamp
}



/** Encodes an harescript exception for logging to the notice log. It logs the exception text, stack trace and the error context info set by SetErrorContextInfo.
    @param e Exception to log
    @param options
    @cell(boolean) options.addcurrentstack If present and FALSE, skip appending the stack trace to the logging function
    @cell(string) options.script If set, log this as the name of the script
    @cell options.info Extra info to log (will be hson encoded)
    @cell options.contextinfo If present, overrides the contextinfo set by SetErrorContextInfo.
*/
RECORD FUNCTION EncodeHarescriptException(OBJECT e, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  RECORD ARRAY errors;
  IF (e EXTENDSFROM HarescriptErrorException)
    errors := e->errors;
  ELSE IF (LENGTH(e->trace) > 0)
  {
    errors := [ MakeOverwrittenRecord(e->trace[0],
                               [ code := 251
                               , param1 := e->what
                               , param2 := ""
                               , message := "Exception: " || e->what
                               ])
              ];
  }

  // Augment trace with a trace to this function (nice to know where this was reported)
  RECORD ARRAY trace := e->trace;
  IF (NOT CellExists(options, "ADDCURRENTSTACK") OR NOT options.addcurrentstack)
    trace := trace CONCAT ArraySlice((SELECT filename, line, col, func FROM GetStackTrace()), 1);

  // Prefer non-builtin modules, then order
  STRING source :=
     (SELECT AS STRING TEMPORARY module := Tokenize(Substring(filename, 5), "/")[0]
           , `${module}:unknown`
        FROM (errors CONCAT trace) AS row
       WHERE filename LIKE "mod::*"
    ORDER BY module NOT IN whconstant_builtinmodules, #row) ?? "system:unknown";

  RECORD data := CELL
      [ warnings :=   DEFAULT RECORD ARRAY
      , message :=    (SELECT AS STRING message FROM errors)
      , errors :=     SELECT filename, line, "column" := col, message FROM errors where code >= 0
      , trace :=      SELECT filename, line, "column" := col, functionname := func FROM trace LIMIT 100
      , source
      ];

  IF (CellExists(options, "SCRIPT") AND options.script != "")
    INSERT CELL script := options.script INTO data;
  IF (CellExists(options, "INFO"))
    INSERT CELL info := options.info INTO data;

  RECORD contextinfo;
  IF (NOT CellExists(options, "CONTEXTINFO"))
    contextinfo := CellExists(GetAuthenticationRecord(), "CONTEXTINFO") ? DecodeJSON(GetAuthenticationRecord().contextinfo) : DEFAULT RECORD;
  ELSE
    contextinfo := options.contextinfo;
  IF (RecordExists(contextinfo))
    INSERT CELL contextinfo := contextinfo INTO data;

  // Keep error notices a bit small
  INTEGER budget := 100*1024 - LENGTH(EncodeJSON(data));
  FOREVERY (RECORD rec FROM data.errors)
  {
    IF (LENGTH(data.errors[#rec].message) > budget)
      data.errors[#rec].message := Left(data.errors[#rec].message, budget + 100) || "...";
    budget := budget - LENGTH(data.errors[#rec].message);
  }
  IF (LENGTH(data.message) > budget)
    data.message := Left(data.message, budget + 100) || "...";

  STRING encodeddata := EncodeJSON(data);
  IF(Length(encodeddata)>126*1024)
    RETURN DEFAULT RECORD;

  RETURN data;
}

PUBLIC MACRO LogNoticeScriptErrorFromRecord(STRING source, RECORD errordata)
{
  LogToJSONLog("system:notice", CELL
      [ type := "script-error"
      , source
      , ...errordata
      ]);
}

PUBLIC MACRO LogNoticeException(STRING type, STRING groupid, OBJECT exc, RECORD options)
{
  options := ValidateOptions(
      [ addcurrentstack :=    FALSE
      , script :=             ""
      , info :=               DEFAULT RECORD
      , contextinfo :=        DEFAULT RECORD
      , data :=               DEFAULT RECORD
      ], options);

  LogToJSONLog("system:notice", CELL
      [ type
      , groupid := groupid ?? __HS_GetCurrentGroupID()
      , session := GetExternalSessionData()
      , ...EncodeHarescriptException(exc, options)
      , ...(RecordExists(options.data) ? CELL[ options.data  ] : CELL[])
      ]);
}
