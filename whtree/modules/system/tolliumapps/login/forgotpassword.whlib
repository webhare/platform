<?wh

LOADLIB "wh::crypto.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::internet/urls.whlib";

LOADLIB "mod::tollium/lib/screenbase.whlib";
LOADLIB "mod::tollium/tolliumcomponents/authenticationsettings.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/screens/userrights/support.whlib";

LOADLIB "mod::wrd/lib/auth.whlib";
LOADLIB "mod::wrd/lib/auth/passwordpolicy.whlib";
LOADLIB "mod::wrd/lib/internal/auth/support.whlib";


PUBLIC STATIC OBJECTTYPE ForgotPassword EXTEND TolliumScreenBase
<
  MACRO Init()
  {
    IF(NOT ReadRegistryKey("system.backend.security.allowpasswordreset"))
    {
      this->tolliumresult := "cancel";
      RETURN;
    }
  }

  BOOLEAN FUNCTION OnEmailNext()
  {
    OBJECT work := this->BeginWork([validate := OBJECT[^email]]);
    IF(NOT work->Finish())
      RETURN FALSE;

    // Bake the currently selected language and transport override into the link
    STRING currenturl := UpdateURLVariables(this->contexts->controller->baseurl, CELL
        [ this->contexts->user->language
        , transport := (SELECT AS STRING value FROM this->contexts->controller->webvariables WHERE ToUppercase(name) = "TRANSPORT")
        ]);

    OBJECT wrdauth := this->contexts->controller->wrdauthplugin;
    wrdauth->HandleForgotPassword(^email->value, CELL[ currenturl ]);
    RETURN TRUE;
  }
>;

PUBLIC STATIC OBJECTTYPE ResetPassword EXTEND TolliumScreenBase
<
  RECORD ARRAY webvariables;
  OBJECT userentity;
  STRING ed;
  STRING verifier;
  STRING validationchecks;

  MACRO Init(RECORD data)
  {
    this->webvariables := this->contexts->controller->webvariables;
    IF (RecordExists(data.message) AND CellExists(data.message, "passwordresetlink"))
      this->webvariables := GetAllVariablesFromUrl(data.message.passwordresetlink);

    //If you get here, the policy was allowing password resets when you sent the request, so no need to double check it

    this->ed := SELECT AS STRING value FROM this->webvariables WHERE name = "_ed";
    this->verifier := SELECT AS STRING value FROM this->webvariables WHERE name = "verifier";
    RECORD result := this->contexts->controller->wrdauthplugin->ProcessVerificationLink(this->ed, this->verifier);

    OBJECT user := this->contexts->userapi->GetUser(result.entityid);
    IF(result.expired AND ObjectExists(user))
    {
      IF (this->RunSimpleScreen("question", this->GetTid(".linkisexpiredresend")) = "yes")
        this->ResendLink(user);
      this->tolliumresult := "cancel";
      RETURN;
    }

    IF(result.expired OR result.failed OR NOT ObjectExists(user))
    {
      this->RunSimpleScreen("error", this->GetTid(".linkisexpired"));
      this->tolliumresult := "cancel";
      RETURN;
    }

    IF (CellExists(result.data, "FC")) // failed password checks
    {
      ^warningpanel->visible := TRUE;
      ^warning_passwordmustbereset->visible := TRUE;
    }

    // set this->contexts->user
    __SetEffectiveUser(user);

    ^verificationcode->visible := result.success = FALSE;
    ^verificationcodeexplain->visible := ^verificationcode->visible;
    ^verificationcode->required := ^verificationcode->visible;

    OBJECT wrdschema := this->contexts->userapi->wrdschema;
    ^username->value := this->contexts->user->entity->GetField(wrdschema->accountlogintag);

    RECORD policy := GetPolicyForUser(this->contexts, result.entityid);
    this->validationchecks := policy.passwordvalidationchecks;
    RECORD ARRAY parsed := ParsePasswordChecks(policy.passwordvalidationchecks);

    INTEGER minlength :=
        (SELECT AS INTEGER value + 1
           FROM parsed
          WHERE check = "minlength") - 1;

    ^password->minlength := minlength > 0 ? minlength : -1;
    ^password->showcounter := minlength > 0;
    ^validationchecksfield->value := DescribePasswordChecks(policy.passwordvalidationchecks);
    ^validationchecksfield->visible := ^validationchecksfield->value != "";
  }

  MACRO ResendLink(OBJECT user)
  {
    // Bake the currently selected language into the link
    STRING currenturl := UpdateURLVariables(this->contexts->controller->baseurl, CELL
        [ language := (SELECT AS STRING value FROM this->webvariables WHERE name = "language")
        ]);

    OBJECT wrdauth := this->contexts->controller->wrdauthplugin;
    wrdauth->HandleForgotPassword(user->entity->GetField(wrdauth->wrdschema->accountlogintag), CELL[ currenturl ]);

    this->RunSimpleScreen("info", this->GetTid(".finished"));
  }

  BOOLEAN FUNCTION Submit()
  {
    // Execute checks first
    RECORD result := this->contexts->controller->wrdauthplugin->ProcessVerificationLink(this->ed, ^verificationcode->value ?? this->verifier);

    // Don't allow using a link that expired while filling in
    IF (result.expired AND this->RunSimpleScreen("question", this->GetTid(".linkisexpiredresend")) = "yes")
    {
      this->ResendLink(this->contexts->user);
      this->tolliumresult := "cancel";
      RETURN FALSE;
    }

    OBJECT wrdschema := this->contexts->userapi->wrdschema;
    OBJECT work := this->BeginWork();
    IF(^password->value != ^passwordrepeat->value AND ^passwordrepeat->value != "")
      work->AddErrorFor(^passwordrepeat, GetTid("tollium:common.errors.passwordmismatch"));
    ELSE IF (this->validationchecks != "")
    {
      RECORD authenticationsettings := this->contexts->user->entity->GetField(wrdschema->accountpasswordtag);
      RECORD rec := CheckPassword(this->validationchecks, ^password->value, CELL[ authenticationsettings ]);
      IF (NOT rec.success)
        work->AddErrorFor(^password, rec.message);
    }

    BOOLEAN fatalerror;
    IF(NOT result.success)
      IF(result.failed_incorrectverifier)
        work->AddErrorFor(^verificationcode, this->GetTid(".verifiermismatch"));
      ELSE
      {
        work->AddError(this->GetTid(".linkisexpired"));
        fatalerror := TRUE;
      }

    IF (NOT work->Finish())
      RETURN fatalerror;

    // Ask for authorization after the checks
    RECORD authorized := this->RunScreen("mod::wrd/tolliumapps/auth/backendauthorization.xml#backendauthorizationdialog", [ askpassword := FALSE ]);
    IF (NOT RecordExists(authorized) OR authorized.result != "ok")
      RETURN FALSE;

    // Apply the change
    work := this->BeginWork();
    RECORD settings := this->contexts->user->entity->GetField(wrdschema->accountpasswordtag) ?? GetDefaultAuthenticationSettings();
    INSERT
        [ passwordhash := CreateWebharePasswordHash(^password->value)
        , validfrom :=    GetCurrentDateTime()
        ] INTO settings.passwords AT END;

    this->contexts->user->entity->UpdateEntity(CellInsert(CELL[], wrdschema->accountpasswordtag, settings));

    work->AddWarning(this->GetTid(".passwordupdated"));

    // mark the verification link as used
    this->contexts->controller->wrdauthplugin->MarkVerificationLinkUsed(result.entityid, result);

    RECORD clientinfo := this->contexts->controller->GetClientInfo();
    WriteWRDAuditEvent(this->contexts->user->entity->wrdtype, "wrd:authsettings.setpassword", CELL
        [ account :=          this->contexts->user->entityid
        , login :=            this->contexts->user->login
        ]);

    IF (NOT work->Finish())
      RETURN FALSE;

    RECORD rec := CheckAuthenticationSettings(this->validationchecks, settings);
    IF (NOT rec.success AND "require2fa" IN rec.failedchecks)
    {
      RunManageTwoFAAuthDialog(this, this->contexts->user->entity, CELL
          [ requireauthorization :=   FALSE // just entered new password, we'll assume the user still knows it
          , this->validationchecks
          ]);
    }

    RETURN TRUE;
  }
>;

PUBLIC MACRO RunManageTwoFactor(OBJECT controller, RECORD data)
{
  RECORD ARRAY webvariables := controller->webvariables;
  IF (RecordExists(data.message) AND CellExists(data.message, "setuplink"))
    webvariables := GetAllVariablesFromUrl(data.message.setuplink);

  STRING s2 := SELECT AS STRING value FROM webvariables WHERE name = "_sr";
  RECORD result := controller->wrdauthplugin->ProcessSetupSecondFactorLink(s2);

  OBJECT user := controller->contexts->userapi->GetUser(result.entityid);
  IF(result.expired OR NOT result.success OR NOT ObjectExists(user))
  {
    controller->RunSimpleScreen("error", GetTid("system:tolliumapps.login.forgotpassword.linkisexpired"));
    RETURN;
  }

  // FIXME: make sure the challenge is only used once! 10 second lifetime is helping, but not enough.
  // hint: check if result.challenge matched the last challenge in the audit log, and log a used challenge too
  __SetEffectiveUser(user);

  RECORD policy := GetPolicyForUser(controller->contexts, result.entityid);

  RunManageTwoFAAuthDialog(controller, user->entity, CELL
      [ requireauthorization :=   TRUE
      , validationchecks := policy.passwordvalidationchecks
      ]);
}
