<?wh
LOADLIB "wh::internet/urls.whlib";

LOADLIB "mod::wrd/lib/api.whlib";
LOADLIB "mod::wrd/lib/screenbase.whlib";

LOADLIB "mod::tollium/lib/screenbase.whlib";
LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/internal/smtpmgr.whlib";

STRING FUNCTION HideSMTPPassword(STRING url)
{
  IF(url="")
    RETURN "";
  RECORD smtpurl := UnpackURL(url);
  if (smtpurl.password != "")
    smtpurl.password := "*";
  RETURN Substitute(RepackURL(smtpurl), ":%2A@", ":******@"); //undo the encoding, it's just presentation anyway
}

PUBLIC RECORD FUNCTION TestSMTP(STRING url)
{
  RECORD retval := [ success := FALSE
                   , error := ""
                   ];

  OBJECT smtpmgr := NEW WebHareSMTPManager;
  TRY
  {
    RECORD conninfo := smtpmgr->GetSMTPconnection(url);
    IF(NOT ObjectExists(conninfo.conn))
    {
      retval.error := conninfo.errors;
      RETURN retval;
    }

    //Try an outgoing mail.
    RECORD res;
    res := conninfo.conn->SendCommand("MAIL FROM: <>");
    IF(res.code != 250)
    {
      retval.error := "The empty MAIL FROM address was not accepted: " || res.code || ": " || res.message;
      RETURN retval;
    }

    res := conninfo.conn->SendCommand("RCPT TO: <info@pr1.pronuntio.com>");
    IF(res.code != 250)
    {
      retval.error := "Mail to an external address was not accepted: " || res.code || ": " || res.message;
      RETURN retval;
    }

    retval.success := TRUE;
    RETURN retval;
  }
  FINALLY
  {
    smtpmgr->Close();
  }
}


// FIXME: due to arrayedit controlling the rowkey's, upon deletion other rows will be selected
PUBLIC STATIC OBJECTTYPE MailRoutes EXTEND TolliumScreenBase
<
  RECORD ARRAY columns;

  STRING smtpserverurl;

  MACRO NEW()
  {
    this->contexts->wrdschema := OpenWRDSchema("system:config");
  }

  MACRO Init()
  {
    this->RefreshConfig();
  }

  MACRO RefreshConfig()
  {
    this->smtpserverurl := ReadRegistryKey("system.services.smtp.defaultmailroute");
    ^smtpserver->value := HideSMTPPassword(this->smtpserverurl);
    ^snsendpoint->value := GetSNSEndpoint();
    ^mailfrom->value := ReadRegistryKey("system.services.smtp.mailfrom");
  }

  MACRO DoMailConfig()
  {
    this->RunScreen("#mailconfig");
    this->RefreshConfig();
  }

  MACRO DoSenderConfig()
  {
    this->RunScreen("#senderconfig");
    this->RefreshConfig();
  }

  RECORD ARRAY FUNCTION OnMapRoutes(RECORD ARRAY routes)
  {
    RETURN SELECT *
                , sendermasks :=    Detokenize((SELECT AS STRING ARRAY mask FROM sendermasks),"\n")
                , recipientmasks := Detokenize((SELECT AS STRING ARRAY mask FROM recipientmasks),"\n")
                , serverurl :=      serverurl = "" ? this->GetTid('.defaultsmtp')
                                    : serverurl = "null" ? this->GetTid('.ignore')
                                    : HideSMTPPassword(serverurl)
          FROM routes;
  }

  RECORD ARRAY FUNCTION OnMapCorrections(RECORD ARRAY corrections)
  {
    RETURN SELECT *
                , masks := Detokenize((SELECT AS STRING ARRAY mask FROM masks),"\n")
                , target := block ? this->GetTid(".blocked") : rewriteto
             FROM corrections;
  }
>;


PUBLIC STATIC OBJECTTYPE MailConfig EXTEND TolliumScreenBase
<
  STRING smtpserverurl;

  MACRO Init()
  {
    this->smtpserverurl := ReadRegistryKey("system.services.smtp.defaultmailroute");
    IF (this->smtpserverurl != "")
    {
      RECORD rec := UnpackURL(this->smtpserverurl);
      ^port->value         := rec.port;
      ^authusername->value := rec.user;
      ^authpassword->value := rec.password;
      ^hostname->value     := rec.host;
    }
  }

  BOOLEAN FUNCTION Submit()
  {
    OBJECT work := this->BeginWork();
    IF (work->HasFailed())
      RETURN FALSE;

    RECORD rec := UnpackURL("smtp://example.net"); // get a record with all URL fields and protocol prefilled
    rec.host     := ^hostname->value;
    rec.port     := ^port->value;
    rec.user     := ^authusername->value;
    rec.password := ^authpassword->value;

    WriteRegistryKey("system.services.smtp.defaultmailroute", RepackURL(rec));

    RETURN work->Finish();
  }
>;

PUBLIC STATIC OBJECTTYPE SenderConfig EXTEND TolliumScreenBase
<
  MACRO Init()
  {
    ^usefornonwhitelisted->value := ReadRegistryKey("system.services.smtp.usemailfromfornonwhitelisted");
    ^mailfrom->value := ReadRegistryKey("system.services.smtp.mailfrom");
  }

  BOOLEAN FUNCTION Submit()
  {
    OBJECT work := this->BeginWork();
    IF (work->HasFailed())
      RETURN FALSE;

    WriteRegistryKey("system.services.smtp.mailfrom", ^mailfrom->value);
    WriteRegistryKey("system.services.smtp.usemailfromfornonwhitelisted", ^usefornonwhitelisted->value);
    RETURN work->Finish();
  }
>;


PUBLIC STATIC OBJECTTYPE EditSMTPServer EXTEND TolliumScreenBase
<
  PUBLIC STRING value;

  MACRO Init(RECORD data)
  {
    IF (data.value = "")
      RETURN;

    RECORD rec := UnpackURL(data.value);
    ^port->value         := rec.port;
    ^authusername->value := rec.user;
    ^authpassword->value := rec.password;
    ^hostname->value     := rec.host;
  }

  MACRO DoTestSMTPServer()
  {
    RECORD result := TestSMTP(this->GetValue());

    IF (result.success)
      this->RunSimpleScreen("info", this->GetTid(".smtp_test_success"));
    ELSE
      this->RunSimpleScreen("error", this->GetTid(".smtp_test_error", result.error));
  }

  STRING FUNCTION GetValue()
  {
    RECORD rec := UnpackURL("smtp://"); // get a record with all URL fields and protcol prefilled
    // RepackURL uses the schemspecificpart?
    rec.host     := ^hostname->value;
    rec.port     := ^port->value;
    rec.user     := ^authusername->value;
    rec.password := ^authpassword->value;
    RETURN RepackURL(rec);
  }

  BOOLEAN FUNCTION Submit()
  {
    OBJECT work := this->BeginWork();
    this->value := this->GetValue();
    RETURN work->Finish();
  }
>;


PUBLIC STATIC OBJECTTYPE EditMailRoute EXTEND WRDEntityEditScreenBase
<
  STRING smtpserverurl;
  STRING org_usealternativesmtpserver;

  UPDATE MACRO Init(RECORD params)
  {
    WRDEntityEditScreenBase::Init(params);
    IF(^entity->entityid = 0)  // Is this a new route?
    {
      ^usealternativesmtpserver->value := "default";
      ^entity->^sendermasks->value := [[ mask := "*" ]];
      ^entity->^recipientmasks->value := [[ mask := "*" ]];
    }
    this->org_usealternativesmtpserver := ^usealternativesmtpserver->value;
  }

  STRING FUNCTION GetRewriteMailAddress()
  {
    RETURN ^outgoingredirect->value ? ^outgoingmailaddress->value : "";
  }

  MACRO SetRewriteMailAddress(STRING address)
  {
    ^outgoingmailaddress->value := address;
    ^outgoingredirect->value := address != "";
  }

  STRING FUNCTION GetServerURL()
  {
    RETURN ^usealternativesmtpserver->value = "alternative"
            ? this->smtpserverurl
            : ^usealternativesmtpserver->value = "dontsend"
              ? "null"
              : "";
  }

  MACRO SetServerURL(STRING serverurl)
  {
    this->smtpserverurl := serverurl;
    ^usealternativesmtpserver->value := serverurl = ""
        ? "default"
        : serverurl = "null"
              ? "dontsend"
              : "alternative";

    this->GotSMTPServerChange();
    this->RefreshConfig();
  }

  MACRO GotSMTPServerChange()
  {
    ^outgoingredirect->enabled := ^usealternativesmtpserver->value != "dontsend";
  }

  // workaround so we can also support getting multiple rows back when editting a single row
  RECORD ARRAY FUNCTION DoEditMailRouteRule_From(RECORD row)
  {
    RETURN this->DoEditMailRouteRule(^masks_from, row);
  }

  RECORD ARRAY FUNCTION DoEditMailRouteRule_To(RECORD row)
  {
    RETURN this->DoEditMailRouteRule(^masks_to, row);
  }

  RECORD ARRAY FUNCTION DoEditMailRouteRule(OBJECT arrayedit, RECORD row)
  {
    OBJECT dialog := this->LoadScreen(".editmailrouterule", [ mask := RecordExists(row) ? row.mask : DEFAULT STRING ]);
    IF (dialog->RunModal() != "ok")
      RETURN DEFAULT RECORD ARRAY;

    // This serves two purposes:
    // A) workaround to support editting morphing a single row into multiple rows
    // B) to remove duplicates
    IF (RecordExists(row))
    {
      RECORD ARRAY rows := arrayedit->value;
      DELETE FROM rows WHERE mask = row.mask;
      rows := rows CONCAT ToRecordArray(dialog->value, "mask");
      arrayedit->value := SELECT DISTINCT(mask) FROM rows;

      RETURN DEFAULT RECORD ARRAY; // we handled it, make the <arrayedit> think we canceled editting
    }

    RETURN ToRecordArray(dialog->value, "mask"); // Return each (space separated) mask as separate row
  }

  MACRO DoEditSMTPServer()
  {
    OBJECT dialog := this->LoadScreen(".editsmtpserver", [ value := this->smtpserverurl ]);
    IF (dialog->RunModal() = "ok")
    {
      this->smtpserverurl := dialog->value;
      this->RefreshConfig();
    }
  }

  MACRO RefreshConfig()
  {
    ^serverurl->value := HideSMTPPassword(this->smtpserverurl);
  }

  UPDATE BOOLEAN FUNCTION Submit()
  {
    IF(NOT this->BeginFeedback()->Finish())
      RETURN FALSE;

    IF (this->org_usealternativesmtpserver != "dontsend" AND ^usealternativesmtpserver->value = "dontsend")
    {
      IF (this->RunSimpleScreen("confirm", this->GetTid(".suredontsendemail")) != "yes")
        RETURN FALSE;
    }

    RETURN WRDEntityEditScreenBase::Submit();
  }
>;


PUBLIC STATIC OBJECTTYPE EditMailRouteRule EXTEND TolliumScreenBase
<
  PUBLIC STRING ARRAY value;

  MACRO Init(RECORD data)
  {
    ^mask->value := data.mask;
  }

  BOOLEAN FUNCTION Submit()
  {
    OBJECT feedback := this->BeginFeedback();
    IF (feedback->HasFailed())
      RETURN feedback->Finish();

    this->value := ^mask->value != "" ? Tokenize(^mask->value, " ") : DEFAULT STRING ARRAY;

    RETURN feedback->Finish();
  }
>;

PUBLIC STATIC OBJECTTYPE EditCorrection EXTEND WRDEntityEditScreenBase
<
  UPDATE MACRO Validator(OBJECT work, RECORD updatevalue)
  {
    IF(NOT ^entity->^block->value AND Left(^entity->^rewriteto->value,2) != "*@")
      work->AddErrorFor(^entity->^rewriteto, this->GetTid(".invalidredirectto"));
  }
>;
