<?wh
LOADLIB "wh::ipc.whlib";
LOADLIB "mod::publisher/lib/siteapi.whlib";
LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/tasks.whlib";
LOADLIB "mod::system/lib/userrights.whlib";
LOADLIB "mod::system/lib/internal/whconfig.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";
LOADLIB "mod::system/lib/whfs.whlib";
LOADLIB "mod::tollium/lib/screenbase.whlib";
LOADLIB "mod::tollium/lib/internal/support.whlib";

LOADLIB "mod::wrd/lib/internal/auth/support.whlib";
LOADLIB "mod::wrd/lib/internal/wrdsettings.whlib";


PUBLIC STATIC OBJECTTYPE Settings EXTEND TolliumScreenBase
<
  RECORD ARRAY save_servertype_options;

  MACRO Init()
  {
    this->save_servertype_options := ^servertype->options;

    // general
    INTEGER curoutputweb := OpenSite(whconstant_whfsid_webharebackend)->outputweb;
    ^webhareurl->options := SELECT rowkey := id
                                 , title := baseurl
                                 , tolliumselected := id = VAR curoutputweb
                              FROM system.webservers
                             WHERE webservers.type = whconstant_webservertype_interface
                          ORDER BY ToUppercase(baseurl);

    ^servername->value := GetServerName();
    ^servertype->value := GetDTAPStage();
    ^displayname->value := ReadRegistryKey("system.backend.layout.title");
    ^checkipallowmask->value := ReadRegistryKey("system.backend.check.ipallowmask");
    ^allowpasswordreset->value := ReadRegistryKey("system.backend.security.allowpasswordreset");

    OBJECT dashboardbg := OpenWHFSObjectByPath("/webhare-private/system/backend/dashboardbackground");
    IF(ObjectExists(dashboardbg))
      ^dashboardbackground->value := dashboardbg->GetWrapped();

    OBJECT displayimage := OpenWHFSObjectByPath("/webhare-private/system/backend/tabbarimage");
    IF(ObjectExists(displayimage))
      ^displayimage->value := displayimage->GetWrapped();

    OBJECT loginbackground := OpenWHFSObjectByPath("/webhare-private/system/backend/loginbackground");
    IF(ObjectExists(loginbackground))
      ^loginbackground->value := loginbackground->GetWrapped();

    ^infotitle->value := ReadRegistryKey("system.backend.layout.infotitle");
    ^infotext->value := ReadRegistryKey("system.backend.layout.infotext");

    ^modulemgr->ReadFromRegistry();
    ^tweaks->ReadFromRegistry();
    ^proxy->ReadFromRegistry();
    ^security->ReadFromRegistry();
    ^wrdglobalconfig->ReadFromRegistry();
    ^tasks->ReadFromRegistry();
    ^smtp->ReadFromRegistry();
    ^geonames->ReadFromRegistry();
    ^geoip2->ReadFromRegistry();

    ^displaytype->value := RecordExists(^displayimage->value) ? "displayimage" : "displayname";

    OBJECT wrdschema := this->contexts->userapi->wrdschema;
    ^passwordvalidationchecks->value := GetWRDSetting(wrdschema, "password_validation_checks");

    STRING docroot := ReadRegistryKey("system.services.webhare.docroot");
    ^embeddeddocs->value := docroot != "";
    ^docroot->value := docroot ?? "https://docs.webhare.dev/embedded-documentation/";
  }

  STRING FUNCTION GotBrowseMaxAge(STRING value)
  {
    RETURN this->RunScreen("#selectduration", CELL[ value, withtime := FALSE ]);
  }

  STRING FUNCTION GotBrowseNoReuse(STRING value)
  {
    RETURN this->RunScreen("#selectduration", CELL[ value, withtime := FALSE ]);
  }

  STRING FUNCTION MapDuration(STRING duration)
  {
    RETURN GetDurationTitle(duration);
  }

  BOOLEAN FUNCTION Submit()
  {
    BOOLEAN recheckindex;
    OBJECT work := this->BeginWork();

    IF (work->HasFailed())
      RETURN work->Finish();

    IF(^servertype->enabled)
      WriteRegistryKey("system.global.servertype",^servertype->value);
    IF(^servername->enabled)
      WriteRegistryKey("system.global.servername", ^servername->value);
    WriteRegistryKey("system.backend.check.ipallowmask", ^checkipallowmask->value);
    WriteRegistryKey("system.backend.layout.title", ^displayname->value);

    IF(NOT work->HasFailed())
    {
      OBJECT systemfolder := OpenWHFSObject(whconstant_whfsid_private_system);
      OBJECT backendfolder := systemfolder->EnsureFolder([name := "backend"]);
      OBJECT bgfile := backendfolder->OpenByName("dashboardbackground");

      IF(ObjectExists(bgfile))
        IF(RecordExists(^dashboardbackground->value))
          bgfile->UpdateMetadata([ wrapped := ^dashboardbackground->value ]);
        ELSE
          bgfile->DeleteSelf();
      ELSE IF(RecordExists(^dashboardbackground->value))
        backendfolder->CreateFile([name := "dashboardbackground", type := 12, data := ^dashboardbackground->value.data]);

      OBJECT displayimage := backendfolder->OpenByName("tabbarimage");
      IF(ObjectExists(displayimage))
        IF(RecordExists(^displayimage->value))
          displayimage->UpdateMetadata([ wrapped := ^displayimage->value ]);
        ELSE
          displayimage->DeleteSelf();
      ELSE IF(RecordExists(^displayimage->value))
        backendfolder->CreateFile([name := "tabbarimage", type := 12, data := ^displayimage->value.data]);

      OBJECT loginbackground := backendfolder->OpenByName("loginbackground");
      IF(ObjectExists(loginbackground))
        IF(RecordExists(^loginbackground->value))
          loginbackground->UpdateMetadata([ wrapped := ^loginbackground->value ]);
        ELSE
          loginbackground->DeleteSelf();
      ELSE IF(RecordExists(^loginbackground->value))
        backendfolder->CreateFile([name := "loginbackground", type := 12, data := ^loginbackground->value.data]);

      WriteRegistryKey("system.backend.layout.infotitle", ^infotitle->value);
      WriteRegistryKey("system.backend.layout.infotext", ^infotext->value);

      GetPrimary()->RegisterCommitHandler("", PTR this->RefreshGLobalDashboard);
      WriteRegistryKey("system.backend.security.allowpasswordreset", ^allowpasswordreset->value);

      OpenSite(whconstant_whfsid_webharebackend)->SetPrimaryOutput(^webhareurl->value, "/");

      OBJECT wrdschema := GetPrimaryWebhareUserAPI()->wrdschema;
      SetWRDSetting(wrdschema, "password_validation_checks", ^passwordvalidationchecks->value);
    }

    ^modulemgr->WriteToRegistry();
    ^tweaks->WriteToRegistry();
    ^proxy->WriteToRegistry();
    ^security->WriteToRegistry();
    ^wrdglobalconfig->WriteToRegistry();
    ^tasks->WriteToRegistry();
    ^smtp->WriteToRegistry();
    ^geonames->WriteToRegistry();
    ^geoip2->WriteToRegistry();

    STRING setdocroot := ^embeddeddocs->value ? ^docroot->value : "";
    IF(setdocroot != ReadRegistryKey("system.services.webhare.docroot"))
    {
      WriteRegistryKey("system.services.webhare.docroot", setdocroot);
      ScheduleTimedTask("tollium:updateremotedocumentation");
    }

    IF(NOT work->Finish())
      RETURN FALSE;

    //TODO we should be sharing with SetSystemServerType
    BroadcastEvent("system:internal.configdata", DEFAULT RECORD);
    BroadcastEvent("system:config.servertype", DEFAULT RECORD);
    UpdateSystemConfigurationRecord();
    ApplyWebHareConfiguration([ subsystems := ["config.base"], source := "WebHare backend settings"]);

    RETURN TRUE;
  }

  MACRO RefreshGLobalDashboard(BOOLEAN iscommit)
  {
    BroadcastToAllSessions( [ type := "tollium:shell.refreshdashboard" ]);
  }

  MACRO DoRegisterGeoNames(OBJECT handler)
  {
    handler->SendURL("https://www.geonames.org/login");
  }
  MACRO DoRegisterGeoIP2(OBJECT handler)
  {
    handler->SendURL("https://www.maxmind.com/en/geolite2/signup");
  }
>;

PUBLIC STATIC OBJECTTYPE SelectDuration EXTEND TolliumScreenBase
< RECORD data;

  MACRO Init(RECORD data)
  {
    data := ValidateOptions(
        [ value :=    ""
        , withtime := TRUE
        , withweeks := FALSE
        ], data);

    this->data := data;

    ^duration->value := ParseDuration(data.value);
    IF (data.withweeks)
    {
      IF (^duration->value.days >= 7 AND data.withweeks)
      {
        ^duration->value.weeks := ^duration->value.weeks + ^duration->value.days / 7;
        ^duration->value.days := ^duration->value.days % 7;
      }
    }
    ELSE
    {
      ^duration->value.days := ^duration->value.weeks * 7 + ^duration->value.days;
      ^duration->value.weeks := 0;
    }

    ^weeks->visible := data.withweeks;
    ^weeks->enabled := data.withweeks;
    ^hours->visible := data.withtime;
    ^hours->enabled := data.withtime;
    ^minutes->visible := data.withtime;
    ^minutes->enabled := data.withtime;
    ^seconds->visible := data.withtime;
    ^seconds->enabled := data.withtime;
    ^milliseconds->visible := data.withtime;
    ^milliseconds->enabled := data.withtime;
  }

  STRING FUNCTION Submit()
  {
    OBJECT work := this->BeginFeedback();
    RECORD value := ^duration->value;

    BOOLEAN gotweek, gotnonweek;
    FOREVERY (RECORD rec FROM UnpackRecord(value))
      IF (rec.value != 0)
      {
        IF (rec.name = "WEEKS")
          gotweek := TRUE;
        ELSE
          gotnonweek := TRUE;
      }

    IF (NOT gotnonweek AND this->data.withweeks)
      RETURN gotweek ? `P${value.weeks}W` : "P";

    STRING retval := "P";
    IF (value.years != 0)
      retval := `${retval}${value.years}Y`;
    IF (value.months != 0)
      retval := `${retval}${value.months}M`;
    IF (value.weeks != 0 OR value.days != 0)
      retval := `${retval}${value.weeks * 7 + value.days}D`;

    STRING timepart;
    IF (value.hours != 0)
      timepart := `${timepart}${value.hours}H`;
    IF (value.minutes != 0)
      timepart := `${timepart}${value.minutes}M`;
    IF (value.seconds != 0)
    {
      value.seconds := value.seconds + value.milliseconds / 1000;
      value.milliseconds := value.milliseconds % 1000;
      timepart := `${timepart}${value.seconds}${value.milliseconds != 0 ? `.${Right("000" || value.milliseconds, 3)}` : ""}S`;
    }
    IF (timepart != "")
      retval := `${retval}T${timepart}`;

    TRY
    {
      ParseDuration(retval);
    }
    CATCH(OBJECT e)
    {
      work->AddError(this->GetTid(".invalidduration"));
    }

    RETURN work->Finish() ? retval : "";
  }
>;
