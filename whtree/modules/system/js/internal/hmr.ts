import { registerResourceDependency } from "@webhare/services";

/** @deprecated In WH5.7+, use registerLoadedResource  */
export const registerLoadedResource = registerResourceDependency;
