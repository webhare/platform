import * as storage from "@webhare/dompack/src/storage";

/** @deprecated Import \@webhare/dompack for storage APIs */
export const isIsolated = storage.isIsolated;
/** @deprecated Import \@webhare/dompack for storage APIs */
export const isStorageAvailable = storage.isStorageAvailable;
/** @deprecated Import \@webhare/dompack for storage APIs */
export const getCookie = storage.getCookie;
/** @deprecated Import \@webhare/dompack for storage APIs */
export const setCookie = storage.setCookie;
/** @deprecated Import \@webhare/dompack for storage APIs */
export const deleteCookie = storage.deleteCookie;
/** @deprecated Import \@webhare/dompack for storage APIs */
export const listCookies = storage.listCookies;
/** @deprecated Import \@webhare/dompack for storage APIs */
export const getLocal = storage.getLocal;
/** @deprecated Import \@webhare/dompack for storage APIs */
export const setLocal = storage.setLocal;
/** @deprecated Import \@webhare/dompack for storage APIs */
export const getSession = storage.getSession;
/** @deprecated Import \@webhare/dompack for storage APIs */
export const setSession = storage.setSession;
