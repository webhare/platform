# Module definition
A module definition is the central configuration file of a module and describes
how the module integrates into WebHare. This file is always named `moduledefinition.xml`
and must appear in the root of the module - a module without this file is not
recognized by WebHare as a module at all
