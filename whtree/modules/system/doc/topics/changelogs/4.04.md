# 4.04
## Incompatible changes
- The classes on `<img>` nodes in the RTD have changed for moduledesign sites. You need to watch for wh-rtd__img, wh-rtd__img--floatleft and wh-rtd__img--floatright now.

## Configuration and security
- The 'force https' option has been removed from access rules. Any virtual-hosted webserver whose outputroot starts with 'https:' will automatically redirect http to https
- If a wrdauth plugin does not explicitly 'securecookie', but the webserver is hosted on https, all wrdauth cookies will be marked https-only

## Publisher
- When the target of an internal or contentlink is deleted from the database (eg. after recyclebin expiry), it will no longer cascade-delete the link, but simply reset its link reference.

## Website and module development
- The assetpacker now provides Babel to support ES2015 for JavaScript files whose extension ends in .es
- Siteprofile apply rule: <adddesignrequire path="..." /> to add javascript libraries to a bundle
- RTDType system to replace embedded object types.
- Implement `allownewwindowlinks="true"` on `<rtdtype`, enabling the 'open link in new window' option on hyperlinks
- Using a RTDType RTD requires you to `require('@webhare-publisher/richcontent/all');`
- New RTE component `<richdocument [rtdtype=...]>` - like a `<rte>` but will inherit the parent's rte rtdtype. Also allows setting the rtdtype= explicitly
- A `<rtdype> .. <allowobject>` can now set `inherit="false"` to disable the object when a `<richdocument>` inherits from a parent rte.
- The outputtools are only available in previewmode. Use the debugpage to enable previewmode
- A new concept, 'webfeatures' - these are additional siteprofiles you can add to a site, but are registered in modules so the siteprofiles do not need to be in the database
- A new folderindex type, 'newfile', allows you to add empty RTDs to folders: `<folderindex indexfile="newfile" newfiletype="http://www.webhare.net/xmlns/publisher/richdocumentfile" newfilename="index.rtd">`
- Moduledesigns can now set `<webdesign async="true">`, and its `<script>` tags will be injected with 'async'.

## Deprecated and removed features
- Tollium's support for windowgrid and portlets have been removed
- webdesign->outputtools has been removed. You can remove references to this flag
- `wh babel-node` and `wh node` are no longer supported. Just use `wh run`

## Tollium
- `<textarea>` now supports placeholder=""

