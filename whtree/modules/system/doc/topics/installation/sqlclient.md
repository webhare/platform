# SQLClient
short sqlclient manual

AUTOCOMMIT - enable or disable autocommit

BEGIN - begin work

COMMIT

## Disable cascades
```SET LOCAL CASCADING TO OFF```
