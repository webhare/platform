# Building WebHare from source

## Getting started
Clone the repository (the examples assume we're extracting to `$HOME/projects`). You will need to have setup your ssh
keys to communicate with gitlab.com

```bash
mkdir ~/projects
cd ~/projects
git clone git@gitlab.com:webhare/platform.git webhare
```

You can use `git clone https://gitlab.com/webhare/platform.git webhare` if you do not want to set up a SSH key.

Now setup the `wh` tool by adding this to your `~/.profile` (or equivalent .bashrc of .bash_profile):
and relogging in or restarting your terminal session.

```bash
eval `~/projects/webhare/whtree/bin/wh setupmyshell`
```

setupmyshell currently requires the use of `bash` as your shell (macOS uses `zsh` as its shell by default)

See [wh](wh.md) for more information about the wh tool. All other documentation
will assume you've set this up and just refer to `wh` when they want you to
invoke the `webhare/whree/bin/wh` tool.

If you do not setup your shell as above, you will need to type the full path to the wh script in all the examples below,
ie `~/projects/webhare/whtree/bin/wh` instead of just `wh`.

Make and install:
```bash
wh mic
```

You may need to install further dependencies before the make can complete. On macOS, you will need to install brew (see https://brew.sh).
On Linux the necessary packages will depend on your distribution. You may want to check our Dockerfile for hints.

The build process will attempt to estimate a proper number of parallel jobs to use (`make -j...`) but you can control the
number of processors used by `wh make` and `wh mic` by setting a `WHBUILD_NUMPROCS=nnn` variable eg `WHBUILD_NUMPROCS=4 wh mic`.

You should never set the number of build processors too high - the build process is highly parallel and can easily overwhelm a system.

Run `wh fixmodules` once to update NPM dependencies.

If you want to run the WebHare webserver on ports 80 and/or 443 (the default ports) without having to set up a [proxy](https://gitlab.com/webhare/proxy#readme)
you will need to either run WebHare as `root` (not recommended) or install the socketbinder by running `wh installsocketbinder`.
The socket binder is a small daemon that runs as root and allows WebHare to listen on port numbers below 1024.

From this point you should be able to use the [Getting started](https://www.webhare.dev/manuals/getting-started/) manual
to configure your WebHare - you can just leave out all the `docker exec webhare` parts. You may need to use higher numbered
ports (eg 8000) for the webinterface if you didn't install the socketbinder or if you're running multiple webservers.

If the webinterface doesn't load due to missing `ap.mjs` files, try `wh assetpack compile "platform:*"`

## Getting Java to work
Some of the software used by WebHare (eg PDFBox for printer.whlib) requires Java. You will need to install that yourself.
Installation of any software mentioned here is at your own risk!

For macOS, you can install openjdk through Homebrew (see also: https://github.com/AdoptOpenJDK/homebrew-openjdk)

```bash
brew cask install adoptopenjdk
```

If you're getting an error that openjdk isn't notarized, you can go to System Preferences, Security & Privacy and click
"Allow anyway" on the General tab to enable it.


## Building for docker
```bash
wh builddocker
```

## Advanced build options
These variables are only used during the WebHare build proces or by 'from source' installations. They have no effect
on a running WebHare or on Docker versions

### Build variables/settings
Environment variables that control the build process:

- WHBUILD_NUMPROC: Number of processors to use (the `make -j` parameter). Estimated based on CPU cores and/or memory if not set.
- WHBUILD_DEBUG: Set to build debug versions of WebHare C++ components. The build will take place in a separate build directory, but any make 'install' target will overwrite the same installation
- WHBUILD_PROFILE: Set to build profiling versions of WebHare C++ components. The build will take place in a separate build directory, but any make 'install' target will overwrite the same installation
- WHBUILD_ASSETROOT: Path to external dependencies. Defaults to https://build.webhare.dev/whbuild/. Set to an invalid URL to test build fallback URLs, eg: `WHBUILD_ASSETROOT="https://123xyz.invalid/" wh builddocker`

### Tips & tricks
To quickly run a specific blextest, eg 'string' (you really want this when editing stringmanip.cc):
```bash
BLEXTEST=string wh make blex-test
```

To replace binaries with their debugging counterparts, use the `-overwrite` targets. Eg to replace the webserver and libblex_webhare with
their debug counterparts

```bash
WHBUILD_DEBUG=1 wh make webserver-overwrite libblex_webhare-overwrite
```

(don't forget to restart the processes, eg `pkill webserver`)

To replace libharescript.wasm with a profiling version

```bash
WHBUILD_PROFILE=1 wh make wasm-overwrite
```

### Running with a Nightly node
For MacOS:
- Download a nightly from nodejs.org/download/nightly/. You will want the `node-XXX-darwin-arm64.tar.xz` file.
- Unpack it, eg `tar -Jxf node-v23.0.0-nightly202409243c5ceff85f-darwin-arm64.tar.xz`
- Run it: `WEBHARE_NODE_BINARY=~/Downloads/node-v23.0.0-nightly202409243c5ceff85f-darwin-arm64/bin/node wh console`

Dealing with Gatekeeper is left as an exercise for the reader.

## Troubleshooting common build failures
Try repeating the make command. If you only see 'error' or 'waiting for finished jobs' you may have to scroll up a bit to find the error (make often runs multiple tasks at the same time. if one task reported an error, it will finish the other running jobs so you may need to look for the error).

Before you try anything else, make sure you are up-to-date and try the fixbuild option and see if it fixes your issue:
```bash
# This executes various cleanup steps that usually fix ICU or NPM issues
wh fixbuild
```

If you get stuck debugging build issues, you can try to ask for help on the [WebHare developers group](https://groups.google.com/a/webhare.dev/g/developers)

Please post as much of the error log as possible of your *last* 'wh make' attempt that didn't generate any further progress.

#### "No rule to make target"
These errors are usually fixed by running `wh make clean-deps`

If tests are failing and you want to ignore this, run `NOTEST=1 wh mic`.

#### "TestLocalization: VersionTest yielded errors"
If 'got' is lower than 'expected', you need to update your ICU library. On macOS, a 'brew update' will update your brew definitions, after which it should carry out this update.

If 'got' is higher than 'expected', update or let us know.

#### "Library not loaded: /usr/local/opt/icu4c/lib/libicui18n.55.dylib Referenced from: /wh/whbuild/blex/tests/dynamic.dylib"
This error, and similar errors, may be caused by updating libraries (especially when the error refers to an older version of the library, like icu v55 above). Try `wh make clean-libs` to remove all compiled libraries

#### lib/hsm_wh_icu.dylib Error 1
There were probably errors building the icu provider, and the autodependency checking tends to be bad at picking up icu recompiles, at least on macOS.

Run `wh make clean-icu-provider` to specifically reset the ICU module. If ICU issues persist, consider downgrading to an earlier version. On macOS with brew:

```bash
# Show available versions
brew list --versions icu4c
# this will return something like: icu4c 59.1_1 60.2

# Pick an earlier version
brew switch icu4c 59.1_1
```

#### libssh2: Unknown `--is-lightweight` option
This is usually caused by broken or out-of-date command line tools on macOS.

Update Xcode, start Xcode and make sure you accepted the EULA and it got a chance to download updated command line tools.

After this, you'll probably have to go through a 'wh fixbuild'.

#### compiler: `error: invalid value 'c++17' in '-std=c++17'`
Ensure your compiler (gcc orclang) is up-to-date.

On macOS, check that the OS, XCode and XCode's developer tools are up-to-date.

#### hsvm_pgsqlprovider.h:18: fatal error: 'libpq-fe.h' file not found
When using Homebrew, you may need to 'link' the version of postgresql you want to use, eg `brew link postgresql@13`.

#### If you're about to give up:
```bash
# Discard build directory and resetup the build proces
rm -rf ~/projects/whbuild
wh mic
```

## Testing changes to C++ code

### Quickly running newly built code
To test changes to C++ code, you can run newly built code without going through a make install:

```
wh make NOTEST=1 && wh execbuilt runscript testfile.whscr
```

Or, to build and run the debug version:
```
WHBUILD_DEBUG=1 wh make NOTEST=1 && WHBUILD_DEBUG=1 wh execbuilt runscript testfile.whscr
```

You can start the runscript process in the system debugger (lldb or gdb) using
```
wh execbuilt --dbg runscript
```

### Dumping HareScript compiler AST nodes
Use the following command to dump the internal AST nodes of the compiler after each stage. Support
for dumping AST nodes is only included in debyug mode.

```
WHBUILD_DEBUG=1 wh execbuilt whcompile -f -d /tmp/debugout/ test.whscr
```

This command will output a list of .txt and .dot (GraphViz) files.

## Building container images
```bash
# Build using docker
wh builddocker

# Build using podman
wh builddocker --podman
```
