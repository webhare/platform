# Core development
Pointers and tips when developing on the WebHare core

## Useful commands

### Before committing changes
`wh runtest checkmodules` - static validation of the core WebHare modules.
