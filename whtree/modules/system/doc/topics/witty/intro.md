Witty is the template engine for the HareScript language.

Witty integrates seamlessly into the HareScript language, fully supporting its data-centric approach to web scripting and helps eliminate common encoding and programming errors.
