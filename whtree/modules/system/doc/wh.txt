console [--profile] [--coverage]        Run WebHare in console mode, optionally enable full profiling and/or coverage modes
exec ...                                Execute the specified process inside a WebHare environment (eg wh exec backup ...)
setupmyshell                            Prepare my shell for easy WebHare use. Invoke as: eval \`$WEBHARE_DIR/bin/wh setupmyshell\`
cachereset                              Clear all caches
run <script>                            Run the specified script
backuplocal                             Create a local snapshot backup of your WebHare
clonerestoredb <destdir>                Clone the database to the specified directory
pstree                                  Show WebHare processes
terminate                               Terminate all WebHare processes nicely (SIGTERM)
kill                                    Kill all WebHare processes abruptly (SIGKILL)
activity                                Show recent hitcounters grouped by minute from the access.log
dirs                                    Show paths and environment settings
check                                   Run selfchecks
shellconfig                             Get WebHare configuration in shell variables
getmodulelist                           Get the list of installed modules
getmoduledir <name>                     Where is module '<name>' ?
getrootdir                              Get the WebHare installation root directory
getdatadir                              Get the WebHare installation data directory
runtest <test>                          Run the specified test
isrunning                               Exits with errorcode 0 if WebHare is running, 1 otherwise
waitfor poststartdone                   Wait for WebHare to reach 'poststartdone' phase

--- developer options ---
freshdbconsole                          Wipe database and run in console mode
up                                      Update git
st                                      Git status
make                                    Build from source
finalize-webhare                        Run postinstall tasks (including rebuilding the resolveplugin)
mic                                     Make Install, run in console mode
umic                                    Update git, make install, run in console mode
installsocketbinder                     Install the socket binder
execbuilt                               Execute the specified executable directly from the build tree (eg 'runscript <script>')
usebuiltwasm ...                        Execute the specified process using the WASM modules from build tree (eg 'wh run <script>')

--- standard options ---
