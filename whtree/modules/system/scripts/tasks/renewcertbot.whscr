<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::os.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/whfs.whlib";
LOADLIB "mod::system/lib/internal/dbschema.whlib";
LOADLIB "mod::system/lib/internal/nginx/config.whlib";
LOADLIB "mod::system/lib/internal/webserver/certbot.whlib";

RECORD args := ParseArguments(GetConsoleArguments(),
    [ [ name := "debug", type := "switch" ]
    ]);

IF (NOT RecordExists(args))
{
  PRINT("Syntax: wh run mod::system/scripts/tasks/renewcertbot.whscr [ --debug ]\n");
  RETURN;
}

BOOLEAN debug := args.debug;
OBJECT trans := OpenPrimary();

RECORD ARRAY allcerts := ListCertficates();
STRING ARRAY allhostnames := GetCertifiableHostnames();

FOREVERY (RECORD cert FROM allcerts)
{
  //In scope? Do we care?
  IF(cert.name NOT LIKE "certbot-*")
    CONTINUE;
  IF(cert.valid_until > AddDaysToDate(30,GetCurrentDatetime()))
    CONTINUE;

  IF(debug)
    Print(`Certbot certificate '${cert.name}' is up for renewal\n`);

  //Which names do we still need to certify?
  STRING ARRAY requestfor;
  FOREVERY(STRING host FROM cert.servernames)
  {
    RECORD best := GetBestCertificateForHost(allcerts, host);
    IF(best.id != cert.id)
    {
      IF(debug)
        Print(`Certificate '${best.name}' is better for '${host}' than us\n`);
      CONTINUE;
    }

    IF(Left(host, 2) != "*." AND ToUppercase(host) NOT IN allhostnames)
    {
      IF(debug)
        Print(`Hostname '${host}' is no longer hosted here\n`);
      CONTINUE;
    }

    INSERT host INTO requestfor AT END;
  }

  IF(Length(requestfor) = 0) //we are no longer hosting this site
  {
    //link the port up to a new key/certificate. note that this generally only happens on non-proxied WebHares that don't do SNI anyway
    INTEGER setupreplacement;
    IF(RecordExists(SELECT FROM system_internal.ports WHERE keypair = cert.id))
    {
      FOREVERY(STRING matchhost FROM cert.servernames)
      {
        RECORD replacement := GetBestCertificateForHost(allcerts, matchhost);
        IF(RecordExists(replacement))
        {
          setupreplacement := replacement.id;
          BREAK;
        }
      }
      IF(setupreplacement = 0)
      {
        PrintTo(2, `Certificate '${cert.name}' is no longer needed here but still referred by a SSL port!\n`);
        SetConsoleExitCode(1);
        CONTINUE;
      }
    }

    Print(`Certificate '${cert.name}' is no longer needed here, deleting it\n`);
    trans->BeginWork();
    OpenWHFSObject(cert.id)->RecycleSelf();
    IF(setupreplacement != 0)
      UPDATE system_internal.ports SET keypair := setupreplacement WHERE keypair = cert.id;
    trans->CommitWork();

    CONTINUE;
  }

  IF(debug)
    Print("Requesting for " || Detokenize(requestfor,', ') || "\n");

  RECORD res := RequestCertbotCertificate(requestfor, FALSE);
  IF(res.success)
  {
    Print(`Renewed certificate for '${cert.name}'\n`);
  }
  ELSE
  {
    PrintTo(2,"Renewal for " || cert.name || " failed: " || res.error || " " || res.errordata || "\n");
    SetConsoleExitCode(1);
  }
}
