<?wh

LOADLIB "wh::os.whlib";
LOADLIB "wh::internet/tcpip.whlib";
LOADLIB "wh::internet/webbrowser.whlib";

LOADLIB "mod::wrd/lib/api.whlib";

LOADLIB "mod::system/lib/checks.whlib";
LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/whfs.whlib";
LOADLIB "mod::system/lib/internal/moduleimexport.whlib";
LOADLIB "mod::system/lib/internal/whconfig.whlib";


OpenPrimary();

RECORD ARRAY receivers := OpenWRDschema("system:config")->^statusupload->RunQuery(
    [ outputcolumns := CELL[ "wrd_id", "wrd_title", "url", "reports" ]
    ]);

OBJECT browser := NEW WebBrowser;
receivers := ShuffleArray(receivers); //in case one of the receivers crashes us
FOREVERY(RECORD receiver FROM receivers)
{
  RECORD startupdata := EnforceStructure([ start := DEFAULT DATETIME], PollWHServiceState("startup"));

  /* The base set (ie no further reports) of information should be something that's safe to share to a party that will treat
     your information confidentially and would only use to report potential compatibility or security issues */
  RECORD serverinfo :=
     [ //we *need* the installation ID to deduplicate servers
       installationid := ReadRegistryKey("system.global.installationid")
     , version := GetWebhareVersionInfo()
       //servername and hostname doesn't reveal much more looking up your IP against public databases or a scan would, so
       //if you're willing to reveal the existence of this WebHare, you're probably okay way with revealing that info too
     , servername := GetServerName()
     , hostname := GetSystemHostName(TRUE)
       //especially the kernel version is useful for compatibility analysis. and this is like useragent, identical for lots of machines
     , osname := GetSystemOSName()
     ];

  IF("modules" IN receiver.reports) //'modules' would allow reporting more precise compatibility/security
  {
    RECORD ARRAY modules := SELECT name
                                 , version
                                 , lastmodified
                              FROM GetInstalledModulesOverview(FALSE); //include_deleted = FALSE

    INSERT CELL modules := modules INTO serverinfo;
  }
  IF("sites" IN receiver.reports) //'sites' is much more sensitive and useful to build centralized site lookups for eg. support functions
  {
    RECORD ARRAY sites :=
            SELECT id
                 , name
                 , webroot
                 , outputweb
                 , cdnbaseurl
              FROM system.sites;

    OBJECT sitesettingstype := OpenWHFSType("http://www.webhare.net/xmlns/publisher/sitesettings");
    sites := sitesettingstype->Enrich(sites, "id", ["sitedesign", "webfeatures"]);

    INSERT CELL sites := sites INTO serverinfo;
  }
  IF("issues" IN receiver.reports)
  {
    INSERT CELL issues := GetCurrentCheckIssues() INTO serverinfo;
  }

  IF(NOT browser->PostWebPageBlob(receiver.url, [[ field := "Content-Type", value := "application/json" ]], EncodeJSONBlob(serverinfo)))
  {
    //Not displaying the URL in the error, it might contain secrets
    PrintTo(2,`Failed to upload to receiver #${receiver.wrd_id}: ${receiver.wrd_title}: ${browser->GetHTTPStatusText()}\n`);
    SetConsoleExitCode(1);
  }
}
