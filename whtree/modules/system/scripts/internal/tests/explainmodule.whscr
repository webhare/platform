<?wh

/*
  to run:
  cat moduledefinition.xml | wh run mod::system/scripts/internal/tests/explainmodule.whscr
*/

LOADLIB "wh::os.whlib";
LOADLIB "wh::xml/dom.whlib";
LOADLIB "wh::xml/xsd.whlib";

LOADLIB "mod::system/lib/internal/whconfig.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";


BLOB input := GetSTDINAsBlob();
OBJECT moduledef := MakeXMLDocument(input);
RECORD ARRAY addedmodules;

FOREVERY (OBJECT pack FROM moduledef->documentelement->GetElements("meta > packaging"))
{
  STRING error := GetApplicabilityError(GetMyApplicabilityInfo(), ReadApplicableToWebHareNode(pack));
  IF (error != "")
  {
    Print(`EXPLAIN_MODULENOTAPPLICABLE="${EncodeJava(error)}"\n`);
    Print(`MODULENOTAPPLICABLE="${EncodeJava(error)}"\n`); //TODO: Once 5.02 is minimum supported version, testdocker.sh can switch to EXPLAIN_ - it's nicer to always use prefixed vars
  }
}

OBJECT validationode := moduledef->documentelement->QuerySelector("meta > validation");
IF(ObjectExists(validationode))
{
  FOREVERY(STRING opt FROM ParseXSList(validationode->GetAttribute("options")))
  {
    Print(`EXPLAIN_OPTION_${Touppercase(opt)}=1\n`);
  }
}

FOREVERY (OBJECT dependency FROM moduledef->documentelement->GetElements("meta > packaging > dependency")
                                 CONCAT
                                 moduledef->documentelement->GetElements("meta > validation > fetchmodule"))
{
  IF(NOT IsNodeApplicableToThisWebHare(dependency))
    CONTINUE;

  STRING depname := dependency->GetAttribute("module");
  IF(depname IN whconstant_builtinmodules OR RecordExists(SELECT FROM addedmodules WHERE module = depname))
    CONTINUE;

  INSERT CELL[ module := depname
             , repository := dependency->GetAttribute("repository") ?? "webhare/" || depname
             , branch := dependency->GetAttribute("branch")
             ] INTO addedmodules AT END;
}

FOREVERY(RECORD mod FROM addedmodules)
{
  Print(`EXPLAIN_DEPMODULE[${#mod}]=${mod.module}\n`);
  Print(`EXPLAIN_DEPREPOSITORY[${#mod}]=${mod.repository}\n`);
  Print(`EXPLAIN_DEPBRANCH[${#mod}]=${mod.branch}\n`);
}
