<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::os.whlib";
LOADLIB "wh::internal/interface.whlib";

LOADLIB "mod::system/lib/services.whlib";

RECORD ARRAY untransferred_table;
INTEGER next_untransferred_id := 1000 * Random(0, 100);
BOOLEAN debug := "--debug" IN GetConsoleArguments();

VARIANT FUNCTION MapToBridgeRecursive(VARIANT input)
{
  IF(TYPEID(input) = TYPEID(RECORD))
  {
    IF(NOT RecordExists(input))
      RETURN DEFAULT RECORD;
    RETURN RepackRecord(SELECT *, value := MapToBridgeRecursive(value) FROM UnpackRecord(input));
  }

  IF(TYPEID(input) IN [TYPEID(OBJECT), TYPEID(FUNCTION PTR)])
  {
    IF(NOT IsValueSet(input))
      RETURN DEFAULT RECORD;

    INTEGER handlepos := (SELECT AS INTEGER #untransferred_table + 1 FROM untransferred_table WHERE TYPEID(val) = TYPEID(input) ? val = input : FALSE) - 1;
    IF(handlepos != -1)
    {
      // Up the generation, so we can detect when JavaScripts sends a cleanup call for an id when a newer generation
      // is already in transit.
      untransferred_table[handlepos].generation := untransferred_table[handlepos].generation + 1;
      RETURN CELL[ __unmarshallable_type := GetTypeName(TYPEID(input)), untransferred_table[handlepos].id, untransferred_table[handlepos].generation ];
    }

    next_untransferred_id := next_untransferred_id + 1;
    IF(debug)
      Print(`[bridge_hsvm:${GetCurrentGroupId()}] MapToBridgeRecursive: #${next_untransferred_id}\n`);
    INSERT [ id := next_untransferred_id, val := input, generation := 1 ] INTO untransferred_table AT END;
    RETURN [ __unmarshallable_type := GetTypeName(TYPEID(input)), id := next_untransferred_id, generation := 1 ];
  }

  RETURN input;
}

VARIANT FUNCTION MapToBridge(VARIANT input)
{
  VARIANT retval := MapToBridgeRecursive(input);
  IF(debug)
    DumpValue(retval, [ name := "MapToBridge" ]);
  RETURN retval;
}

VARIANT FUNCTION MapFromBridgeRecursive(VARIANT input)
{
  IF(TYPEID(input) = TYPEID(RECORD) AND CellExists(input, "__unmarshallable_type"))
  {
    RECORD match := SELECT *, pos := #untransferred_table FROM untransferred_table WHERE id = input.id;
    IF(NOT RecordExists(match))
    {
      IF(debug)
        Print(`[bridge_hsvm:${GetCurrentGroupId()}] MapFromBridgeRecursive: No such unmarshallable #${input.id}\n`);
      THROW NEW Exception(`No such unmarshallable #${input.id}`);
    }
    RETURN match.val;
  }
  RETURN input;

  IF(TYPEID(input) = TYPEID(RECORD))
  {
    IF(NOT RecordExists(input))
      RETURN DEFAULT RECORD;
    RETURN RepackRecord(SELECT *, value := MapFromBridgeRecursive(value) FROM UnpackRecord(input));
  }

  RETURN input;
}

VARIANT FUNCTION MapFromBridge(VARIANT input)
{
  VARIANT retval := MapFromBridgeRecursive(input);
  IF(debug)
    DumpValue(retval, [ name := "MapFromBridge" ]);
  RETURN retval;
}

INTEGER connecttimeout_cb := RegisterTimedCallback(AddTimeToDate(10000, GetCurrentDateTime()), PTR TerminateScriptWithError("HSVM bridge job not connected to within 10 seconds"));

VARIANT FUNCTION InvokeFPTR(FUNCTION PTR fptr, VARIANT ARRAY args)
{
  IF(__INTERNAL_DEBUGGETFUNCTIONPTRRETURNTYPE(fptr) IN [ 0, 2 ]) //it's a macro!
  {
    CallMacroPTRVA(fptr, args);
    RETURN [ __unmarshallable_type := "undefined" ];
  }
  ELSE
  {
    RETURN MapToBridge(CallFunctionPtrVA(fptr, args));
  }
}

OBJECTTYPE Controller
<
  MACRO NEW()
  {
    IF (connecttimeout_cb != 0)
      UnregisterCallback(connecttimeout_cb);
    connecttimeout_cb := 0;
  }
  PUBLIC ASYNC FUNCTION CreatePrintCallback(STRING text)
  {
    IF(debug)
      Print(`[bridge_hsvm:${GetCurrentGroupId()}] CreatePrintCallback(...)\n`);
    RETURN MapToBridge(PTR Print(text));
  }
  PUBLIC ASYNC FUNCTION Invoke(STRING lib, STRING name, VARIANT ARRAY args)
  {
    IF(debug)
      Print(`[bridge_hsvm:${GetCurrentGroupId()}] Invoke('${lib}', '${name}', ${Length(args)} arguments)\n`);

    args := SELECT AS VARIANT ARRAY MapFromBridge(arg) FROM ToRecordArray(args,"arg");
    RETURN InvokeFPTR(MakeFunctionPtr(lib,name), args);
  }
  PUBLIC ASYNC FUNCTION ObjInvoke(VARIANT obj, STRING name, VARIANT ARRAY args)
  {
    IF(debug)
      Print(`[bridge_hsvm:${GetCurrentGroupId()}] ObjInvoke(obj, '${name}', ${Length(args)} arguments)\n`);

    obj := MapFromBridge(obj);
    args := SELECT AS VARIANT ARRAY MapFromBridge(arg) FROM ToRecordArray(args,"arg");

    VARIANT retval;
    IF(ToUppercase(name) = "GET" OR ToUppercase(name) = "$GET")
      IF(MemberExists(obj, args[0]))
        RETURN MapToBridge(GetMember(obj, args[0]));
      ELSE
        THROW NEW Exception(`No such member '${args[0]}'`); //'nice' exception as a HS Error will terminate the bridge without explanation
    ELSE
      RETURN InvokeFPTR(GetObjectMethodPtr(obj, name), args);
  }
  PUBLIC INTEGER FUNCTION GetNumObjects()
  {
    IF(debug)
      Print(`[bridge_hsvm:${GetCurrentGroupId()}] GetNumObjects()\n`);

    RETURN Length(untransferred_table);
  }
  PUBLIC MACRO ObjCleanup(INTEGER objid, INTEGER generation)
  {
    IF(debug)
      Print(`[bridge_hsvm:${GetCurrentGroupId()}] ObjCleanup(${objid}, ${generation})\n`);

    // Delete only if we didn't resend it since the last time JS saw the id
    DELETE FROM untransferred_table WHERE id = objid AND COLUMN generation <= VAR generation;
  }

  // Called when service link is closed
  PUBLIC MACRO OnClose()
  {
    IF(debug)
      Print(`[bridge_hsvm:${GetCurrentGroupId()}] OnClose()\n`);

    TerminateScript();
  }
>;

OBJECT FUNCTION CreateController()
{
  RETURN NEW Controller;
}

RunWebhareService("system:bridge_hsmv_" || GetCurrentGroupId(), PTR CreateController, [ autorestart := FALSE ]);
