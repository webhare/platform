﻿<?wh

// syntax: [--measure] <objectid>[...]
// short: Republish the selected file(s), specified by id


LOADLIB "wh::datetime.whlib";
LOADLIB "wh::money.whlib";
LOADLIB "wh::os.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/internal/tasks.whlib";
LOADLIB "mod::system/lib/internal/tasks/execution.whlib";
LOADLIB "mod::system/lib/internal/tasks/queuemgr.whlib";
LOADLIB "mod::system/lib/whfs.whlib";

LOADLIB "mod::publisher/lib/internal/publishing-backend.whlib";

LOADLIB "mod::publisher/lib/internal/publication/publish.whlib";


// ----------------------------------------------------------------------------
//
// Main code
//


RECORD ARRAY vars :=
  [ [ name := "measure",       type := "switch", defaultvalue := FALSE ]
  , [ name := "debug",         type := "switch", defaultvalue := FALSE ]
  , [ name := "files",         type := "paramlist" ]
  ];

RECORD arguments := ParseArguments(GetConsoleArguments(), vars);
IF (NOT RecordExists(arguments))
{
  PRINT("Couldn't parse commandline arguments: "||Detokenize(GetConsoleArguments(), " ")||"\n");
  PRINT("\n");
  PRINT("Syntax:\n");
  PRINT("publishing [ --measure ] [ --debug ] [ files ]\n");
  SetConsoleExitCode(1); // EXIT_FAILURE
  RETURN;
}

measure := arguments.measure;
publishing_debug := arguments.debug;

// Parse parameters
INTEGER ARRAY my_files;

OBJECT trans := OpenPrimary();
RECORD taskinfo := SELECT * FROM GetAllTaskTypeInfo().tasktypes WHERE ephemeral = TRUE AND tasktype = "publisher:publication";
DATETIME total_start := GetCurrentDateTime();

FOREVERY (STRING file FROM arguments.files)
{
  INTEGER fileid := ToInteger(file, 0);
  IF (fileid <= 0) //looks like a path
  {
    OBJECT fileobj := OpenWHFSObjectByPath(file);
    IF(ObjectExists(fileobj) AND NOT fileobj->isfolder)
      fileid := fileobj->id;
    trans->Close();
  }
  IF(fileid <= 0)
  {
    PRINT("Illegal file id '"||file||"'\n");
    SetConsoleExitCode(1); // EXIT_FAILURE
    RETURN;
  }

  DATETIME start := GetCurrentDateTime();
  RECORD executeinfo := GetTaskExecuteInfo([ queueid := "ephemeral"
                                           , isephemeral := TRUE
                                           , tasktype := "publisher:publication"
                                           , taskdata := [ task := [ id := fileid ]]
                                           , options := DEFAULT RECORD
                                           ], taskinfo);
  RECORD res := ExecuteEphemeralTask(executeinfo, publishing_debug);
  IF(res.type != "taskdone")
    ABORT(res);

  PrintTo(1, "STATUS:" || res.result.status || "\n");
  IF (measure)
  {
    INTEGER msecsdiff := GetDatetimeDifference(start, GetCurrentDateTime()).msecs;
    PrintTo(1,"Publication time:" || FormatMoney(msecsdiff / 1000m, 3, ".", "", TRUE) || " seconds\n");
  }
}

IF (measure)
{
  INTEGER msecsdiff := GetDatetimeDifference(total_start, GetCurrentDateTime()).msecs;
  PRINT("Total publication time:" || FormatMoney(msecsdiff / 1000m, 3, ".", "", TRUE) || " seconds\n");
}
