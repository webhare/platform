<?wh
// syntax: <catalogmask> [contentsourcemask]
// short: directly index (part of a) catalog

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::os.whlib";
LOADLIB "mod::consilio/lib/api.whlib";
LOADLIB "mod::consilio/lib/internal/fetcher_commands.whlib";
LOADLIB "mod::system/lib/database.whlib";

RECORD args;
INTEGER numdone;
INTEGER setlimit;

MACRO ProcessContentSource(OBJECT catalog, RECORD csource, STRING groupmask)
{
  OBJECT contentsource := catalog->OpenContentSourceById(csource.id);
  Print(`Processing catalog '${catalog->tag}' content source '${contentsource->tag ?? contentsource->GetWHFSPath()}'\n`);

  DATETIME commanddate := GetCurrentDatetime();

  RECORD res := CheckIndex([ indexid := catalog->id
                           , contentsourceid := contentsource->id
                           , commanddate := commanddate
                           ]);
  IF(RecordExists(res.error) AND res.error.message != "")
    ABORT(res.error.message);

  RECORD ARRAY groups := res.tocheck_groups;
  IF(Length(groups) = 0)
    Print("No groups in this content source!\n");

  IF(groupmask != "")
  {
    DELETE FROM groups WHERE ToUppercase(id) NOT LIKE ToUppercase(groupmask);
    IF(Length(groups) = 0)
    {
      Print(`No groups in this content source match the mask '${groupmask}'\n`);
      RETURN;
    }
  }

  IF(args.listgroups)
  {
    DumpValue(groups,'csv');
    RETURN;
  }

  FOREVERY(RECORD grouptocheck FROM groups)
  {
    Print((csource.tag ?? "#" || csource.id) || ": " || #grouptocheck || "/" || Length(groups) || (args.debug ? "\n" : "\r"));

    TRY
    {
      CheckGroup([ groupid := grouptocheck.id, contentsourceid := contentsource->id, indexid := catalog->id, commanddate := commanddate],
          [ printprogress := FALSE
          , debugobjects := args.debug
          , noindexmodify := FALSE
          ]);
    }
    CATCH(OBJECT e)
    {
      SetConsoleExitCode(1);
      IF(args.breakonerror)
        THROW;

      Print(`Errors indexing '${grouptocheck.id}': ${e->what}\n`);
    }

    numdone := numdone + 1;
    IF(setlimit != 0 AND numdone >= setlimit)
      BREAK;
  }
  IF(setlimit = 0 AND groupmask = "") //allowed to index all
  {
    Print("\nCleaning the index...");
    res := CleanupIndex([ indexid := catalog->id
                        , contentsourceid := contentsource->id
                        , commanddate := commanddate
                        , active := TRUE //actually clean things
                        , dontdelete := FALSE
                        ]);
    Print("\n");
  }
}

 args := ParseArguments(GetConsoleArguments(),
                              [ [ name := "debug", type := "switch" ]
                              , [ name := "limit", type := "stringopt" ]
                              , [ name := "breakonerror", type := "switch" ]
                              , [ name := "listgroups", type := "switch" ]
                              , [ name := "catalogmask", type := "param", required := TRUE ]
                              , [ name := "contentsourcemask", type := "param", required := FALSE ]
                              , [ name := "groupmask", type := "param", required := FALSE ]
                              ]);

IF(NOT RecordExists(args))
{
  Print("Syntax: wh consilio:manualindex [--debug] [--limit <num>] [--breakonerror] [--listgroups] <catalogmask> [contentsourcemask] [groupmask]\n");
  SetConsoleExitCode(1);
  RETURN;
}

STRING contentsourcemask := args.contentsourcemask ?? "*";
setlimit := ToInteger(args."limit",0);

DATETIME start := GetCurrentDateTime();
OpenPrimary();

RECORD ARRAY matchcatalogs := SELECT * FROM ListConsilioCatalogs() WHERE ToUppercase(tag) LIKE ToUppercase(args.catalogmask);
IF(Length(matchcatalogs) = 0)
  TerminateScriptWithError(`No such catalog '${args.catalogmask}'`);

FOREVERY(RECORD catalogrec FROM matchcatalogs)
{
  OBJECT catalog := OpenConsilioCatalogById(catalogrec.id);
  RECORD ARRAY matchcsources := SELECT *
                                  FROM catalog->ListContentSources()
                                 WHERE ToUppercase(tag) LIKE ToUppercase(contentsourcemask)
                                       OR ToUppercase(title) LIKE ToUppercase(contentsourcemask)
                                       OR "#" || id = contentsourcemask;

  IF(Length(matchcsources) = 0)
  {
    Print(`No content sources in catalog '${catalog->tag}' match the mask '${contentsourcemask}'\n`);
    CONTINUE;
  }
  FOREVERY(RECORD csource FROM matchcsources)
    ProcessContentSource(catalog, csource, args.groupmask);
}
