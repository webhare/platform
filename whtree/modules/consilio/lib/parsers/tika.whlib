<?wh

LOADLIB "wh::crypto.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::xml/dom.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";
LOADLIB "mod::system/lib/internal/services/javaservice.whlib";

LOADLIB "mod::consilio/lib/parsers/parser_html.whlib";


PUBLIC STRING FUNCTION __GetTikaCacheRoot()
{
  RETURN `${GetModuleStorageRoot("consilio")}tikacache/`;
}

PUBLIC OBJECT FUNCTION ParseUsingTIKA(BLOB file, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(CELL[], options);

  STRING inputtemp := GenerateTemporaryPathname();
  StoreDiskFile(inputtemp, file, [ exclusive := TRUE ]);

  TRY
  {
    RECORD result := RunJavaServiceApp([ GetInstallationRoot() || "/libexec/tika-app.jar"
                                       , "--config=" || GetInstallationRoot() || "/etc/tika-default-config.xml" //disables OCR
                                       ,  "--html"
                                       , inputtemp ]);
    IF(result.exitcode != 0)
      THROW NEW Exception(`TIKA parse failed, errorcode ${result.exitcode}`);
    RETURN MakeXMLDocumentFromHTML(StringToBlob(result.output));
  }
  FINALLY
  {
    DeleteDiskFile(inputtemp);
  }
}

RECORD FUNCTION CachableParseTikaPage(BLOB file)
{
  OBJECT doc := ParseUsingTIKA(file);
  RECORD parsed_page := ParseHTMLPage(StringToBlob(doc->body->outerhtml), FALSE);
  OBJECT meta_titlenode := doc->QuerySelector(`meta[name="dc:title"]`);
  IF(ObjectExists(meta_titlenode))
    parsed_page.title := meta_titlenode->GetAttribute("content");

  // Html structure: <html><body>(<div class="page">page contents</div>)*<ul>overview</ul></body></html>
  STRING ARRAY pages;
  FOREVERY (OBJECT node FROM doc->documentelement->QuerySelectorAll("body > div.page")->GetCurrentElements())
  {
    RECORD parsed := ParseHTMLPage(StringToBlob(node->outerhtml), FALSE);
    INSERT parsed.text INTO pages AT END;
  }

  RETURN CELL[ ...parsed_page, pages ];
}

PUBLIC RECORD FUNCTION ParseTikaPage(BLOB file, BOOLEAN _contentlinksonly_ignored)
{
  // contentlinksonly isn't actually used in ParseHTMLPage, so we'll ignore it

  STRING hash := ToLowercase(EncodeBase16(GetHashForBlob(file, "SHA-256")));
  STRING cachefilepath := `${__GetTikaCacheRoot()}${Left(hash, 2)}/${SubString(hash, 2)}.hson`;
  BLOB cachefilecontents := GetDiskResource(cachefilepath, [ allowmissing := TRUE ]);

  DATETIME now := GetCurrentDateTime();
  RECORD cachefiledata :=
      [ data :=           DEFAULT RECORD
      , lastuse :=        now
      , creationdate :=   DEFAULT DATETIME
      , filesize :=       0i64
      , versiontag :=     ""
      ];

  IF (IsValueSet(cachefilecontents))
  {
    TRY
      cachefiledata := EnforceStructure(cachefiledata, DecodeHSONBlob(cachefilecontents));
    CATCH (OBJECT e)
      LogHarescriptException(e);
  }

  BOOLEAN needwrite := FALSE;
  IF (cachefiledata.versiontag != whconstants_consilio_tikacache_versiontag OR NOT RecordExists(cachefiledata.data))
  {
    cachefiledata.data := CachableParseTikaPage(file);
    cachefiledata.lastuse := now;
    cachefiledata.versiontag := whconstants_consilio_tikacache_versiontag;
    cachefiledata.creationdate := now;
    cachefiledata.filesize := LENGTH64(file);
    needwrite := TRUE;
  }
  ELSE IF (GetDayCount(cachefiledata.lastuse) != GetDayCount(now))
  {
    cachefiledata.lastuse := now;
    needwrite := TRUE;
  }

  IF (needwrite)
  {
    TRY
    {
      CreateDiskDirectoryRecursive(GetDirectoryFromPath(cachefilepath), TRUE);
      StoreDiskFile(cachefilepath, EncodeHSONBlob(cachefiledata), [ overwrite := TRUE ]);
    }
    CATCH (OBJECT e)
      LogHarescriptException(e);
  }

  RETURN cachefiledata.data;
}

/** Clears the tika cache
*/
PUBLIC MACRO ClearTikaCache()
{
  FOREVERY (RECORD rec FROM ReadDiskDirectory(__GetTikaCacheRoot(), "*"))
    DeleteDiskDirectoryRecursive(rec.path);
}
