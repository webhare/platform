[component searchpage]
  <div class="wh-searchpage__root">
    [searchheader]

    [if querytext] [! a search was done !]
      [searchresults]
    [/if]
  </div>
[/component]

[component searchheader]
  <div class="wh-searchpage__header">
    <form class="wh-searchpage__searchform" role="search" method="get" action="[searchurl]" autocomplete="off"> [! role=search is okay: https://github.com/w3c/html-aria/issues/18 !]
      <label class="wh-searchpage__searchlabel" for="wh-searchpage__query">
        [gettid frontend.sitecontent.searchpage.searchlabel]
      </label>
      <input class="wh-searchpage__query" id="wh-searchpage__query" name="q" type="search" autocomplete="off" value="[querytext]" required>
      <button class="wh-searchpage__searchbutton" type="submit">
        <span class="wh-searchpage__searchbuttontitle">[gettid frontend.sitecontent.searchpage.searchbutton]</span>
      </button>
    </form>
  </div>
[/component]

[component searchresults]
  <div class="wh-searchpage__results">
    <p class="wh-searchpage__feedback">
      [if totalcount]
        [gettid frontend.sitecontent.searchpage.numresults totalcount querytext]
      [else]
        [gettid frontend.sitecontent.searchpage.noresults querytext]
      [/if]
    </p>

    <ol class="wh-searchpage__resultlist">
      [forevery results]
        [searchresult]
      [/forevery]
    </ol>
  </div>
[/component]

[component searchresult]
  <li class="wh-searchpage__result">
    <article>
      <h2 class="wh-searchpage__resulttitle"><a href="[link]">[title]</a></h2>
      [if summary]
        <div class="wh-searchpage__resultsummary">[summary:none]</div>
      [/if]
      <div class="wh-searchpage__resultlink" aria-hidden="true">  [! hide the link for screenreaders, it's already behind the title too so consider it decoration and it doesn't add much to read it aloud !]
        <a href="[link]">[link]</a>
      </div>
    </article>
  </li>
[/component]
