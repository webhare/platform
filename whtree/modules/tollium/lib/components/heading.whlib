<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::internet/urls.whlib";

LOADLIB "mod::tollium/lib/componentbase.whlib";
LOADLIB "mod::tollium/lib/screenbase.whlib";

PUBLIC OBJECTTYPE Heading EXTEND TolliumFragmentBase
<
  STRING __doclink;

  PUBLIC PROPERTY checkbox(GetCheckBox, SetCheckBox);
  PUBLIC PROPERTY value(GetValue, SetValue);
  PUBLIC PROPERTY onchange(GetOnChange, SetOnChange);
  PUBLIC PROPERTY enablecomponents(GetEnableComponents, SetEnableComponents);
  PUBLIC PROPERTY doclink(__doclink, SetDocLink);
  UPDATE PUBLIC PROPERTY title(GetSubTitle, SetSubTitle);
  UPDATE PUBLIC PROPERTY width(pvt_width, SetWidth);

  PUBLIC PROPERTY wordwrap(this->headingtitle->wordwrap, this->headingtitle->wordwrap);

  MACRO NEW()
  {
    EXTEND this BY TolliumIsComposable;
    EXTEND this BY TolliumIsDirtyable;
  }

  UPDATE PUBLIC MACRO StaticInit(RECORD def)
  {
    TolliumFragmentBase::StaticInit(def);

    this->checkbox := def.checkbox;
    this->value := def.value;
    this->enablecomponents := def.enablecomponents;
    this->onchange := def.onchange;
    this->title := def.title;
    this->headingtitle->isheading := TRUE;
    this->doclink := def.doclink;
    this->wordwrap := def.wordwrap;
  }

  UPDATE MACRO PreInitComponent()
  {
    this->cbox->dirtylistener := this->dirtylistener;
  }

  UPDATE MACRO PostInitComponent()
  {
    this->headingtitle->isheading := TRUE;
  }

  BOOLEAN FUNCTION GetCheckbox()
  {
    RETURN this->cbox->visible;
  }
  MACRO SetCheckBox(BOOLEAN newvalue)
  {
    this->cbox->visible := newvalue;
    this->headingtitle->labelfor := newvalue ? this->cbox : DEFAULT OBJECT;
  }

  BOOLEAN FUNCTION GetValue()
  {
    RETURN this->cbox->value;
  }
  MACRO SetValue(BOOLEAN newvalue)
  {
    this->cbox->value:=newvalue;
  }
  MACRO OpenHelp(RECORD clickinfo)
  {
    RECORD info := this->contexts->controller->GetDynamicDocumentationInfo(this->__doclink);
    IF(RecordExists(info))
    {
      STRING url;
      IF(info.url != "")
        url := UpdateURLVariables(info.url, [ "ts" := FormatISO8601Datetime(GetCurrentdatetime(),"","","",FALSE)]);
      this->contexts->controller->OpenAppDocumentation(url, [ editinfo := info.editinfo ]);
    }
  }
  MACRO SetDoclink(STRING newlink)
  {
    IF(this->__doclink = newlink)
      RETURN;

    this->__doclink := newlink;

    RECORD helpinfo := this->contexts->controller->GetDynamicDocumentationInfo(this->__doclink);
    ^helpicon->visible := RecordExists(helpinfo);
    ^helpicon->opacity := RecordExists(helpinfo) AND helpinfo.missing ? 0.5f : 1f;
  }
  PUBLIC STRING FUNCTION GetDocumentationUrl()
  {
    RECORD info := this->contexts->controller->GetDynamicDocumentationInfo(this->__doclink);
    RETURN RecordExists(info) AND info.url != "" ? info.url : "";
  }

  UPDATE MACRO SetEnabled(BOOLEAN newstate)
  {
    TolliumFragmentBase::SetEnabled(newstate);
    this->cbox->enabled := newstate;
  }

  MACRO PTR FUNCTION GetOnChange()
  {
    RETURN this->cbox->onchange;
  }
  MACRO SetOnChange(MACRO PTR onchange)
  {
    this->cbox->onchange := onchange;
  }

  OBJECT ARRAY FUNCTION GetEnableComponents()
  {
    RETURN this->cbox->enablecomponents;
  }
  MACRO SetEnableComponents(OBJECT ARRAY comps)
  {
    this->cbox->enablecomponents := comps;
  }

  UPDATE PUBLIC BOOLEAN FUNCTION IsValidValue(VARIANT value)
  {
    RETURN this->cbox->IsValidValue(value);
  }

  STRING FUNCTION GetSubTitle()
  {
    RETURN this->headingtitle->value;
  }
  MACRO SetSubTitle(STRING newtitle)
  {
    this->headingtitle->value := newtitle;
  }

  MACRO SetWidth(STRING width)
  {
    this->pvt_width := width;
    this->headingtitle->width := width;
  }
>;
