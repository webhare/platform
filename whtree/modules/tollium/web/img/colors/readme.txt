Images in this namespace are automatically generated. Specify the color value as the image name. Valid colors are all colors
supported by GfxCreateColorFromCSS.

Examples:

tollium:colors/#ffffff        White
tollium:colors/#ff0           Yellow
tollium:colors/rgb(0,0,0)     Black
tollium:colors/rgba(0,0,0,0)  Transparent
tollium:colors/purple         Purple
