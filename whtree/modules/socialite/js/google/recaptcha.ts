import * as googleRecaptcha from "@mod-publisher/js/captcha/google-recaptcha";

export function setup() {
  googleRecaptcha.setupGoogleRecaptcha();
}
