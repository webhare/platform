<?wh

LOADLIB "wh::internet/urls.whlib";
LOADLIB "wh::witty.whlib";
LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::publisher/lib/publisher.whlib";
LOADLIB "mod::publisher/lib/siteprofiles.whlib";
LOADLIB "mod::publisher/lib/webdesign.whlib";

LOADLIB "mod::system/lib/internal/rightsmgmt.whlib";
LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/services.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";
LOADLIB "mod::system/lib/userrights.whlib";
LOADLIB "mod::system/lib/webserver.whlib";
LOADLIB "mod::system/lib/webserver/whdebug.whlib";

PUBLIC STATIC OBJECTTYPE DebugPage EXTEND DynamicPageBase
<
  OBJECT currentuser;
  BOOLEAN issupervisor;

  MACRO NEW()
  {
    this->currentuser := RequireLoggedinWebHareUser();
    this->issupervisor :=this->currentuser->HasRight("system:supervisor");
  }
  UPDATE PUBLIC MACRO RunBody(OBJECT webdesign)
  {
    IF(IsRequestPost())
      this->ApplySettings();
    ELSE
      this->ShowSettings();
  }
  MACRO ApplySettings()
  {
    STRING ARRAY debugopts;
    STRING backurl := UnpackURL(GetRequestURL()).origin || "/" || GetWebVariable("b");
    IF(GetWebVariable("submit") != "") //pressed submit
    {
      STRING ARRAY currentopts := Tokenize(GetWebCookie("wh-debug"), ".");
      RECORD fromfile := LookupPublisherUrl(backurl);
      INTEGER targetobject := fromfile.file ?? fromfile.folder ?? whconstant_whfsid_repository; //1 = repository, so we at least get all global options

      RECORD optresult := this->GetOptions(targetobject);
      debugopts := SELECT AS STRING ARRAY value FROM GetAllWebVariables() WHERE name = "debug" AND value IN (SELECT AS STRING ARRAY tag FROM optresult.options);
      IF(this->issupervisor)
      {
        STRING ARRAY extraopts := Tokenize(GetWebVariable("extraoptions")," ");
        FOREVERY(STRING opt FROM extraopts)
        {
          IF(opt LIKE "*.*")
            THROW NEW Exception("Debugoptions may not contain dots");
          IF(opt!="")
            INSERT opt INTO debugopts AT END;
        }
      }

      IF(this->issupervisor)
      {
        STRING ARRAY webserverskiptry := ArrayDelete(Tokenize(GetWebVariable("webserverskiptries"), " "), [ "" ]);
        IF (IsValueSet(webserverskiptry))
        {
          STRING opt := `wst=${Detokenize(webserverskiptry, ",")}`;
          IF(opt LIKE "*.*")
            THROW NEW Exception("Debugoptions may not contain dots");
          INSERT opt INTO debugopts AT END;
        }
      }
    }

    //Used for the original debug mode. Clear the cookie for anyone who still has it
    UpdateWebCookie("__whpub_preview",'', [ httponly := FALSE ]);

    RECORD cookieopts := [ httponly := FALSE, lifetime := 30*86400 ];
    IF(GetRequestURL() LIKE "https:*") //ensures 'etr' can be set for cross-domain RPC calls
      cookieopts := CELL[ ...cookieopts,  samesite := "none", secure := TRUE ];

    IF(this->issupervisor) //only for supervisors we allow the private flags and will actually sign
      UpdateWebCookie("wh-debug", GetSignedWHDebugOptions(debugopts), cookieopts);
    ELSE
      UpdateWebCookie("wh-debug", Detokenize(debugopts,'.'), cookieopts);

    IF(this->issupervisor)
      UpdateWebCookie("wh-debugwebserver", GetWebserverDebugCookie(debugopts));
    ELSE
      UpdateWebCookie("wh-debugwebserver", "");

    Redirect(backurl);
  }
  BOOLEAN FUNCTION IsGranted(STRING requireright, INTEGER targetobject)
  {
    TRY
    {
      IF(requireright = "")
        RETURN TRUE;
      IF(IsRightGlobal(requireright))
        RETURN this->currentuser->HasRight(requireright);
      ELSE
        RETURN this->currentuser->HasRightOn(requireright, targetobject);
    }
    CATCH(OBJECT e) //eg broken right
    {
      RETURN FALSE;
    }
  }

  RECORD FUNCTION GetOptions(INTEGER targetobject)
  {
    RECORD ARRAY options;
    OBJECT webcontext := GetWebContext(targetobject);
    STRING userwarning;

    FOREVERY(OBJECT plugin FROM webcontext->GetPluginsByFeature("debugsettings"))
      options := options CONCAT SELECT grouptitle, title, tag, requireright := CellExists(settings, "requireright") ? settings.requireright : "system:supervisor" FROM plugin->HookGetDebugSettings() AS settings;

    FOREVERY(RECORD debugsettings FROM GetCustomSiteProfileSettings("http://www.webhare.net/xmlns/publisher/siteprofile", "debugsettings", targetobject))
    {
      IF(debugsettings.node->HasAttribute("userwarning"))
        userwarning := debugsettings.node->GetAttribute("userwarning");

      FOREVERY (OBJECT subnode FROM debugsettings.node->childnodes->GetCurrentElements())
        IF(subnode->localname = "option")
          INSERT [ grouptitle := debugsettings.node->GetAttribute("title")
                 , title := subnode->GetAttribute("title")
                 , tag := subnode->GetAttribute("tag")
                 , requireright := subnode->HasAttribute("requireright") ? subnode->GetAttribute("requireright") : "system:supervisor"
                 ] INTO options AT END;
    }

    STRING ARRAY grantedrights :=
        SELECT AS STRING ARRAY requireright
          FROM (SELECT DISTINCT requireright FROM options)
         WHERE this->IsGranted(requireright, targetobject);
    options := SELECT * FROM options WHERE requireright IN grantedrights;
    RETURN CELL[options, userwarning];
  }

  MACRO ShowSettings()
  {
    STRING fromurl := GetWebVariable("back") ?? GetWebHeader("Referer") ?? ResolveToAbsoluteURL(GetClientRequestURL(),'/');
    //rewrite to our origin as client might not know the real url
    fromurl := UnpackURL(GetRequestURL()).origin || "/" || UnpackURL(fromurl).urlpath;

    STRING ARRAY currentopts := Tokenize(GetWebCookie("wh-debug"), ".");

    RECORD fromfile := LookupPublisherUrl(fromurl);
    INTEGER targetobject := fromfile.file ?? fromfile.folder ?? whconstant_whfsid_repository; //1 = repository, so we at least get all global options
    RECORD optresult := this->GetOptions(targetobject);

    STRING ARRAY originalgroupordering := SELECT AS STRING ARRAY grouptitle FROM optresult.options;
    RECORD ARRAY optionspergroup := SELECT title := grouptitle
                                         , options := GroupedValues(options)
                                      FROM optresult.options
                                     GROUP BY grouptitle
                                  ORDER BY SearchElement(originalgroupordering, grouptitle);

    STRING ARRAY knownopts;
    FOREVERY(RECORD grp FROM optionspergroup)
    {
      grp.options := SELECT rowkey := tag
                          , title
                          , selected := tag IN currentopts
                       FROM grp.options;
      knownopts := knownopts CONCAT (SELECT AS STRING ARRAY rowkey FROM grp.options);
      optionspergroup[#grp] := grp;
    }

    STRING ARRAY extraopts := SELECT AS STRING ARRAY val FROM ToRecordArray(currentopts,"val") WHERE val NOT IN knownopts AND val NOT LIKE "sig=*" AND val NOT LIKE "wst=*";
    STRING ARRAY webserverskiptries;
    FOREVERY (STRING opt FROM currentopts)
      IF (opt LIKE "wst=*")
        webserverskiptries := ArrayDelete(Tokenize(SubString(opt, 4), ","), [ "" ]);

    STRING feedbackurl := UpdateURLVariables( GetClientRequestURL(), [ back := "", b := UnpackURL(fromurl).urlpath ]);
    RECORD wittydata := CELL[ optgroups := optionspergroup
                            , extraoptions := Detokenize(extraopts," ")
                            , webserverskiptries := Detokenize(webserverskiptries," ")
                            , feedbackurl := feedbackurl
                            , optresult.userwarning
                            , wrdauthpluginerror := ""
                            , wrdauthplugin := DEFAULT RECORD
                            , this->issupervisor
                            ];

    IF(GetWebVariable("debug_wrdauthaction") = "forcelogout")
    {
      OBJECT wrdauthplugin := GetWebContextForURL(fromurl)->GetPlugin("http://www.webhare.net/xmlns/wrd", "wrdauth");
      wrdauthplugin->Logout();
      Redirect("/" || UnpackURL(fromurl).urlpath);
    }

    //Are we logged in to WRD?
    TRY
    {
      OBJECT wrdauthplugin := GetWebContextForURL(fromurl)->GetPlugin("http://www.webhare.net/xmlns/wrd", "wrdauth");
      IF(ObjectExists(wrdauthplugin))
      {
        wittydata.wrdauthplugin := [ entityid := wrdauthplugin->entityid
                                   , entitylink := wrdauthplugin->entityid != 0 AND this->currentuser->HasRightOn("wrd:read", wrdauthplugin->wrdschema->id)
                                                    ? GetPrimaryWebhareInterfaceURL() || "?app=wrd(" || wrdauthplugin->entityid || ")"
                                                    : ""
                                   , wrdschema := wrdauthplugin->wrdschema->tag
                                   , logoutlink := UpdateURLVariables(GetRequestURL(), [ debug_wrdauthaction := "forcelogout" ])
                                   ];
      }
    }
    CATCH(OBJECT e)
    {
      LogHarescriptException(e);
      wittydata.wrdauthpluginerror := e->what;
    }

    EmbedWittyComponent("mod::publisher/lib/internal/builtinpages/debug.witty:debugoptions", wittydata);
  }
>;

OpenPrimary();
NEW DebugPage->RunPageForWHFSId(whconstant_whfsid_webharebackend);
