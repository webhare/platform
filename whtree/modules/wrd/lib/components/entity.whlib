﻿<?wh

LOADLIB "wh::datetime.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::tollium/lib/componentbase.whlib" EXPORT TolliumCompositionBase, TolliumComponentBase;

LOADLIB "mod::wrd/lib/database.whlib";
LOADLIB "mod::wrd/lib/components/field.whlib";

//Follow the list of compositions until a WRDEntity is found
PUBLIC OBJECT FUNCTION GetWRDEntityComposition(OBJECT composition)
{
  FOR (; ObjectExists(composition); composition := composition->composition)
    IF(composition EXTENDSFROM TolliumWRDEntity)
      RETURN composition;
  RETURN DEFAULT OBJECT;
}

INTEGER FUNCTION AllocateStorageID(OBJECT wrdtype)
{
  RETURN wrdtype->CreateEntity(CELL[], [ temp := TRUE ])->id;
}
MACRO DestroyStorage(INTEGER entityid)
{
  DELETE FROM wrd.entities WHERE id = entityid AND creationdate = MAX_DATETIME; //verifying it's a temporary
}

PUBLIC STATIC OBJECTTYPE TolliumWRDEntity EXTEND TolliumCompositionBase
<
  OBJECT pvt_wrdtype;
  OBJECT pvt_entity;
  STRING pvt_attributebase;
  OBJECT wrdentitylistener;
  STRING defwrdtype;
  ///if no entity loaded, expected final id
  INTEGER __storageid;
  PUBLIC MACRO PTR onupdate;

  PUBLIC BOOLEAN __hasstaticwrdtype;

  PUBLIC FUNCTION PTR onstorevalue;

  PUBLIC PROPERTY wrdtype(GetWRDType, SetWRDType);
  PUBLIC PROPERTY wrdentity(pvt_entity, SetWRDEntity);
  PUBLIC PROPERTY entityid(GetEntityId, -);

  PUBLIC PROPERTY attributebase(pvt_attributebase, SetAttributeBase);

  PUBLIC PROPERTY autoupdate(GetAutoUpdate, SetAutoUpdate);

  PUBLIC STRING ARRAY getfields;

  UPDATE PUBLIC MACRO StaticInit(RECORD def)
  {
    TolliumCompositionBase::StaticInit(def);
    this->attributebase := def.attributebase;
    this->onstorevalue := def.onstorevalue;
    this->onupdate := def.onupdate;
    this->getfields := def.getfields;


    IF(def.wrdtype != "")
    {
      this->defwrdtype := def.wrdtype;
      this->__hasstaticwrdtype := TRUE;
    }
    this->autoupdate := def.autoupdate;
  }
  OBJECT FUNCTION GetWRDType()
  {
    IF(this->__hasstaticwrdtype AND this->pvt_wrdtype = DEFAULT OBJECT) //time to load it!
    {
      IF(NOT ObjectExists(this->contexts->wrdschema))
        THROW NEW TolliumException(this, '<wrd:entity wrdtype=""> requires a contexts->wrdschema');

      this->pvt_wrdtype := this->contexts->wrdschema->GetType(this->defwrdtype);
      IF(NOT ObjectExists(this->pvt_wrdtype))
        THROW NEW TolliumException(this, `No such type '${this->defwrdtype}'`);
    }
    RETURN this->pvt_wrdtype;
  }

  PUBLIC BOOLEAN FUNCTION GetAutoUpdate()
  {
    RETURN ObjectExists(this->wrdentitylistener);
  }
  PUBLIC MACRO SetAutoupdate(BOOLEAN newautoupdate)
  {
    IF(newautoupdate = ObjectExists(this->wrdentitylistener))
      RETURN;

    IF(newautoupdate = FALSE)
    {
      this->wrdentitylistener->DeleteComponent();
      this->wrdentitylistener := DEFAULT OBJECT;
      RETURN;
    }

    this->wrdentitylistener := this->CreateCustomSubComponent("http://www.webhare.net/xmlns/wrd/components", "entitylistener");
    this->wrdentitylistener->onupdate := PTR this->GotUpdateEvent;
    this->wrdentitylistener->SetListener(this->pvt_entity);
  }

  STRING ARRAY FUNCTION GetFieldsToGet()
  {
    STRING ARRAY getnames;
    STRING ARRAY availablenames := SELECT AS STRING ARRAY ToLowercase(tag) FROm this->wrdtype->Listattributes(0);

    FOREVERY(STRING name FROM this->GetComposedCellNames())
    {
      name := ToLowercase(name);
      IF(name IN availablenames)
        INSERT name INTO getnames AT END;
    }
    FOREVERY(STRING name FROM this->getfields)
    {
      name := ToLowercase(name);
      IF(name IN getnames)
        CONTINUE;
      IF(name IN availablenames)
        INSERT name INTO getnames AT END;
      ELSE
        THROW NEW TolliumException(this,`No such field '${name}' in type '${this->wrdtype->tag}', but it was explicitly requested by the 'getfields' property`);
    }

    RETURN getnames;
  }

  /** @short Load an entity
      @long Load an entity into the composition. Throws if the id cannot be resolved to an entity, except if the id is 0
      @param id ID of entity to load */
  PUBLIC MACRO Load(INTEGER id)
  {
    OBJECT wrdtype := this->wrdtype;
    IF(NOT ObjectExists(wrdtype))
      THROW NEW TolliumException(this, "Load() requires a wrdtype= to be set on the entity");
    FOREVERY(OBJECT field FROM this->cells)
      IF(field EXTENDSFROM TolliumWRDField)
        THROW NEW TolliumException(this, "TolliumWRDField is not supported by the 2017 WRD API");

    IF(id=0)
    {
      this->pvt_entity := DEFAULT OBJECT;
    }
    ELSE
    {
      this->pvt_entity := this->wrdtype->GetEntity(id);
      IF(NOT ObjectExists(this->pvt_entity))
        THROW NEW TolliumException(this, `No such entity #${id}`);

      this->value := this->pvt_entity->GetFields(this->GetFieldsToGet());
    }

    this->DestroyCurrentStorage();
    IF(this->onupdate != DEFAULT MACRO PTR)
      this->onupdate();
    IF(ObjectExists(this->wrdentitylistener))
      this->wrdentitylistener->SetListener(this->pvt_entity);
  }

  MACRO DestroyCurrentStorage()
  {
    IF(this->__storageid != 0)
      RunInSeparatePrimary(PTR DestroyStorage(this->__storageid), [ work := TRUE ]);
    this->__storageid := 0;
  }

  /** Get the expected storage id for this entity
      @return The (expected) entity id (temporary or permanent), or 0 if no id has yet been allocated */
  PUBLIC INTEGER FUNCTION GetReservedEntityId()
  {
    RETURN ObjectExists(this->wrdentity) ? this->wrdentity->id : this->__storageid;
  }

  /** Ensure we have a storage id for this entity, and return it
      @return The (expected) entity id (temporary or permanent) */
  PUBLIC INTEGER FUNCTION EnsureReservedEntityId()
  {
    IF(NOT ObjectExists(this->wrdtype))
      THROW NEW TolliumException(this, "EnsureReservedEntityId() requires a wrdtype= to be set on the entity");

    IF(ObjectExists(this->wrdentity))
      RETURN this->wrdentity->id;

    IF(this->__storageid = 0)
      this->__storageid := RunInSeparatePrimary(PTR AllocateStorageID(this->wrdtype), [ work := TRUE ]);

    RETURN this->__storageid;
  }


  MACRO GotUpdateEvent()
  {
    IF(ObjectExists(this->pvt_entity))
      this->value := this->pvt_entity->GetFields(this->GetFieldsToGet());
    IF(this->onupdate != DEFAULT MACRO PTR)
      this->onupdate();
  }

  /** @short Store the entity and any extra data
      @param work Work object for feedback
      @param options
      @cell options.entity Override entity to store to
      @cell options.extradata Extra fields to store
      @return Entity id */
  PUBLIC INTEGER FUNCTION Store(OBJECT work, RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    options := ValidateOptions(
        [ entity :=   DEFAULT OBJECT
        , extradata := DEFAULT RECORD
        ], options);

    RECORD updates := CELL[ ...this->extradata
                          , ...this->__GetEntityUpdates(DEFAULT RECORD)
                          , ...options.extradata
                          ];

    OBJECT entity := options.entity ?? this->pvt_entity;
    IF(ObjectExists(entity))
    {
      IF(this->__storageid != 0) //either you passed an option, or somehow entity got set ??
        THROW NEW TolliumException(this, "Store() cannot be used with an entity if a temporary has already been created");

      //if you modify wrd_tag, uppercase it! (unless its unchanged)
      IF(CellExists(updates,'wrd_tag') AND updates.wrd_tag != ToUppercase(updates.wrd_tag) AND entity->GetField("wrd_tag") != updates.wrd_tag)
        updates.wrd_tag := ToUppercase(updates.wrd_tag);
      this->__UpdateEntityWithValues(work, entity, updates);
      RETURN entity->id;
    }
    ELSE
    {
      //if you set wrd_tag, uppercase it!
      IF(CellExists(updates,'wrd_tag'))
        updates.wrd_tag := ToUppercase(updates.wrd_tag);

      this->__CreateEntityWithValues(work, updates);
      this->__storageid := 0;
      RETURN ObjectExists(this->wrdentity) ? this->wrdentity->id : 0;
    }
  }

  MACRO SetAttributeBase(STRING newbase)
  {
    IF(newbase!="" AND newbase NOT LIKE "*.")
      newbase := newbase || ".";
    this->pvt_attributebase := newbase;
  }

  ///A version that gets attribute info for the current entity/array we are pointing to
  PUBLIC RECORD FUNCTION GetLocalAttributeInfoByTag(STRING cellname)
  {
    IF(NOT ObjectExists(this->pvt_wrdtype))
      RETURN DEFAULT RECORD;

    RETURN this->pvt_wrdtype->GetAttribute(this->pvt_attributebase || cellname); //FIXME: Embed into WRDField
  }

  MACRO SetWRDType(OBJECT newtype)
  {
    IF(this->__hasstaticwrdtype)
      THROW NEW TolliumException(this, "A wrdtype set through XML cannot be modified afterwards");

    IF(this->pvt_wrdtype = newtype)
      RETURN;

    this->pvt_entity := DEFAULT OBJECT; //reset entity after a type change
    this->pvt_wrdtype := newtype;

    STRING ARRAY fields;

    FOREVERY(OBJECT field FROM this->cells)
    {
      STRING tag := ToUppercase(field->cellname);
      IF (tag IN fields)
      {
        STRING ARRAY names := SELECT AS STRING ARRAY cells->name FROM ToRecordArray(this->cells, "CELLS") WHERE cells->cellname = field->cellname AND cells->name != "";

        STRING componentnames := (LENGTH(names) = 0?"":" (names: " || Detokenize(names, ", ") || ")");
        THROW NEW TolliumException(this, `Multiple components use field ${field->cellname} ${componentnames}`); // in type ${this->pvt_wrdtype->tag}`);
      }

      INSERT tag INTO fields AT END;
      IF(MemberExists(field,'attrinfo')) //FIXME
      {
        field->attrinfo := this->GetLocalAttributeInfoByTag(field->cellname);
        IF(NOT RecordExists(field->attrinfo))
        {
          IF(this->attributebase!="")
            THROW NEW TolliumException(this, "Type '" || newtype->tag || "' has no field '" || field->cellname || "' (looking in attribute base '" || this->attributebase || "')");
          ELSE
            THROW NEW TolliumException(this, "Type '" || newtype->tag || "' has no field '" || field->cellname || "'");
        }

        IF (field->attrinfo.attributetype != 13) // Array
          field->required := field->required OR field->attrinfo.isrequired;

        field->WRD_Setup(this->pvt_wrdtype);
      }
    }

    this->SignalUpdatedCompositionMetadata();
  }

  MACRO SetWRDEntity(OBJECT newentity)
  {
    IF(NOT ObjectExists(newentity))
      THROW NEW TolliumException(this, "Entity passed to 'wrdentity' does not exist");
    IF(this->__hasstaticwrdtype)
      THROW NEW TolliumException(this, "A <wrd:entity> with a wrdtype set through XML can only be written using '->Store'");

    IF(this->pvt_wrdtype = DEFAULT OBJECT)
    {
      this->wrdtype := newentity->wrdtype;
    }
    ELSE
    {
      // If the wrd type was set through the attribute, through code or from a previously set entity
      // we don't allow setting an entity of another type.
      IF(newentity->wrdtype->id != this->pvt_wrdtype->id)
        THROW NEW TolliumException(this, "The entity is of type '" || newentity->wrdtype->tag || "' but should be of type '" || this->pvt_wrdtype->tag || "'");
    }

    this->pvt_entity := newentity;
    this->value := newentity->GetFields(this->GetFieldsToGet());
  }

  PUBLIC UPDATE VARIANT FUNCTION GetUpdateValue()
  {
    IF(NOT ObjectExists(this->wrdtype))
      THROW NEW TolliumException(this, "Trying to request the value of a wrd:entity without first setting a wrdtype");
    RETURN TolliumCompositionBase::GetUpdateValue();
  }
  MACRO ReportError(OBJECT work, RECORD err)
  {
    STRING attrtag := err.tag;
    OBJECT comp;

    FOREVERY (OBJECT fld FROM this->cells)
    {
      IF (ToUppercase(fld->cellname) = ToUppercase(attrtag))
      {
        comp := fld;
        BREAK;
      }
    }

    STRING comptitle := ObjectExists(comp) ? comp->errorlabel ?? comp->title : "";
    SWITCH (err.code)
    {
      CASE "TOOLARGE"                 { work->AddErrorFor(comp, GetTid("tollium:common.errors.too_long", comptitle, ToString(err.maxlength))); }
      CASE "NOTUNIQUE"                { work->AddErrorFor(comp, GetTid("wrd:errors.field_unique", comptitle)); }
      CASE "DATEOFDEATH_IN_FUTURE"    { work->AddErrorFor(comp, GetTid("wrd:errors.dateofdeath_in_future")); }
      CASE "DATEOFDEATH_BEFORE_BIRTH" { work->AddErrorFor(comp, GetTid("wrd:errors.dateofdeath_before_birth")); }
      CASE "LIMITDATE_BEFORE_CREATIONDATE" { work->AddErrorFor(comp, GetTid("wrd:errors.limitdate_before_creationdate")); }
      CASE "INVALIDVALUE"             { work->AddErrorFor(comp, GetTid("wrd:errors.field_invalidvalue", comptitle)); } //FIXME should have passed the info to the validation checks ?
      CASE "REQUIRED"
         {
           STRING title := ObjectExists(comp) ? comp->title : "Attribute " || attrtag; // The latter shouldn't happen with good app design!
           work->AddErrorFor(comp, GetTid("tollium:common.errors.field_required", title));
         }

      // If this is the case, add a proper error
      DEFAULT                         { work->AddErrorFor(comp, "WRD field error: " || err.code); }
    }
  }


  /** Return the values that can be used for updating. Generated attributes are filtered out
      @param additional_values Additional values to add to the update value
      @return Value to update the entity with
  */
  PUBLIC RECORD FUNCTION __GetEntityUpdates(RECORD additional_values)
  {
    RECORD base := this->GetUpdateValue();

    // Filter out generated attributes (only root attributes can be generated).
    IF (this->pvt_attributebase = "")
    {
      DELETE CELL wrd_id FROM base;

      IF(NOT ObjectExists(this->pvt_wrdtype))
        THROW NEW TolliumException(this, "wrdtype not yet known, cannot return updateable values");

      FOREVERY (RECORD rec FROM UnpackRecord(base))
      {
        STRING cellname := rec.name;
        RECORD attr := this->pvt_wrdtype->GetAttribute(cellname);

        IF(NOT RecordExists(attr))
          THROW NEW TolliumException(this, `No such attribute ${cellname} in type ${this->pvt_wrdtype->tag}`);

        IF (attr.isreadonly)
          base := CellDelete(base, cellname); // In case a field has been made readonly due to rights of the user we must silently ignore an update(? but how did they update? ..)
          //THROW NEW TolliumException(this, `Field ${cellname} in type ${this->pvt_wrdtype->tag} is readonly`);
      }
    }

    FOREVERY(RECORD val FROM UnpackRecord(additional_values))
      base := CellDelete(base, val.name);

    RETURN MakeMergedRecord(base, additional_values);
  }

  MACRO OnCommitAfterCreate(BOOLEAN committed)
  {
    // Rolled back. Forget we created.
    IF (NOT committed)
      this->pvt_entity := DEFAULT OBJECT;
  }

  /// Internal, used by entityedit screen
  PUBLIC MACRO __StoreEntityWithValues(OBJECT work, RECORD updates)
  {
    IF(this->__storageid != 0)
      THROW NEW TolliumException(this, "The 'temporary' wrd:entity API requires a static wrdtype= to be set on the wrd:entity");
    IF (ObjectExists(this->wrdentity))
      this->__UpdateEntityWithValues(work, this->pvt_entity, updates);
    ELSE
      this->__CreateEntityWithValues(work, updates);
  }

  OBJECT FUNCTION __DoCreateEntityWithValues(OBJECT work, RECORD updates)
  {
    IF(this->onstorevalue != DEFAULT FUNCTION PTR)
      updates := this->onstorevalue(work, updates);
    IF(work->HasFailed())
      RETURN DEFAULT OBJECT;

    IF(this->__storageid = 0)
    {
      RETURN this->wrdtype->__DoCreateEntity(updates, [ errorcallback := PTR this->ReportError(work, #1) ]);
    }
    ELSE
    {
      updates := CELL[ wrd_creationdate := GetCurrentDatetime()
                     , wrd_limitdate := MAX_DATETIME
                     , ...updates
                     ];

      this->wrdtype->UpdateEntity(this->__storageid, updates, [ errorcallback := PTR this->ReportError(work, #1) ]);
      RETURN this->wrdtype->GetEntity(this->__storageid);
    }
  }

  MACRO __CreateEntityWithValues(OBJECT work, RECORD updates)
  {
    this->pvt_entity := this->__DoCreateEntityWithValues(work, updates);
    IF(NOT work->HasFailed())
      GetPrimaryWebhareTransactionObject()->RegisterCommitHandler("", PTR this->OnCommitAfterCreate);
  }

  BOOLEAN FUNCTION __UpdateEntityWithValues(OBJECT work, OBJECT entity, RECORD updates)
  {
    IF(this->onstorevalue != DEFAULT FUNCTION PTR)
      updates := this->onstorevalue(work, updates);
    IF(work->HasFailed())
      RETURN FALSE;

    entity->UpdateEntity(
        updates,
        [ errorcallback := PTR this->ReportError(work, #1)
        ]);

    RETURN NOT work->HasFailed();
  }

  /** @short Look up a component associated with this entity by its tag name
    @param tag Tag to look up
    @return The component, or a default objec if the component could not be located */
  /////////////////////////////////////////////////////////////////////
  //
  // WRDEntityComposition
  //
  PUBLIC OBJECT FUNCTION GetFieldByTag(STRING tag)
  {
    tag := ToUppercase(tag);
    FOREVERY (OBJECT obj FROM this->cells)
      IF (ToUppercase(obj->cellname) = tag)
        RETURN obj;
    RETURN DEFAULT OBJECT;
  }

  PUBLIC INTEGER FUNCTION CreateEntity(OBJECT work, RECORD additional_values DEFAULTSTO DEFAULT RECORD)
  {
    RECORD updates := this->__GetEntityUpdates(additional_values);
    IF (work->HasFailed())
      RETURN 0;

    this->__CreateEntityWithValues(work, updates);
    RETURN ObjectExists(this->wrdentity) ? this->wrdentity->id : 0;
  }

  // Returns true when no errors occurred
  PUBLIC BOOLEAN FUNCTION UpdateEntity(OBJECT work, RECORD additional_values DEFAULTSTO DEFAULT RECORD)
  {
    IF(NOT ObjectExists(this->pvt_entity))
      ABORT("Invoking UpdateEntity without having set the 'entity' member");

    RECORD updates := this->__GetEntityUpdates(additional_values);
    IF (work->HasFailed())
      RETURN FALSE;

    RETURN this->__UpdateEntityWithValues(work, this->pvt_entity, updates);
  }

  /** @short Initialize the wrd:entity with type and the id to edit */
  PUBLIC MACRO LoadEntity(OBJECT wrdtype, INTEGER id)
  {
    IF(this->__hasstaticwrdtype)
      THROW NEW TolliumException(this, "A <wrd:entity> with a wrdtype set through XML can only be configured using '->Load'");

    IF(id!=0)
    {
      OBJECT entity := wrdtype->GetEntity(id);
      IF(NOT ObjectExists(entity))
        THROW NEW TolliumException(this, "The specified entity does not exist");
      this->wrdentity := entity;
    }
    ELSE
    {
      this->wrdtype := wrdtype;
    }
  }

  PUBLIC INTEGER FUNCTION StoreEntity(OBJECT work, RECORD additional_values DEFAULTSTO DEFAULT RECORD)
  {
    IF(ObjectExists(this->wrdentity))
    {
      IF(this->UpdateEntity(work, additional_values))
        RETURN this->wrdentity->id;
      ELSE
        RETURN 0;
    }
    ELSE
    {
      RETURN this->CreateEntity(work, additional_values);
    }
  }

  PUBLIC INTEGER FUNCTION GetEntityId()
  {
    IF(ObjectExists(this->wrdentity))
      RETURN this->wrdentity->id;
    ELSE
      RETURN 0;
  }

  UPDATE PUBLIC MACRO Invalidate()
  {
    IF (ObjectExists(this->wrdentity))
      this->value := this->wrdentity->GetFields(this->GetFieldsToGet());
  }

  /** Return the fields that would be modified when calling StoreEntity (optionally with additional values)
  */
  PUBLIC RECORD FUNCTION FilterFieldUpdates(RECORD additional_values DEFAULTSTO DEFAULT RECORD)
  {
    RECORD updates := this->__GetEntityUpdates(additional_values);
    IF (ObjectExists(this->wrdentity))
    {
      updates := this->wrdentity->FilterFieldUpdates(updates);
    }
    ELSE
    {
      FOREVERY (RECORD rec FROM UnpackRecord(updates))
        IF(IsDefaultValue(rec.value))
          updates := CellDelete(updates, rec.name);
      IF(Length(UnpackRecord(updates))=0)
        RETURN DEFAULT RECORD;
    }
    RETURN updates;
  }
>;
