<?wh

LOADLIB "wh::util/algorithms.whlib";
LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::wrd/lib/internal/dbschema.whlib";
LOADLIB "mod::wrd/lib/internal/metadata/moduledefs.whlib";

PUBLIC RECORD ARRAY FUNCTION GetWRDIssueList()
{
  RECORD ARRAY msgs;
  RECORD ARRAY wrdschemas := SELECT name
                                  , id
                                  , module := ToLowercase(name LIKE "*:*" ? Tokenize(name, ':')[0] : "")
                               FROM wrd.schemas
                              WHERE name NOT LIKE "$wrd$deleted*";

  RECORD ARRAY definedschemas;
  RECORD ARRAY moduleschemas;

  FOREVERY(RECORD wrdschema FROM SELECT * FROM wrdschemas WHERE module = "")
  {
    INSERT CELL[ message_text := `WRD Schema '${wrdschema.name}' does not have a module:tag name so we cannot verify whether it's supposed to exist here`
               , scopes := ["gdpr"]
               , type := "wrd:unexpectedschema"
               , metadata := [ wrdschema := wrdschema.name ]
               , jump_to := [ app := "wrd", task := "selectschema", id := wrdschema.id ]
               ] INTO msgs AT END;
  }

  //We also need to pick up modules which have no active WRD schemas at all
  STRING ARRAY allmodulenames := GetSortedSet(GetInstalledModuleNames() CONCAT SELECT AS STRING ARRAY module FROM wrdschemas WHERE module != "");
  FOREVERY(STRING module FROM allmodulenames)
  {
    RECORD ARRAY schemas := SELECT * FROM wrdschemas WHERE wrdschemas.module = VAR module;
    IF(NOT IsModuleInstalled(module))
    {
      FOREVERY(RECORD wrdschema FROM schemas)
        INSERT CELL[ message_text := `WRD Schema '${wrdschema.name}' does not have its corresponding module installed and should be deleted`
                   , scopes := ["gdpr"]
                   , type := "wrd:unexpectedschema"
                   , metadata := [ wrdschema := wrdschema.name ]
                   , jump_to := [ app := "wrd", task := "selectschema", id := wrdschema.id ]
                   ] INTO msgs AT END;
      CONTINUE;
    }
    ELSE
    {
      definedschemas := definedschemas CONCAT GetModuleDefinedWRDSchemas(module);
    }

    moduleschemas := moduleschemas CONCAT schemas;
  }

  definedschemas := SELECT *, seen := FALSE FROM definedschemas;

  //we need to analyze all schemas together, eg module example can request schema 'newsletter:examplemailings'
  FOREVERY(RECORD wrdschema FROM moduleschemas)
  {
    RECORD matchschema := SELECT * FROM definedschemas WHERE wrdschema.name LIKE definedschemas.tag;
    IF(NOT RecordExists(matchschema))
    {
      INSERT CELL[ message_text := `WRD Schema '${wrdschema.name}' is not claimed by module '${Tokenize(wrdschema.name,':')[0]}' and should be deleted`
                 , scopes := ["gdpr"]
                 , type := "wrd:unexpectedschema"
                 , metadata := [ wrdschema := wrdschema.name ]
                 , jump_to := [ app := "wrd", task := "selectschema", id := wrdschema.id ]
                 ] INTO msgs AT END;
      CONTINUE;
    }
    UPDATE definedschemas SET seen := TRUE WHERE wrdschema.name LIKE definedschemas.tag;
  }

  FOREVERY(RECORD defschema FROM SELECT * FROM definedschemas WHERE NOT seen AND autocreate AND tag LIKE "*:*")
  {
    INSERT CELL[ message_text := `Missing WRD Schema '${defschema.tag}' which should have been autocreated by module '${Tokenize(defschema.tag,':')[0]}'`
               , type := "wrd:missingschema"
               , metadata := [ wrdschema := defschema.tag ]
               ] INTO msgs AT END;
  }

  RETURN msgs;
}
