<?wh
/** @private WRD database definitions, should never be directly accessed as that's what the WRD API is for
*/

LOADLIB "mod::system/lib/database.whlib";

/** @schemadef wrd
*/
PUBLIC SCHEMA
  < TABLE
    < INTEGER "id" NULL := 0
    , STRING "name"
    , STRING "title"
    , STRING "description"
    , BOOLEAN "protected"
    , BOOLEAN "usermgmt"
    , INTEGER "accounttype" NULL := 0
    , INTEGER "accountemail" NULL := 0
    , INTEGER "accountlogin" NULL := 0
    , INTEGER "accountpassword" NULL := 0
    , DATETIME "creationdate"
    ; KEY id
    > schemas
  , TABLE
    < INTEGER "id" NULL := 0
    , INTEGER "wrd_schema" NULL := 0
    , STRING "name"
    , STRING "data"
    , BLOB "blobdata"
    ; KEY id
    > keyvaluestore
  , TABLE
    < INTEGER "id" NULL := 0
    , INTEGER "wrd_schema" NULL := 0
    , STRING "title"
    , STRING "tag"
    , STRING "description"
    , INTEGER "metatype"
    , INTEGER "requiretype_left" NULL := 0
    , INTEGER "requiretype_right" NULL := 0
    , INTEGER "parenttype" NULL := 0
    , BOOLEAN "abstract"
    , INTEGER "deleteclosedafter"
    , INTEGER "keephistorydays"
    , BOOLEAN "haspersonaldata"
    ; KEY id
    > types
  , TABLE
    < INTEGER "id" NULL := 0
    , INTEGER "domain" NULL := 0
    , INTEGER "attributetype"
    , INTEGER "type" NULL := 0
    , STRING "title"
    , STRING "tag"
    , STRING "description"
    , BOOLEAN "required"
    , BOOLEAN "isunique"
    , BOOLEAN "isunsafetocopy"
    , INTEGER "parent" NULL := 0
    , BOOLEAN "ordered"
    , BOOLEAN "checklinks"
    , BOOLEAN "multiline"
    , STRING "allowedvalues"
    , STRING "typedeclaration"
    ; KEY id
    > attrs
  , TABLE
    < INTEGER "id" NULL := 0
    , STRING "guid" __ATTRIBUTES__(BINARY)
    , STRING "tag"
    , DATETIME "creationdate"
    , DATETIME "modificationdate"
    , DATETIME "limitdate"
    , INTEGER "type" NULL := 0
    , INTEGER "leftentity" NULL := 0
    , INTEGER "rightentity" NULL := 0
    , STRING "title"
    , STRING "initials"
    , STRING "firstname"
    , STRING "firstnames"
    , STRING "infix"
    , STRING "lastname"
    , STRING "titles"
    , STRING "titles_suffix"
    , INTEGER "gender"
    , DATETIME "dateofbirth"
    , DATETIME "dateofdeath"
    , INTEGER "ordering"
    ; KEY id
    > entities
  , TABLE
    < INTEGER64 "id" NULL := 0
    , INTEGER "entity" NULL := 0
    , INTEGER "attribute" NULL := 0
    , INTEGER "setting" NULL := 0
    , STRING "rawdata"
    , STRING "unique_rawdata"
    , INTEGER "ordering"
    , BLOB "blobdata"
    , INTEGER64 "parentsetting" NULL := 0
    ; KEY id
    > entity_settings

  , TABLE
    < INTEGER64 "id" NULL := 0
    , INTEGER "fsobject" NULL := 0
    , INTEGER "linktype"
    ; KEY id
    > entity_settings_whfslink

  , TABLE
    < INTEGER "id" NULL := 0
    , DATETIME "creationdate"
    , DATETIME "nextcheck"
    , STRING "uuid"
    , STRING "returnurl"
    , INTEGER "paymentattr" NULL := 0
    , INTEGER "providerattr" NULL := 0
    , INTEGER "paymententity" NULL := 0
    , STRING error
    ; KEY id
    > pendingpayments

  , TABLE
    < INTEGER "id" NULL := 0
    , DATETIME "creationdate"
    , INTEGER "wrdschema" NULL := 0
    , INTEGER "entity" NULL := 0
    , BOOLEAN "impersonated"
    , STRING "ip"
    , STRING "country"
    , STRING "browsertriplet"
    , STRING "login"
    , STRING "type"
    , STRING "data"
    , INTEGER "byentity" NULL := 0
    , STRING "bylogin"
    , INTEGER "impersonator_entity" NULL := 0
    , STRING "impersonator_login"
    ; KEY id
    > auditevents

  , TABLE
    < INTEGER "id" NULL := 0
    , DATETIME "creationdate"
    , INTEGER "wrdschema" NULL := 0
    , INTEGER "entity" NULL := 0
    , STRING "userdata"
    ; KEY id
    > changesets

  , TABLE
    < INTEGER "id" NULL := 0
    , INTEGER "changeset" NULL := 0
    , DATETIME "creationdate"
    , INTEGER "type" NULL := 0
    , STRING "entity" __ATTRIBUTES__(BINARY)
    , STRING "summary"
    , STRING "oldsettings"
    , BLOB "oldsettings_blob"
    , STRING "modifications"
    , BLOB "modifications_blob"
    , STRING "source"
    , BLOB "source_blob"
    ; KEY id
    > changes

  , TABLE
    < INTEGER "id" NULL := 0
    , INTEGER "change" NULL := 0
    , INTEGER "seqnr"
    , BLOB "data"
    ; KEY id
    > change_attachments

  , TABLE
    < INTEGER "id" NULL := 0
    , INTEGER "client" NULL := 0
    , DATETIME "creationdate"
    , INTEGER "entity" NULL := 0
    , DATETIME "expirationdate"
    , STRING "hash" __ATTRIBUTES__(BINARY)
    , STRING "scopes"
    , STRING "type"
    ; KEY "id"
    > "tokens"
  > wrd;

/** @short Bind the WRD tables
    @long This macro binds all of the WRD module's tables to the specified transaction
    @param transaction Transaction ID to bind the tables to */
MACRO BindWrdTables(INTEGER transaction)
{
  wrd := BindTransactionToSchema(transaction, "wrd");
}

//Autobind to primary transaction, if available
SetupPrimaryTransactionBinder(PTR BindWrdTables);
