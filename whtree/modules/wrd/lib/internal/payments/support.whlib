<?wh
LOADLIB "wh::datetime.whlib";
LOADLIB "mod::wrd/lib/internal/dbschema.whlib";

PUBLIC STRING ARRAY validstatusses := [ "pending", "approved", "failed" ];

PUBLIC RECORD ARRAY FUNCTION NormalizeOrderLines(RECORD ARRAY orderlines)
{
  //TODO we should become strict and start validating that all the VAT info is there
  orderlines := EnforceStructure([[ type := ""
                                  , vatincluded := TRUE
                                  , vatamount := 0m
                                 ]], orderlines);
  RETURN orderlines;
}

PUBLIC RECORD FUNCTION ValidatePaymentValue(RECORD val)
{
  IF(NOT CellExists(val,"payments") OR TypeID(val.payments) != TYPEID(RECORD ARRAY))
  {
    IF(NOT CellExists(val,"__paymentdata")) //not a legacy format either
    {
      RETURN DEFAULT RECORD;
    }

    val := CELL[ ...val, payments := RecordExists(val.__paymentdata) ? [val] : RECORD[] ]; //legacy format, probalby received from wrd sync
  }

  FOREVERY(RECORD payment FROM val.payments)
  {
    IF(NOT CellExists(payment,"__paymentdata") OR TypeID(payment.__paymentdata) != TYPEID(RECORD))
      RETURN DEFAULT RECORD;
    IF(NOT CellExists(payment.__paymentdata,'m') OR TypeID(payment.__paymentdata.m) != TYPEID(RECORD))
      RETURN DEFAULT RECORD;
    IF(CellExists(payment.__paymentdata,'x') AND TypeID(payment.__paymentdata.x) != TYPEID(RECORD)) //x = user data
      RETURN DEFAULT RECORD;
    IF(NOT CellExists(payment.__paymentdata,'s') OR TypeID(payment.__paymentdata.s) != TYPEID(STRING) OR payment.__paymentdata.s NOT IN validstatusses)
      RETURN DEFAULT RECORD;
    IF(NOT CellExists(payment,'paymentprovider') OR TypeID(payment.paymentprovider) != TYPEID(INTEGER))
      RETURN DEFAULT RECORD;
  }
  RETURN val;
}

RECORD __wrdpaymenthandlerbase_passthrough;

/** @topic payments/payments
    @public
    @loadlib mod::wrd/lib/payments.whlib
*/
PUBLIC STATIC OBJECTTYPE WRDPaymentHandlerBase
<
  OBJECT paymententity;

  /// Constructs a new WRDPaymentHandlerBase object
  MACRO NEW()
  {
    this->paymententity := __wrdpaymenthandlerbase_passthrough.paymententity;
  }

  /** Invoked whenever the payment is started
      @param payment Payment data
  */
  PUBLIC MACRO OnPaymentStarted(RECORD payment)
  {
  }

  /** Invoked whenever the payment reaches a final status
      @param payment Payment data
  */
  PUBLIC MACRO OnPaymentFinalized(RECORD payment)
  {
    THROW NEW Exception("OnPaymentFinalized should be overridden");
  }
>;

PUBLIC OBJECT FUNCTION InstantiatePaymentHandler(OBJECT paymententity, STRING handler)
{
  RECORD save := __wrdpaymenthandlerbase_passthrough;
  TRY
  {
    __wrdpaymenthandlerbase_passthrough := CELL[ paymententity ];
    OBJECT retval := MakeObject(handler);
    IF(NOT (retval EXTENDSFROM WRDPaymentHandlerBase))
      THROW NEW Exception("Your handler must extend from WRDPaymentHandlerBase");
    RETURN retval;
  }
  FINALLY
  {
    __wrdpaymenthandlerbase_passthrough := save;
  }
}

PUBLIC BOOLEAN FUNCTION ProcessUpdatedPayment(STRING paymenthandler, STRING paymentattribute, OBJECT paymentobject, OBJECT paymententity, BOOLEAN nostatuscheck)
{
  IF(paymentobject->pvt_paymentdate = DEFAULT DATETIME)
    paymentobject->pvt_paymentdate := GetCurrentDatetime();

  RECORD currentstatus := paymententity->GetField(paymentattribute);

  IF(NOT nostatuscheck)
  { /* This shouldn't matter anymore now that we only trigger OnPaymentFinalized based on the best payment
       and disabling this path will make payment flows more consistent

    IF(currentstatus.status NOT IN [ "pending", "failed" ])
      RETURN FALSE; //no longer safe to update
      */
  }

  RECORD setstatus := paymentobject->MakePaymentValue();
  IF(Length(currentstatus.payments) = 0)
  {
    IF(setstatus.status != "failed") //we *can* do cancellations of a never-started payment. just create a fake' status record
      THROW NEW Exception(`Cannot set a payment status other than 'failed' for a payment that was never started (payment #${paymententity->id})`);
    IF(paymentobject->paymentuuid != "")
      THROW NEW Exception(`Trying to fail payment #${paymententity->id}) uuid ${paymentobject->paymentuuid} but we shouldn't have an uuid if no payment attempt was ever committed`);

    currentstatus.payments := setstatus.payments;
  }
  ELSE
  {
    INTEGER pos := (SELECT AS INTEGER #payments + 1 FROM currentstatus.payments WHERE __paymentdata.u = paymentobject->paymentuuid)-1;
    IF(pos<0)
      THROW NEW Exception(`Cannot find payment status '${paymentobject->paymentuuid}' in entity #${paymententity->id}`);

    // prevent us from losing metadata fields in this update. Meta should probably be a cleaner set-once-only keyvalue store serialized as separate DB rows instead of require updates of database rows
    setstatus.payments[0].__paymentdata.m := CELL[...currentstatus.payments[pos].__paymentdata.m, ...setstatus.payments[0].__paymentdata.m];
    currentstatus.payments[pos] := setstatus.payments[0];
  }

  RECORD upd := CellInsert(DEFAULT RECORD, paymentattribute, currentstatus);
  paymententity->UpdateEntity(upd);

  IF(setstatus.status != "pending")
  {
    IF(paymenthandler != "" AND setstatus.status != currentstatus.status)
    {
      OBJECT handler := InstantiatePaymentHandler(paymententity, paymenthandler);
      RECORD newpaymentvalue := paymententity->GetField(paymentattribute);
      IF(newpaymentvalue.status != currentstatus.status) //really changed? setstatus is just about the current payment, not the best payment..
        handler->OnPaymentFinalized(newpaymentvalue);
    }

    UPDATE wrd.pendingpayments SET nextcheck := MAX_DATETIME, error := "" WHERE uuid = paymentobject->paymentuuid AND COLUMN paymententity = VAR paymententity->id;
  }
  RETURN TRUE;
}

PUBLIC RECORD FUNCTION CreatePaymentRecord(RECORD ARRAY paymentrows)
{
  RECORD ARRAY payments := SELECT amountpayable := MONEY(a)
                                , paymentprovider := INTEGER(provider)
                                , status := STRING(s)
                                , paymentref := STRING(p)
                                , paymentdate := DATETIME(d)
                                , userdata := RECORD(CellExists(paymentrows,'x') ? paymentrows.x : DEFAULT RECORD)
                                , __paymentdata := CELL[ ...paymentrows, DELETE provider ]
                             FROM paymentrows;

  //Get last approved, or if not, last payment
  RECORD bestpayment := SELECT * FROM payments ORDER BY status = "approved" DESC, #payments DESC;
  MONEY amountpaid := SELECT AS MONEY SUM(amountpayable) FROM payments WHERE status = "approved";
  MONEY amountpayable := RecordExists(bestpayment) ? bestpayment.amountpayable : 0m;

  RETURN [ paymentprovider := RecordExists(bestpayment) ? bestpayment.paymentprovider : 0
         , amountpayable   := amountpayable
         , amountpaid      := amountpaid
         , status          := RecordExists(bestpayment) ? bestpayment.status : "pending"
         , paymentref      := RecordExists(bestpayment) ? bestpayment.paymentref : ""
         , paymentdate     := RecordExists(bestpayment) ? bestpayment.paymentdate : DEFAULT DATETIME
         , userdata        := RecordExists(bestpayment) ? bestpayment.userdata : DEFAULT RECORD
         , payments        := payments
         , __bestpayment   := bestpayment
         ];
}
