# Authors

## Active developers
- [Herman Borger](https://gitlab.com/hjborger)
- [Kris Breuker](https://gitlab.com/krisbreuker)
- [Arnold Hendriks](https://gitlab.com/arnoldhendriks)
- [Rob Hulswit](https://gitlab.com/webhare_rob)
- [Mark de Jong](https://gitlab.com/markdejong)

## UI/UX design
- [Wouter van den Berg](https://gitlab.com/vdberg)
- Marcel Hein

## Past developers
We haven't listed all past developers because we haven't received explicit
permission from them to be added. If you contributed to WebHare in the past
please contact us at info@webhare.nl and tell us how you would like to be listed.

- [Wouter Hendriks](https://gitlab.com/webwerf_wouter)

## Past commit history
Contributions made before WebHare was released as open source (ie before 25 Nov 2019) are attributed
to `WebHare bv <info@webhare.nl>` in the commit logs. All changes before the
open source release have been squashed to protect the identity of individuals
who may not have given permission for their name to be associated with the code
or whom may have been mentioned in material that was supplied for automatic tests.

We can still lookup things or provide access to this history if ever necessary. For reference,
the last commit in the unsquashed history was 585e947de345f8c62aac7ea92baf6f34eebb2ec2.
