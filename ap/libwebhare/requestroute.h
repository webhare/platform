#ifndef webhare_webserver_requestroute
#define webhare_webserver_requestroute

#include "webserve.h"

namespace WebServer
{

/** Variables storing the path a request took */
struct BLEXLIB_PUBLIC RequestRoute
{
        void Reset();
};


} //end namespace webserver

#endif
